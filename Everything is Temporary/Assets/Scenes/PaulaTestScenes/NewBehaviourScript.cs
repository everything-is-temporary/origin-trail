﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInventory
{
    public class NewBehaviourScript : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            GameObject inventory = GameObject.Find("InventoryManager");
            InventoryManager partyMemberScript = inventory.GetComponent<InventoryManager>();
            Debug.Log(ItemReference.FromId(GameUtility.IdConverter.FromReadableString("36BD_CA25_477B_F06E")));
            partyMemberScript.ModifyInventoryItem(new ItemWithAmount(ItemReference.FromId(GameUtility.IdConverter.FromReadableString("36BD_CA25_477B_F06E")), 1));
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
