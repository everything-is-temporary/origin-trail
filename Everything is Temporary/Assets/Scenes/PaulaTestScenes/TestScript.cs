﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//

namespace Characters 
{

	public class TestScript : MonoBehaviour
	{
		double time;
	    Actor partyMemberScript2;
        ActorManager managerScript;

        // Start is called before the first frame update
        void Start()
	    {
	        GameObject partyMember = GameObject.Find("PartyMember");
	        GameObject partyMember2 = GameObject.Find("PartyMember2");
            GameObject partyMember3 = GameObject.Find("PartyMember3");
            GameObject partyMember4 = GameObject.Find("PartyMember4");
            GameObject player = GameObject.Find("Player");

            Actor partyMemberScript = partyMember.GetComponent<Actor>();
	        partyMemberScript2 = partyMember2.GetComponent<Actor>();
            Actor partyMemberScript3 = partyMember3.GetComponent<Actor>();
            Actor partyMemberScript4 = partyMember4.GetComponent<Actor>();
            Actor playerScript = player.GetComponent<Actor>();

            GameObject manager = GameObject.Find("ActorManager");
            managerScript = manager.GetComponent<ActorManager>();
            //managerScript.player = playerScript;

            //managerScript.AddNewPartyMember(partyMemberScript);
            //managerScript.AddNewPartyMember(partyMemberScript2);
            //managerScript.AddNewPartyMember(partyMemberScript3);
            //managerScript.AddNewPartyMember(partyMemberScript4);

            //Debug.Log(partyMemberScript.GetHealth());
            //partyMemberScript.SetHealth(-10f);
            //Debug.Log(partyMemberScript.GetHealth());


            time = Time.time; 
	    }

	    //Update is called once per frame
	    //after 5 seconds, pM2 stops following and moves to location
	    void Update()
	    {
            if (Time.time > time + 5f)
            {
                //managerScript.RemovePartyMember(partyMemberScript2);
                //partyMemberScript2.MoveToLocation(new Vector3(4f, 4f, 4f));
                //Debug.Log("after move set");
            }
        }
	}
}
