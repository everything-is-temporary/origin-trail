﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GameUI.BookScript))]
public class TemporaryBookFlipControls : MonoBehaviour
{
    private void Awake()
    {
        m_bookScript = GetComponent<GameUI.BookScript>();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.J))
        {
            GameUI.BookUIManager.Instance.FocusBook();
            m_bookScript.FlipToEarlierPage();
        }
        else if (Input.GetKeyUp(KeyCode.L))
        {
            GameUI.BookUIManager.Instance.FocusBook();
            m_bookScript.FlipToLaterPage();
        }
        else if (Input.GetKeyUp(KeyCode.I))
            GameUI.BookUIManager.Instance.FocusGame();
        else if (Input.GetKeyUp(KeyCode.K))
            GameUI.BookUIManager.Instance.Unfocus();
        else if (Input.GetKeyUp(KeyCode.Comma))
            GameUI.BookUIManager.Instance.FocusBook();
    }

    private GameUI.BookScript m_bookScript;
}
