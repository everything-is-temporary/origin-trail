﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    [ExecuteInEditMode]
    public class PartialTransformScript : MonoBehaviour
    {
        public bool animateX;
        public bool animateY;
        public bool animateZ;

        public float positionX;
        public float positionY;
        public float positionZ;
        
        void Update()
        {
            if (animateX)
                transform.SetX(positionX);
            if (animateY)
                transform.SetY(positionY);
            if (animateZ)
                transform.SetZ(positionZ);
        }
    }
}