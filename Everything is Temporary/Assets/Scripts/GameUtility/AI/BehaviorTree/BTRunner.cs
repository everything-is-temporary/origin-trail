﻿using BehaviorTreeAI;
namespace GameUtility
{
    /// <summary>
    /// Utility class to run behavior trees. Does not currently contain a lot of code, but may be useful later for cleanup/ teardown.
    /// </summary>
    public class BTRunner
    {
        public BTRunner(IBTNode root)
        {
            this.m_root = root;
        }

        public bool Run() { return m_root.Run(); }

        private IBTNode m_root;
    }
}
