﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// Inverst the value returned by the decorated node.
    /// </summary>
    public class BTInverter : IBTDecorator
    {

        public BTInverter(IBTNode node): base(node){}

        protected override bool Decorate(bool decoratedNodeResult)
        {
            return !this.m_Decorated.Run();
        }
    }
}
