﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// Decorates teh node by making it true
    /// </summary>
    public class BTTrue : IBTDecorator
    {

        public BTTrue(IBTNode node) : base(node) { }

        protected override bool Decorate(bool decoratedNodeResult)
        {
            return true;
        }
    }
}
