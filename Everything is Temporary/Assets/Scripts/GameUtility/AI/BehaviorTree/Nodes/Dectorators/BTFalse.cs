﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// Decorates the node by making it false.
    /// </summary>
    public class BTFalse : IBTDecorator
    {

        public BTFalse(IBTNode node) : base(node) { }

        protected override bool Decorate(bool decoratedNodeResult)
        {
            return false;
        }
    }
}