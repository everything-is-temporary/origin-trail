﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// Continues execution until the decorated node is true, then returns true.
    /// </summary>
    public class BTDoUntilTrue : IBTDecorator
    {

        public static int MaxIterations = 10000;

        public BTDoUntilTrue(IBTNode node) : base(node) { }

        protected override bool Decorate(bool decoratedNodeResult)
        {
            for (int i = 0; !this.m_Decorated.Run(); i++){
                if (i > MaxIterations){
                    throw new System.Data.ConstraintException(string.Format("Attempted to execute {0} more times than allowed by {1}.", this.m_Decorated.GetType(), this.GetType()));
                }
            }
            return true;
        }
    }
}