﻿using System;
namespace BehaviorTreeAI
{
    /// <summary>
    /// A BT Nide that contains an internal list of nodes.
    /// </summary>
    public abstract class IBTListNode
    {

        public IBTListNode(System.Collections.Generic.IList<IBTNode> nodes)
        {
            this.m_Nodes = nodes;
        }

        protected System.Collections.Generic.IList<IBTNode> m_Nodes { get; private set; }

    }
}
