﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// The simple <see cref=" IBTNode"/> type which contains a parameterless run method.
    /// </summary>
    public interface IBTNode
    {
        bool Run();
    }
}
