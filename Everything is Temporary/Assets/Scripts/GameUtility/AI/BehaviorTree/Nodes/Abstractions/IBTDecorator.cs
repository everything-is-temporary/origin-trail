﻿using System;
namespace BehaviorTreeAI
{
    public abstract class IBTDecorator : IBTNode
    {

        protected IBTDecorator(IBTNode decorated)
        {
            this.m_Decorated = decorated;
        }

        public virtual bool Run()
        {
            return this.Decorate(this.m_Decorated.Run());
        }

        protected abstract bool Decorate(bool decoratedNodeResult);

        protected IBTNode m_Decorated { get; private set; }

    }
}
