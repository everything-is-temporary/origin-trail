﻿using System;

namespace BehaviorTreeAI
{
    /// <summary>
    /// Simple leaf node which perfoms <see langword="async"/> boolean function which takes in no parameters.
    /// </summary>
    public class BTLeaf : IBTNode
    {

        public BTLeaf(Func<bool> operation)
        {
            this.m_Func = operation;
        }

        public virtual bool Run()
        {
            return m_Func.Invoke();
        }

        private Func<bool> m_Func;
    }
}
