﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// Runs the contained nodes in their initial sequence returning false if the result of any is false. Otherwise returns true.
    /// </summary>
    public class BTSequence : IBTListNode
    {

        public BTSequence(System.Collections.Generic.IList<IBTNode> nodes) : base(nodes) { }

        public bool Run()
        {
            foreach (IBTNode node in this.m_Nodes)
            {
                if (!node.Run())
                {
                    return false;
                }
            }
            return true;
        }
    }
}
