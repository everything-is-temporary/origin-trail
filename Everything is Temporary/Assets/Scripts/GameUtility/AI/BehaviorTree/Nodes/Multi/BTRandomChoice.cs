﻿using System;
namespace BehaviorTreeAI
{
    /// <summary>
    /// Chooses a random element from the provided nodes and returns its value.
    /// </summary>
    public class BTRandomChoice : IBTListNode
    {
       
        public BTRandomChoice(System.Collections.Generic.IList<IBTNode> nodes) : base(nodes) { }

        public bool Run()
        {
            return this.m_Nodes[m_Rng.Next(this.m_Nodes.Count)].Run();
        }

        private static Random m_Rng = new Random();

    }
}
