﻿namespace BehaviorTreeAI
{
    /// <summary>
    /// Runs the contained nodes in their initial sequence returning true if the result of any is true. Otherwise returns false.
    /// </summary>
    public class BTSelector : IBTListNode
    {

        public BTSelector(System.Collections.Generic.IList<IBTNode> nodes): base(nodes){}

        public bool Run()
        {
            foreach(IBTNode node in this.m_Nodes){
                if (node.Run())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
