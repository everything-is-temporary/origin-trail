﻿using GameUtility.DataPersistence;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace GameUtility
{
    /// <summary>
    /// Helper class to expose collider trigger events through the Unity inspector.
    /// </summary>
    public class TriggerEventScript : MonoBehaviour
    {
        [Tooltip("Whether this script should remove itself after firing off the first trigger enter event.")]
        [FormerlySerializedAs("m_destroyOnEnter")]
        public bool destroyOnEnter = true;

        [Tooltip("Whether this script should remove itself after firing off the first trigger exit event.")]
        [FormerlySerializedAs("m_destroyOnExit")]
        public bool destroyOnExit = false;

        [Tooltip("Whether to save the destroyed state of this object.")]
        [FormerlySerializedAs("m_saveDestroyedState")]
        public bool saveDestroyedState = false;

        [FormerlySerializedAs("m_onTriggerEnter2d")]
        public ColliderEvent onTriggerEnter2d = new ColliderEvent();

        [FormerlySerializedAs("m_onTriggerExit2d")]
        public ColliderEvent onTriggerExit2d = new ColliderEvent();

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (m_isDestroyed)
                return;

            if (!collision.CompareTag("Player"))
                return;

            onTriggerEnter2d.Invoke(collision);

            if (destroyOnEnter)
                DestroyAndSave();
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (m_isDestroyed)
                return;

            if (!collision.CompareTag("Player"))
                return;

            onTriggerExit2d.Invoke(collision);

            if (destroyOnExit)
                DestroyAndSave();
        }

        private void DestroyAndSave()
        {
            if (saveDestroyedState)
                m_saver?.SetSaveState(m_destroySaveKey, true);

            DestroySelf();
        }

        private void Awake()
        {
            m_saver = GetComponent<SceneSaveableObject>();
            m_isDestroyed = false;

            if (m_saver == null && saveDestroyedState)
                Debug.LogWarning($"Did not find a {nameof(SceneSaveableObject)} on the TriggerEventScript on the object '{gameObject.name}'.");
        }

        private void Start()
        {
            if (m_saver != null && saveDestroyedState)
            {
                if (m_saver.TryGetSaveState(m_destroySaveKey, out object state))
                {
                    if ((bool)state)
                        DestroySelf();
                }
                else
                {
                    m_saver.SetSaveState(m_destroySaveKey, false);
                }
            }
        }

        private void DestroySelf()
        {
            // Without this, OnTriggerEnter2D() can be called even if Destroy()
            // is called inside Start().
            m_isDestroyed = true;
            Destroy(this);
        }

        private SceneSaveableObject m_saver;

        private const string m_destroySaveKey = "TriggerEventScript.IsDestroyed";

        private bool m_isDestroyed;

        // Unity can't serialize generic types, but it can serialize non-generic
        // subtypes.
        [System.Serializable]
        public class ColliderEvent : UnityEvent<Collider2D>
        {
        }
    }
}