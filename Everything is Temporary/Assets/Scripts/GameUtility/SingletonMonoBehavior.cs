﻿using UnityEngine;
using System.Collections;

namespace GameUtility
{
    /// <summary>
    /// Class following the singleton design pattern. Only one can be active at any given moment.
    /// </summary>
    public abstract class SingletonMonoBehavior<SingletonClass> : MonoBehaviour
        where SingletonClass : class
    {
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = GetComponent<SingletonClass>();
            }

            PostAwake();
        }

        protected virtual void OnDestroy()
        {
            Instance = null;
        }

        /// <summary>
        /// Gets the class singleton.
        /// </summary>
        /// <value>Single instance of the singleton class.</value>
        public static SingletonClass Instance { get; protected set; }

        /// <summary>
        /// Gets the class singleton (callable function).
        /// </summary>
        /// <value>Single instance of the singleton class.</value>
        public static SingletonClass GetInstance()
        {
            return Instance;
        }


        protected virtual void PostAwake() { }
    }
}
