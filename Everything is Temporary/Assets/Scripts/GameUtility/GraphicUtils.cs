﻿using System;
using UnityEngine;

namespace GameUtility
{
    public static class GraphicUtils
    {
        public static void ClearTexture(RenderTexture texture)
        {
            var oldActive = RenderTexture.active;
            RenderTexture.active = texture;

            GL.Clear(true, true, Color.clear);

            RenderTexture.active = oldActive;
        }

        public static void ClearDepthOnly(RenderTexture texture)
        {
            var oldActive = RenderTexture.active;
            RenderTexture.active = texture;

            GL.Clear(true, false, Color.clear);

            RenderTexture.active = oldActive;
        }
    }
}
