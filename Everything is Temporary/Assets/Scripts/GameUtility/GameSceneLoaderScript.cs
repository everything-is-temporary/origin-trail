﻿using UnityEngine;
using System.Collections;

namespace GameUtility
{
    /// <summary>
    /// Lightweight helper script that can be put on UI elements to add a "LoadScene"
    /// function that invokes <see cref="CoreGameManager.OpenGameSceneAsync(string)"/>.
    /// </summary>
    public class GameSceneLoaderScript : MonoBehaviour
    {
        public void LoadScene(string sceneName)
        {
            // Temporary code to focus game whenever a new scene loads.
            GameUI.BookUIManager.Instance.FocusGame();
            LoadSceneAsync(sceneName);
        }

        // This is "async void" so that exceptions are reported in Unity.
        private async void LoadSceneAsync(string sceneName)
        {
            await CoreGameManager.Instance.OpenGameSceneAsync(sceneName);
        }
    }
}