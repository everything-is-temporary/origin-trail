﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace GameUtility
{
    public interface IErrorHandler
    {
        void HandleError(Exception e);
    }

    public class UnityErrorLogger : IErrorHandler
    {
        public void HandleError(Exception e)
        {
            Debug.LogException(e);
        }
    }

    public static class TaskUtils
    {
        /// <summary>
        /// Safely runs the given task asynchronously, passing any errors to the error handler.
        /// </summary>
        /// <remarks>
        /// Taken from https://johnthiriet.com/removing-async-void/.
        /// </remarks>
        /// <param name="task">Task.</param>
        /// <param name="errorHandler">Error handler.</param>
#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        public static async void FireAndForget(this Task task, IErrorHandler errorHandler)
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
        {
            try
            {
                await task;
            }
            catch (Exception e)
            {
                errorHandler.HandleError(e);
            }
        }

        /// <summary>
        /// Safely runs the given task asynchronously, printing any exceptions through Unity.
        /// </summary>
        /// <param name="task">The task to run.</param>
        public static void FireAndForget(this Task task)
        {
            task.FireAndForget(new UnityErrorLogger());
        }
    }
}
