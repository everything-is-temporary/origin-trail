using System;

namespace GameUtility
{
    public interface IKeyable<out TKeyClass> where TKeyClass : IEquatable<TKeyClass>
    {
        TKeyClass GetKey();
    }
}