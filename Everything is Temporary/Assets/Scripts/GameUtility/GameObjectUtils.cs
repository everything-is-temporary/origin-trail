﻿using System;
using UnityEngine;
namespace GameUtility
{
    /// <summary>
    /// A class containing utilities for dealing with Unity's game objects.
    /// </summary>
    public static class GameObjectUtils
    {
        /// <summary>
        /// Reparents <paramref name="transform"/> to <paramref name="parent"/>
        /// preserving local scale, local position and local rotation.
        /// </summary>
        /// <param name="transform">Transform.</param>
        /// <param name="parent">Parent.</param>
        public static void ReparentKeepingLocals(Transform transform, Transform parent)
        {
            var localScale = transform.localScale;
            var localPosition = transform.localPosition;
            var localRotation = transform.localRotation;

            transform.parent = parent;

            transform.localScale = localScale;
            transform.localPosition = localPosition;
            transform.localRotation = localRotation;
        }

        public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
        {
            T component = gameObject.GetComponent<T>();

            if (component == null)
                component = gameObject.AddComponent<T>();

            return component;
        }

        public static void SetX(this Transform transform, float x)
        {
            var position = transform.position;
            position.x = x;
            transform.position = position;
        }

        public static void SetY(this Transform transform, float y)
        {
            var position = transform.position;
            position.y = y;
            transform.position = position;
        }

        public static void SetZ(this Transform transform, float z)
        {
            var position = transform.position;
            position.z = z;
            transform.position = position;
        }
        public static void SetLocalX(this Transform transform, float x)
        {
            var position = transform.localPosition;
            position.x = x;
            transform.localPosition = position;
        }

        public static void SetLocalY(this Transform transform, float y)
        {
            var position = transform.localPosition;
            position.y = y;
            transform.localPosition = position;
        }

        public static void SetLocalZ(this Transform transform, float z)
        {
            var position = transform.localPosition;
            position.z = z;
            transform.localPosition = position;
        }


        /// <summary>
        /// Checks if a <see cref="UnityEngine.Object"/> exists in memory but has been destroyed.
        /// </summary>
        /// <returns><c>true</c>, if object was destroyed, <c>false</c> otherwise.</returns>
        /// <param name="obj">The object for which to check this.</param>
        public static bool IsDestroyed(UnityEngine.Object obj)
        {
            // Unity Objects override the equality (==) operator but due to the
            // way C#'s garbage collection works, they might still be in memory
            // and therefore would not be the same as null ('is' is the same as 'ReferenceEquals()')
            return obj == null && !(obj is null);
        }
    }
}
