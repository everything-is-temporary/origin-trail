﻿using System;
using UnityEngine;

namespace GameUtility
{
    /// <summary>
    /// Attribute used to ensure that a GameObject field can only be filled
    /// by an Object that is of a particular type. This is useful for fields
    /// that accept interfaces.
    /// </summary>
    public class RequiresTypeAttribute : PropertyAttribute
    {
        public Type requiredType;

        public RequiresTypeAttribute(Type requiredType)
        {
            this.requiredType = requiredType;
        }
    }
}