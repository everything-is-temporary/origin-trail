﻿using System;
using UnityEngine;
namespace GameUtility
{
    /// <summary>
    /// A wrapper for a GameObject and some extra data. This is useful in
    /// UI scripts that need to instantiate some component prefab, and then
    /// keep a reference to both the component and the GameObject.
    /// </summary>
    public class GameObjectWith<T>
    {
        public GameObject GameObject { get; set; }
        public T Data { get; set; }

    }

    public static class GameObjectWith
    {
        public static GameObjectWith<J> FromComponentPrefab<J>(Component componentPrefab, Transform parent = null)
            where J : class
        {
            var instantiated = UnityEngine.Object.Instantiate(componentPrefab, parent);

            return new GameObjectWith<J>
            {
                GameObject = instantiated.gameObject,
                Data = instantiated as J
            };
        }
    }
}
