﻿using System;
using UnityEngine;

namespace GameUtility
{
    /// <summary>
    /// A component that adjust its own transform so that its relative position
    /// with respect to some reference point is the same as the relative position
    /// of some object and some other reference point.
    /// </summary>
    public class ObjectMirrorScript : MonoBehaviour
    {
        public Transform referencePosition;
        public Transform mirroredReferencePosition;
        public Transform mirroredObjectPosition;

        private void LateUpdate()
        {
            Vector3 targetDelta = mirroredObjectPosition.position - mirroredReferencePosition.position;

            transform.position = referencePosition.position + targetDelta;
        }
    }
}