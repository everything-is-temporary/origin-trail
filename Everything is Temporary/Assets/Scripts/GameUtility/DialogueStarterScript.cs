﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    /// <summary>
    /// Helper script for in-game objects to initiate dialogue. This will
    /// show the dialogue overlay and disable player input.
    /// </summary>
    public class DialogueStarterScript : MonoBehaviour
    {
        public void StartDialogue(string knotPath)
        {
            CoreGameManager.Instance.PerformDialog(knotPath).FireAndForget();
        }
    }
}