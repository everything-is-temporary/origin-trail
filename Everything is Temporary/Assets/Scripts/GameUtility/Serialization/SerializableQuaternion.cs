﻿using System;
using UnityEngine;
namespace GameUtility.Serialization
{
    [Serializable]
    public class SerializableQuaternion
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public Quaternion Quaternion => new Quaternion(x, y, z, w);

        public SerializableQuaternion(Quaternion quaternion)
        {
            x = quaternion.x;
            y = quaternion.y;
            z = quaternion.z;
            w = quaternion.w;
        }
    }
}
