﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace GameUtility
{
    [Serializable]
    public class SerializableDict<TKey, TVal> : IDictionary<TKey, TVal>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<KeyValuePair<TKey, TVal>> _internalSerialization = new List<KeyValuePair<TKey, TVal>>();

        private Dictionary<TKey, TVal> _internal_dict = new Dictionary<TKey, TVal>();

        public SerializableDict(){}

        public SerializableDict(Dictionary<TKey, TVal> dict) {
            this._internal_dict = dict;
        }

        public void OnAfterDeserialize()
        {
            _internal_dict.Clear();
            foreach (KeyValuePair<TKey, TVal> keyValuePair in _internalSerialization)
            {
                _internal_dict.Add(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public void OnBeforeSerialize()
        {
            _internalSerialization.Clear();
            foreach (KeyValuePair<TKey, TVal> keyValuePair in _internal_dict)
            {
                _internalSerialization.Add(keyValuePair);
            }
        }

        public ICollection<TKey> Keys => ((IDictionary<TKey, TVal>)_internal_dict).Keys;

        public ICollection<TVal> Values => ((IDictionary<TKey, TVal>)_internal_dict).Values;

        public int Count => ((IDictionary<TKey, TVal>)_internal_dict).Count;

        public bool IsReadOnly => ((IDictionary<TKey, TVal>)_internal_dict).IsReadOnly;

        public TVal this[TKey key] { get => ((IDictionary<TKey, TVal>)_internal_dict)[key]; set => ((IDictionary<TKey, TVal>)_internal_dict)[key] = value; }
       

        bool IDictionary<TKey, TVal>.TryGetValue(TKey key, out TVal value)
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<TKey, TVal> item)
        {
            ((IDictionary<TKey, TVal>)_internal_dict).Add(item);
        }

        public bool Contains(KeyValuePair<TKey, TVal> item)
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TVal>[] array, int arrayIndex)
        {
            ((IDictionary<TKey, TVal>)_internal_dict).CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TVal> item)
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).Remove(item);
        }

        public IEnumerator<KeyValuePair<TKey, TVal>> GetEnumerator()
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).GetEnumerator();
        }

        public void Add(TKey key, TVal value)
        {
            ((IDictionary<TKey, TVal>)_internal_dict).Add(key, value);
        }

        public bool ContainsKey(TKey key)
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).ContainsKey(key);
        }

        public bool Remove(TKey key)
        {
            return ((IDictionary<TKey, TVal>)_internal_dict).Remove(key);
        }

        public void Clear()
        {
            ((IDictionary<TKey, TVal>)_internal_dict).Clear();
        }
    }
}
