﻿using System;
using UnityEngine;
using UnityEditor;

namespace GameUtility.Serialization
{
    /// <summary>
    /// Put this on a serializable dictionary property so that it can be rendered
    /// nicely with a custom inspector.
    /// </summary>
    public class SerializableDictionaryAttribute : PropertyAttribute
    {
        public readonly bool ShowAddButton;
        public readonly bool ShowDeleteButton;
        public readonly bool ReadonlyKey;

        /// <summary>
        /// Whether the key is of type long and should be formatted like an ID.
        /// </summary>
        public readonly bool KeyIsLongId;

        public SerializableDictionaryAttribute(bool readonlyKey = true, bool showAddButton = true, bool showDeleteButton = true, bool keyIsLongId = false)
        {
            ReadonlyKey = readonlyKey;
            ShowAddButton = showAddButton;
            ShowDeleteButton = showDeleteButton;
            KeyIsLongId = keyIsLongId;
        }
    }
}
