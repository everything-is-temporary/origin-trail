﻿using System;
using UnityEngine;
namespace GameUtility.Serialization
{
    [Serializable]
    public class SerializableVector3
    {
        public float x;
        public float y;
        public float z;

        public Vector3 Vector => new Vector3(x, y, z);

        public SerializableVector3(Vector3 vec)
        {
            x = vec.x;
            y = vec.y;
            z = vec.z;
        }
    }
}
