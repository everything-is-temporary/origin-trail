﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Serialization
{
    /// <summary>
    /// A serializable dictionary. For Unity to be able to serialize this type, you must
    /// make a non-generic subclass marked with [Serializable] (which may be empty otherwise).
    /// Use the <see cref="CreateNewElement"/> property to define how new elements should
    /// be created (e.g. to generate random keys).
    /// See also the following attribute: <see cref="SerializableDictionaryAttribute"/>.
    /// </summary>
    [Serializable]
    public class SerializableDictionary<KeyType, ValueType> : IDictionary<KeyType, ValueType>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private List<KeyType> m_keys = new List<KeyType>();

        [SerializeField]
        private List<ValueType> m_values = new List<ValueType>();

        public Dictionary<KeyType, ValueType> Dictionary { get; private set; } = new Dictionary<KeyType, ValueType>();

        /// <summary>
        /// If this is set, it is invoked by this class to add entries to the dictionary.
        /// It must return a key that is not present in <see cref="Dictionary"/>, or
        /// an exception will be thrown. The best way to set this inside a Unity
        /// serializable object (e.g. a MonoBehaviour or a ScriptableObject)
        /// is to use the object initializer syntax like so:
        /// 
        /// <para>
        /// <code>
        /// new SerializableDictionary { CreateNewElement = ... };
        /// </code>
        /// </para>
        /// </summary>
        public Func<KeyValuePair<KeyType, ValueType>> CreateNewElement { get; set; }

        #region IDictionary
        public ValueType this[KeyType key] { get => ((IDictionary<KeyType, ValueType>)Dictionary)[key]; set => ((IDictionary<KeyType, ValueType>)Dictionary)[key] = value; }

        public ICollection<KeyType> Keys => ((IDictionary<KeyType, ValueType>)Dictionary).Keys;

        public ICollection<ValueType> Values => ((IDictionary<KeyType, ValueType>)Dictionary).Values;

        public int Count => ((IDictionary<KeyType, ValueType>)Dictionary).Count;

        public bool IsReadOnly => ((IDictionary<KeyType, ValueType>)Dictionary).IsReadOnly;

        public void Add(KeyType key, ValueType value)
        {
            ((IDictionary<KeyType, ValueType>)Dictionary).Add(key, value);
        }

        public void Add(KeyValuePair<KeyType, ValueType> item)
        {
            ((IDictionary<KeyType, ValueType>)Dictionary).Add(item);
        }

        public void Clear()
        {
            ((IDictionary<KeyType, ValueType>)Dictionary).Clear();
        }

        public bool Contains(KeyValuePair<KeyType, ValueType> item)
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).Contains(item);
        }

        public bool ContainsKey(KeyType key)
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<KeyType, ValueType>[] array, int arrayIndex)
        {
            ((IDictionary<KeyType, ValueType>)Dictionary).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<KeyType, ValueType>> GetEnumerator()
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).GetEnumerator();
        }

        public bool Remove(KeyType key)
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).Remove(key);
        }

        public bool Remove(KeyValuePair<KeyType, ValueType> item)
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).Remove(item);
        }

        public bool TryGetValue(KeyType key, out ValueType value)
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary<KeyType, ValueType>)Dictionary).GetEnumerator();
        }
        #endregion

        #region ISerializationCallbackReceiver
        public void OnAfterDeserialize()
        {
            // Sometimes the number of keys temporarily doesn't match the number
            // of values because of the custom inspector for some reason.
            int entryCount = Mathf.Min(m_keys.Count, m_values.Count);

            Dictionary = new Dictionary<KeyType, ValueType>();

            for (int i = 0; i < entryCount; ++i)
            {
                KeyType key = m_keys[i];
                ValueType value = m_values[i];

                if (!Dictionary.ContainsKey(key))
                {
                    Dictionary[key] = value;
                }
                else
                {
                    // Duplicate elements are used by the inspector script to signal
                    // that a new element should be created.

                    if (CreateNewElement != null)
                    {
                        KeyValuePair<KeyType, ValueType> newEntry = CreateNewElement();

                        // Note that this will throw an exception if newEntry.Key is not unique.
                        Dictionary.Add(newEntry.Key, newEntry.Value);
                    }
                    else
                    {
                        KeyType newKey;

                        if (typeof(KeyType) == typeof(string)) // special case for strings: can use string.Empty
                            newKey = (KeyType)(object)string.Empty;
                        else if (typeof(KeyType).IsValueType) // for example if KeyType is long
                            newKey = default;
                        else // otherwise, this is a reference type, so try to use parameterless constructor
                        {
                            try
                            {
                                // Call a parameterless constructor, possibly a private one.
                                newKey = (KeyType)Activator.CreateInstance(typeof(KeyType), true);
                            }
                            catch (Exception e) when (
                                e is MissingMemberException ||
                                e is MethodAccessException)
                            {
                                // This happens if KeyType doesn't have a parameterless constructor or
                                // if "the caller does not have permission" to call it.
                                Debug.LogWarning($"Duplicate key {key}. Cannot generate a new key of the correct type.");

                                // For reference types, the default is null, which cannot
                                // be used as a Dictionary key.
                                continue;
                            }
                        }

                        if (!Dictionary.ContainsKey(newKey))
                            Dictionary[newKey] = value;
                    }
                }
            }
        }

        public void OnBeforeSerialize()
        {
            if (Dictionary == null)
                return;

            m_keys = new List<KeyType>();
            m_values = new List<ValueType>();

            foreach (var entry in Dictionary)
            {
                m_keys.Add(entry.Key);
                m_values.Add(entry.Value);
            }
        }
        #endregion
    }
}