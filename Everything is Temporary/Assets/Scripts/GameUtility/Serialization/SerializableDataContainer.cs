﻿using UnityEngine;

namespace GameUtility.Serialization
{
    // A simple class that inherits from ISerializationCallbackReceiver to work with the SaveManager
    [System.Serializable]
    public class SerializableDataContainer<TData> : ISerializationCallbackReceiver
    {
        public TData Data { get; protected set; }

        public SerializableDataContainer(TData serializableData)
        {
            Data = serializableData;
        }

        public virtual void OnAfterDeserialize()
        {
        }

        public virtual void OnBeforeSerialize()
        {
        }
    }
}
