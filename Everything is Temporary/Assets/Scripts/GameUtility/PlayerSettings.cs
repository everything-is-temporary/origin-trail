using System;
using UnityEngine;

namespace GameUtility
{
    public static class PlayerSettings
    {
        #region PlayerPrefs Properties

        public static float? TextSpeed
        {
            get
            {
                if (!PlayerPrefs.HasKey(m_textSpeedKey)) return null;
                return PlayerPrefs.GetFloat(m_textSpeedKey);
            }
            set
            {
                CheckNonNullValue(value);
                PlayerPrefs.SetFloat(m_textSpeedKey, (float) value);
            }
        }

        #endregion
        
        #region PlayerPrefs String Keys
        
        private static readonly string m_textSpeedKey = "TextSpeedKey";
        
        #endregion

        private static void CheckNonNullValue<T>(T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value), "Value assigned in PlayerPrefs cannot be null.");
            }
        }
        
    }
}