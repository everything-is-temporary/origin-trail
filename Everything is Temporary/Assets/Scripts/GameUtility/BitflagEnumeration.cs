using System;
using System.Diagnostics;

namespace GameUtility
{
    public abstract class BitflagEnumeration
    {
        public readonly int Value;

        protected BitflagEnumeration(int bitValue)
        {
            Value = bitValue;
        }

        protected BitflagEnumeration(BitflagEnumeration enumeration)
        {
            Debug.Assert(enumeration.GetType() == this.GetType());
            Value = enumeration.Value;
        }
        
        public bool ContainsAll(BitflagEnumeration other)
        {
            return other.GetType() == this.GetType() && (Value & other.Value) == other.Value;
        }

        public bool ContainsAny(BitflagEnumeration other)
        {
            return other.GetType() == this.GetType() && (Value & other.Value) != 0;
        }
        
        public static bool operator ==(BitflagEnumeration first, BitflagEnumeration second)
        {
            return first.Value == second.Value;
        }
        
        public static bool operator !=(BitflagEnumeration first, BitflagEnumeration second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            return obj.GetType() == this.GetType() && (obj as BitflagEnumeration)?.Value == Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{base.ToString()}:{Convert.ToString(Value, 2)}";
        }

        public static BitflagEnumeration operator +(BitflagEnumeration first, BitflagEnumeration second)
        {
            return Add(first.GetType(), first, second);
        }


        public static BitflagEnumeration operator -(BitflagEnumeration first, BitflagEnumeration second)
        {
            return Subtract(first.GetType(), first, second);
        }

        public static BitflagEnumeration operator |(BitflagEnumeration first, BitflagEnumeration second)
        {
            return Add(first.GetType(), first, second);
        }
        
        private static BitflagEnumeration Add(Type t, BitflagEnumeration first, BitflagEnumeration second)
        {
            BitflagEnumeration n = Activator.CreateInstance(t, first.Value | second.Value) as BitflagEnumeration;
            return n;
        }
        
        private static BitflagEnumeration Subtract(Type t, BitflagEnumeration first, BitflagEnumeration second)
        {
            BitflagEnumeration n = Activator.CreateInstance(t, first.Value & ~second.Value) as BitflagEnumeration;
            return n;
        }
    }
}