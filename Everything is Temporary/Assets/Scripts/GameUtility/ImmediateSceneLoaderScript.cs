﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameUtility
{
    public class ImmediateSceneLoaderScript : MonoBehaviour
    {
        [Header("Additively loads the specified scenes, and then destroys self.")]
        [SerializeField]
        private List<string> m_scenesToLoad = default;

        private void Awake()
        {
            foreach (string sceneName in m_scenesToLoad)
                SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);

            Destroy(gameObject);
        }
    }
}