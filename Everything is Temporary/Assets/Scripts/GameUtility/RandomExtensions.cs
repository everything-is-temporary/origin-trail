﻿using System;
namespace GameUtility
{
    public static class RandomExtensions
    {
        public static long NextLong(this Random rand)
        {
            byte[] bytes = new byte[8];
            rand.NextBytes(bytes);

            return BitConverter.ToInt64(bytes, 0);
        }
    }
}
