﻿using System;

namespace GameUtility
{
    /// <summary>
    /// Keeps track of a nonnegative number that can be temporarily incremented.
    /// </summary>
    public class Counter
    {
        public int Total { get; private set; }
        public event Action<int> TotalChanged;

        /// <summary>
        /// Increments the counter, returning an <see cref="IDisposable"/> that
        /// decrements it in <see cref="IDisposable.Dispose"/>. Use in <see langword="using"/>
        /// statement.
        /// </summary>
        /// <returns>A disposable object that can be used to decrement the counter.</returns>
        public IDisposable Increase()
        {
            Increment();
            return new DisposableAction(Decrement);
        }

        private void Increment()
        {
            ++Total;
            TotalChanged?.Invoke(Total);
        }

        private void Decrement()
        {
            --Total;
            TotalChanged?.Invoke(Total);
        }
    }
}
