﻿using System;
using System.Linq;
using System.Collections.Generic;
namespace GameUtility
{
    public interface IIntPermutation
    {
        int Size { get; }

        int Permute(int idx);
        int Inverse(int idx);
    }

    [Serializable]
    public class MutableIntPermutation : IIntPermutation
    {
        public MutableIntPermutation(int size)
        {
            SetIdentity(size);
        }

        public MutableIntPermutation(IEnumerable<int> permutation)
        {
            Set(permutation);
        }

        public void SetIdentity(int size)
        {
            m_forward = new List<int>(size);
            m_inverse = new List<int>(size);

            for (int i = 0; i < size; ++i)
            {
                m_forward.Add(i);
                m_inverse.Add(i);
            }
        }

        public MutableIntPermutation GetInverse()
        {
            MutableIntPermutation permutation = new MutableIntPermutation(Size);

            permutation.m_forward = new List<int>(m_inverse);
            permutation.m_inverse = new List<int>(m_forward);

            return permutation;
        }

        public void Set(IEnumerable<int> permutation)
        {
            m_forward = permutation.ToList();
            m_inverse = new List<int>(m_forward.Capacity);

            for (int i = 0; i < m_forward.Count; ++i)
                m_inverse.Add(-1);

            for (int i = 0; i < m_forward.Count; ++i)
            {
                int nextIdx = m_forward[i];

                if (nextIdx < 0 || nextIdx >= m_inverse.Count)
                    throw new ArgumentException("Given 'permutation' parameter" +
                        " has values that are out of range. Range is" +
                        $" [0, {m_inverse.Count - 1}] but values are: " +
                        $" {string.Join(", ", m_forward)}");

                if (m_inverse[nextIdx] != -1)
                    throw new ArgumentException("Given 'permutation' parameter" +
                    	" is not an invertible permutation (it has duplicate values):" +
                    	$" {string.Join(", ", m_forward)}");

                m_inverse[nextIdx] = i;
            }
        }

        public int Size => m_forward.Count;

        public int Permute(int idx)
        {
            return m_forward[idx];
        }

        public int Inverse(int idx)
        {
            return m_inverse[idx];
        }

        public void RemoveObject(int idx)
        {
            int nextIdx = m_forward[idx];

            m_forward.RemoveAt(idx);
            for (int i = 0; i < m_forward.Count; ++i)
            {
                if (m_forward[i] > nextIdx)
                    m_forward[i] = m_forward[i] - 1;
            }

            m_inverse.RemoveAt(nextIdx);
            for (int i = 0; i < m_inverse.Count; ++i)
            {
                if (m_inverse[i] > idx)
                    m_inverse[i] = m_inverse[i] - 1;
            }
        }

        public int AddObject()
        {
            int newIdx = m_forward.Count;

            m_forward.Add(newIdx);
            m_inverse.Add(newIdx);

            return newIdx;
        }

        [UnityEngine.SerializeField]
        private List<int> m_forward;

        [UnityEngine.SerializeField]
        private List<int> m_inverse;
    }
}
