﻿using System;
using UnityEngine;
using CoreGame.Progression;

namespace GameUtility
{
    /// <summary>
    /// Injects the time of day into an animator as a float.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class AnimatorFloatTimeOfDayScript : MonoBehaviour
    {
        [Tooltip("This float parameter on the Animator will be set to the time of day," +
            " as a percentage (range 0.0 to 1.0). 0 and 1 are midnight, 0.5 is noon.")]
        public string paramName;

        private void Awake()
        {
            m_animator = GetComponent<Animator>();
        }

        private void Start()
        {
            ProgressionManager.Instance.OnGameTimeChange += HandleTimeAdjusted;
        }

        private void HandleTimeAdjusted(float newTime)
        {
            m_animator.SetFloat(paramName, newTime);
        }

        private Animator m_animator;
    }
}