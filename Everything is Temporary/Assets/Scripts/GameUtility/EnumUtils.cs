using System;

namespace GameUtility
{
    public static class EnumUtils
    {
        public static bool ContainsAny<T>(this T thisIntEnum, T other) where T : IConvertible
        {
            return ((int)(IConvertible)thisIntEnum & (int)(IConvertible)other) != 0;
        }
        
        public static bool ContainsAll<T>(this T thisIntEnum, T other) where T : IConvertible
        {
            var second = (int) (IConvertible) other;
            return ((int)(IConvertible)thisIntEnum & second) == second;
        }
    }
}