﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace GameUtility
{
    public static class IdConverter
    {
        public static string ToReadableString(long id)
        {
            byte[] bytes = BitConverter.GetBytes(id);

            List<string> byteStrings = (from b in bytes select ToHex(b)).ToList();

            return byteStrings[0] + byteStrings[1] + "_" +
                byteStrings[2] + byteStrings[3] + "_" +
                byteStrings[4] + byteStrings[5] + "_" +
                byteStrings[6] + byteStrings[7];
        }

        public static long FromReadableString(string idString)
        {
            // TODO: Test this method. Create automated tests making sure
            // FromReadableString(ToReadableString(id)) == id.
            var pieces = idString.Split('_');

            if (pieces.Length != 4)
                throw new ArgumentException($"The given string '{idString}' did not come from the {nameof(ToReadableString)} method");

            byte[] bytes = new byte[8];

            for (int pieceIdx = 0; pieceIdx < 4; ++pieceIdx)
                for (int byteIdx = 0; byteIdx < 2; ++byteIdx)
                    bytes[pieceIdx * 2 + byteIdx] = ByteFromHex(pieces[pieceIdx].Substring(byteIdx * 2, 2));

            return BitConverter.ToInt64(bytes, 0);
        }

        public static string ToHex(byte b)
        {
            return $"{HexChars[b >> 4]}{HexChars[b & 0xF]}";
        }

        public static byte ByteFromHex(string hexByte)
        {
            return (byte)((
                          ((byte)HexChars.IndexOf(hexByte[0])) << 4)
                        | ((byte)HexChars.IndexOf(hexByte[1]))
                      );
        }

        private static readonly string HexChars = "0123456789ABCDEF";
    }
}
