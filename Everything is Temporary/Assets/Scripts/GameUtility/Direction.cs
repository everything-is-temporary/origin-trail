﻿
/// <summary>
/// Represents either "left" or "right".
/// </summary>
public enum Direction
{
    Left, Right
}
