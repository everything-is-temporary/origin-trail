﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public class ZoomCameraScript : MonoBehaviour
    {
        [SerializeField] private Camera m_camera = default;
        [SerializeField] private float m_toValue = default;
        [SerializeField] private float m_zoomDurationSec = default;

        public void ZoomToValue()
        {
            if (m_cameraZoomCoroutine != null)
                StopCoroutine(m_cameraZoomCoroutine);

            m_cameraZoomCoroutine = StartCoroutine(ZoomCameraCR(m_toValue));
        }

        public void ZoomBack()
        {
            if (m_cameraZoomCoroutine != null)
                StopCoroutine(m_cameraZoomCoroutine);

            m_cameraZoomCoroutine = StartCoroutine(ZoomCameraCR(m_cameraOriginalValue));
        }

        public IEnumerator ZoomCameraCR(float endZ)
        {
            float startZ = m_camera.transform.position.z;

            // Duration of zoom in seconds.
            float startTime = Time.fixedTime;

            while (Time.fixedTime - startTime < m_zoomDurationSec)
            {
                float changeInTime = Time.fixedTime - startTime;
                m_camera.transform.SetZ(Mathf.SmoothStep(startZ, endZ, changeInTime / m_zoomDurationSec));
                yield return new WaitForFixedUpdate();
            }

            m_camera.transform.SetZ(endZ);
        }

        private void Start()
        {
            m_cameraOriginalValue = m_camera.transform.position.z;
        }

        private Coroutine m_cameraZoomCoroutine;
        private float m_cameraOriginalValue;
    }
}