﻿using GameInventory;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    public static class ItemsTestFunctions
    {
        private static Random rng = new Random();
        private static List<string> someInventoryItems = new List<string> { "7098_3110_116A_4470", "FC5B_1D6D_A134_05F1", "DBB2_C3DE_75FC_8669", "AC46_8AEE_C62D_1044", "0C0D_15E8_D3B4_E9E8" };


        public static Inventory MakeFakeInventory()
        {
            Inventory fakeInventory = new Inventory();
            foreach (string s in someInventoryItems)
            {
                fakeInventory.ModifyItem(ItemReference.FromId(IdConverter.FromReadableString(s)), Mathf.FloorToInt(Random.Range(1, 10)));
            }
            return fakeInventory;
        }
    }
}
