﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility
{
    /// <summary>
    /// Randomizes the given floats on an animator once. This is useful to
    /// inject random numbers into an animator state machine.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class AnimatorFloatRandomizerScript : MonoBehaviour
    {
        public List<NameAndRange> floatsToRandomize;

        [System.Serializable]
        public struct NameAndRange
        {
            public string paramName;
            public float min;
            public float max;
        }

        private void Awake()
        {
            Animator animator = GetComponent<Animator>();

            if (animator == null)
            {
                Debug.LogWarning($"No animator on {gameObject}, required by {name}.", gameObject);
                return;
            }

            // Put existing parameters into a dictionary.
            var existingParams = new Dictionary<string, AnimatorControllerParameterType>();
            foreach (var param in animator.parameters)
            {
                existingParams.Add(param.name, param.type);
            }

            foreach (NameAndRange nameAndRange in floatsToRandomize)
            {
                if (!existingParams.TryGetValue(nameAndRange.paramName, out var paramType))
                {
                    Debug.LogWarning($"No parameter '{nameAndRange.paramName}' in animator controller.", gameObject);
                    continue;
                }

                if (paramType != AnimatorControllerParameterType.Float)
                {
                    Debug.LogWarning($"Parameter '{nameAndRange.paramName}' is not a float.", gameObject);
                    continue;
                }

                float value = Random.Range(nameAndRange.min, nameAndRange.max);
                animator.SetFloat(nameAndRange.paramName, value);
            }
        }
    }
}