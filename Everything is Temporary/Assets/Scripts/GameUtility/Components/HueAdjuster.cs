﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Components
{
    /// <summary>
    /// Calculates and applies a random hue color from determinisitc x position within a particular range.
    /// </summary>
    [ExecuteInEditMode]
    public class HueAdjuster : MonoBehaviour
    {
        [Range(0.0f, 1.0f)]
        public float minRendererColorValue = 0.5f;
        [Range(0.0f, 1.0f)]
        public float maxRendererColorValue = 0.7f;

        private void Update(){
            SetPositionDeterminedColor();
        }

        private void SetPositionDeterminedColor()
        {
            gameObject.GetComponent<SpriteRenderer>().color = GetPositionDeterminedRandomColor();
        }

        private Color GetPositionDeterminedRandomColor()
        {
            int positionX = (int)System.BitConverter.DoubleToInt64Bits(gameObject.transform.position.x);
            System.Random rng = new System.Random(new System.Random(positionX).Next());
            return new Color(
                FitToConstraints(rng.NextDouble()), 
                FitToConstraints(rng.NextDouble()), 
                FitToConstraints(rng.NextDouble())
            );
        }

        private float FitToConstraints(double toFit)
        {
            return Mathf.Lerp(minRendererColorValue, maxRendererColorValue, (float)toFit);
        }
    }
}
