﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUI;

namespace GameUtility.Components
{
    /// <summary>
    /// Helper component that exposes the function for opening the save-load menu.
    /// </summary>
    public class OpenSaveLoadScript : MonoBehaviour
    {
        public void OpenSaveLoadMenu()
        {
            GameUIHelper.Instance.OpenSaveLoadModal();
        }
    }
}