﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GameUtility.Components.PixelScaler
{
    [ExecuteInEditMode]
    public class PixelSizeHelperScript : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField]
        [Tooltip("Sprite that will be used as a reference for pixel size.")]
        private SpriteRenderer m_reference = default;

        [SerializeField]
        [Tooltip("The camera that will draw the scene.")]
        private Camera m_mainCamera = default;

        [MenuItem("GameObject/Add Pixel Size Helper", false, 10)]
        private static void AddPixelSizeHelper(MenuCommand command)
        {
            GameObject parent = command.context as GameObject;

            GameObject helper = new GameObject("Pixel Size Helper")
            {
                tag = "EditorOnly"
            };

            Scene targetScene = parent == null ? helper.scene : parent.scene;

            if (GetHelperForScene(targetScene) != null)
            {
                Debug.Log("There already exists a helper in this scene.");
                DestroyImmediate(helper);
            }
            else
            {
                SceneManager.MoveGameObjectToScene(helper, targetScene);
                helper.AddComponent<PixelSizeHelperScript>();

                Undo.RegisterCreatedObjectUndo(helper, "Created Pixel Size Helper");
            }
        }

        [MenuItem("GameObject/Fix Pixel Size", false, 10)]
        [MenuItem("CONTEXT/SpriteRenderer/Fix Pixel Size", false, 10)]
        private static void FixPixelSize(MenuCommand command)
        {
            SpriteRenderer spriteRenderer = null;

            if (command.context is GameObject go)
            {
                spriteRenderer = go.GetComponent<SpriteRenderer>();
            }
            else if (command.context is SpriteRenderer renderer)
            {
                spriteRenderer = renderer;
            }

            if (spriteRenderer != null)
            {
                var helperInScene = GetHelperForScene(spriteRenderer.gameObject.scene);

                // In the GameObject context menu, the validate function doesn't run.
                if (helperInScene == null)
                {
                    Debug.Log($"No {nameof(PixelSizeHelperScript)} instance found in this scene. " +
                    	"Create an empty GameObject and add that component to use this functionality.");
                    return;
                }

                Undo.RecordObject(spriteRenderer.transform, "Rescaled object's pixels.");
                helperInScene.ScaleFromBottomCenterToCorrectDensity(spriteRenderer);
            }
            else
            {
                Debug.Log("This object does not have a SpriteRenderer component.");
            }
        }

        [MenuItem("GameObject/Fix Pixel Size", true, 10)]
        [MenuItem("CONTEXT/SpriteRenderer/Fix Pixel Size", true, 10)]
        private static bool CanFixPixelSize(MenuCommand command)
        {
            SpriteRenderer spriteRenderer = null;

            if (command.context is GameObject go)
            {
                spriteRenderer = go.GetComponent<SpriteRenderer>();
            }
            else if (command.context is SpriteRenderer renderer)
            {
                spriteRenderer = renderer;
            }

            if (spriteRenderer != null)
            {
                return GetHelperForScene(spriteRenderer.gameObject.scene) != null;
            }

            return false;
        }

        private static PixelSizeHelperScript GetHelperForScene(Scene scene)
        {
            return allHelpers.FirstOrDefault(helper => helper.gameObject.scene == scene);
        }

        private void OnEnable()
        {
            if (GetHelperForScene(gameObject.scene) != null)
            {
                Destroy(this);
                return;
            }

            allHelpers.Add(this);

            EditorApplication.hierarchyWindowItemOnGUI += OnHierarchyGUI;
            EditorApplication.hierarchyChanged += OnHierarchyChanged;

            CheckAllSpriteRenderers();
        }

        private void OnDisable()
        {
            allHelpers.Remove(this);

            EditorApplication.hierarchyChanged -= OnHierarchyChanged;
            EditorApplication.hierarchyWindowItemOnGUI -= OnHierarchyGUI;
        }

        private void OnHierarchyChanged()
        {
            CheckAllSpriteRenderers();
        }

        private void CheckAllSpriteRenderers()
        {
            if (!Application.isEditor)
                return;

            if (m_reference == null || m_mainCamera == null)
                return;

            m_trackedInstances.Clear();
            foreach (var spriteRenderer in FindObjectsOfType<SpriteRenderer>())
                CheckAndRecord(spriteRenderer);
        }

        private void CheckAndRecord(SpriteRenderer spriteRenderer)
        {
            bool? isPixelSizeGood = CheckPixelSize(spriteRenderer);

            if (!isPixelSizeGood.HasValue)
                return;

            if (isPixelSizeGood.Value)
                UpdateStatusGood(spriteRenderer.gameObject);
            else
                UpdateStatusBad(spriteRenderer.gameObject);
        }

        private void UpdateStatusGood(GameObject go)
        {
            GetOrCreateTracker(go).SetGood();
        }

        private void UpdateStatusBad(GameObject go)
        {
            GetOrCreateTracker(go).SetBad();
        }

        private PixelSizeTracker GetOrCreateTracker(GameObject go)
        {
            int instanceId = go.GetInstanceID();

            if (!m_trackedInstances.TryGetValue(instanceId, out PixelSizeTracker tracker))
            {
                tracker = new PixelSizeTracker
                {
                    HasSpriteRenderer = go.GetComponent<SpriteRenderer>() != null
                };

                m_trackedInstances[instanceId] = tracker;

                GameObject parent = go.transform.parent?.gameObject;
                if (parent != null)
                    tracker.Parent = GetOrCreateTracker(parent);
            }

            return tracker;
        }

        private bool? CheckPixelSize(SpriteRenderer spriteRenderer)
        {
            Vector2? densityOrNull = ComputePixelDensity(spriteRenderer);
            Vector2? referenceDensityOrNull = GetReferencePixelDensity();

            if (!densityOrNull.HasValue || !referenceDensityOrNull.HasValue)
                return null;

            Vector2 density = densityOrNull.Value;
            Vector2 referenceDensity = referenceDensityOrNull.Value;

            float sensitivity = 0.0001f;

            return Mathf.Abs(density.x - referenceDensity.x) < sensitivity
                && Mathf.Abs(density.y - referenceDensity.y) < sensitivity;
        }

        private Vector2? ComputePixelDensity(SpriteRenderer spriteRenderer)
        {
            bool isSimple = spriteRenderer.drawMode == SpriteDrawMode.Simple;
            bool isTiledContinuously = spriteRenderer.drawMode == SpriteDrawMode.Tiled
                && spriteRenderer.tileMode == SpriteTileMode.Continuous;

            if (!isSimple && !isTiledContinuously)
                return null;

            if (spriteRenderer.sprite == null)
                return null;

            Vector2 numPixels = spriteRenderer.sprite.rect.size;

            Vector2 spriteSize;
            if (isTiledContinuously)
            {
                spriteSize = numPixels * spriteRenderer.size / spriteRenderer.sprite.bounds.size;
            }
            else
            {
                spriteSize = numPixels;
            }

            var bounds = spriteRenderer.bounds;

            float distToCamera = bounds.center.z - m_mainCamera.transform.position.z;

            return distToCamera * spriteSize / bounds.size;
        }

        private Vector2? GetReferencePixelDensity()
        {
            return ComputePixelDensity(m_reference);
        }

        private void OnHierarchyGUI(int instanceID, Rect selectionRect)
        {
            if (!Application.isEditor)
                return;

            if (m_trackedInstances.TryGetValue(instanceID, out PixelSizeTracker tracker))
            {
                GUIContent icon = null;

                if (tracker.IsBad)
                    icon = EditorGUIUtility.IconContent(errorIconName);
                else if (tracker.HasBadChildren)
                    icon = EditorGUIUtility.IconContent(warnIconName);
                else if (tracker.HasSpriteRenderer)
                    icon = EditorGUIUtility.IconContent(goodIconName);

                if (icon != null)
                    EditorGUI.LabelField(selectionRect, icon);
            }
        }

        private void ScaleFromBottomCenterToCorrectDensity(SpriteRenderer spriteRenderer)
        {
            Vector2? desiredDensity = GetReferencePixelDensity();
            if (!desiredDensity.HasValue)
            {
                return;
            }

            ScaleFromBottomCenter(spriteRenderer, desiredDensity.Value);

            int instanceId = spriteRenderer.gameObject.GetInstanceID();
            if (m_trackedInstances.TryGetValue(instanceId, out PixelSizeTracker tracker))
                tracker.SetGood();
        }

        private void ScaleFromBottomCenter(SpriteRenderer spriteRenderer, Vector2 desiredDensity)
        {
            Vector2? densityOrNull = ComputePixelDensity(spriteRenderer);

            if (!densityOrNull.HasValue)
                return;

            Vector2 density = densityOrNull.Value;

            Vector3 offsetToBottom = spriteRenderer.bounds.center + Vector3.down * spriteRenderer.bounds.extents.y;

            Vector2 scale = density / desiredDensity;

            var oldScale = spriteRenderer.transform.localScale;
            oldScale.Scale(new Vector3(scale.x, scale.y, 1));
            spriteRenderer.transform.localScale = oldScale;

            Vector3 newOffsetToBottom = spriteRenderer.bounds.center + Vector3.down * spriteRenderer.bounds.extents.y;

            // Subtract the difference to preserve position of sprite renderer's
            // bottom center point.
            spriteRenderer.transform.position -= newOffsetToBottom - offsetToBottom;
        }

        private readonly Dictionary<int, PixelSizeTracker> m_trackedInstances = new Dictionary<int, PixelSizeTracker>();

        private static readonly string errorIconName = "redLight";
        private static readonly string goodIconName = "greenLight";
        private static readonly string warnIconName = "orangeLight";

        private static readonly List<PixelSizeHelperScript> allHelpers = new List<PixelSizeHelperScript>();
#endif
    }
}