﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GameUtility.Components.PixelScaler
{
    [ExecuteInEditMode]
    public class PixelSizeTracker
    {
        public bool HasSpriteRenderer { get; set; }
        public bool IsGood { get; private set; } = true;
        public bool IsBad => !IsGood;
        public bool HasBadChildren => BadChildren.Count > 0;

        public PixelSizeTracker Parent
        {
            get => m_parent;
            set
            {
                if (m_parent == value)
                    return;

                if (!IsGood || HasBadChildren)
                    m_parent?.RemoveBadChild(this);

                m_parent = value;

                if (!IsGood || HasBadChildren)
                    m_parent?.AddBadChild(this);
            }
        }
        public IReadOnlyCollection<PixelSizeTracker> BadChildren => m_badChildren;



        public void SetGood()
        {
            bool wasBad = !IsGood;
            IsGood = true;

            if (wasBad)
                Parent?.RemoveBadChild(this);
        }

        public void SetBad()
        {
            IsGood = false;

            Parent?.AddBadChild(this);
        }

        private void AddBadChild(PixelSizeTracker badChild)
        {
            m_badChildren.Add(badChild);

            Parent?.AddBadChild(this);
        }

        private void RemoveBadChild(PixelSizeTracker badChild)
        {
            m_badChildren.Remove(badChild);

            if (IsGood && BadChildren.Count == 0)
                Parent?.RemoveBadChild(this);
        }

        private PixelSizeTracker m_parent;
        private readonly HashSet<PixelSizeTracker> m_badChildren = new HashSet<PixelSizeTracker>();
    }
}
#endif