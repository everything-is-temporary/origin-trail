﻿using System.Threading.Tasks;
using GameUI;
using UnityEngine;

namespace GameUtility.Components
{
    /// <summary>
    /// Allows a UI element to flip book pages.
    /// </summary>
    public class PageTurnerScript : MonoBehaviour
    {
        public void GoToEarlierPage()
        {
            BookScript.Instance.FlipToEarlierPage();
        }

        public void GoToLaterPage()
        {
            BookScript.Instance.FlipToLaterPage();
        }

        public void GoToEarlierPageAfterKnot(string knotPath)
        {
            PerformDialogueAndFlipPage(knotPath, true).FireAndForget();
        }

        public void GoToLaterPageAfterKnot(string knotPath)
        {
            PerformDialogueAndFlipPage(knotPath, false).FireAndForget();
        }

        public void GoToEarlierPageDuringKnot(string knotPath)
        {
            GoToEarlierPage();
            CoreGameManager.Instance.PerformDialog(knotPath).FireAndForget();
        }

        public void GoToLaterPageDuringKnot(string knotPath)
        {
            GoToLaterPage();
            CoreGameManager.Instance.PerformDialog(knotPath).FireAndForget();
        }

        async Task PerformDialogueAndFlipPage(string knotPath, bool flipToEarlier)
        {
            await CoreGameManager.Instance.PerformDialog(knotPath);

            if (flipToEarlier)
                GoToEarlierPage();
            else
                GoToLaterPage();
        }
    }
}