﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Components
{
    /// <summary>
    /// Internded to darken the sprite by changing material color. Will conflict with HueAdjuster.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteDarkener : MonoBehaviour
    {
        [Range(0.0f, 1.0f)]
        public float Darkness;
        private Color baseColor = Color.white;

        // Start is called before the first frame update
        void Start()
        {
            baseColor = gameObject.GetComponent<SpriteRenderer>().color;
            DarkenSprite(Darkness);
        }

        void OnValidate()
        {
            DarkenSprite(Darkness);
        }

        public void DarkenSprite(float darknessFactor)
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.Lerp(baseColor, Color.black, darknessFactor);
        }
    }
}
