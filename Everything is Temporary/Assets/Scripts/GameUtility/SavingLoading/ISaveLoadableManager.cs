﻿using UnityEngine;

namespace GameUtility.DataPersistence
{
    [System.Serializable]
    public abstract class ISaveLoadableManager<SingletonClass>: SingletonMonoBehavior<SingletonClass>, ISaveLoadable where SingletonClass: class
    {
        public abstract ISerializationCallbackReceiver GetSerializedData();

        public abstract bool SetSerializedData(ISerializationCallbackReceiver data);

        protected virtual void Start()
        {
            SaveManager.RegisterSaveLoadable(this);
        }

        protected override void OnDestroy()
        {
            SaveManager.UnregisterSaveLoadable(this);
        }
    }
}
