﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameUtility.Serialization;
using UnityEngine;

namespace GameUtility.DataPersistence
{
    /// <summary>
    /// This component allows other components to register data that should be
    /// saved with the scene. When the scene is reloaded, the saved state can
    /// be accessed through this object.
    /// </summary>
    public class SceneSaveableObject : MonoBehaviour
    {
        public delegate object SaveDataProvider();

        /// <summary>
        /// Adds a function that generates a save state. The object returned
        /// by this function must be of a class that is marked [Serializable].
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if the key is already in use.</exception>
        /// <param name="key">Key under which to register this function. Only needs
        /// to be unique among the other keys used on this same saveable object.</param>
        /// <param name="value">Function to generate save state.</param>
        public void AddSaveState(string key, SaveDataProvider value)
        {
            if (m_saveStateObjects.ContainsKey(key))
                throw new ArgumentException("Cannot call AddSaveState() with a key that has been used in SetSaveState().");

            m_saveState.Add(key, value);
        }

        /// <summary>
        /// Sets a save state for the key.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if the key is in use by <see cref="AddSaveState(string, SaveDataProvider)"/>.</exception>
        /// <param name="key">Key. Only needs to be unique among the other keys used on this same saveable object.</param>
        /// <param name="value">An object of a serializable class (must be marked [Serializable]).</param>
        public void SetSaveState(string key, object value)
        {
            if (m_saveState.ContainsKey(key))
                throw new ArgumentException("Cannot call SetSaveState() with a key that has been used in AddSaveState().");

            m_saveStateObjects[key] = value;
        }

        /// <summary>
        /// Gets the value saved for the key.
        /// </summary>
        /// <returns><c>true</c>, if a value was returned, <c>false</c> otherwise.</returns>
        /// <param name="key">Key. Only needs to be unique among the other keys used on this same saveable object.</param>
        /// <param name="state">The value previously saved for the key.</param>
        public bool TryGetSaveState(string key, out object state)
        {
            if (m_saveStateObjects.TryGetValue(key, out state))
                return true;

            if (TryGetSavedData(out var data))
            {
                return data.TryGetValue(key, out state);
            }

            state = null;
            return false;
        }

        /// <summary>
        /// Gets a snapshot of the data saved on this object.
        /// </summary>
        /// <returns>The save data.</returns>
        public IDictionary<string, object> GetSaveData()
        {
            var data = new Dictionary<string, object>();

            if (m_saveLocalTransform)
            {
                data["transform.localPosition"] = new SerializableVector3(transform.localPosition);
                data["transform.localRotation"] = new SerializableQuaternion(transform.localRotation);
            }

            foreach (var saveState in m_saveState)
                data.Add(saveState.Key, saveState.Value());

            foreach (var saveState in m_saveStateObjects)
                data.Add(saveState.Key, saveState.Value);

            return data;
        }

        private void Awake()
        {
            m_saveKey = $"{CoreGameManager.Instance.CurrentSceneName}/{m_name}";
        }

        private void Start()
        {
            // Restore the transform if it should be restored and was previously saved.
            if (m_saveLocalTransform && TryGetSavedData(out var savedData))
            {
                transform.localPosition = ((SerializableVector3)savedData["transform.localPosition"]).Vector;
                transform.localRotation = ((SerializableQuaternion)savedData["transform.localRotation"]).Quaternion;
            }

            SceneDataManager.Instance.SetSaveableObject(m_saveKey, this);

            CoreGameManager.Instance.UnloadGameSceneActions.Add(HandleSceneUnloading);
        }

        private Task HandleSceneUnloading()
        {
            m_didSceneUnload = true;
            return Task.CompletedTask;
        }

        private void OnDestroy()
        {
            if (m_didSceneUnload)
            {
                SceneDataManager.Instance.SaveObject(m_saveKey);
            }
            else
            {
                // This means that the object is being removed from the scene,
                // and not that the scene is being unloaded. We should remove
                // the object's save data from the scene.

                SceneDataManager.Instance?.RemoveSavedData(m_saveKey);
            }

            SceneDataManager.Instance?.RemoveSaveableObject(m_saveKey, this);
            CoreGameManager.Instance?.UnloadGameSceneActions.Remove(HandleSceneUnloading);

            // Release resources to avoid memory leaks.
            m_saveState = null;
            m_saveStateObjects = null;
        }

        private bool TryGetSavedData(out Dictionary<string, object> savedData)
        {
            if (SceneDataManager.Instance.TryGetSaveData(m_saveKey, out object data))
            {
                if (data is Dictionary<string, object> saved)
                {
                    savedData = saved;
                    return true;
                }
            }

            savedData = null;
            return false;
        }

        [SerializeField]
        [Tooltip("Name used for saving this object. Should be unique within a scene.")]
        private string m_name = "";

        [SerializeField]
        private bool m_saveLocalTransform = true;

        private string m_saveKey;
        private bool m_didSceneUnload;

        private Dictionary<string, object> m_saveStateObjects = new Dictionary<string, object>();
        private Dictionary<string, SaveDataProvider> m_saveState = new Dictionary<string, SaveDataProvider>();
    }
}