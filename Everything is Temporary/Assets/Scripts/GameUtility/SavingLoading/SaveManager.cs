﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

namespace GameUtility.DataPersistence
{
    public class SaveManager : SingletonMonoBehavior<SaveManager>
    {
        //pdb: "Program Database"
        public static string saveMetafileName = "saves.pdb";
        public string SaveMetaFilePath => Path.GetFullPath(Path.Combine(savesPath, saveMetafileName));

        private static string savesPath;

        public static void RegisterSaveLoadable(ISaveLoadable saveLoadable)
        {
            if (Instance != null)
            {
                if (Instance.m_registeredSaveLoadables.ContainsKey(saveLoadable.GetType()))
                {
                    throw new ArgumentException($"Cannot register duplicate ISaveLoadable class: {saveLoadable.GetType()}");
                }

                Instance.m_registeredSaveLoadables.Add(saveLoadable.GetType(), new WeakReference<ISaveLoadable>(saveLoadable));
            }
            else
            {
                if (m_saveLoadablesToRegister == null)
                    m_saveLoadablesToRegister = new List<ISaveLoadable>();

                m_saveLoadablesToRegister.Add(saveLoadable);
            }
        }

        public static void UnregisterSaveLoadable(ISaveLoadable saveLoadable)
        {
            if (Instance != null)
            {
                Instance.m_registeredSaveLoadables.Remove(saveLoadable.GetType());
            }
            else
            {
                if (m_saveLoadablesToRegister != null)
                    m_saveLoadablesToRegister.Remove(saveLoadable);
            }
        }

        protected override void PostAwake()
        {
            savesPath = Path.Combine(Application.persistentDataPath, "SaveData");

            if (m_saveLoadablesToRegister != null)
            {
                foreach (ISaveLoadable saveLoadable in m_saveLoadablesToRegister)
                    RegisterSaveLoadable(saveLoadable);

                m_saveLoadablesToRegister = null;
            }
        }

        private void Start()
        {
            SetUpClass();
            RetrieveSaveMetadata();
            CleanSaveDirectory();
        }

        public GameSaveMetadata SaveGame()
        {
            string saveFileName = GenerateUniqueSavefileName();
            try
            {
                WriteSerializationToDataFile(saveFileName, CreateSaveDataAggregation());
            }
            catch (System.Runtime.Serialization.SerializationException serializationException)
            {
                Debug.LogWarning(serializationException);
                return null;
            }
            GameSaveMetadata metadata = new GameSaveMetadata(saveFileName);
            m_saveMetadata.Add(metadata.FilePath, metadata);
            UpdateSaveMetadata();
            return metadata;
        }

        public List<GameSaveMetadata> GetGameSaveMetadata()
        {
            return m_saveMetadata.Values.OrderBy(metadata => metadata.CreationDate).ToList();
        }

        public bool LoadGame(GameSaveMetadata gameSaveMetadata)
        {
            return SetDataSerialization(LoadSerializationFromSaveFile(gameSaveMetadata.FilePath));
        }

        public bool DeleteGameSave(GameSaveMetadata gameSaveMetadata)
        {
            if (File.Exists(gameSaveMetadata.FilePath))
            {
                if (m_saveMetadata.Remove(gameSaveMetadata.FilePath))
                {
                    File.Delete(gameSaveMetadata.FilePath);
                    UpdateSaveMetadata();
                    return true;
                }
                return false;
            }
            return false;
        }

        private void SetUpClass()
        {
            if (!Directory.Exists(savesPath))
            {
                Directory.CreateDirectory(savesPath);
            }
        }

        private void RetrieveSaveMetadata()
        {
            if (File.Exists(SaveMetaFilePath))
            {
                m_saveMetadata = (Dictionary<string, GameSaveMetadata>)DeserializeFileData(SaveMetaFilePath);
            }
        }

        private void UpdateSaveMetadata()
        {
            WriteSerializationToDataFile(SaveMetaFilePath, m_saveMetadata);
        }

        private void CleanSaveDirectory()
        {
            foreach (string path in Directory.EnumerateFiles(savesPath))
            {
                if (!m_saveMetadata.Keys.Any(item => item == Path.GetFullPath(path)) && !Path.GetFullPath(path).Equals(SaveMetaFilePath))
                {
                    File.Delete(path);
                }
            }
        }

        private string GenerateUniqueSavefileName()
        {
            string fpath;
            do
            {
                fpath = Path.Combine(savesPath, string.Format("{0}.dat", Path.GetRandomFileName()));

            } while (File.Exists(fpath));
            return fpath;
        }

        private SerializableDict<Type, ISerializationCallbackReceiver> CreateSaveDataAggregation()
        {
            SerializableDict<Type, ISerializationCallbackReceiver> serializedData = new SerializableDict<Type, ISerializationCallbackReceiver>();
            foreach (WeakReference<ISaveLoadable> saveLoadableRef in m_registeredSaveLoadables.Values)
            {
                if (saveLoadableRef.TryGetTarget(out ISaveLoadable saveLoadable))
                {
                    // Call this manually because we are not using the Unity serializer.
                    var serialized = saveLoadable.GetSerializedData();
                    serialized.OnBeforeSerialize();

                    serializedData.Add(saveLoadable.GetType(), serialized);
                }
                else
                {
                    Debug.LogError($"Unable to access Weakreference for Type {saveLoadableRef.GetType()}, make sure that you deregister ISaveLoadableObjects from this class before collection.");
                }
            }
            return serializedData;
        }

        private void WriteSerializationToDataFile(string filename, object serializableData)
        {
            FileStream saveStream = new FileStream(filename, FileMode.Create);
            m_binaryFormatter.Serialize(saveStream, serializableData);
            saveStream.Close();
        }

        private SerializableDict<Type, ISerializationCallbackReceiver> LoadSerializationFromSaveFile(string filename)
        {
            return (SerializableDict<Type, ISerializationCallbackReceiver>)DeserializeFileData(filename);
        }

        private object DeserializeFileData(string filename)
        {
            FileStream loadStream = new FileStream(filename, FileMode.Open);
            object deserializedData = m_binaryFormatter.Deserialize(loadStream);
            loadStream.Close();
            return deserializedData;
        }

        /// <summary>
        /// Goes through the deserialized data and passes it to the corresponding
        /// registered <see cref="ISaveLoadable"/>s.
        /// </summary>
        /// <returns><c>true</c>, if everything succeeded during deserialization, <c>false</c> otherwise.</returns>
        /// <param name="deserializedData">Deserialized data.</param>
        private bool SetDataSerialization(SerializableDict<Type, ISerializationCallbackReceiver> deserializedData)
        {
            Dictionary<Type, bool> loadStatus = new Dictionary<Type, bool>();
            foreach (KeyValuePair<Type, ISerializationCallbackReceiver> typeDataPair in deserializedData)
            {
                loadStatus[typeDataPair.Key] = false;

                if (!m_registeredSaveLoadables.TryGetValue(typeDataPair.Key, out WeakReference<ISaveLoadable> saveLoadableRef))
                {
                    Debug.LogError($"Attempted to load data for {typeDataPair.Key.Name} but did not find registered object of that type.");
                }
                else if (!saveLoadableRef.TryGetTarget(out ISaveLoadable saveLoadable))
                {
                    Debug.LogError($"Attempted to access {typeDataPair.Key} by WeakReference but was unable.");
                }
                else
                {
                    // This is not called automatically because we are not using
                    // Unity's serialization.
                    typeDataPair.Value.OnAfterDeserialize();
                    loadStatus[typeDataPair.Key] = saveLoadable.SetSerializedData(typeDataPair.Value);
                }
            }

            return loadStatus.Values.All(successStatus => successStatus);
        }

        private Dictionary<Type, WeakReference<ISaveLoadable>> m_registeredSaveLoadables = new Dictionary<Type, WeakReference<ISaveLoadable>>();

        private Dictionary<string, GameSaveMetadata> m_saveMetadata = new Dictionary<string, GameSaveMetadata>();

        private static BinaryFormatter m_binaryFormatter = new BinaryFormatter();

        /// <summary>
        /// This contains all objects that were registered through
        /// <see cref="RegisterSaveLoadable(ISaveLoadable)"/> before the
        /// singleton was instantiated.
        /// </summary>
        private static List<ISaveLoadable> m_saveLoadablesToRegister;
    }
}
