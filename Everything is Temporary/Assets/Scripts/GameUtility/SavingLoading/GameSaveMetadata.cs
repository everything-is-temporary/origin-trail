﻿using System;
using System.IO;
using UnityEngine;

namespace GameUtility.DataPersistence
{
    [Serializable]
    public class GameSaveMetadata : ISerializationCallbackReceiver
    {

        public string FilePath { get; protected set; }
        public DateTime CreationDate { get; protected set; }
        public string SaveFriendlyName { get; protected set; }
        public float ProgressBar { get; protected set; }
        public bool IsLoadable => File.Exists(FilePath);

        public GameSaveMetadata(string filePath, string saveFriendlyName = "", float progressBar = 0f)
        {
            FilePath = Path.GetFullPath(filePath);
            SaveFriendlyName = saveFriendlyName;
            CreationDate = DateTime.Now;
            ProgressBar = progressBar;
        }

        public bool Load()
        {
            return SaveManager.Instance.LoadGame(this);
        }

        public bool Delete()
        {
            return SaveManager.Instance.DeleteGameSave(this);
        }

        #region Serialization

        private long _internalDateTimeAsString;

        public void OnAfterDeserialize()
        {
            CreationDate = DateTime.FromBinary(_internalDateTimeAsString);
        }

        public void OnBeforeSerialize()
        {
            _internalDateTimeAsString = CreationDate.ToBinary();
        }

        #endregion

    }
}
