﻿namespace GameUtility.DataPersistence
{
    public interface ISaveLoadable
    {
        UnityEngine.ISerializationCallbackReceiver GetSerializedData();

        bool SetSerializedData(UnityEngine.ISerializationCallbackReceiver data);
    }
}
