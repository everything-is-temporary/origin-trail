﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace GameUtility
{
    /// <summary>
    /// Asynchronous extensions for some Unity methods.
    /// </summary>
    public static class UnityAsyncExt
    {
        public static AsyncOperationAwaiter GetAwaiter(this AsyncOperation operation)
        {
            return new AsyncOperationAwaiter(operation);
        }
    }

    public class AsyncOperationAwaiter : INotifyCompletion
    {
        public AsyncOperationAwaiter(AsyncOperation operation)
        {
            m_asyncOperation = operation;
            m_continuation = null;
        }

        public bool IsCompleted => m_asyncOperation?.isDone ?? true;

        public void GetResult()
        {
            if (IsCompleted)
            {
                if (m_asyncOperation != null && m_continuation != null)
                    m_asyncOperation.completed -= m_continuation;

                m_continuation = null;
                m_asyncOperation = null;
                return;
            }

            throw new ApplicationException("Cannot block to wait for AsyncOperation.");
        }

        public void OnCompleted(Action continuation)
        {
            m_continuation = _ => continuation();
            m_asyncOperation.completed += m_continuation;
        }

        private AsyncOperation m_asyncOperation;
        private Action<AsyncOperation> m_continuation;
    }
}
