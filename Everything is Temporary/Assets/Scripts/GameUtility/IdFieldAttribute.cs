﻿using System;
using UnityEngine;

namespace GameUtility
{
    /// <summary>
    /// Place this on a long property that should be displayed like an ID.
    /// </summary>
    public class IdFieldAttribute : PropertyAttribute
    {
    }
}
