﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimatorTriggerScript : MonoBehaviour
{

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetAnimatorTrigger(string triggerName)
    {
        animator.SetTrigger(triggerName);
    }

    private Animator animator;

}
