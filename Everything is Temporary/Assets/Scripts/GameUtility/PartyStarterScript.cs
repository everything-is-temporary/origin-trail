﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyStarterScript : MonoBehaviour
{
    // Start is called before the first frame update
    public void AddCharacterToParty(string charID)
    {
        long longCharID = GameUtility.IdConverter.FromReadableString(charID);
        Characters.CharacterReference charRef = Characters.CharacterReference.FromId(longCharID);
        Characters.ActorManager.Instance.AddNewPartyMember(charRef);
    }

    public void AddGrandmaToParty()
    {
        long grandmaID = GameUtility.IdConverter.FromReadableString("E839_AE0F_8E3B_BF80");
        Characters.CharacterReference grandmaRef = Characters.CharacterReference.FromId(grandmaID);
        Characters.ActorManager.Instance.AddNewPartyMember(grandmaRef);
        Characters.CharacterManager.Instance.AddNewPartyMember(grandmaRef);
    }
}
