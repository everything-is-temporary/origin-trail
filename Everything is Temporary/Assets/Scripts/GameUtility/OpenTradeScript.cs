﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using GameInventory;

namespace GameUtility
{
    public class OpenTradeScript : MonoBehaviour
    {
        [SerializeField]
        private ShopReference shopRef = default;

        public void TestOpenEmptyTradeMenu()
        {
            TestOpenEmptyTradeMenuAsync().FireAndForget();
        }

        private async Task TestOpenEmptyTradeMenuAsync()
        {
            await TradeManager.PerformTradeWithShopAsync(InventoryManager.Instance.ConvoyAndPartyInventory, shopRef.Shop);
        }

    }
}