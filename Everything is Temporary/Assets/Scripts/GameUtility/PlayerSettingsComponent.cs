using UnityEngine;

namespace GameUtility
{
    public class PlayerSettingsComponent : MonoBehaviour
    {
        public void SetTextSpeed(float newValue)
        {
            PlayerSettings.TextSpeed = newValue;
        }
    }
}