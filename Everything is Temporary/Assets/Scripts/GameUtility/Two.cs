﻿using System;
namespace GameUtility
{
    /// <summary>
    /// A pair of the same type of object.
    /// </summary>
    public class Two<T>
    {
        public Two(T first, T second)
        {
            First = first;
            Second = second;
        }

        public T First { get; set; }
        public T Second { get; set; }
    }
}
