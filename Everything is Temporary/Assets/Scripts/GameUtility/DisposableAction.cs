﻿using System;

namespace GameUtility
{
    /// <summary>
    /// Allows returning an <see cref="IDisposable"/> that runs a simple action
    /// without having to create a new class.
    /// </summary>
    public class DisposableAction : IDisposable
    {
        public DisposableAction(Action onDispose)
        {
            m_onDispose = onDispose;
        }

        public void Dispose()
        {
            if (m_onDispose == null)
                return;

            m_onDispose();
            m_onDispose = null;
        }

        private Action m_onDispose;
    }
}
