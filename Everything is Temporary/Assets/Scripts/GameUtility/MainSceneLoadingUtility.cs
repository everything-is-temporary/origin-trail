﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneLoadingUtility : MonoBehaviour
{

    [SerializeField]
    private static int m_StartSceneBuildIndex = 1;
    
    public void ChangeScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadStartingScene()
    {
        SceneManager.LoadScene(m_StartSceneBuildIndex);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

}
