﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The purpose of this object is to force ScriptableObjects to be initialized
/// when a scene is loaded. See https://forum.unity.com/threads/scriptableobject-behaviour-discussion-how-scriptable-objects-work.541212/
/// </summary>
public class ReferenceHolder : MonoBehaviour
{
    [SerializeField]
    private List<ScriptableObject> m_references;
}
