﻿using System.Collections.Generic;
using GameUtility.ObserverPattern;

namespace Audio
{
    /// <summary>
    /// A virtual audio "scene" containing a set of audio sources and a unique
    /// audio listener.
    /// </summary>
    public class VirtualScene
    {
        public void Add(GrabbableAudioSource audioSource)
        {
            if (m_audioSources.Add(audioSource))
                m_audioListener?.AddSource(audioSource);
        }

        public void Remove(GrabbableAudioSource audioSource)
        {
            if (m_audioSources.Remove(audioSource))
                m_audioListener?.RemoveSource(audioSource);
        }

        /// <summary>
        /// Sets the unique audio listener for the scene. The only way to
        /// reset the listener is to call <see cref="IUnsubscriber.Unsubscribe"/>
        /// on the returned <see cref="IUnsubscriber"/>.
        /// </summary>
        /// <returns>A token that can be used to reset the scene's listener.
        /// Using it will remove all sources in the scene from the listener.</returns>
        /// <param name="listener">The audio listener.</param>
        public IUnsubscriber SetUniqueAudioListener(IAudioListener listener)
        {
            if (m_audioListener != null && m_audioListener != listener)
                throw new System.ApplicationException("Tried to set an" +
                    " audio listener on a virtual scene that already has one.");

            m_audioListener = listener;

            foreach (var source in m_audioSources)
                m_audioListener.AddSource(source);

            return new ActionUnsubscriber(() =>
            {
                foreach (var source in m_audioSources)
                    m_audioListener.RemoveSource(source);

                m_audioListener = null;
            });
        }

        public IEnumerable<GrabbableAudioSource> GetAudioSources()
        {
            return m_audioSources;
        }

        private readonly HashSet<GrabbableAudioSource> m_audioSources = new HashSet<GrabbableAudioSource>();
        private IAudioListener m_audioListener;
    }
}