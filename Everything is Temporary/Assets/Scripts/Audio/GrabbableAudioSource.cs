﻿using UnityEngine;
using GameUtility.ObserverPattern;

namespace Audio
{
    /// <summary>
    /// A 3D point from which sound should emanate and a Unity AudioSource that
    /// actually produces the sound.
    /// </summary>
    /// <remarks>
    /// In Unity, only an AudioSource component can produce sound, and there is
    /// no neat way to copy or move a component to a separate object. Because of
    /// this, the AudioSource component should be placed on an empty object which
    /// will be moved by the virtual audio system (which is needed because in
    /// Unity there can only be one AudioListener, but our game is composed of
    /// at least a game scene and a book scene).
    /// </remarks>
    public class GrabbableAudioSource
    {
        /// <summary>
        /// World-space position of the point from which sound should emanate.
        /// </summary>
        public Transform Position { get; }

        public GrabbableAudioSource(Transform worldPosition, AudioSource audioSource)
        {
            Position = worldPosition;
            m_audioSource = audioSource;
            m_audioSource.enabled = false;

            Debug.Assert(!worldPosition.IsChildOf(m_audioSource.transform));
        }

        /// <summary>
        /// Grabs the AudioSource. Nothing else will be able to grab it until
        /// it is released by calling the Unsubscribe() method on the returned
        /// value.
        /// </summary>
        /// <returns>An object that can be used to release the AudioSource.</returns>
        /// <param name="audioSource">The audio source.</param>
        public IUnsubscriber Grab(out AudioSource audioSource)
        {
            if (m_isGrabbed)
                throw new System.ApplicationException("Tried to grab" +
                	" a GrabbableAudioSource that has already been grabbed.");

            m_audioSource.enabled = true;
            m_isGrabbed = true;
            audioSource = m_audioSource;

            return new ActionUnsubscriber(() =>
            {
                m_isGrabbed = false;
                m_audioSource.enabled = false;
            });
        }

        private bool m_isGrabbed;
        private AudioSource m_audioSource;
    }
}