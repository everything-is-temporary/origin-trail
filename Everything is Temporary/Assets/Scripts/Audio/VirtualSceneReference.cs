﻿using System;
using UnityEngine;
using GameUtility;

namespace Audio
{
    /// <summary>
    /// A serializable reference to a <see cref="VirtualScene"/> that is
    /// resolved through a <see cref="VirtualSceneManager"/>.
    /// </summary>
    [Serializable]
    public class VirtualSceneReference
    {
        public VirtualScene Scene => m_scene.Value;

        public bool IsValid => Scene != null;

        public override int GetHashCode()
        {
#pragma warning disable RECS0025 // Non-readonly field referenced in 'GetHashCode()'
            return (int)m_sceneId;
#pragma warning restore RECS0025 // Non-readonly field referenced in 'GetHashCode()'
        }

        public override bool Equals(object obj)
        {
            if (obj is VirtualSceneReference reference)
                return reference.m_sceneId == m_sceneId && reference.m_sceneManager == m_sceneManager;
            return false;
        }

        private VirtualSceneReference()
        {
            m_scene = new Lazy<VirtualScene>(() => m_sceneManager?.GetScene(m_sceneId));
        }

        private readonly Lazy<VirtualScene> m_scene;

        [SerializeField]
        private VirtualSceneManager m_sceneManager = default;

        [SerializeField, IdField]
        private long m_sceneId = default;
    }
}