﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Audio
{
    /// <summary>
    /// Represents a source of sound inside a <see cref="VirtualScene"/>. This
    /// registers its <see cref="AudioSource"/> to be used in the <see cref="VirtualScene"/>.
    /// </summary>
    public class VirtualAudioSourceScript : VirtualSceneObjectScript
    {
        public AudioSource audioSource;

        private void Start()
        {
            m_grabbableAudioSource = new GrabbableAudioSource(transform, audioSource);
            virtualScene.Scene.Add(m_grabbableAudioSource);
        }

        private void OnDestroy()
        {
            virtualScene.Scene.Remove(m_grabbableAudioSource);
        }

#if UNITY_EDITOR
        private void Reset()
        {
            GenerateAudioSource();
        }

        private void OnValidate()
        {
            if (audioSource == null || transform.IsChildOf(audioSource.transform))
            {
                GenerateAudioSource();
            }
        }

        private void GenerateAudioSource()
        {
            GameObject audioObject = new GameObject("Audio Source");
            GameObjectUtility.SetParentAndAlign(audioObject, gameObject);

            audioSource = audioObject.AddComponent<AudioSource>();

            // Full 3D sound by default.
            audioSource.spatialBlend = 1;

            Undo.RegisterCreatedObjectUndo(audioObject, $"Created AudioSource for {nameof(VirtualAudioSourceScript)}");
        }
#endif

        private GrabbableAudioSource m_grabbableAudioSource;
    }
}