﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;
using GameUtility.ObserverPattern;

namespace Audio
{
    /// <summary>
    /// Represents a pair of ears in a <see cref="VirtualScene"/>. This will mirror
    /// any <see cref="AudioSource"/> in the virtual scene to a correct position
    /// relative to the one true Unity <see cref="AudioListener"/>.
    /// </summary>
    public class VirtualAudioListenerScript : VirtualSceneObjectScript, IAudioListener
    {
        public IEnumerable<GrabbableAudioSource> AudioSources => m_audioSources.Keys;

        public void AddSource(GrabbableAudioSource source)
        {
            IUnsubscriber grabToken = source.Grab(out AudioSource audioSource);

            ObjectMirrorScript mirror = audioSource.gameObject.AddComponent<ObjectMirrorScript>();
            mirror.mirroredReferencePosition = transform;
            mirror.mirroredObjectPosition = source.Position;
            mirror.referencePosition = m_audioListener.Value.transform;

            MirrorAndToken mirrorAndToken = new MirrorAndToken
            {
                mirror = mirror,
                grabToken = grabToken
            };

            m_audioSources.Add(source, mirrorAndToken);
        }

        public void RemoveSource(GrabbableAudioSource source)
        {
            if (m_audioSources.TryGetValue(source, out MirrorAndToken mirrorAndToken))
            {
                Destroy(mirrorAndToken.mirror);
                mirrorAndToken.grabToken.Unsubscribe();
                m_audioSources.Remove(source);
            }
        }

        private void Awake()
        {
            m_sceneUnsubscriber = virtualScene.Scene.SetUniqueAudioListener(this);
        }

        private void OnDestroy()
        {
            m_sceneUnsubscriber.Unsubscribe();
        }

        private IUnsubscriber m_sceneUnsubscriber;

        private readonly Dictionary<GrabbableAudioSource, MirrorAndToken> m_audioSources =
            new Dictionary<GrabbableAudioSource, MirrorAndToken>();

        private struct MirrorAndToken
        {
            public ObjectMirrorScript mirror;
            public IUnsubscriber grabToken;
        }

        private static readonly Lazy<AudioListener> m_audioListener
            = new Lazy<AudioListener>(FindObjectOfType<AudioListener>);
    }
}