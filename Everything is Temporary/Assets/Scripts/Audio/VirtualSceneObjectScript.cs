﻿using System;
using UnityEngine;
namespace Audio
{
    /// <summary>
    /// An object in a <see cref="VirtualScene"/>.
    /// </summary>
    public class VirtualSceneObjectScript : MonoBehaviour
    {
        public VirtualSceneReference virtualScene;
    }
}