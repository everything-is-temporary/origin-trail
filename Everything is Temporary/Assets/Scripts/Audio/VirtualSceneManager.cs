﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;
using GameUtility.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Audio
{
    /// <summary>
    /// Defines a mapping from IDs to <see cref="VirtualScene"/>s so that Unity
    /// objects from different Unity scenes can reference the same virtual scenes.
    /// </summary>
    public class VirtualSceneManager : ScriptableObject
    {
        public VirtualScene GetScene(long id)
        {
            if (!m_scenes.TryGetValue(id, out VirtualScene scene))
                m_scenes[id] = scene = new VirtualScene();

            return scene;
        }

        public string GetSceneName(long id)
        {
            if (m_sceneNames.TryGetValue(id, out string name))
                return name;

            return null;
        }

        public long GenerateUniqueId()
        {
            return GenerateUniqueId(new System.Random());
        }

        public long GenerateUniqueId(System.Random rand)
        {
            long id;

            do
            {
                id = rand.NextLong();
            } while (id == 0 || m_scenes.ContainsKey(id));

            return id;
        }

#if UNITY_EDITOR
        public IEnumerable<long> ScenesWithNames => m_sceneNames.Keys;
#endif

        private VirtualSceneManager()
        {
            m_sceneNames = new SceneNameDict
            {
                CreateNewElement = () => new KeyValuePair<long, string>(GenerateUniqueId(), "Virtual Scene")
            };
        }

        [SerializeField, SerializableDictionary(keyIsLongId: true)]
        private SceneNameDict m_sceneNames = default;

        private readonly Dictionary<long, VirtualScene> m_scenes = new Dictionary<long, VirtualScene>();

        [Serializable]
        private class SceneNameDict : SerializableDictionary<long, string> { }
    }
}