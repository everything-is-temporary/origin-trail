﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameUtility.ObserverPattern;

public class AudioSourceSlotAcquirer : MonoBehaviour
{
    [SerializeField]
    private AudioSource m_audioSource = default;

    [SerializeField]
    private AudioSourceSlot m_audioSourceSlot = default;

    private void Awake()
    {
        m_audioSourceSlotUnsubscriber = m_audioSourceSlot.SetAudioSource(m_audioSource);
    }

    private void OnDestroy()
    {
        m_audioSourceSlotUnsubscriber?.Unsubscribe();
    }

    private IUnsubscriber m_audioSourceSlotUnsubscriber;
}
