﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Audio Clip Slot", menuName = "Audio Clip Slot", order = 8)]
public class AudioClipSlot : ScriptableObject
{
    public IReadOnlyList<AudioClip> AudioClips => m_audioClips;

    [SerializeField]
    [Tooltip("A list of audio clips, one of which will be picked randomly when played.")]
    private List<AudioClip> m_audioClips = default;
}
