﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameUtility.ObserverPattern;

[CreateAssetMenu(fileName = "Audio Source Slot", menuName = "Audio Source Slot", order = 7)]
public class AudioSourceSlot : ScriptableObject
{
    public UnityEngine.AudioSource AudioSource { get; private set; }

    /// <summary>
    /// Set the AudioSource in the slot. This can only happen if AudioSource is null. This
    /// returns a token whose Unsubscribe() method can be called to release
    /// the slot.
    /// </summary>
    /// <returns>An IUnsubscriber that can be used to release the slot. If the slot
    /// is not released, it can never be used by anything else again.</returns>
    /// <param name="view">View.</param>
    public IUnsubscriber SetAudioSource(UnityEngine.AudioSource AudioSourceToSet)
    {
        if (AudioSource != null)
        {
            Debug.LogError("Trying to call SetAudioSource() on an occupied slot. The previous" +
                    " occupant must first release the slot by calling Unsubscribe() on the" +
                    " given return value.");

            return null;
        }

        AudioSource = AudioSourceToSet;

        return new AudioSourceSlotResetter(this);
    }

    /// <summary>
    /// Only AudioSourceSlotResetter can call this function.
    /// Therefore, only the owner of this AudioSourceSlot can release the slot.
    /// </summary>
    private void Release()
    {
        AudioSource = null;
    }

    public void PlaySound(AudioClip audioClip)
    {
        if (audioClip != null)
        {
            AudioSource.PlayOneShot(audioClip);
        }
        else
        {
            Debug.LogWarning("AudioSourceSlot.PlaySound receives a null for its AudioClip parameter!");
        }
    }

    /// <summary>
    /// This class provides a way for the owner to release the slot.
    /// </summary>
    private class AudioSourceSlotResetter : IUnsubscriber
    {
        public AudioSourceSlotResetter(AudioSourceSlot slot)
        {
            m_slotRef = new WeakReference<AudioSourceSlot>(slot);
            m_alreadyReset = false;
        }

        public void Unsubscribe()
        {
            if (m_alreadyReset) { return; }

            if (m_slotRef.TryGetTarget(out AudioSourceSlot slot))
            {
                slot.Release();
                m_alreadyReset = true;
            }
        }

        private WeakReference<AudioSourceSlot> m_slotRef;

        // This prevents this IUnsubscriber from resetting again, when the slot is owned by something else!
        private bool m_alreadyReset;
    }
}
