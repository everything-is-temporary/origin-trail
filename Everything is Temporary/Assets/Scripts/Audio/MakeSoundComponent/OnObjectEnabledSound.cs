﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnObjectEnabledSound : MakeSound
{
    private void OnEnable()
    {
        if (m_audioSourceSlot.AudioSource != null)
        {
            PlaySound();
        }
        else
        // This implies that this object is active when the scene starts.
        {
            m_isActiveFromStart = true;
        }
    }

    // Because OnEnable is called before AudioSourceSlotAcquirer's Awake,
    // We need to delay making the sound here in Start.
    private void Start()
    {
        if (m_isActiveFromStart)
        {
            PlaySound();
        }
    }

    protected bool m_isActiveFromStart;
}
