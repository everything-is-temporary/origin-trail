﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeSound : MonoBehaviour
{
    /// <summary>
    /// Play a random sound from the audio clips
    /// Found within this component.
    /// </summary>
    public void PlaySound()
    {
        AudioClip audioClip;

        if (m_audioClipsOverride.Length > 0)
        {
            audioClip = m_audioClipsOverride[Random.Range(0, m_audioClipsOverride.Length)];
        }
        else if (m_defaultAudioClipsSlot != null && m_defaultAudioClipsSlot.AudioClips.Count > 0)
        {
            audioClip = m_defaultAudioClipsSlot.AudioClips[Random.Range(0, m_defaultAudioClipsSlot.AudioClips.Count)];
        }
        else
        {
            Debug.LogError(gameObject.name + "'s MakeSound component doesn't have any audio clips assigned.");

            return;
        }

        PlayAudioClip(audioClip);
    }

    /// <summary>
    /// Play a sound provided externally.
    /// </summary>
    /// <param name="audioClip"></param>
    public void PlayGivenSound(AudioClip audioClip)
    {
        PlayAudioClip(audioClip);
    }

    protected void PlayAudioClip(AudioClip audioClip)
    {
        if (m_audioSourceSlot == null)
        {
            Debug.LogError(gameObject.name + "'s MakeSound component's Audio Source Slot is null!");

            return;
        }

        m_audioSourceSlot.PlaySound(audioClip);
    }

    [SerializeField]
    [Tooltip("The ScriptableObject that stores the default AudioSource for UI sounds in MainGameScene.")]
    protected AudioSourceSlot m_audioSourceSlot;

    [SerializeField]
    protected AudioClipSlot m_defaultAudioClipsSlot;

    [SerializeField]
    [Tooltip("This overrides the default audio clips slot.")]
    protected AudioClip[] m_audioClipsOverride;
}
