﻿using System;
using System.Collections;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Audio
{
    /// <summary>
    /// A zone with sound such that the sound is only played if the camera
    /// is within the zone's bounds.
    /// </summary>
    public class AudioZoneScript : MonoBehaviour
    {
        public AudioSource audioSource;
        public Transform cameraTransform;

        [Tooltip("Time in seconds to go from 0 volume to 1")]
        public float volumeChangeDuration;

        [Tooltip("Audio starts when the camera enters this zone.")]
        public Rect audioZone;

        [Tooltip("Audio ends when the camera goes out of these bounds. " +
        	"Should be bigger than Audio Zone.")]
        public Rect audioBounds;

        private void Start()
        {
            audioSource.enabled = false;
            audioSource.volume = 0;
        }

        private void Update()
        {
            if (m_wasVolumeOn)
            {
                if (!audioBounds.Contains(cameraTransform.position - transform.position))
                {
                    m_wasVolumeOn = false;
                    BeginAdjustVolume(0, true);
                }
            }
            else
            {
                if (audioZone.Contains(cameraTransform.position - transform.position))
                {
                    m_wasVolumeOn = true;
                    audioSource.enabled = true;
                    BeginAdjustVolume(1);
                }
            }
        }

        private void BeginAdjustVolume(float target, bool disableAtEnd = false)
        {
            if (m_audioVolumeCoroutine != null)
                StopCoroutine(m_audioVolumeCoroutine);

            m_audioVolumeCoroutine = StartCoroutine(VolumeCoroutine(target, disableAtEnd));
        }

        private IEnumerator VolumeCoroutine(float target, bool disableAtEnd = false)
        {
            float start = audioSource.volume;

            // Scaled so that volume always adjusts at the same speed.
            float duration = Mathf.Abs(target - start) * volumeChangeDuration;
            float startTime = Time.time;

            while (Time.time < startTime + duration)
            {
                float t = (Time.time - startTime) / duration;

                audioSource.volume = Mathf.SmoothStep(start, target, t);

                yield return null;
            }

            audioSource.volume = target;

            if (disableAtEnd)
                audioSource.enabled = false;

            m_audioVolumeCoroutine = null;
        }

#if UNITY_EDITOR
        private void Reset()
        {
            GenerateAudioSource();

            audioZone = new Rect
            {
                size = Vector2.one,
                center = Vector2.zero
            };

            audioBounds = new Rect
            {
                size = Vector2.one * 2,
                center = Vector2.zero
            };

            cameraTransform = GameObject.FindWithTag("MainCamera")?.transform;
            volumeChangeDuration = 1;
        }

        private void OnValidate()
        {
            if (audioSource == null || transform.IsChildOf(audioSource.transform))
            {
                GenerateAudioSource();
            }
        }

        private void GenerateAudioSource()
        {
            GameObject audioObject = new GameObject("Audio Source");
            GameObjectUtility.SetParentAndAlign(audioObject, gameObject);

            audioSource = audioObject.AddComponent<AudioSource>();
            audioSource.enabled = false;

            Undo.RegisterCreatedObjectUndo(audioObject, $"Created AudioSource for {nameof(AudioZoneScript)}");
        }
#endif

        private Coroutine m_audioVolumeCoroutine;
        private bool m_wasVolumeOn;


        private static readonly Lazy<AudioListener> m_audioListener
            = new Lazy<AudioListener>(FindObjectOfType<AudioListener>);
    }
}