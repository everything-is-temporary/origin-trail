﻿using System.Collections.Generic;

namespace Audio
{
    public interface IAudioListener
    {
        IEnumerable<GrabbableAudioSource> AudioSources { get; }

        void AddSource(GrabbableAudioSource source);
        void RemoveSource(GrabbableAudioSource source);
    }
}