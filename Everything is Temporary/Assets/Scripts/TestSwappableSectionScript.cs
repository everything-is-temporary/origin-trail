﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameUI;
using GameUI.Abstractions;
using GameUtility;

// FIXME: Is this code used? Should we remove it?
public class TestSwappableSectionScript : MonoBehaviour
{
    [SerializeField]
    private SwappablePageSection m_section = default;

    [SerializeField]
    [RequiresType(typeof(IInteractiveView))]
    private Object m_page1 = default;

    [SerializeField]
    [RequiresType(typeof(IInteractiveView))]
    private Object m_page2 = default;

    // Start is called before the first frame update
    private void Start()
    {
        SetPages();

        BookScript.Instance.InsertSection(m_section, -1);
    }

    [ContextMenu("Swap Left Pages")]
    private void Swap()
    {
        Object temp = m_page1;
        m_page1 = m_page2;
        m_page2 = temp;

        SetPages();
    }

    private void SetPages()
    {
        m_section.SetLeftPage(m_page1 as IInteractiveView);
        m_section.SetRightPage(m_page2 as IInteractiveView);
    }
}
