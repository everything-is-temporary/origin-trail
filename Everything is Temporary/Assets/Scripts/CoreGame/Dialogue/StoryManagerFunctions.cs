﻿using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using GameUI;
using Ink.Runtime;
using GameInventory;
using System;
using System.Threading.Tasks;
using CoreGame.Progression;
using UnityEngine;

namespace Dialogue
{
    public partial class StoryManager
    {
        #region Bound Functions

        public void BindDefaults(Story inkStory)
        {
            inkStory.BindExternalFunction(nameof(CountInventoryItem),
                (string resourceId) => CountInventoryItem(resourceId));
            inkStory.BindExternalFunction(nameof(HasInventoryItem),
                (string resourceId, int atLeast) => HasInventoryItem(resourceId, atLeast));
            inkStory.BindExternalFunction(nameof(CountInventoryByTag),
                (string itemTag) => CountInventoryByTag(itemTag));
            inkStory.BindExternalFunction(nameof(CountInventoryConvoy),
                (string resourceId) => CountInventoryConvoy(resourceId));
            inkStory.BindExternalFunction(nameof(CountInventoryConvoyByTag),
                (string itemTag) => CountInventoryConvoyByTag(itemTag));
            inkStory.BindExternalFunction(nameof(GetTimeDaysRemaining), () => GetTimeDaysRemaining());
            inkStory.BindExternalFunction(nameof(GetTimeFractionOfDay), () => GetTimeFractionOfDay());
            inkStory.BindExternalFunction(nameof(GetTimeFractionOfGame), () => GetTimeFractionOfGame());
        }

        //BELOW ARE FUNCTIONS THAT JUSE USE THE PLAYER'S INVENTORY
        //They are here so that the story doesn't break with the character inventory additions 
        public int CountInventoryItem(string resourceID)
        {
            return InventoryManager.Instance.CountItem(
                ItemReference.FromId(GameUtility.IdConverter.FromReadableString(resourceID)));
        }

        public bool HasInventoryItem(string resourceID, int atLeast)
        {
            return InventoryManager.Instance.CountItem(ItemReference.FromId(GameUtility.IdConverter.FromReadableString(resourceID))) >= atLeast;
        }

        public int CountInventoryByTag(string itemtag)
        {
            //TODO: If ItemTag changes from string, this function will need to be modified.
            return InventoryManager.Instance.CountItem(new ItemTag(itemtag));
        }
        
        #region New Inventory Readonly Access
        
        public int CountInventoryConvoy(string itemID)
        {
            return InventoryManager.Instance.CountItemConvoyOnly(ItemReference.FromId(GameUtility.IdConverter.FromReadableString(itemID)));
        }

        public int CountInventoryConvoyByTag(string itemTag)
        {
            return InventoryManager.Instance.CountItemConvoyOnly(new ItemTag(itemTag));
        }        
        
        #endregion //New Inventory Readonly Access

        public int GetTimeDaysRemaining()
        {
            return ProgressionManager.Instance.GameDaysRemaining;
        }

        public float GetTimeFractionOfDay()
        {
            return ProgressionManager.Instance.CurrentDayCompletionFraction;
        }
        
        public double GetTimeFractionOfGame()
        {
            return ProgressionManager.Instance.GameProgressionCompletionFraction;
        }

        /* This might not be necessary, check implementation
       /// <summary>
       /// Determines if a specified invokeable menu exists.
       /// </summary>
       /// <returns><c>true</c>, if menu exists and can be invoked, <c>false</c> otherwise.</returns>
       /// <param name="menuName">Menu name.</param>
       /// <param name="menuIdentifier">Identifier by which to retrieve a particular variation of the specified menu. 
       /// For menuName "shop", the identifier might retireve the inventory of an earlygame shop.</param>        
       public bool MenuAvaliable(string menuName, string menuIdentifier)
       {
           //TODO: Implement
           return false;
       }
       */

        #endregion

        #region Parsed Functions

        //-----This just modifies the player's inventory, it's so that the current story doesn't break
        
        public Task ModifyItemQuantity(string itemID, string val)
        {
            
            long itemRefID = GameUtility.IdConverter.FromReadableString(itemID);

            int change = int.Parse(val);

            ItemReference itemRef = ItemReference.FromId(itemRefID);

            InventoryManager.Instance.ModifyInventoryItem(new ItemWithAmount(itemRef, change));

            return Task.CompletedTask;
        }
        //---------------------------------------------------------------------------------

        
        // FIXME: Are the top 2 of these parsed functions? They return values which the story would never be able to use if they are.
        //-----Here the characterinventory stuff begins---------------------------------------
        public int CountCharacterInventoryItem(string charID, string resourceID)
        {
            long itemRefID = GameUtility.IdConverter.FromReadableString(resourceID);
            long charRefID = GameUtility.IdConverter.FromReadableString(charID);

            ItemReference itemRef = ItemReference.FromId(itemRefID);
            Characters.CharacterReference charRef = Characters.CharacterReference.FromId(charRefID);

            return charRef.Character.Inventory.CountItem(itemRef);
        }

        public bool HasCharacterInventoryItem(string charID, string resourceID, string atLeast)
        {
            long itemRefID = GameUtility.IdConverter.FromReadableString(resourceID);
            long charRefID = GameUtility.IdConverter.FromReadableString(charID);

            ItemReference itemRef = ItemReference.FromId(itemRefID);
            Characters.CharacterReference charRef = Characters.CharacterReference.FromId(charRefID);

            return charRef.Character.Inventory.ContainsItem(itemRef);
        }

        public Task ModifyCharacterItemQuantity(string charID, string itemID, string val)
        {
            long itemRefID = GameUtility.IdConverter.FromReadableString(itemID);
            long charRefID = GameUtility.IdConverter.FromReadableString(charID);

            int change = int.Parse(val);

            ItemReference itemRef = ItemReference.FromId(itemRefID);
            Characters.CharacterReference charRef = Characters.CharacterReference.FromId(charRefID);

            charRef.Character.Inventory.ModifyItem(new ItemWithAmount(itemRef, change));
            
            return Task.CompletedTask;
        }
        
        //-----------------------------------------------------------------------------

        public Task LockCurrentStory(string methodAndArgs)
        {
            LockStory(GetCurrentStoryPath(), methodAndArgs);
            return Task.CompletedTask;
        }

        public Task LockStory(string storyKnot, string methodAndArgs)
        {
            Dictionary<MethodInfo, List<string>> checkCommands = StoryParser.GetCommands(methodAndArgs);
            foreach (MethodInfo methodInfo in checkCommands.Keys)
            {
                StoryManager.Instance.AddStoryLock(storyKnot,
                    (System.Func<bool>) methodInfo.Invoke(StoryManager.Instance, checkCommands[methodInfo].ToArray()));
            }
            
            return Task.CompletedTask;
        }

//        public Task InvokeMenu(string menuName, string menuId)
//        {
//            // TODO: Implement, when we have a MenuManager of some kind
//            return Task.CompletedTask;
//        }

        public async Task LoadScene(string sideScrollerSceneToLoad)
        {
            this.m_CurrentStory.ResetCallstack();
            await CoreGameManager.Instance.OpenGameSceneAsync(sideScrollerSceneToLoad);
        }

        public async Task OpenTrade(string shopID)
        {
            Shop shop = ShopDefinitionList.Instance.GetShop(GameUtility.IdConverter.FromReadableString(shopID));
            if (shop == null)
            {
                UnityEngine.Debug.LogError($"No shop exists with ID: {shopID}");
                return;
            }

            await TradeManager.PerformTradeWithShopAsync(InventoryManager.Instance.ConvoyAndPartyInventory, shop);
        }

        public Task AddCharacterToParty(string charID)
        {
            long charRefID = GameUtility.IdConverter.FromReadableString(charID);

            Characters.CharacterReference charRef = Characters.CharacterReference.FromId(charRefID);

            //TODO: couple these all together so that ActorManager & InventoryManager will listen to an event from CharacterManager
            //add character to general party
            Characters.CharacterManager.Instance.AddNewPartyMember(charRef);

            //now add it to party in current scene
            Characters.ActorManager.Instance.AddNewPartyMember(charRef);

            //create new inventory for character
            InventoryManager.Instance.AddCharacterToParty(charRef);
            return Task.CompletedTask;
        }

        public Task EnableWall(string commaSeparatedListOfWalls)
        {
            foreach (string wallId in commaSeparatedListOfWalls.Split(','))
            {
                string trimmedString = wallId.Trim();
                if (int.TryParse(trimmedString, out int index))
                {
                    EnableWallByIndex(index);
                }
                else
                {
                    EnableWallByName(trimmedString);
                }
            }
            return Task.CompletedTask;
        }

        public Task DisableWall(string commaSeparatedListOfWalls)
        {
            foreach (string wallId in commaSeparatedListOfWalls.Split(','))
            {
                string trimmedString = wallId.Trim();
                if (int.TryParse(trimmedString, out int index))
                {
                    DisableWallByIndex(index);
                }
                else
                {
                    DisableWallByName(trimmedString);
                }
            }
            return Task.CompletedTask;
        }

        public Task DisableAllWalls()
        {
            foreach (var wall in invisibleWalls)
            {
                wall.Disable();
            }
            return Task.CompletedTask;
        }

        public Task ModifyDays(string numberOfDaysToAdd)
        {
            if(int.TryParse(numberOfDaysToAdd, out int days))
            {
//                Debug.Log($"Increasing Days by {days}");
                ProgressionManager.Instance.ModifyDays(days);
            }
            return Task.CompletedTask;
        }
        
        public Task ModifyCurrentDay(string fractionOfDayToAdd)
        {
            if(float.TryParse(fractionOfDayToAdd, out float fractionOfDay))
            {
//                Debug.Log($"Increasing Current Day by {fractionOfDay}");
                ProgressionManager.Instance.ModifyCurrentDay(fractionOfDay);
            }
            return Task.CompletedTask;
        }

        public Task RemoveAllItemsByTag(string tagToRemove)
        {
            ItemTag itemTagWrapper = new ItemTag(tagToRemove);
            InventoryManager.Instance.RemoveInventoryItemsByTagGetDelta(itemTagWrapper);
            return Task.CompletedTask;
        }
        
        public Task RemoveSomeItemsByTag(string tagToRemove, string quantityToRemove)
        {
            if (int.TryParse(quantityToRemove, out int quantity))
            {
                InventoryManager.Instance.RemoveInventoryItemsByTagGetDelta(new ItemTag(tagToRemove), quantity);
            }
            return Task.CompletedTask;
        }

        public Task RemoveItemByTag(string itemToRemoveReference, string quantityToRemove)
        {
            if (int.TryParse(quantityToRemove, out int quantity))
            {
                InventoryManager.Instance.RemoveInventoryItem(new ItemWithAmount(
                    ItemReference.FromId(GameUtility.IdConverter.FromReadableString(itemToRemoveReference)), quantity));
            }
            return Task.CompletedTask;
        }

        public async Task GameCommand(string commandName)
        {
            // Block story while invoking command.
            using (var token = m_blockingElementCount.Increase())
                await InvokeGameCommand(commandName);
        }

    #endregion
    }
}
