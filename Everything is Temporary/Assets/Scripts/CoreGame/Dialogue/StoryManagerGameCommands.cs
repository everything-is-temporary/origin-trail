﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using GameUtility;
using UnityEngine;

namespace Dialogue
{
    public partial class StoryManager
    {
        /// <summary>
        /// Adds a handler for the game command. The handler should return a
        /// Task that completes when the handler finishes processing the command
        /// (e.g. when the corresponding cutscene finishes).
        /// </summary>
        /// <returns><c>true</c>, if the handler was added, <c>false</c> if it was already registered.</returns>
        /// <param name="gameCommand">The game command for which to register the handler.</param>
        /// <param name="handler">A handler that returns a Task that completes when
        /// the handler is done processing the command. This is used to block the
        /// story from continuing until all game commands are handled. If the handler
        /// should do something without blocking the story, just return Task.CompletedTask.</param>
        public bool AddGameCommandHandler(string gameCommand, Func<Task> handler)
        {
            if (!m_gameCommandHandlers.TryGetValue(gameCommand, out var handlerSet))
                m_gameCommandHandlers[gameCommand] = handlerSet = new HashSet<Func<Task>>();

            return handlerSet.Add(handler);
        }

        /// <summary>
        /// Unsubscribes the handler from the game command.
        /// </summary>
        /// <returns><c>true</c>, if the handler was removed, <c>false</c> if it wasn't subscribed.</returns>
        public bool RemoveGameCommandHandler(string gameCommand, Func<Task> handler)
        {
            if (m_gameCommandHandlers.TryGetValue(gameCommand, out var handlerSet))
                return handlerSet.Remove(handler);

            return false;
        }

        /// <summary>
        /// Runs all handlers for the game command without blocking the story.
        /// </summary>
        /// <param name="gameCommand">Game command.</param>
        private async Task InvokeGameCommand(string gameCommand)
        {
            if (m_gameCommandHandlers.TryGetValue(gameCommand, out var handlers))
            {
                // Make a copy because handlers can call RemoveHandler() when we
                // invoke them, but modifying a collection while iterating through
                // it with foreach is not allowed.
                var handlersCopy = new List<Func<Task>>(handlers);

                await Task.WhenAll(handlersCopy.Select(handler => handler()));
            }
            else
            {
                Debug.LogWarning($"No handlers for command '{gameCommand}'.");
            }
        }

        private readonly Dictionary<string, HashSet<Func<Task>>> m_gameCommandHandlers
            = new Dictionary<string, HashSet<Func<Task>>>();
    }
}
