﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace Dialogue
{
    /// <summary>
    /// The base class for all StoryElements such as <see cref="DialogueElement"/> or <see cref="ChoicesElement"/>.
    /// </summary>
    public abstract class StoryElement
    {

        public static Action<StoryElement> OnStoryElementHandled;

        /// <summary>
        /// Marks the element as "autoadvanceable" in case it needs to be preformed immediately after the previous element in sequence.
        /// </summary>
        public abstract bool AutoAdvance { get; }

        /// <summary>
        /// Passes this element to an appropriate singleton Manager class to affect the game state.
        /// </summary>
        public async Task Handle()
        {
            await HandleIndividual();
            OnStoryElementHandled?.Invoke(this);
        }

        protected abstract Task HandleIndividual();

        /// <summary>
        /// Function to indicate whether or not the <see cref=" StoryElement"/> contains story content.
        /// </summary>
        /// <returns><c>true</c>, the element has content to display or perform, <c>false</c> otherwise.</returns>
        public abstract bool ContainsContent();
    }

}
