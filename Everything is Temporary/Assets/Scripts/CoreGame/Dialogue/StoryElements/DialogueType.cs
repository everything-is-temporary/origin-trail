﻿using System;
namespace Dialogue
{
    public enum DialogueType
    {
        Character, Narrator, HistoryRecord
    }
}
