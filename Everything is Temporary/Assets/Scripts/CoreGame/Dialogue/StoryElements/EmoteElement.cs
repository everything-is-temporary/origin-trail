﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Characters;
using Characters.Emotes;
using CoreGame.Characters.Emotes.Visualization;
using GameUtility;
using UnityEngine;

namespace Dialogue
{
    public class EmoteElement : StoryElement
    {
        public override bool AutoAdvance => true;

        public EmoteElement(EmoteData emoteData, IEnumerable<CharacterReference> characterReferences)
        {
            m_emoteData = emoteData;
            m_characterReferences = characterReferences.ToList();
        }
        
        protected override Task HandleIndividual()
        {
            foreach (CharacterReference characterReference in m_characterReferences)
            {
                HandleEmoteForCharacterReference(characterReference);
            }

            return Task.CompletedTask; // runs synchronously
        }

        private void HandleEmoteForCharacterReference(CharacterReference characterReference)
        {
            if (characterReference.CharacterOrNull == null || m_emoteData.EmoteReference.Emote == null)
            {
                Debug.LogWarning("Either character or emote is null, unable to handle emote.");
                return;
            }
            if (m_emoteData.EmoteReference.Emote.EmoteVisualizationType.ContainsAny(EmoteVisualizationType.BubbleAndMovement))
            {
                ActorManager.Instance.ApplyEmote(characterReference, m_emoteData);
            }
        }

        public override bool ContainsContent()
        {
            return m_emoteData != null && m_characterReferences.Count > 0;
        }

        private EmoteData m_emoteData;
        private List<CharacterReference> m_characterReferences;
    }
}
