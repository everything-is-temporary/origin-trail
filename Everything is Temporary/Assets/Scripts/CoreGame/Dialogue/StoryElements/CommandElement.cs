﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dialogue
{
    public class CommandElement : StoryElement
    {
        public override bool AutoAdvance => true;

        /// <summary>
        /// The dictionary containing stored functions and their string arguments. This is not an ordered dictionary so order of input arguments may not be preserved.
        /// .NET does not have a generic version of an ordered dictionary, so will not implement one here. If command order needs to be preserved, simply call commands in sequential lines.
        /// </summary>
        private Dictionary<MethodInfo, List<string>> m_MethodArgumentDictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Dialogue.CommandElement"/> class.
        /// </summary>
        /// <param name="methodArgumentDictionary">Dictionary, keyed by MethodInfo with values equal to a list of the strings the method is to be called with.</param>
        public CommandElement(Dictionary<MethodInfo, List<string>> methodArgumentDictionary)
        {
            m_MethodArgumentDictionary = methodArgumentDictionary;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Dialogue.CommandElement"/> class. Alternate constructor from a single MethodInfo and IList argument.
        /// </summary>
        /// <param name="methodInfo">Function info.</param>
        /// <param name="arguments">Iterable string arguments list.</param>
        public CommandElement(MethodInfo methodInfo, IList<string> arguments)
        {
            m_MethodArgumentDictionary.Add(methodInfo, new List<string>(arguments));
        }

        protected override Task HandleIndividual()
        {
            // !Important: Uncomment the below line to see function and arguments logged in console
//            UnityEngine.Debug.Log(this);
            return Task.WhenAll(InvokeAll());
        }

        /// <summary>
        /// Function to indicate whether or not the <see cref=" CommandElement"/> contains story content.
        /// </summary>
        /// <returns><c>true</c>, if there is at least one command which can be performed, <c>false</c> otherwise.</returns>
        public override bool ContainsContent()
        {
            return m_MethodArgumentDictionary.Count > 0;
        }

        /// <summary>
        /// Calls all stored functions in this command element (in arbitrary order).
        /// </summary>
        /// <returns>A list of return values from each function. Ideally, there should not be any need to return anything since the actual story parser won't be able to relay this info back to the story.</returns>
        public List<Task> InvokeAll()
        {
            List<Task> retList = new List<Task>();
            foreach(MethodInfo methodInfo in m_MethodArgumentDictionary.Keys)
            {
                retList.Add((Task)methodInfo.Invoke(StoryManager.Instance, m_MethodArgumentDictionary[methodInfo].ToArray()));
            }
            return retList;
        }

        /// <summary>
        /// Invokes a method by its string name. This is here for convenience or testing purposes. <see cref="InvokeAll"/> should be called normally.
        /// </summary>
        /// <returns>The by string.</returns>
        /// <param name="methodName">Method name.</param>
        public object InvokeByString(string methodName)
        {
            foreach(MethodInfo methodInfo in m_MethodArgumentDictionary.Keys)
            {
                if(methodInfo.Name.Equals(methodName))
                {
                    return methodInfo.Invoke(StoryManager.Instance, m_MethodArgumentDictionary[methodInfo].ToArray());
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Dialogue.CommandElement"/>.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Dialogue.CommandElement"/>.</returns>
        public override string ToString(){
            List<string> formattedCommands = new List<string>();
            foreach(MethodInfo methodInfo in m_MethodArgumentDictionary.Keys)
            {
                formattedCommands.Add(string.Format("{0}, Args: {1}", methodInfo.Name, string.Join(", ", m_MethodArgumentDictionary[methodInfo].ToArray())));
            }
            return string.Join("\n", formattedCommands);
        }
    }
}
