﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Dialogue
{
    public class DialogueElement : StoryElement
    {
        public override bool AutoAdvance => DialogueType == DialogueType.HistoryRecord; // Autoadvance only if the element is a history record

        /// <summary>
        /// Occurs when a dialogue element's <see cref="Handle"/> is invoked.
        /// </summary>
        public static event DialogueElementHandler OnDialogueElement;
        public delegate void DialogueElementHandler(DialogueElement element);

        public static event HistoryRecordElementHandler OnHistoryRecord;
        public delegate void HistoryRecordElementHandler(DialogueElement element);

        public DialogueType DialogueType { get; private set; }
        public string Text { get; private set; }

        public List<string> Sources { get; private set; }
        public List<string> Aliases { get; private set; }

        public DialogueElement(IList<string> sources, IList<string> aliases, string text, DialogueType dialogueType=DialogueType.Character)
        {
            DialogueType = dialogueType;
            Text = text;
            Sources = new List<string>(sources);
            Aliases = aliases != null ? new List<string>(aliases) : new List<string>();
        }

        protected override Task HandleIndividual()
        {
            if (DialogueType == DialogueType.HistoryRecord)
            {
                OnHistoryRecord?.Invoke(this);
                //TODO: Add implementation handling this element type.
                Debug.Log($"HISTORY_RECORD: {this}");
                return Task.CompletedTask; // runs synchronously
            }
            OnDialogueElement?.Invoke(this);
            return Task.CompletedTask; // runs synchronously
        }

        /// <summary>
        /// Function to indicate whether or not the <see cref=" DialogueElement"/> contains story content.
        /// </summary>
        /// <returns><c>true</c>, if the story line has at least one visible character, <c>false</c> otherwise.</returns>
        public override bool ContainsContent()
        {
            return !string.IsNullOrWhiteSpace(Text);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Dialogue.DialogueElement"/>. For use in debugging contexts only.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Dialogue.DialogueElement"/>.</returns>
        public override string ToString()
        {
            if (Sources.Count > 0)
            {
                if (Aliases.Count > 0){
                    return string.Format("{0}@{1}: {2}", string.Join("&", Sources.ToArray()), string.Join("&", Aliases.ToArray()), Text);
                }
                return string.Format("{0}: {1}", string.Join("&", Sources.ToArray()), Text);
            }
            return Text;
        }
    }
}
