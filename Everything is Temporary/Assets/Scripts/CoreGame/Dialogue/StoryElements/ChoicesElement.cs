﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dialogue
{
    public class ChoicesElement : StoryElement
    {
        public override bool AutoAdvance => false;

        /// <summary>
        /// Invoked in a ChoicesElement's <see cref="Handle"/> method.
        /// </summary>
        public static event ChoicesElementHandler OnChoicesElement;
        public delegate void ChoicesElementHandler(ChoicesElement element);

        public List<KeyValuePair<int, DialogueElement>> Choices { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Dialogue.ChoicesElement"/> class.
        /// </summary>
        /// <param name="choices">Iterable <see cref="IList{T}"/> of <see cref="KeyValuePair{TKey, TValue}"/> where the key is the iteger choice index and the value is the associated <see cref="DialogueElement"/>.</param>
        public ChoicesElement(IList<KeyValuePair<int, DialogueElement>> choices)
        {
            Choices = new List<KeyValuePair<int, DialogueElement>>(choices);
        }

        protected override Task HandleIndividual()
        {
            //TODO: Handle this by passing choices to the UI Manager to display.
            OnChoicesElement?.Invoke(this);
            return Task.CompletedTask; // runs synchronously
        }

        /// <summary>
        /// Function to indicate whether or not the <see cref=" ChoicesElement"/> contains story content.
        /// </summary>
        /// <returns><c>true</c>, if at least one of the internal <see cref="DialogueElement"/>s contains content, <c>false</c> otherwise.</returns>
        public override bool ContainsContent()
        {
            return Choices.Any(choice => choice.Value.ContainsContent());
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:Dialogue.ChoicesElement"/>. For use in debugging contexts only.
        /// </summary>
        /// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:Dialogue.ChoicesElement"/>.</returns>
        public override string ToString()
        {
            return string.Join("\n", Choices.Select((choice) => string.Format("{0}) {1}", choice.Key, choice.Value.ToString())));
        }
    }
}
