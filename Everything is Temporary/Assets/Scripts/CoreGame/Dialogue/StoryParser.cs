﻿using System.Text.RegularExpressions;
using System.Collections.Generic;
using Ink.Runtime;
using System.Linq;
using System.Reflection;
using System;
using Characters;
using Characters.Emotes;
using UnityEngine;

namespace Dialogue
{
    public class StoryParser
    {
    
        public static readonly string narratorNameString = "NAR";
        public static readonly string historyRecordNameString = "REC";
        public static readonly string commandIndicator = ">>>";
        public static readonly string emoteIndicator = ";)";

        // Splits a dialogue line Charlie@Farmer: "Who stole my rake?" into groups of Author (Charlie), Alias (Farmer), and Content ("Who stole my rake?")
        public static readonly string dialogueLineAuthorAliasTextSplitRegexString = @"^\s*(?:([^\:\@\n]+)(?:\@((?:[^\n\:]+\&?)+))?\:)?\s*(.*)\s*$";
        // Splits a command line >>> StealRake("Charlie", "1") into the function name (StealRake) and its arguments ("Charlie", "1")
        public static readonly string commandLineFunctionNameArgumentRegexString = @"(\w+)\(((?:\s*(?:[""|'].+?[""|']|[^\s|\)]+)\s*\,?)*)\)";
        // Splits the arguments of a command line "Charlie", "1" into component strings ["Charlie", "1"]
        public static readonly string commandLineArgumentSplitRegexString = @"\s*(?:[""|'](.+?)[""|']|([^\s|\,]+))\s*"; // Should not be combined into the above string due to capture groups
        // Splits the arguments of an emote line ;) angry bel nextline into component strings ["angry", "bel", "nextline"]
        public static readonly string emoteLineArgumentSplitRegexString = @"([A-z]+)(?:\s+\((.+)\)|\s+([A-z]+))(?:\s+([\d|.]+)sec|\s+([A-z]+))?(?:\s+([\d|.]+)str)?\s*$";
                
        private static readonly Regex m_DialogueLineAuthorAliasTextSplitRegex = new Regex(dialogueLineAuthorAliasTextSplitRegexString);
        private static readonly Regex m_CommandLineArgumentSplitRegex = new Regex(commandLineArgumentSplitRegexString);
        private static readonly Regex m_CommandLineFunctionNameArgumentSplitRegex = new Regex(commandLineFunctionNameArgumentRegexString);
        private static readonly Regex m_EmoteLineArgumentSplitRegex = new Regex(emoteLineArgumentSplitRegexString);
        
        private List<string> m_DialogueInitiators = new List<string> { narratorNameString };
        private List<string> m_DialogueAliases = new List<string>();

        /// <summary>
        /// Parses the story line creating a <see cref="StoryElement"/>.
        /// </summary>
        /// <returns>A story element created from parsing the input line.</returns>
        /// <param name="line">The text of the current line.</param>
        /// <param name="tags">List of tags from the current story line.</param>
        public List<StoryElement> ParseStoryLine(string line, List<string> tags = null)
        {
            if (line.StartsWith(commandIndicator, StringComparison.OrdinalIgnoreCase))
            {
                return ParseCommandLine(line); // Currently ignoring tags if they are on a command line. Will implement if necessary.
            }
            else if (line.StartsWith(emoteIndicator, StringComparison.OrdinalIgnoreCase))
            {
                return ParseEmoteLine(line);
            }
            else
            {
                return ParseDialogueLine(line, tags);
            }
        }

        public static Dictionary<MethodInfo, List<string>> GetCommands(string commandString)
        {
            MatchCollection commandMatches = m_CommandLineFunctionNameArgumentSplitRegex.Matches(commandString);
            Dictionary<MethodInfo, List<string>> commands = new Dictionary<MethodInfo, List<string>>();
            foreach (Match match in commandMatches)
            {
                MatchCollection argumentsCollection = m_CommandLineArgumentSplitRegex.Matches(match.Groups[2].Value);
                //The regular expression contains an OR clause, which matches arguments not in quotes by the second group. If the first group is empty, use the second instead.
                MethodInfo methodInfo = GetMethodFromString(match.Groups[1].Value);
                if(methodInfo == null){
                    UnityEngine.Debug.LogWarning(string.Format("\"{0}\" not found via reflection. Please check spelling.", match.Groups[1].Value));
                    continue;
                }
                commands.Add(methodInfo, argumentsCollection.Cast<Match>().Select(argumentMatch => argumentMatch.Groups[1].Value.Equals(string.Empty) ? argumentMatch.Groups[2].Value : argumentMatch.Groups[1].Value).ToList());
            }
            return commands;
        }

        /// <summary>
        /// Parses a <see cref="CommandElement"/> from a string beginning with ">>>".
        /// </summary>
        /// <returns>The command element.</returns>
        /// <param name="line">Line.</param>
        public List<StoryElement> ParseCommandLine(string line)
        {
            return new List<StoryElement> {new CommandElement(GetCommands(line))};
        }

        /// <summary>
        /// Split a line of dialogue into component parts.
        /// </summary>
        /// <returns>A StoryElement representing the interpretation of the input line.</returns>
        /// <param name="line">The string line as displayed in the ".ink" file.</param>
        public List<StoryElement> ParseDialogueLine(string line, List<string> tags=null)
        {
            var dialogueElementInLine = ParseDialogueText(line);
            var dialogueLineElements = new List<StoryElement>{dialogueElementInLine};
            dialogueLineElements.AddRange(ParseTags(tags, dialogueElementInLine.Sources.Select(characterName => CharacterReference.FromName(characterName))));
            return dialogueLineElements;
        }

        public List<StoryElement> ParseTags(List<string> tags, IEnumerable<CharacterReference> sourceCharacters)
        {  
            var retList = new List<StoryElement>();
            if (tags != null)
            {
                foreach (string tag in tags)
                {
                    retList.Add(new EmoteElement(new EmoteData(tag), sourceCharacters));
                }
            }

            return retList;
        }

        /// <summary>
        /// Creates a <see cref="DialogueElement"/> for a string of Dialogue text.
        /// </summary>
        /// <param name="text">The line of dialogue text without its tags.</param>
        /// <returns>A <see cref="DialogueElement"/> containing the specified <paramref name="text"/>.</returns>
        public DialogueElement ParseDialogueText(string text)
        {
            Match dialogueLineMatch = m_DialogueLineAuthorAliasTextSplitRegex.Match(text);
            if (dialogueLineMatch!=Match.Empty)
            {
                if (!string.IsNullOrEmpty(dialogueLineMatch.Groups[1].Value))
                {
                    m_DialogueInitiators = RegexSplitToStringListUtility(dialogueLineMatch.Groups[1].Value);
                    if (!string.IsNullOrEmpty(dialogueLineMatch.Groups[2].Value))
                    {
                        m_DialogueAliases = RegexSplitToStringListUtility(dialogueLineMatch.Groups[2].Value);
                    }else{
                        m_DialogueAliases = null;
                    }
                }

                DialogueType dialogueLineDialogueType;
                if (m_DialogueInitiators.Contains(narratorNameString))
                {
                    dialogueLineDialogueType = DialogueType.Narrator;
                }
                else if (m_DialogueInitiators.Contains(historyRecordNameString))
                {
                    dialogueLineDialogueType = DialogueType.HistoryRecord;
                }
                else
                {
                    dialogueLineDialogueType = DialogueType.Character;
                }
                return new DialogueElement(m_DialogueInitiators, m_DialogueAliases, dialogueLineMatch.Groups[3].Value, dialogueLineDialogueType);
            }
            UnityEngine.Debug.Log("No match found for line:\n\t"+text);
            return null;
        }

        /// <summary>
        /// Creates a <see cref="ChoicesElement"/> given a <see cref="List{T}"/> of <see cref="Choice"/>. 
        /// </summary>
        /// <returns>A <see cref="ChoicesElement"/> composed of several <see cref="DialogueElement"/>.</returns>
        /// <param name="choices">A list of Ink's <see cref="Choice"/> objects.</param>
        public ChoicesElement ParseChoices(List<Choice> choices)
        {
            return new ChoicesElement(choices.Select(choice => new KeyValuePair<int, DialogueElement>(choice.index, ParseDialogueText(choice.text))).ToList());
        }

        public List<StoryElement> ParseEmoteLine(string line)
        {
            Match emoteLineMatch = m_EmoteLineArgumentSplitRegex.Match(line);
            if (emoteLineMatch != Match.Empty)
            {
                float? durationOverride = float.TryParse(emoteLineMatch.Groups[4].Value, out float dur) ? dur : (float?) null;
                float? strengthOverride = float.TryParse(emoteLineMatch.Groups[6].Value, out float str) ? str : (float?) null;

                EmoteData emoteData = new EmoteData(
                    EmoteReference.FromString(emoteLineMatch.Groups[1].Value),
                    durationOverride == null ? 
                        Enum.TryParse(emoteLineMatch.Groups[5].Value, out EmoteDurationType durType) ? durType : EmoteDurationType.NextLine
                        : EmoteDurationType.Timed,
                    durationOverride,
                    strengthOverride
                );
                
                if (emoteData.EmoteReference.Emote == null)
                {
                    Debug.LogWarning($"Unable to find emote for given string: '{emoteLineMatch.Groups[1].Value}'");
                }
                List<string> characterNames = string.IsNullOrEmpty(emoteLineMatch.Groups[3].Value) ? RegexSplitToStringListUtility(emoteLineMatch.Groups[2].Value, @",?\s+") : new List<string>{emoteLineMatch.Groups[3].Value};
                List<CharacterReference> characters = characterNames.Select(name => CharacterReference.FromName(name))
                    .Where(reference => reference.Character != null).ToList();
                return new List<StoryElement> {new EmoteElement(emoteData, characters)};
            }
            UnityEngine.Debug.Log("No match found for emote line:\n\t"+line);
            return null;        
        }

        /// <summary>
        /// Splits a string into <see cref="System.Collections.Generic.List{T}"/> of <see cref="string"/> using a <paramref name="splitRegex"/>.
        /// </summary>
        /// <returns>The split to string list utility.</returns>
        /// <param name="combinedItems">Single string representing several string items delimited by the split regex.</param>
        /// <param name="splitRegex">The regex delimiting items, .</param>
        public List<string> RegexSplitToStringListUtility(string combinedItems, string splitRegex = @"\@")
        {
            return new List<string>(Regex.Split(combinedItems, splitRegex));
        }

        /// <summary>
        /// Finds a method of StoryManager using the string name of that method.
        /// </summary>
        /// <returns>A <see cref="MethodInfo"/> of the found Method.</returns>
        /// <param name="methodNameString">Method name string.</param>
        public static MethodInfo GetMethodFromString(string methodNameString)
        {
            return StoryManager.Instance.GetType().GetMethod(methodNameString);
        }

        /// <summary>
        /// Returns the story path base given a dot-separated path.subpath structure.
        /// </summary>
        /// <returns>The story path base.</returns>
        /// <param name="storyKnotFullPath">Story knot full path.</param>
        public static string GetStoryPathBase(string storyKnotFullPath)
        {
            return storyKnotFullPath != null ? storyKnotFullPath.Split('.')[0] : "";
        }
    }
}
