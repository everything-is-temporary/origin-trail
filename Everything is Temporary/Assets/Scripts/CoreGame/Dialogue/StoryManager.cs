﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using GameUtility;
using GameUtility.DataPersistence;
using GameUtility.Serialization;
using Ink.Runtime;
using UnityEngine;

namespace Dialogue
{
    /// <summary>
    /// Singleton class for controlling progression of the story.
    /// Contains methods for starting a sequence of dialogue, querying next line of script, or selecting among choices.
    /// </summary>
    public partial class StoryManager : ISaveLoadableManager<StoryManager>
    {
        // Code for testing the Dialogue System. TODO: Make GameManager load in the story file.
        [SerializeField] private TextAsset StoryToLoad = default;
        protected override void Start()
        {
            base.Start();
            LoadStoryFile(StoryToLoad);

            m_blockingElementCount.TotalChanged += total =>
            {
                if (total > 0)
                    OnBlockedChanged?.Invoke(true);
                else
                    OnBlockedChanged?.Invoke(false);
            };
        }
        // End code for testing Dialogue System


        /// <summary>
        /// Tests whether the story is currently prevented from proceeding.
        /// </summary>
        public bool IsBlocked => m_blockingElementCount.Total > 0;

        /// <summary>
        /// Occurs when <see cref="IsBlocked"/> changes.
        /// </summary>
        public event Action<bool> OnBlockedChanged;

        /// <summary>
        /// Tests whether the story will continue if Proceed() is called. This
        /// will return false if the next story element is a choice point or if
        /// the end is reached.
        /// </summary>
        /// <value><c>true</c> if can continue; otherwise, <c>false</c>.</value>
        public bool CanContinue => m_CurrentStory.canContinue;

        /// <summary>
        /// Returns whether calling Proceed() will take us to a choice point.
        /// </summary>
        /// <value><c>true</c> if story is at choice point; otherwise, <c>false</c>.</value>
        public bool IsAtChoicePoint => m_CurrentStory.currentChoices.Count > 0;

        /// <summary>
        /// Returns whether the current story has reached an end.
        /// </summary>
        /// <value><c>true</c> if story is at end; otherwise, <c>false</c>.</value>
        public bool IsAtEnd => !CanContinue && !IsAtChoicePoint && m_pendingStoryElements.Count == 0; // Quickfix for accessors determining the story is over even with elements in the queue.

        /// <summary>
        /// Occurs when a Proceed() call reaches an end marker.
        /// </summary>
        /// <remarks>
        /// Calling Proceed() can encounter a '>>>' statement, which will invoke
        /// a command and then call Proceed() again. This event can be used to
        /// detect if a Proceed() call only invoked a bunch of commands and then
        /// immediately reached an end marker.
        /// </remarks>
        public event Action OnEndReached;

        /// <summary>
        /// Loads a knot by name.
        /// </summary>
        /// <returns><c>true</c>, if the story path was changed, <c>false</c> otherwise.</returns>
        /// <param name="path">Period-separated path string following ink convention ("knot_name.stitch_name").</param>
        public bool LoadStoryKnot(string path)
        {
            if (m_CurrentStory == null)
            {
                Debug.LogWarning("Did not load knot because current story is null.");
                return false;
            }

            string currentPath = m_CurrentStory.state.currentPathString;
            m_CurrentStory.ChoosePathString(path);

            if (this.m_CurrentStory.hasError)
            {
                Debug.LogError($"Current story contains errors:\n{String.Join("\n", m_CurrentStory.currentErrors)}");
            }

            bool pathHasChanged = StoryParser.GetStoryPathBase(currentPath) == StoryParser.GetStoryPathBase(m_CurrentStory.state.currentPathString);
            if (pathHasChanged)
            {
                m_ElementHistory.Clear();
            }

            return pathHasChanged;
        }

        /// <summary>
        /// Returns true if the given knot exists as a callable object in the story.
        /// </summary>
        /// <param name="path">Knot or function path for story.</param>
        /// <returns><c>true</c> if the path exists in the story, <c>false</c> otherwise.</returns>
        public bool StoryKnotExists(string path)
        {
            // "Horrendous Hack" according to the issue here: https://github.com/inkle/ink/issues/189
            return m_CurrentStory.HasFunction(path);
        }

        public void Proceed(int? choiceIndex = null)
        {
            ProceedAsync(choiceIndex).FireAndForget();
        }

        /// <summary>
        /// Proceed with the story, takes in a choiceIndex as an optional argument to make a choice first if required.
        /// </summary>
        /// <param name="choiceIndex">Choice index.</param>
        public async Task ProceedAsync(int? choiceIndex = null)
        {
            if (IsBlocked)
            {
                return;
            }
            
            if (choiceIndex != null)
            {
                MakeChoice((int)choiceIndex);
            }

            PreprocessNext();
            bool blockingElementPerformed = false; //implementation requires at least one blocking element to be performed in a loop or reach the end of content.
            do
            {
                if (IsAtEnd)
                {
                    OnEndReached?.Invoke();
                }

                if (m_pendingStoryElements.Count == 0)
                {
                    // OnEndReached?.Invoke(); // Event already invoked in preprocess step
                    return;
                }

                var element = m_pendingStoryElements.Dequeue();
                if (element.ContainsContent())
                {
                    blockingElementPerformed |= element.AutoAdvance == false;
                    await element.Handle();
                }

                if (element is ChoicesElement) // Don't preprocess choices again here
                {
                    return; // Immediately break out of the loop, we need the choice to be handled before we do anything else.
                }
                PreprocessNext();
            } while (!blockingElementPerformed || (m_pendingStoryElements.Count > 0 && m_pendingStoryElements.Peek().AutoAdvance));

        }

        public StoryElement PeekNext()
        {
            PreprocessNext();
            return m_pendingStoryElements.Count > 0 ? m_pendingStoryElements.Peek() : null;
        }

        public void PreprocessNext()
        {
            if (m_pendingStoryElements.Count == 0)
            {
                if (CanContinue)
                {
                    m_Parser.ParseStoryLine(m_CurrentStory.Continue(), m_CurrentStory.currentTags).ForEach(
                        element =>
                        {
                            m_pendingStoryElements.Enqueue(element);
                            m_ElementHistory.Append(element);
                        });
                }
                else if (IsAtChoicePoint)
                {
                    ChoicesElement choicesElement = m_Parser.ParseChoices(m_CurrentStory.currentChoices);
                    m_pendingStoryElements.Enqueue(choicesElement);
                    m_ElementHistory.Append(choicesElement);
                }
            }
        }

        /// <summary>
        /// Selects <see langword="async"/> choice by index and allows the story to continue.
        /// </summary>
        /// <param name="choiceIndex">Choice index, contained in the choices data structure.</param>
        public void MakeChoice(int choiceIndex)
        {
            this.m_CurrentStory.ChooseChoiceIndex(choiceIndex);
        }

        /// <summary>
        /// Gets the base knot for the current story, used for checking and creating locks on certain story parts.
        /// </summary>
        /// <returns>The current story knot as a <see cref="string"/>.</returns>
        public string GetCurrentStoryPath()
        {
            // returns empty string on choice
            return this.m_CurrentStory.state.currentPathString != null ? StoryParser.GetStoryPathBase(this.m_CurrentStory.state.currentPathString) : string.Empty;
        }

        public void LoadStoryFile(TextAsset inkFile, string storyJsonState = null)
        {
            this.m_CurrentStory = new Story(inkFile.text);
            if (storyJsonState != null)
            {
                this.CurrentStoryLoadJson(storyJsonState);
            }
            BindDefaults(this.m_CurrentStory);
            this.m_CurrentStory.ValidateExternalBindings();
        }

        #region SaveLoad

        public override ISerializationCallbackReceiver GetSerializedData()
        {
            return new SerializableDataContainer<string>(CurrentStoryStateAsJson());
        }

        public override bool SetSerializedData(ISerializationCallbackReceiver data)
        {
            // TODO: Serialize the StoryElement Queue
            string story = ((SerializableDataContainer<string>)data).Data;
            return CurrentStoryLoadJson(story);
        }

        public string CurrentStoryStateAsJson()
        {
            return this.m_CurrentStory?.state.ToJson();
        }

        /// <summary>
        /// Loads story state from a json string.
        /// </summary>
        /// <param name="json">Story state as json string.</param>
        private bool CurrentStoryLoadJson(string json)
        {
            if (this.m_CurrentStory != null)
            {
                this.m_CurrentStory.state.LoadJson(json);
                return true;
            }
            return false;
        }

        #endregion

        /// <summary>
        /// Checks if a lock for the current knot exists in the storyLocks dictionary. If it does, tests the lock condition and returns the value of that check, removing the lock from the <see cref="m_StoryLocks"/> dictionary if the return value is true./>
        /// </summary>
        /// <returns><c>true</c>, if story lock was checked and returned true or if no story lock for the specified knot exists, <c>false</c> if the check returned false.</returns>
        /// <param name="knot">The story knot to check.</param>
        private bool CheckStoryLock(string knot)
        {
            if (this.m_StoryLocks.TryGetValue(knot, out List<Func<bool>> checkFunctions))
            {
                for (int i = checkFunctions.Count - 1; i >= 0; --i)
                {
                    Func<bool> checkFunction = checkFunctions[i];
                    if (checkFunction.Invoke())
                    {
                        checkFunctions.RemoveAt(i);
                    }
                }
                if (checkFunctions.Count == 0)
                {
                    this.m_StoryLocks.Remove(knot);
                    return true;
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Adds the specified check function as a story lock.
        /// </summary>
        /// <param name="knot">Name of the knot (scene) to lock.</param>
        /// <param name="check">Locking function (returns true to unlock).</param>
        private void AddStoryLock(string knot, Func<bool> check)
        {
            if (m_StoryLocks.TryGetValue(knot, out List<Func<bool>> checkFunctions))
            {
                checkFunctions.Add(check);
            }
            m_StoryLocks.Add(knot, new List<Func<bool>> { check });
        }

        /// <summary>
        /// Unlocks the specified story knot.
        /// </summary>
        /// <param name="knot">The story knot to unlock.</param>
        private void UnlockKnot(string knot)
        {
            m_StoryLocks.Remove(knot);
        }

        private Story m_CurrentStory;
        private StoryParser m_Parser = new StoryParser();
        private VariablesState m_PersistentGlobals; //unused currently

        private Stack<StoryElement> m_ElementHistory = new Stack<StoryElement>();
        private Queue<StoryElement> m_pendingStoryElements = new Queue<StoryElement>();

        //Functionality to pause dialogue line
        private Dictionary<string, List<Func<bool>>> m_StoryLocks = new Dictionary<string, List<Func<bool>>>(); //locks certain knots from proceeding until the function returns true

        /// <summary>
        /// If this is greater than zero, <see cref="IsBlocked"/> will be true.
        /// </summary>
        private readonly Counter m_blockingElementCount = new Counter();

        #region Invisible Walls

        public void RegisterWall(Interaction.InvisibleWall wall)
        {
            invisibleWalls.Add(wall);
            // Intentionally throws an exception when atempting to add 2 walls of the same name.
            wallsByName.Add(wall.wallName, wall);
        }

        public void UnregisterWall(Interaction.InvisibleWall wall)
        {
            invisibleWalls.Remove(wall);
            wallsByName.Remove(wall.wallName);
        }

        public void EnableWallByIndex(int wallIndex)
        {
            GetWall(wallIndex)?.Enable();
        }

        public void EnableWallByName(string wallName)
        {
            GetWall(wallName)?.Enable();
        }

        public void DisableWallByIndex(int wallIndex)
        {
            GetWall(wallIndex)?.Disable();
        }

        public void DisableWallByName(string wallName)
        {
            GetWall(wallName)?.Disable();
        }

        private Interaction.InvisibleWall GetWall(int wallIndex)
        {
            if (invisibleWalls.Count <= wallIndex || wallIndex < 0)
            {
                Debug.LogWarning($"Invalid index {wallIndex} for enabling/ disabling wall given.");
                return null;
            }
            return invisibleWalls.ElementAt(wallIndex);
        }

        private Interaction.InvisibleWall GetWall(string wallName)
        {
            if (!wallsByName.ContainsKey(wallName))
            {
                Debug.LogWarning($"No wall of name {wallName} exists in this scene");
                return null;
            }
            return wallsByName[wallName];
        }

        private SortedSet<Interaction.InvisibleWall> invisibleWalls = new SortedSet<Interaction.InvisibleWall>();
        private Dictionary<string, Interaction.InvisibleWall> wallsByName = new Dictionary<string, Interaction.InvisibleWall>();

        #endregion
    }
}
