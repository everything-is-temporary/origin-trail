﻿using System;
using System.Collections.Generic;
using Ink.Runtime;

namespace Dialogue
{
    public partial class StoryManager
    {
        /// <summary>
        /// Helper function to create checks for menu-related items.
        /// </summary>
        /// <returns>A lambda check returning <c>true</c> if the provided menu is closed and <c>false</c> otherwise.</returns>
        /// <param name="whichMenu">Which menu.</param>
        public Func<bool> MenuClosed(string whichMenu)
        {
            return () => whichMenu.Length > 0; // TODO: !Important! Implement, link with class handling menus and pages
        }
    }


}
