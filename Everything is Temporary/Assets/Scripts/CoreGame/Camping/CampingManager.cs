﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using GameUtility;
using UnityEngine;
using Characters;
using GameUI.Camping;
using UnityEngine.SceneManagement;
using Dialogue;

namespace Camping
{
    /// <summary>
    /// A singleton that manages camping placed in every camping scene.
    /// </summary>
    public class CampingManager : SingletonMonoBehavior<CampingManager>
    {
        [SerializeField]
        private CampingSpawnPointScript[] m_spawnPositions = default;

        [SerializeField]
        private CampingPartyMemberDisplay m_partyMemberDisplayPrefab = default;


        /// <summary>
        /// Call this function to end the camping phase. Returns true if the
        /// camping phase was ended, and returns false if the camping phase
        /// was not in progress.
        /// </summary>
        public bool FinishCamping()
        {
            if (m_campingTask == null)
                return false;

            m_campingTask.TrySetResult(null);
            m_campingTask = null;

            foreach (CharacterReference charRef in m_allCharacters)
            {
                bool didFeed = m_itemInteractedCharacters.Contains(charRef);

                if (!didFeed)
                {
                    // TODO By how much should happiness change?
                    charRef.Character.Stats.Change(StatEnum.Happiness, -1);

                    Debug.Log($"{charRef.Character.Name} is unfed and unhappy.");
                }
                else
                {
                    Debug.Log($"{charRef.Character.Name} is content.");
                }
            }

            return true;
        }

        public async Task WaitForCampingToFinish()
        {
            if (m_campingTask != null)
                await m_campingTask.Task;
        }

        protected override void PostAwake()
        {
            m_campingTask = new TaskCompletionSource<object>();
        }

        private void Start()
        {
            m_partyMemberDisplay = UIOverlayManager.Instance.InstantiateOverlayOnTop(m_partyMemberDisplayPrefab);
            m_partyMemberDisplay.OnFeed += ItemInteractCharacter;
            m_partyMemberDisplay.OnTalk += TalkCharacter;

            SpawnParty();
        }

        protected override void OnDestroy()
        {
            if (m_campingTask != null)
                m_campingTask.SetCanceled();

            UIOverlayManager.Instance?.DestroyOverlay(m_partyMemberDisplay);

            base.OnDestroy();
        }

        private void SpawnParty()
        {
            int spawnIdx = 0;

            foreach (var charRef in CharacterManager.Instance.CurrentParty)
            {
                if (spawnIdx >= m_spawnPositions.Length)
                    break;

                CampingActorScript actor = m_spawnPositions[spawnIdx].Spawn(charRef);
                if (charRef.Character.CanInteractWithDuringCamping)
                {
                    actor.OnClick += m_partyMemberDisplay.ShowForPartyMember;
                }
                else
                {
                    actor.HighlightEnabled = false;
                }

                m_allCharacters.Add(actor.Actor.Character);

                ++spawnIdx;
            }
        }

        private void ItemInteractCharacter(CharacterReference charRef)
        {
            if (m_itemInteractedCharacters.Contains(charRef))
            {
                GameUI.GameUIHelper.Instance.DisplayPopupInfoAsync(
                    $"You already gave something to {charRef.Character.Name}.").FireAndForget();
                return;
            }
            
            PerformDialogue(charRef.Character.GiveItemKnotPath, charRef);
            m_itemInteractedCharacters.Add(charRef);
        }

        private void TalkCharacter(CharacterReference charRef)
        {
            if (m_numCharacterInteractions.TryGetValue(charRef, out var interactionsThisCampingScene))
            {
                PerformDialogue(charRef.Character.CampingKnotPathR2, charRef);
                m_numCharacterInteractions[charRef] = interactionsThisCampingScene + 1;
            }
            else
            {
                PerformDialogue(charRef.Character.CampingKnotPath, charRef);
                m_numCharacterInteractions.Add(charRef, 1);
            }
        }

        private void PerformDialogue(string path, CharacterReference charRef)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                Debug.LogWarning($"No camping knot path set on {charRef.Character.Name}.");
            }else if (!StoryManager.Instance.StoryKnotExists(path))
            {
                Debug.LogWarning($"Camping knot path {path}" +
                                 $" on {charRef.Character.Name} is invalid.");
            }
            else
            {
                CoreGameManager.Instance.PerformDialog(path).FireAndForget();
                return;
            }

            GameUI.GameUIHelper.Instance.DisplayPopupInfoAsync(
                $"{charRef.Character.Name} has nothing to say.").FireAndForget();
        }

        private TaskCompletionSource<object> m_campingTask;

        private readonly HashSet<CharacterReference> m_allCharacters = new HashSet<CharacterReference>();
        private readonly HashSet<CharacterReference> m_itemInteractedCharacters = new HashSet<CharacterReference>();
        private readonly Dictionary<CharacterReference, int> m_numCharacterInteractions = new Dictionary<CharacterReference, int>();

        private CampingPartyMemberDisplay m_partyMemberDisplay;
    }
}