﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

namespace Camping
{
    /// <summary>
    /// Represents a spawn location in a camping scene.
    /// </summary>
    public class CampingSpawnPointScript : MonoBehaviour
    {
        public CampingActorScript Spawn(CharacterReference charRef)
        {
            CampingActorScript camper = Instantiate(charRef.Character.CampingPrefab, transform);

            camper.Actor.FacingDirection = m_directionToCampfire;

            return camper;
        }

        [SerializeField]
        private Direction m_directionToCampfire = default;
    }
}