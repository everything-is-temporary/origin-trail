﻿using System;
using Characters;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Camping
{
    /// <summary>
    /// Adds camping-related functionality to Actors representing party members
    /// during the party scene.
    /// </summary>
    [RequireComponent(typeof(Actor))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class CampingActorScript : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        /// <summary>
        /// Occurs when the actor is clicked.
        /// </summary>
        public event Action<CharacterReference> OnClick;

        /// <summary>
        /// The <see cref="Actor"/> component on this object.
        /// </summary>
        public Actor Actor => m_actor;

        public bool HighlightEnabled
        {
            get => m_highlightEnabled;
            set
            {
                m_highlightEnabled = value;

                if (!m_highlightEnabled)
                {
                    m_spriteRenderer.color = m_spriteRendererInitialColor;
                }
            }
        }

        private void Awake()
        {
            m_actor = GetComponent<Actor>();
            m_spriteRenderer = GetComponent<SpriteRenderer>();

            m_spriteRendererInitialColor = m_spriteRenderer.color;
        }

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke(m_actor.Character);
        }

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            if (m_highlightEnabled)
            {
                m_spriteRenderer.color = m_spriteRendererInitialColor * 0.8f;
            }
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            if (m_highlightEnabled)
            {
                m_spriteRenderer.color = m_spriteRendererInitialColor;
            }
        }


        private Actor m_actor;
        private SpriteRenderer m_spriteRenderer;
        private Color m_spriteRendererInitialColor;
        private bool m_highlightEnabled = true;
    }
}