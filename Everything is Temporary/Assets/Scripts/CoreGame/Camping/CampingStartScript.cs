﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;

namespace Camping
{
    /// <summary>
    /// Provides a function to initiate camping.
    /// </summary>
    public class CampingStartScript : MonoBehaviour
    {
        public void InitiateCamping()
        {
            InitiateCamping("Forest Camping Scene");
        }

        public void InitiateCamping(string campingScene)
        {
            CoreGameManager.Instance.PerformCamping(campingScene).FireAndForget();
        }
    }
}