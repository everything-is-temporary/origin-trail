﻿using System;
using UnityEngine;
namespace Camping
{
    /// <summary>
    /// Provides a function to end the camping scene.
    /// </summary>
    public class CampingFinishScript : MonoBehaviour
    {
        public void FinishCamping()
        {
            CampingManager.Instance.FinishCamping();
        }
    }
}