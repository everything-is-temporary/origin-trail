﻿using UnityEngine;
using System.Collections;
using GameUtility;

/// <summary>
/// Controls the sidescrolling scene, including trigger points for animation/ dialogue and movement of NonPartyActors.
/// </summary>
public class SidescrollManager : SingletonMonoBehavior<SidescrollManager>
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
