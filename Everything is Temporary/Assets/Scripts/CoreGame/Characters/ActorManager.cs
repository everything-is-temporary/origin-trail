using System.Collections.Generic;
using Characters.Emotes;
using GameUtility;
using UnityEngine;

namespace Characters
{
    /// <summary>
    /// Maps characters to the actors that represent them.
    /// </summary>
    public class ActorManager : SingletonMonoBehavior<ActorManager>
    {

        /// <summary>
        /// Registers the actor in scene if another actor has not been registered
        /// for the same character.
        /// </summary>
        /// <param name="actor">Actor to register.</param>
        public void RegisterActorInScene(Actor actor)
        {
            if (m_characterToActor.ContainsKey(actor.Character))
            {
                // FIXME: If an actor is present in a scene but is also in
                // the player's party, on scene start, the player's party actor
                // is instantiated second. This code destroys the previous
                // actor.
                Debug.LogWarning("A duplicate actor was detected for the character" +
                    $" {actor.Character.Character.Name}. The previous actor will be deleted.");

                Destroy(m_characterToActor[actor.Character].gameObject);
            }

            m_characterToActor[actor.Character] = actor;
        }

        /// <summary>
        /// Opposite of <see cref="RegisterActorInScene(Actor)"/>.
        /// </summary>
        /// <param name="actor">The actor to remove.</param>
        public void UnregisterActorFromScene(Actor actor)
        {
            if (m_characterToActor.TryGetValue(actor.Character, out Actor prevActor) && prevActor == actor)
            {
                m_characterToActor.Remove(actor.Character);
            }
        }


        public void RegisterPlayer(Actor actor)
        {
            Debug.Assert(m_playerActor == null);
            m_playerActor = actor;
        }

        public void RemovePlayer()
        {
            m_playerActor = null;
        }

        /// <summary>
        /// Add an NPC to the player's party, and make the NPC follow the player.
        /// </summary>
        public void AddNewPartyMember(CharacterReference charRef)
        {
            Actor actor = m_characterToActor[charRef];

            // FIXME Quickfix that disables dialogue on characters.
            //disable dialogue, now character is just following you around
            Interaction.BasicInteractiveObject dialogue = actor.GetComponent<Interaction.BasicInteractiveObject>();
            if (dialogue != null)
                Destroy(dialogue);

            actor.FollowOther(m_playerActor);
        }

        public Actor GetActor(CharacterReference characterReference)
        {
            if (m_characterToActor.TryGetValue(characterReference, out Actor actor))
            {
                return actor;
            }
            Debug.LogWarning($"Character {characterReference.Character?.Name} does not exist in scene.");
            return null;
        }

        public bool ExistsInScene(CharacterReference characterReference)
        {
            return m_characterToActor.ContainsKey(characterReference);
        }

        public void ApplyEmote(CharacterReference characterReference, EmoteData emoteData)
        {
            Actor actor = GetActor(characterReference);
            if (actor == null)
            {
                Debug.LogWarning("EmoteReference not applied to character not existing in scene.");
                return;
            }
            actor.DisplayEmote(emoteData);
        }

        private Actor m_playerActor;
        private Dictionary<CharacterReference, Actor> m_characterToActor = new Dictionary<CharacterReference, Actor>();
    }
}
