﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
namespace Characters
{
    /// <summary>
    /// Keeps track of the target offsets for a chain of actors that are
    /// following a leader.
    /// </summary>
    public class ActorChain
    {
        /* ASSUMPTION: Position +- Width/2 == bounds of sprite */
        // TODO: Add spacing between chain elements.

        public ActorChain(float leaderWidth)
        {
            LeaderWidth = leaderWidth;
        }

        public IElement AddFollower(float followerWidth)
        {
            Element newFollower = new Element(this, followerWidth);

            if (m_allFollowers.Count > 0)
            {
                Element lastFollower = m_allFollowers[m_allFollowers.Count - 1];
                newFollower.OffsetFromLeader = lastFollower.OffsetFromLeader + lastFollower.HalfWidth + newFollower.HalfWidth;
            }
            else
            {
                newFollower.OffsetFromLeader = LeaderHalfWidth + newFollower.HalfWidth;
            }

            m_allFollowers.Add(newFollower);

            return newFollower;
        }

        private void RemoveFollower(Element follower)
        {
            if (m_allFollowers.Count == 1)
            {
                m_allFollowers.Remove(follower);
                Debug.Assert(m_allFollowers.Count == 0);
                return;
            }

            int index = m_allFollowers.IndexOf(follower);
            m_allFollowers.RemoveAt(index);

            if (index == 0)
            {
                Element nextFollower = m_allFollowers[0];
                nextFollower.OffsetFromLeader = LeaderHalfWidth + nextFollower.HalfWidth;
                ++index;
            }

            for (int i = index; i < m_allFollowers.Count; ++i)
            {
                Element prevFollower = m_allFollowers[i - 1];
                Element currFollower = m_allFollowers[i];

                currFollower.OffsetFromLeader = prevFollower.OffsetFromLeader + prevFollower.HalfWidth + currFollower.HalfWidth;
            }
        }

        private float LeaderWidth { get; }
        private float LeaderHalfWidth => LeaderWidth / 2;

        private readonly List<Element> m_allFollowers = new List<Element>();

        public interface IElement
        {
            float OffsetFromLeader { get; }
            void RemoveFromChain();
        }

        private class Element : IElement
        {
            public float Width { get; }
            public float HalfWidth => Width / 2;
            public float OffsetFromLeader { get; set; }

            public Element(ActorChain chain, float width)
            {
                m_chain = chain;
                Width = width;
            }

            public void RemoveFromChain()
            {
                m_chain.RemoveFollower(this);
                m_chain = null;
            }

            private ActorChain m_chain;
        }
    }
}