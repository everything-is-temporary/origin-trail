﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Characters
{
    /// <summary>
    /// NPC movement for NPCs not in the player's party.
    /// </summary>
    /// 
    public class NonPartyBehavior : IMovementBehavior<Actor>
    {
        public MovementSummary StepMove(Actor obj, Rigidbody2D rigidbody)
        {
            //TODO: ask designers what NPC movement they want
            //Standing still? walking back and forth?
            return new MovementSummary(Vector2.zero, null);
        }
    }
}
