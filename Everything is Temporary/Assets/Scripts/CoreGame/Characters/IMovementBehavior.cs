﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface that all movement behavior should implement. 
/// </summary>
public interface IMovementBehavior<T>
{
    /// <summary>
    /// This should take in an object (Actor, Animal, etc.) and move it
    /// one step forward in whichever movement pattern the implementing class
    /// represents. 
    /// </summary>
    /// <param name="obj"></param>
    MovementSummary StepMove(T obj, Rigidbody2D rigidbody);
}
