﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;

/// <summary>
/// The summary of the movement enacted on whatever is using this movement behavior.
/// </summary>
public class MovementSummary
{
    public Direction? FacingDirection { get; }
    public Vector2 NewVelocity { get; }

    public MovementSummary(Vector2 newVelocity, Direction? facingDirection = null)
    {
        NewVelocity = newVelocity;
        FacingDirection = facingDirection;
    }
}
