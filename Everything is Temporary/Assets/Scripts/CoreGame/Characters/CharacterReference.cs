﻿using GameUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using GameInventory;
using UnityEngine;

namespace Characters
{
    /// <summary>
    /// A reference to an character. Use this class to represent characters.
    /// </summary>
    [Serializable]
    public class CharacterReference : ISerializationCallbackReceiver
    {
        #region Public Interface

        public CharacterDefinition Character
        {
            get => GetCharacter(true);
        }

        public CharacterDefinition CharacterOrNull
        {
            get => GetCharacter(false);
        }

        public long Id => m_id;
        
        public List<ItemWithAmountAndCharRef> InventoryAsItemsWithAmountAndCharRef()
        {
            return Character.Inventory?.Select(itemWithAmount => new ItemWithAmountAndCharRef(this, itemWithAmount)).ToList();
        }

        #endregion


        /// <summary>
        /// Creates a CharacterReference at runtime from an ID. 
        /// </summary>
        /// <returns>The identifier.</returns>
        /// <param name="id">Identifier.</param>
        public static CharacterReference FromId(long id)
        {
            return new CharacterReference(id);
        }

        public static CharacterReference FromName(string characterName)
        {
            return new CharacterReference(CharacterDefinitionList.Instance.GetCharacterIdByName(characterName));
        }

        /// <summary>
        /// Converts the reference to a CharacterDefinition implicitly, allowing CharacterReferences
        /// to be passed to methods expecting CharacterDefinitions.
        /// </summary>
        /// <returns>The character.</returns>
        /// <param name="itemRef">The character reference.</param>
        public static implicit operator CharacterDefinition(CharacterReference characterReference)
        {
            return characterReference.Character;
        }

        private CharacterReference(long id)
        {
            m_id = id;
        }

        #region Standard Overrides
        public override string ToString()
        {
            return Character?.Name ?? "BAD_CHARACTER_ID";
        }

        public override int GetHashCode()
        {
#pragma warning disable RECS0025 // Non-readonly field referenced in 'GetHashCode()'
            return (int)m_id;
#pragma warning restore RECS0025 // Non-readonly field referenced in 'GetHashCode()'
        }

        public override bool Equals(object obj)
        {
            if (obj is CharacterReference itemRef)
                return itemRef.Id == Id;

            return false;
        }
        #endregion

        #region ISerializationCallbackReceiver
        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            // Important for editor scripting if m_id changes.
            m_character = null;
            m_queriedCharacter = false;
        }
        void ISerializationCallbackReceiver.OnBeforeSerialize() { }
        #endregion

        private CharacterDefinition GetCharacter(bool warnIfNull)
        {
            if (!m_queriedCharacter)
            {
                m_character = CharacterDefinitionList.Instance.GetCharacter(m_id);
                m_queriedCharacter = true;
            }

            if (m_character == null && warnIfNull)
                Debug.LogError($"Bad character ID: {IdConverter.ToReadableString(m_id)}");

            return m_character;
        }

        [NonSerialized]
        private CharacterDefinition m_character;

        [NonSerialized]
        private bool m_queriedCharacter = false;

        [SerializeField, IdField] private long m_id;
    }
}
