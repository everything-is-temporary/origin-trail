﻿using UnityEngine;
using System.Collections;

namespace Characters
{
    /// <summary>
    /// Moves according to the player's input.
    /// </summary>
    public class PlayerMovementBehavior : IMovementBehavior<Actor>
    {
        public MovementSummary StepMove(Actor actor, Rigidbody2D rigidbody)
        {
            // The following method doesn't use event handling / consuming...
            if (BookCameraScript.Instance.TargetFocus != BookCameraScript.FocusState.Game)
            {
                return new MovementSummary(Vector2.zero, null);
            }

            float xInput = GameInputManager.Instance.GetHorizontalAxis();
            float xSpeed = xInput * actor.MovementSpeed;

            var newVelocity = rigidbody.velocity;
            newVelocity.x = xSpeed;
            rigidbody.velocity = newVelocity;

            Direction? newDirection = null;
            if (!Mathf.Approximately(xSpeed, 0))
            {
                newDirection = xSpeed > 0 ? Direction.Right : Direction.Left;
            }

            return new MovementSummary(newVelocity, newDirection);
        }
    }
}