using System.Threading.Tasks;
using Characters.Emotes;
using Dialogue;
using GameUtility;
using Interaction;
using UnityEngine;

namespace Characters
{
    /// <summary>
    /// An actor plays a character. An actor is responsible for moving, emoting
    /// and speaking as that character.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Animator))]
    public class Actor : MonoBehaviour, IInteractiveObject
    {
        [SerializeField] private CharacterReference m_character = default;
        [SerializeField] private float m_movementSpeed = 3f;
        [SerializeField] private bool m_spriteFacesRight = default;
        [SerializeField] private string m_initialInteractionKnotPath = default;

        [SerializeField]
        [Tooltip("Object to activate when the Actor is selected.")]
        private GameObject m_selectionVisual = default;

        // TODO: Rename to CharacterReference
        public CharacterReference Character => m_character;
        public float MovementSpeed => m_movementSpeed;
        public Direction FacingDirection { get; set; }
        public string InteractionKnotPath
        {
            get => m_interactionKnotPath;
            set
            {
                if (!StoryManager.Instance.StoryKnotExists(value))
                {
                    Debug.LogWarning($"Tried to set an invalid knot path: {value}");

                    // Set to null because user probably expects knot path to change.
                    m_interactionKnotPath = null;
                }
                else
                {
                    m_interactionKnotPath = value;
                }
            }
        }

        public ActorChainElement AddFollower(float followerWidth)
        {
            return new ActorChainElement(m_actorChain.AddFollower(followerWidth), this);
        }

        public void FollowOther(Actor other)
        {
            float spriteWidth = m_spriteRenderer.bounds.size.x;

            m_chainElement?.LeaveChain();
            m_chainElement = other.AddFollower(spriteWidth);

            if (m_interactor != null)
                m_interactor.enabled = false;

            m_movementBehavior = new PartyMovement(m_chainElement);
        }

        public void SetControlledByPlayer()
        {
            m_chainElement?.LeaveChain();
            m_chainElement = null;

            m_movementBehavior = new PlayerMovementBehavior();

            if (m_interactor == null)
            {
                m_interactor = gameObject.AddComponent<BasicInteractor>();
            }
            else
            {
                m_interactor.enabled = true;
            }
        }

        /// <summary>
        /// Sets an arbitrary movement behavior for the Actor. The actor will
        /// leave any actor chain and will no longer be controlled by the player
        /// (if it was previously controlled by the player).
        /// </summary>
        /// <param name="movementBehavior">Movement behavior.</param>
        public void SetMovementBehavior(IMovementBehavior<Actor> movementBehavior)
        {
            m_chainElement?.LeaveChain();
            m_chainElement = null;

            if (m_interactor != null)
                m_interactor.enabled = false;

            m_movementBehavior = movementBehavior;
        }

        public void DisplayEmote(EmoteData emoteData)
        {
            EmoteDisplayerComponent emoteDisplayerComponent = gameObject.GetComponent<EmoteDisplayerComponent>();
            if (emoteDisplayerComponent == null)
            {
                emoteDisplayerComponent = gameObject.AddComponent<EmoteDisplayerComponent>();
            }
            emoteDisplayerComponent.DisplayEmote(emoteData);
        }

        private void Awake()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_rigidbody = GetComponent<Rigidbody2D>();
            m_animator = GetComponent<Animator>();
            m_interactor = GetComponent<BasicInteractor>();

            m_actorChain = new ActorChain(m_spriteRenderer.sprite.bounds.size.x);

            if (StoryManager.Instance.StoryKnotExists(m_initialInteractionKnotPath))
                InteractionKnotPath = m_initialInteractionKnotPath;
        }

        private void Start()
        {
            ActorManager.Instance.RegisterActorInScene(this);
            InteractionSystem.Instance.RegisterInteractiveObject(this);

            m_selectionVisual.SetActive(false);
        }

        private void OnDestroy()
        {
            InteractionSystem.Instance?.UnregisterInteractiveObject(this);
            ActorManager.Instance?.UnregisterActorFromScene(this);
        }

        private void FixedUpdate()
        {
            if (m_movementBehavior != null)
            {
                // Move the actor.
                MovementSummary pastMove = m_movementBehavior.StepMove(this, m_rigidbody);

                // Animate the actor based on the movement.
                float xVelocity = pastMove.NewVelocity.x;
                m_animator.SetFloat("MovementSpeed", xVelocity / MovementSpeed);

                if (pastMove.FacingDirection != null)
                    FacingDirection = pastMove.FacingDirection.Value;
            }

            bool shouldFlipToFaceRight = !m_spriteFacesRight;
            switch (FacingDirection)
            {
                case Direction.Left:
                    m_spriteRenderer.flipX = !shouldFlipToFaceRight;
                    break;
                case Direction.Right:
                    m_spriteRenderer.flipX = shouldFlipToFaceRight;
                    break;
                default:
                    throw new System.NotSupportedException();
            }
        }

        #region IInteractiveObject

        bool IInteractiveObject.CanInteractWith(IInteractor interactor)
        {
            return !string.IsNullOrWhiteSpace(InteractionKnotPath);
        }

        bool IInteractiveObject.TryInteractWith(IInteractor interactor)
        {
            if (InteractionKnotPath == null)
                return false;

            DisableInputAndShowDialogue().FireAndForget();
            return true;
        }

        float IInteractiveObject.GetDistanceTo(IInteractor interactor)
        {
            return Mathf.Abs(transform.position.x - interactor.GetPosition().x);
        }

        void IInteractiveObject.ShowSelectionVisual()
        {
            m_selectionVisual.SetActive(true);
        }

        void IInteractiveObject.HideSelectionVisual()
        {
            m_selectionVisual.SetActive(false);
        }

        private async Task DisableInputAndShowDialogue()
        {
            GameInputManager.Instance.ToggleMovementAndInteraction(false);

            StoryManager.Instance.LoadStoryKnot(InteractionKnotPath);
            await GameUI.GameUIHelper.Instance.DisplayDialogueAsync();

            GameInputManager.Instance.ToggleMovementAndInteraction(true);
        }

        #endregion

        private ActorChain m_actorChain;
        private ActorChainElement m_chainElement;
        private IMovementBehavior<Actor> m_movementBehavior;
        private string m_interactionKnotPath;

        private SpriteRenderer m_spriteRenderer;
        private Rigidbody2D m_rigidbody;
        private Animator m_animator;
        private BasicInteractor m_interactor;
    }
}
