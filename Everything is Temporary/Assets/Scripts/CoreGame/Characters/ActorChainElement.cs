using System.Collections.Generic;
using UnityEngine;

namespace Characters
{
    public class ActorChainElement
    {
        public ActorChainElement(ActorChain.IElement chainElement, Actor leader)
        {
            m_chainElement = chainElement;
            m_leader = leader;
        }

        /// <summary>
        /// Computes the X position of where the party member should be.
        /// </summary>
        /// <returns></returns>
        public float GetTargetXPosition()
        {
            float leaderLocation = m_leader.transform.position.x;
            float offsetFromLeader = m_chainElement.OffsetFromLeader;

            switch (m_leader.FacingDirection)
            {
                case Direction.Right:
                    return leaderLocation - offsetFromLeader;
                case Direction.Left:
                    return leaderLocation + offsetFromLeader;
                default:
                    throw new System.NotSupportedException();
            }
        }


        /// <summary>
        /// Computes the direction to the leader's position from a given position.
        /// </summary>
        /// <param name="fromXPosition">The position from which to get a direction.</param>
        /// <returns>The direction to the leader.</returns>
        public Direction GetDirectionToLeader(float fromXPosition)
        {
            return fromXPosition < m_leader.transform.position.x ?
                    Direction.Right : Direction.Left;
        }

        /// <summary>
        /// Frees the actor's spot in the chain of followers.
        /// </summary>
	    public void LeaveChain()
        {
            m_chainElement.RemoveFromChain();
        }

        private readonly ActorChain.IElement m_chainElement;
        private readonly Actor m_leader;
    }
}