﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;
using GameUtility.DataPersistence;
using GameUtility.Serialization;
using Newtonsoft.Json;
using System.Linq;

namespace Characters
{
    public class CharacterManager : ISaveLoadableManager<CharacterManager>
    {
        public IReadOnlyList<CharacterReference> CurrentPartyExcludingPlayer => m_currentParty;
        public IEnumerable<CharacterReference> CurrentParty => new[] { Player }.Concat(m_currentParty);
        public CharacterReference Player => m_playerCharacter;
        public GameObject Wagon => m_wagon;

        public event System.Action<CharacterReference> OnAddNewPartyMember;

        public void AddNewPartyMember(CharacterReference charRef)
        {
            m_currentParty.Add(charRef);

            OnAddNewPartyMember?.Invoke(charRef);
        }

        public override ISerializationCallbackReceiver GetSerializedData()
        {
            return new SerializableDataContainer<List<CharacterReference>>(m_currentParty);
        }

        public override bool SetSerializedData(ISerializationCallbackReceiver data)
        {
            m_currentParty = ((SerializableDataContainer<List<CharacterReference>>)data).Data;

            return true;
        }
        
        [SerializeField]
        private GameObject m_wagon = default;

        [SerializeField]
        private CharacterReference m_playerCharacter = default;

        [SerializeField]
        private List<CharacterReference> m_currentParty = default;
    }
}