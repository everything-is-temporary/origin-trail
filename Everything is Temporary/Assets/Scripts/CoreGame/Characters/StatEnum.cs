﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatEnum
{
    // Do not change order. If you add stats, add them to the end.
    Health,
    Attack,
    Defense,
    Speed,
    Luck,
    Happiness
}