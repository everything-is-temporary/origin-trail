﻿using UnityEngine;
using System;

namespace Characters
{
    /// <summary>
    /// Movement for actors who are in the player's party. 
    /// </summary>
    public class WagonMovementBehavior : IMovementBehavior<WagonActorScript>
    {
        public WagonMovementBehavior(ActorChainElement chain)
        {
            m_chain = chain;
        }

        /// <summary>
        /// Moves the actor a step (based on the actor's speed) closer to the target position, 
        /// which is mandated by the chain element.
        /// 
        /// Use this in the actor's Update() function to have them follow behind the player.
        /// 
        /// NOTE: this only changes the x value of the actor, since for now each actor should
        /// stay standing on the ground. Might want to change this later, though. 
        /// </summary>
        /// <param name="actor"></param>
        public MovementSummary StepMove(WagonActorScript wagon, Rigidbody2D rigidbody)
        {
            float movementSpeed = wagon.MovementSpeed;
            Vector3 actorPosition = wagon.transform.position;
            float xTarget = m_chain.GetTargetXPosition();

            Vector3 targetVector = new Vector3(xTarget, actorPosition.y, actorPosition.z);

            //get movementSpeed based on how far the actor is from the player
            float distanceBetweenTargetAndActor = Vector3.Distance(targetVector, actorPosition);
            float maxLaggingDistance = 0.5f; // TODO
            if (distanceBetweenTargetAndActor < maxLaggingDistance)
            {
                movementSpeed = Mathf.Lerp(wagon.MovementSpeed * .1f, wagon.MovementSpeed, distanceBetweenTargetAndActor / maxLaggingDistance);
            }

            float newXVelocity;
            Direction? newDirection;

            if (Mathf.Abs(actorPosition.x - xTarget) < 0.01f)
            {
                newXVelocity = 0;
                newDirection = m_chain.GetDirectionToLeader(wagon.transform.position.x);
            }
            else if (actorPosition.x > xTarget)
            {
                newXVelocity = -movementSpeed;
                newDirection = Direction.Left;
            }
            else
            {
                newXVelocity = movementSpeed;
                newDirection = Direction.Right;
            }

            rigidbody.velocity = new Vector2(newXVelocity, rigidbody.velocity.y);
            return new MovementSummary(rigidbody.velocity, newDirection);
        }

        private readonly ActorChainElement m_chain;
    }
}
