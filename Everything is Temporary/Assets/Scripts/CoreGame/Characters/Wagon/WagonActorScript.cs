﻿using System.Threading.Tasks;
using Dialogue;
using GameUtility;
using Interaction;
using UnityEngine;

namespace Characters
{
    /// <summary>
    /// This is a component specifically for the wagon. 
    /// It's basically a skeleton Actor class without any of the interaction / emotes,
    /// since the wagon only has to move with the player. 
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Animator))]
    public class WagonActorScript : MonoBehaviour
    {
        [SerializeField] private float m_movementSpeed = 3f;
        [SerializeField] private bool m_spriteFacesRight = default;

        public float MovementSpeed => m_movementSpeed;
        public Direction FacingDirection { get; set; }

        public void FollowOther(Actor other)
        {
            float spriteWidth = m_spriteRenderer.bounds.size.x;
            
            m_chainElement?.LeaveChain();
            m_chainElement = other.AddFollower(spriteWidth);

            if (m_interactor != null)
                m_interactor.enabled = false;

            m_movementBehavior = new WagonMovementBehavior(m_chainElement);
        }

        /// <summary>
        /// Sets an arbitrary movement behavior for the Actor. The actor will
        /// leave any actor chain and will no longer be controlled by the player
        /// (if it was previously controlled by the player).
        /// </summary>
        /// <param name="movementBehavior">Movement behavior.</param>
        public void SetMovementBehavior(IMovementBehavior<WagonActorScript> movementBehavior)
        {
            m_chainElement?.LeaveChain();
            m_chainElement = null;

            if (m_interactor != null)
                m_interactor.enabled = false;

            m_movementBehavior = movementBehavior;
        }

        private void Awake()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_rigidbody = GetComponent<Rigidbody2D>();
            m_animator = GetComponent<Animator>();
            m_interactor = GetComponent<BasicInteractor>();

            m_actorChain = new ActorChain(m_spriteRenderer.sprite.bounds.size.x);
        }

        private void Start()
        {
            //i dont fuckin know lmfao
            //ActorManager.Instance.RegisterActorInScene(this);
            //InteractionSystem.Instance.RegisterInteractiveObject(this);

            //m_selectionVisual.SetActive(false);
        }

        private void OnDestroy()
        {
            //InteractionSystem.Instance?.UnregisterInteractiveObject(this);
            //ActorManager.Instance?.UnregisterActorFromScene(this);
        }

        private void FixedUpdate()
        {
            if (m_movementBehavior != null)
            {
                // Move the actor.
                MovementSummary pastMove = m_movementBehavior.StepMove(this, m_rigidbody);

                // Animate the actor based on the movement.
                float xVelocity = pastMove.NewVelocity.x;
                m_animator.SetFloat("MovementSpeed", xVelocity / MovementSpeed);
                if (pastMove.FacingDirection != null)
                    FacingDirection = pastMove.FacingDirection.Value;
            }

            bool shouldFlipToFaceRight = !m_spriteFacesRight;
            switch (FacingDirection)
            {
                case Direction.Left:
                    m_spriteRenderer.flipX = !shouldFlipToFaceRight;
                    break;
                case Direction.Right:
                    m_spriteRenderer.flipX = shouldFlipToFaceRight;
                    break;
                default:
                    throw new System.NotSupportedException();
            }
        }

        private ActorChain m_actorChain;
        private ActorChainElement m_chainElement;
        private IMovementBehavior<WagonActorScript> m_movementBehavior;

        private SpriteRenderer m_spriteRenderer;
        private Rigidbody2D m_rigidbody;
        private Animator m_animator;
        private BasicInteractor m_interactor;
    }
}
