﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameUtility;
using GameUtility.DataPersistence;
using GameUtility.Serialization;
using UnityEngine;
using UnityEditor;

namespace Characters
{
    [CreateAssetMenu(fileName = "Character List", menuName = "Character Definition List")]
    public class CharacterDefinitionList : ScriptableObject, ISaveLoadable
    {
        public static CharacterDefinitionList Instance
        {
#if UNITY_EDITOR
            get
            {
                if (_instance == null)
                    _instance = AssetDatabase.LoadAssetAtPath<CharacterDefinitionList>("Assets/Prefabs/_Character List.asset");

                return _instance;
            }
#else
            get => _instance;
#endif
        }
        private static CharacterDefinitionList _instance;

        public GameObject characterEmotePrefab;

        public CharacterDefinition GetCharacter(long id)
        {
            if (m_characters.Dictionary.TryGetValue(id, out CharacterDefinition character))
                return character;
            return null;
        }

        public long GetCharacterIdByName(string characterName)
        {
            return m_characters
                .Where(idCharacterDefinition => String.Equals(idCharacterDefinition.Value.Name, characterName,
                    StringComparison.OrdinalIgnoreCase)).Select(idCharacterDefinition => idCharacterDefinition.Key)
                .FirstOrDefault();
        }

        private void OnEnable()
        {
            if (Instance == null)
            {
                _instance = this;

                SaveManager.RegisterSaveLoadable(this);
            }
        }

        ISerializationCallbackReceiver ISaveLoadable.GetSerializedData()
        {
            var characterData = new SerializableDictionary<long, object>();

            foreach (var idAndChar in m_characters)
                characterData.Add(idAndChar.Key, idAndChar.Value.GetSaveableData());

            return characterData;
        }

        bool ISaveLoadable.SetSerializedData(ISerializationCallbackReceiver data)
        {
            if (data is SerializableDictionary<long, object> savedData)
            {
                foreach (var idAndData in savedData)
                {
                    if (m_characters.TryGetValue(idAndData.Key, out CharacterDefinition character))
                        character.SetSaveableData(idAndData.Value);
                    else
                    {
                        // This can happen if IDs get changed or characters get removed,
                        // in which case it is okay. This can also happen if this object
                        // has not yet been deserialized by Unity, in which case we have
                        // an issue.
                        Debug.LogWarning($"Had save data for a nonexistent character ID ({idAndData.Key}).");
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Generates character ID that has not yet been used within this list.
        /// </summary>
        /// <param name="rand">The random number generator to use.</param>
        /// <returns>A unique (within this list) item identifier.</returns>
        public long GenerateUniqueId(System.Random rand = null)
        {
            if (rand == null) rand = new System.Random();
            long id;

            do
            {
                id = rand.NextLong();
            } while (id == 0 || m_characters.Dictionary.ContainsKey(id));

            return id;
        }
        
#if UNITY_EDITOR
        [ContextMenu("Add New Character")]
        public void CreateNew()
        {
            long newId = GenerateUniqueId(_rand);

            m_characters.Dictionary[newId] = new CharacterDefinition();
        }

        private CharacterDefinitionList()
        {
            m_characters = new CharacterDict
            {
                CreateNewElement = () => new KeyValuePair<long, CharacterDefinition>(GenerateUniqueId(_rand), null)
            };
        }

        private static System.Random _rand = new System.Random();
#endif

        [SerializeField]
        [SerializableDictionary(keyIsLongId: true)]
        private CharacterDict m_characters;

        [Serializable]
        private class CharacterDict : SerializableDictionary<long, CharacterDefinition> { }
    }
}
