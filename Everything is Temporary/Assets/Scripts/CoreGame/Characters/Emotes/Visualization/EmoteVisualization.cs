using System;
using CoreGame.Characters.Emotes.Visualization;
using UnityEngine;

namespace Characters.Emotes.Abstractions
{
    public abstract class EmoteVisualization : MonoBehaviour
    {

        public EmoteVisualizationType VisualizationType => m_emoteData.EmoteReference.Emote.EmoteVisualizationType;
        public EmoteData EmoteData => m_emoteData;
        
        public void Create(EmoteData emoteData, Actor actor)
        {
            m_emoteData = emoteData;
            Setup(emoteData, actor);
        }

        protected abstract void Setup(EmoteData emoteData, Actor actor);

        public abstract void Display();

        public virtual void Teardown()
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }

        private EmoteData m_emoteData;
    }
}