using System;

namespace CoreGame.Characters.Emotes.Visualization
{
    [Flags]
    public enum EmoteVisualizationType
    {
        None = 0b0,
        Bubble = 0b1,
        Movement = 0b10,
        All = int.MinValue,
        
        BubbleAndMovement = Bubble | Movement
    }
}