﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Characters;
using Characters.Emotes;
using Characters.Emotes.Abstractions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using VFX.TweenAnimations;

namespace GameUI.Emotes
{
    public class ActorEmoteController : EmoteVisualization
    {        
        protected override void Setup(EmoteData emoteData, Actor actor)
        {
            DOTween.Kill(actor.transform);
            gameObject.SetActive(false);
            if (emoteData.EmoteReference.Emote.ActorTweenAnimation != null)
            {
                m_tweenEnsemble = new TweenEnsemble(emoteData.EmoteReference.Emote.ActorTweenAnimation.GetTweensForActor(actor));
            }
        }

        public override void Display()
        {
            m_tweenEnsemble?.Start();
            gameObject.SetActive(true);
        }

        public override void Teardown()
        {
            m_tweenEnsemble?.Stop();
            base.Teardown();
        }

        private TweenEnsemble m_tweenEnsemble;
    }
}