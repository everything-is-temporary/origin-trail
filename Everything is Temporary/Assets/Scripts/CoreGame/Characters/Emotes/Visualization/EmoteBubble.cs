﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Characters;
using Characters.Emotes;
using Characters.Emotes.Abstractions;
using UnityEngine;
using UnityEngine.UI;
using VFX.TweenAnimations;

namespace GameUI.Emotes
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class EmoteBubble : EmoteVisualization
    {
        [SerializeField] private float yPositionPadding = 0.1f;
        private BoxCollider2D TriggerCollider => gameObject.GetComponent<BoxCollider2D>();

        public SpriteRenderer EmoteIcon => m_emoteIcon;
        
        public List<EmoteTween> AppliedTweens { get; private set; }

        protected override void Setup(EmoteData emoteData, Actor actor)
        {
            gameObject.SetActive(false);
            EmoteIcon.sprite = emoteData.EmoteReference.Emote.BubbleEmoteSprite;
            RepositionEmoteBubble(actor.GetComponent<SpriteRenderer>());
            if (emoteData.EmoteReference.Emote.SpriteTweenAnimation != null)
            {
                m_tweenEnsemble = new TweenEnsemble(emoteData.EmoteReference.Emote.SpriteTweenAnimation.GetTweensForRenderer(m_emoteIcon));
            }
        }

        private void RepositionEmoteBubble(SpriteRenderer belowRenderer)
        {
            var topCenterOfBelowRenderer = belowRenderer.bounds.center + new Vector3(0f, belowRenderer.bounds.extents.y, 0f);
            var rise = GetComponent<SpriteRenderer>().bounds.extents.y;
            transform.position = topCenterOfBelowRenderer + new Vector3(0f,rise + yPositionPadding, 0f);
        }

        public override void Display()
        {
            m_tweenEnsemble?.Start();
            gameObject.SetActive(true);
        }

        public override void Teardown()
        {
            m_tweenEnsemble?.Stop();
            base.Teardown();
        }

        private TweenEnsemble m_tweenEnsemble = default;

        [SerializeField] private SpriteRenderer m_emoteIcon = default;
    }
}