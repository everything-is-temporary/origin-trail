using UnityEngine;

namespace Characters.Emotes
{
    public class EmoteData
    {
        public EmoteData(EmoteReference emoteReference, EmoteDurationType emoteDurationType, float? emoteDurationOverride, float? emoteStrengthOverride)
        {
            m_emoteReference = emoteReference;
            m_emoteDurationType = emoteDurationType;
            m_emoteDurationValue = emoteDurationOverride;
            m_emoteStrengthValue = emoteStrengthOverride;
        }

        public EmoteData(string emoteName)
        {
            m_emoteReference = EmoteReference.FromString(emoteName);
            if (m_emoteReference.Emote == null)
            {
                Debug.LogWarning($"Unable to find emote for given string: '{emoteName}'");
            }
            m_emoteDurationType = EmoteDurationType.NextLine;
            m_emoteDurationValue = null;
            m_emoteStrengthValue = null; 
        }
            
        public EmoteReference EmoteReference => m_emoteReference;
        public EmoteDurationType DurationType => m_emoteDurationType;
        public float? Duration => m_emoteDurationValue;
        public float? Stength => m_emoteStrengthValue;

        private EmoteReference m_emoteReference;
        private EmoteDurationType m_emoteDurationType;

        private float? m_emoteDurationValue;
        private float? m_emoteStrengthValue;
    }
}