using GameUtility;
using UnityEngine;

namespace Characters.Emotes
{
    public class EmoteReference
    {
        #region Public Interface

        public EmoteDefinition Emote
        {
            get => GetEmote();
        }

        #endregion // Public Interface

        public EmoteReference(long id)
        {
            m_id = id;
        }

        public static EmoteReference FromId(long id)
        {
            return new EmoteReference(id);
        }

        public static EmoteReference FromString(string s)
        {
            return new EmoteReference(EmoteDefinitionList.Instance.GetEmoteIdForString(s));
        }

        private EmoteDefinition GetEmote()
        {
            return EmoteDefinitionList.Instance.GetEmote(m_id);
        }

        [SerializeField, IdField] private long m_id;
    }
}