using System.Collections.Generic;
using CoreGame.Characters.Emotes.Visualization;
using UnityEngine;
using UnityEngine.Serialization;
using VFX.TweenAnimations;

namespace Characters.Emotes
{
    [CreateAssetMenu]
    public class EmoteDefinition : ScriptableObject
    {
        public string EmoteName => m_emoteName;
        public EmoteVisualizationType EmoteVisualizationType => m_emoteVisualizationType;
        public Sprite BubbleEmoteSprite => m_bubbleEmoteSprite;
        public SpriteTweenAnimation SpriteTweenAnimation => m_bubbleTweenAnimation;
        public ActorTweenAnimation ActorTweenAnimation => m_actorTweenAnimation;
        
        private void OnValidate() // Using this for automatic editor validation of additions
        {
            if (!m_editorAutoUpdateVisualizationType) return;

            m_emoteVisualizationType = EmoteVisualizationType.None;
            
            if (m_bubbleEmoteSprite != null)
            {
                m_emoteVisualizationType |= EmoteVisualizationType.Bubble;
            }

            if (m_actorTweenAnimation != null)
            {
                m_emoteVisualizationType |= EmoteVisualizationType.Movement;
            }
        }

        [SerializeField] private bool m_editorAutoUpdateVisualizationType = true;
        [SerializeField] private EmoteVisualizationType m_emoteVisualizationType = EmoteVisualizationType.None;
        [SerializeField] private string m_emoteName = default;
        [SerializeField] private Sprite m_bubbleEmoteSprite = default;
        [SerializeField] private SpriteTweenAnimation m_bubbleTweenAnimation = default;
        [SerializeField] private ActorTweenAnimation m_actorTweenAnimation = default;
        private long m_id;
    }
}