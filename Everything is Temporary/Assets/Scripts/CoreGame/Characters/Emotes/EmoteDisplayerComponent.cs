using System.Collections.Generic;
using System.Linq;
using Characters.Emotes.Abstractions;
using CoreGame.Characters.Emotes.Visualization;
using Dialogue;
using GameUI.Emotes;
using GameUtility;
using UnityEngine;

namespace Characters.Emotes
{
    /// <summary>
    /// Handles setting up the physical representation of emotes on a per-actor level. Handles timing for emote instantiation and teardown by subscribing to events.
    /// </summary>
    [RequireComponent(typeof(Actor))]
    public class EmoteDisplayerComponent : MonoBehaviour
    {
        public Actor Actor => m_actor;

        private void HandleStoryElementInvoked(StoryElement storyElement)
        {
            if (storyElement is DialogueElement dialogueElement)
            {
                CheckEmoteDuration(dialogueElement.Sources.ConvertAll(name => CharacterReference.FromName(name)).Contains(Actor.Character) ? EmoteDurationType.NextOwnLine : EmoteDurationType.NextOtherLine);
            }
        }

        private void HandleDialogueEnd()
        {
            CheckEmoteDuration(EmoteDurationType.EndedByDialogueEnd);
        }

        private void CheckEmoteDuration(EmoteDurationType emoteDurationType)
        {
            foreach (EmoteVisualization emoteVisualization in m_emoteVisualizations.ToList())
            {
                if (emoteDurationType.ContainsAny(emoteVisualization.EmoteData.DurationType))
                {
                    RemoveOne(emoteVisualization);
                }
            }
        }

        public void DisplayEmote(EmoteData emoteData)
        {
            Debug.Assert(Actor != null);
            EmoteDefinition emote = emoteData.EmoteReference.Emote;
            if (emote == null)
            {
                Debug.LogWarning($"EmoteDefinition not found for reference {emoteData.EmoteReference}.");
                return;
            }

            // Remove any emotes that share a visualization type with this one
            foreach (EmoteVisualization visualization in m_emoteVisualizations.ToList())
            {
                if (visualization != null && visualization.VisualizationType.ContainsAny(emoteData.EmoteReference.Emote.EmoteVisualizationType))
                {
                    RemoveOne(visualization);
                }
            }

            EmoteVisualization emoteVisualization;
            foreach (EmoteVisualization visualizationPrefab in EmoteDefinitionList.Instance.GetVisualizationsForEmoteType(emoteData.EmoteReference.Emote.EmoteVisualizationType))
            {
                emoteVisualization = Instantiate(visualizationPrefab, Actor.transform);
                emoteVisualization.Create(emoteData, Actor);
                emoteVisualization.Display();
                m_emoteVisualizations.Add(emoteVisualization);
            }                
        }

        private void RemoveOne(EmoteVisualization emoteVisualization)
        {
            m_emoteVisualizations.Remove(emoteVisualization);
            emoteVisualization.Teardown();
        }

        public void Teardown()
        {
            foreach (EmoteVisualization visualizationComponent in m_emoteVisualizations)
            {
                visualizationComponent.Teardown();
            }
            m_emoteVisualizations.Clear();
        }

        private void Awake()
        {
            m_actor = gameObject.GetComponent<Actor>();
        }
        
        private void Start()
        {
            StoryElement.OnStoryElementHandled += HandleStoryElementInvoked;
            StoryManager.Instance.OnEndReached += HandleDialogueEnd;
        }

        private void OnDestroy()
        {
            StoryElement.OnStoryElementHandled -= HandleStoryElementInvoked;
            StoryManager.Instance.OnEndReached -= HandleDialogueEnd;
            Teardown();
        }

        private readonly HashSet<EmoteVisualization> m_emoteVisualizations = new HashSet<EmoteVisualization>();
        private Actor m_actor = default;
    }
}