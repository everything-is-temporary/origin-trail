using System;
using System.Reflection;
using CombatSim.Utility.Enumerations;
using Dialogue;

namespace Characters.Emotes
{
    [Flags]
    public enum EmoteDurationType
    {
        None = 0b0,
        Timed = 0b1,
        NextOtherLine = 0b10,
        NextOwnLine = 0b100,
        EndOfDialogue = 0b1000,
        
        NextLine = NextOwnLine | NextOtherLine,
        EndedByDialogueEnd = NextLine | EndOfDialogue
    }
}