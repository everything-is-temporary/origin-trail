using System;
using System.Collections.Generic;
using System.Linq;
using Characters.Emotes.Abstractions;
using CoreGame.Characters.Emotes.Visualization;
using GameUI.Emotes;
using GameUtility;
using GameUtility.Serialization;
using UnityEditor;
using UnityEngine;

namespace Characters.Emotes
{
    public class EmoteDefinitionList : ScriptableObject
    {
        public EmoteBubble EmoteBubblePrefab => m_emoteBubblePrefab;
        public ActorEmoteController ActorEmoteControllerPrefab => m_actorEmoteController;
                        
        public static EmoteDefinitionList Instance
        {
#if UNITY_EDITOR
            get
            {
                if (_instance == null)
                    _instance = AssetDatabase.LoadAssetAtPath<EmoteDefinitionList>(
                        "Assets/Prefabs/_Emote List.asset");
                return _instance;
            }
#else
            get => _instance;
#endif
        }

        public long GetEmoteIdForString(string s)
        {
            return emoteDefinitions
                .Where(idDefinition =>
                    String.Equals(idDefinition.Value.EmoteName, s, StringComparison.CurrentCultureIgnoreCase))
                .Select(idDefinition => idDefinition.Key).FirstOrDefault();
        }

        public EmoteDefinition GetEmote(long id)
        {
            if (emoteDefinitions.TryGetValue(id, out EmoteDefinition emoteDefinition))
                return emoteDefinition;
            return null;
        }

        public List<EmoteVisualization> GetVisualizationsForEmoteType(EmoteVisualizationType emoteVisualizationType)
        {
            List<EmoteVisualization> emoteVisualizations = new List<EmoteVisualization>();
            if (emoteVisualizationType.ContainsAll(EmoteVisualizationType.Bubble))
            {
                emoteVisualizations.Add(m_emoteBubblePrefab);
            }

            if (emoteVisualizationType.ContainsAll(EmoteVisualizationType.Movement))
            {
                emoteVisualizations.Add(m_actorEmoteController);
            }

            return emoteVisualizations;
        }
        
        #if UNITY_EDITOR
        public void AddEmoteDefinition(EmoteDefinition emoteDefinition)
        {
            if (!emoteDefinitions.Dictionary.ContainsValue(emoteDefinition))
            {
                emoteDefinitions.Add(GenerateUniqueId(), emoteDefinition);
            }            
        }

        public void CleanNullEmotes() //Same as RemoveNullShops in ShopDefinitionList
        {
            foreach (KeyValuePair<long,EmoteDefinition> idEmoteDefinitionPair in emoteDefinitions.ToList())
            {
                if (idEmoteDefinitionPair.Value == null)
                {
                    emoteDefinitions.Remove(idEmoteDefinitionPair.Key);
                }
            }
        }
        
        public long GenerateUniqueId()
        {
            long id;
            do
            {
                id = _rand.NextLong();
            } while (id == 0 || emoteDefinitions.ContainsKey(id));

            return id;
        }
        
        #endif // UNITY_EDITOR
        
        private void OnEnable()
        {
            if (Instance == null)
            {
                _instance = this;
            }
        }

        [SerializeField] private EmoteBubble m_emoteBubblePrefab = default;
        [SerializeField] private ActorEmoteController m_actorEmoteController = default;
        
        [SerializeField]
        [SerializableDictionary(readonlyKey: false, showAddButton: true, showDeleteButton: true, keyIsLongId: true)]
        private EmoteDict emoteDefinitions = default;
        
        [Serializable]
        private class EmoteDict : SerializableDictionary<long, EmoteDefinition> { }
        
        private static EmoteDefinitionList _instance = default;
        
        private static readonly System.Random _rand = new System.Random();
    }
}