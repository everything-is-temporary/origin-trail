﻿using System;
using UnityEngine;

namespace Characters
{
    /// <summary>
    /// Stats of an object accessed using <see cref="StatEnum"/> keys.
    /// </summary>
    [Serializable]
    public class Stats : ISerializationCallbackReceiver
    {
        public static readonly StatEnum[] AllStats = (StatEnum[])Enum.GetValues(typeof(StatEnum));
        public static readonly string[] StatNames = Enum.GetNames(typeof(StatEnum));

        #region Accessor Methods for Common Stats

        public double Health => Get(StatEnum.Health);
        public double Attack => Get(StatEnum.Attack);
        public double Defense => Get(StatEnum.Defense);

        #endregion

        /// <summary>
        /// Get the specified stat.
        /// </summary>
        /// <returns>The value for the requested <paramref name="stat"/>.</returns>
        /// <param name="stat">The <see cref="StatEnum"/> for which to retrieve the value.</param>
        public double Get(StatEnum stat)
        {
            return m_statValues[(int)stat];
        }

        /// <summary>
        /// Set the specified stat to the provided value.
        /// </summary>
        /// <param name="stat">The <see cref="StatEnum"/> to set.</param>
        /// <param name="value">The new value.</param>
        public void Set(StatEnum stat, double value)
        {
            m_statValues[(int)stat] = value;
        }

        /// <summary>
        /// Change the specified stat by some amount.
        /// </summary>
        /// <param name="stat">The <see cref="StatEnum"/> to change.</param>
        /// <param name="delta">The amount by which to change <paramref name="stat"/>.</param>
        public void Change(StatEnum stat, double delta)
        {
            m_statValues[(int)stat] += delta;
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            if (m_statValues == null || m_statValues.Length < AllStats.Length)
            {
                double[] newValues = new double[AllStats.Length];

                m_statValues?.CopyTo(newValues, 0);
                m_statValues = newValues;
            }
            else if (m_statValues.Length > AllStats.Length)
            {
                m_statValues = new double[AllStats.Length];
            }
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
        }

        [SerializeField]
        private double[] m_statValues;
    }
}
