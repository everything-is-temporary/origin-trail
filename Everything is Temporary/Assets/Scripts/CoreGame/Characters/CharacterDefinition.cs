﻿using System;
using System.Collections.Generic;
using GameInventory;
using GameUtility;
using UnityEngine;
using Camping;
using GameUtility.Serialization;

namespace Characters
{
    /// <summary>
    /// Contains all of the data that defines a character.
    /// </summary>
    [Serializable]
    public class CharacterDefinition
    {
        public string Name => m_name;

        public Sprite Icon => m_icon;

        public Stats Stats => m_stats;

        public bool CanInteractWithDuringCamping => m_canInteractWithDuringCamping;
        
        public string CampingKnotPath => m_campingKnotPath;

        public string CampingKnotPathR2 => m_campingKnotPathR2;

        public string GiveItemKnotPath => m_giveItemKnotPath;

        public CampingActorScript CampingPrefab
        {
            get
            {
                if (m_campingPrefab == null)
                    Debug.LogError($"Camping Prefab not set on character '{Name}'");

                return m_campingPrefab;
            }
        }
        
        public Inventory Inventory => m_inventory;

        /// <summary>
        /// The prefab to use for this character in a sidescroller scene.
        /// </summary>
        /// <value>The scene prefab.</value>
        public GameObject ScenePrefab
        {
            get
            {
                if (m_scenePrefab == null)
                    Debug.LogError($"Scene Prefab not set on character '{Name}'");

                return m_scenePrefab;
            }
        }

        /// <summary>
        /// The prefab to use for this character in a combat scene.
        /// </summary>
        /// <value>The combat prefab.</value>
        public Combat.BaseCombatActor CombatPrefab
        {
            get
            {
                if (m_combatPrefab == null)
                {
                    Debug.LogError($"Combat Prefab not set on character '{Name}'");
                    return null;
                }

                return (Combat.BaseCombatActor)m_combatPrefab;
            }
        }

        /// <summary>
        /// Gets the data that is saveable for this character.
        /// </summary>
        /// <returns>The saveable data.</returns>
        public object GetSaveableData()
        {
            return new Dictionary<string, object>
            {
                {"Name", m_name},
                {"Stats", m_stats},
                {"Inventory", m_inventory}
            };
        }

        /// <summary>
        /// Updates the character based on some saved data.
        /// </summary>
        /// <param name="data">The data that came from <see cref="GetSaveableData"/>.</param>
        public void SetSaveableData(object data)
        {
            if (data is Dictionary<string, object> savedData)
            {
                if (savedData.TryGetValue("Name", out object nameObj))
                {
                    m_name = (string) nameObj;
                }

                if (savedData.TryGetValue("Stats", out object statsObj))
                {
                    m_stats = (Stats) statsObj;
                }

                if (savedData.TryGetValue("Inventory", out object inventoryObj))
                {
                    m_inventory = (Inventory) inventoryObj;
                }
            }
        }

        [SerializeField]
        private string m_name = default;

        [SerializeField]
        private Sprite m_icon = default;

        [SerializeField]
        private Stats m_stats = default;

        [SerializeField] [Tooltip("Enable interaction during the camping scene.")]
        private bool m_canInteractWithDuringCamping = true;
        
        [SerializeField]
        [Tooltip("The knot path to use for the first interaction per camping scene with this character when camping.")]
        private string m_campingKnotPath = default;

        [SerializeField] [Tooltip("The knot path to use for the second or later interaction when camping (to allow for one interaction per scene).")]
        private string m_campingKnotPathR2 = default;

        [SerializeField]
        [Tooltip("The knot to call when invoking the 'give item' menu option during the campfire scene.")]
        private string m_giveItemKnotPath = default;

        [SerializeField]
        [Tooltip("The prefab to use for this character when camping.")]
        private CampingActorScript m_campingPrefab = default;

        [SerializeField]
        [Tooltip("The character's prefab for sidescroller scenes.")]
        private GameObject m_scenePrefab = default;

        [SerializeField]
        [Tooltip("The character's prefab for combat scenes.")]
        [RequiresType(typeof(Combat.BaseCombatActor))]
        private UnityEngine.Object m_combatPrefab = default;

        [SerializeField] private Inventory m_inventory = default;
    }
}
