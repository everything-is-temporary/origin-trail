﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;

public enum InputType
{
    InteractKey,
    ToggleBookKey,
    EscapeKey
}

/// <summary>
/// Class to use instead of UnityEngine.Input.
/// </summary>
public class GameInputManager : SingletonMonoBehavior<GameInputManager>
{
    [SerializeField]
    private bool m_isMovementEnabled = true;

    [SerializeField]
    private bool m_isInteractionEnabled = true;

    public bool IsMovementEnabled => m_isMovementEnabled;

    public bool IsPageFlipEnabled { get; set; }

    /// <summary>
    /// This "interaction" is for interacting with Interactive Objects.
    /// </summary>
    public bool IsInteractionEnabled => m_isInteractionEnabled;

    public float GetHorizontalAxis()
    {
        if (m_isMovementEnabled)
            return Input.GetAxis("Horizontal");

        return 0;
    }

    /// <summary>
    /// Toggles whether the player should be able to move or interact with objects.
    /// This is generally toggled simultaneously, hence this method.
    /// </summary>
    public void ToggleMovementAndInteraction(bool enabled)
    {
        m_isInteractionEnabled = enabled;
        m_isMovementEnabled = enabled;
    }

    public void RegisterAsInputHandler(InputType t, Func<bool> handler)
    {
        if (!m_inputHandlers[(int)t].Contains(handler))
        {
            m_inputHandlers[(int)t].Add(handler);
        }
    }

    public void UnregisterAsInputHandler(InputType t, Func<bool> handler)
    {
        m_inputHandlers[(int)t].Remove(handler);
    }

    public void RegisterAsHorizontalAxisInputHandler(Func<float, bool> handler, bool continuously)
    {
        if (!m_horizontalAxisInputHandlers.ContainsKey(handler))
        {
            m_horizontalAxisInputHandlers.Add(handler, continuously);
        }
    }

    public void UnregisterAsHorizontalAxisInputHandler(Func<float, bool> handler)
    {
        m_horizontalAxisInputHandlers.Remove(handler);
    }

    protected override void PostAwake()
    {
        for (int i = 0; i < TotalInputTypes; i++)
        {
            m_inputHandlers[i] = new List<Func<bool>>();
        }

        m_inputDetectors[(int)InputType.InteractKey] = GetInteractKey;
        m_inputDetectors[(int)InputType.ToggleBookKey] = GetToggleBookKey;
        m_inputDetectors[(int)InputType.EscapeKey] = GetEscapeKey;
    }

    private void Update()
    {
        // Iterate through all input detector functions.
        for (int inputTypeIndex = 0; inputTypeIndex < TotalInputTypes; inputTypeIndex++)
        {
            if (m_inputDetectors[inputTypeIndex].Invoke() == true)
            {
                List<Func<bool>> handlers = m_inputHandlers[inputTypeIndex];

                // Iterate through all input handlers.
                for (int handlerIndex = handlers.Count - 1; handlerIndex >= 0; handlerIndex--)
                {
                    if (handlers[handlerIndex].Invoke() == true)
                    {
                        break;
                    }
                }
            }
        }

        foreach (KeyValuePair<Func<float, bool>, bool> pair in m_horizontalAxisInputHandlers)
        {
            Func<float, bool> handler = pair.Key;
            bool continuously = pair.Value;

            float xInput = Input.GetAxis("Horizontal");
            if (continuously && xInput != 0.0f)
            {
                if (handler.Invoke(xInput) == true)
                {
                    break;
                }
            }

            float xInputRaw = Input.GetAxisRaw("Horizontal");
            if (!continuously && xInputRaw != 0.0f && Input.GetButtonDown("Horizontal"))
            {
                if (handler.Invoke(xInputRaw) == true)
                {
                    break;
                }
            }
        }
    }

    #region Input detector functions
    private bool GetInteractKey()
    {
        return Input.GetButtonUp("Interact");
    }

    private bool GetToggleBookKey()
    {
        return Input.GetButtonUp("ToggleBook");
    }

    private bool GetEscapeKey()
    {
        return Input.GetButtonUp("EscapeKey");
    }
    #endregion

    private static readonly int TotalInputTypes = Enum.GetNames(typeof(InputType)).Length;
    private readonly Func<bool>[] m_inputDetectors = new Func<bool>[TotalInputTypes];
    private List<Func<bool>>[] m_inputHandlers = new List<Func<bool>>[TotalInputTypes];

    /// <summary>
    /// These handlers all handle horizontal axis input.
    /// Each handler is mapped to / marked with a bool,
    /// Which specifies if it handles continuous (true) or discrete (false) input.
    /// </summary>
    private Dictionary<Func<float, bool>, bool> m_horizontalAxisInputHandlers = new Dictionary<Func<float, bool>, bool>();
}
