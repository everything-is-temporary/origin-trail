using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Characters;
using UnityEngine;

namespace GameInventory
{
    [Serializable]
    public class Inventory : IModifiableInventory, ISerializationCallbackReceiver
    {
        
        #region Events and Delegates

        public event ItemAmountChangedHandler OnItemAmountChanged;

        #endregion

        #region Item Access

        /// <summary>
        /// Sets a specified item's quantity to the given value.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount to set.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        public ItemWithAmount SetItem(ItemWithAmount itemWithAmount)
        {
            ItemWithAmount oldAmount;

            if (m_itemStacks.TryGetValue(itemWithAmount.ItemReference, out oldAmount))
            {
                if (itemWithAmount.Amount <= 0)
                {
                    m_itemStacks.Remove(itemWithAmount);
                }
                else
                {
                    m_itemStacks[itemWithAmount.ItemReference] = itemWithAmount;
                }
            }
            else
            {
                oldAmount = itemWithAmount.WithAmount(0);

                if (itemWithAmount.Amount <= 0)
                {
                    return itemWithAmount.WithAmount(0);
                }

                m_itemStacks.Add(itemWithAmount.ItemReference, itemWithAmount);
            }

            OnItemAmountChanged?.Invoke(oldAmount, itemWithAmount);
            return itemWithAmount;
        }

        /// <summary>
        /// Sets a specified item's quantity to the given value.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to set.</param>
        /// <param name="newItemQuantity">The quantity of the item to set.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        public ItemWithAmount SetItem(ItemReference itemReference, int newItemQuantity)
        {
            return SetItem(new ItemWithAmount(itemReference, newItemQuantity));
        }

        public bool ContainsItem(ItemReference itemReference)
        {
            return m_itemStacks.ContainsKey(itemReference);
        }


        public bool ContainsItem(ItemTag itemTag)
        {
            return m_itemStacks.Values.Any(itemStack => itemStack.Item.Tags.Contains(itemTag));
        }

        public bool ContainsItemQuantity(ItemWithAmount itemWithAmount)
        {
            return ContainsItemQuantity(itemWithAmount.ItemReference, itemWithAmount.Amount);
        }

        public bool ContainsItemQuantity(ItemReference itemReference, int minQuantity)
        {
            return CountItem(itemReference) >= minQuantity;
        }

        public bool ContainsItemQuantity(ItemTag itemTag, int minQuantity)
        {
            int itemSum = m_itemStacks.Values.Where(itemStack => itemStack.Item.Tags.Contains(itemTag))
                .Select(itemStack => itemStack.Amount).Sum();
            return itemSum >= minQuantity;
        }

        public ItemWithAmount GetItem(ItemReference itemReference)
        {
            if (m_itemStacks.TryGetValue(itemReference, out ItemWithAmount internalStack))
            {
                return internalStack;
            }

            return new ItemWithAmount(itemReference, 0);
        }

        public int CountItem(ItemReference itemReference)
        {
            return GetItem(itemReference).Amount;
        }

        public int CountItem(ItemTag itemTag)
        {
            return m_itemStacks.Values.Where(itemStack => itemStack.Item.Tags.Contains(itemTag)).Select(itemWithAmount => itemWithAmount.Amount).Sum();
        }

        public ItemWithAmount ModifyItem(ItemWithAmount itemWithAmount)
        {
            if (m_itemStacks.TryGetValue(itemWithAmount.ItemReference, out ItemWithAmount internalStack))
            {
                return SetItem(itemWithAmount + internalStack);
            }

            return SetItem(itemWithAmount);
        }

        public ItemWithAmount ModifyItem(ItemReference itemReference, int deltaChange)
        {
            return ModifyItem(new ItemWithAmount(itemReference, deltaChange));
        }

        /// <summary>
        /// A function to modify and return the actual change in quantity of a specified item.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount of the item and the quantity to change it by.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the change in the held quantity of the item.</returns>
        public ItemWithAmount ModifyAndGetDelta(ItemWithAmount itemWithAmount)
        {
            ItemWithAmount existingQuantity = GetItem(itemWithAmount);
            return ModifyItem(itemWithAmount) - existingQuantity;
        }


        public ItemWithAmount ModifyAndGetDelta(ItemReference itemReference, int deltaAmount)
        {
            return ModifyAndGetDelta(new ItemWithAmount(itemReference, deltaAmount));
        }

        public ItemWithAmount AddAndGetDelta(ItemWithAmount itemWithAmount)
        {
            ItemWithAmount existingQuantity = GetItem(itemWithAmount);
            return AddItem(itemWithAmount) - existingQuantity;
        }

        public ItemWithAmount AddAndGetDelta(ItemReference itemReference, int amountToAdd)
        {
            return AddAndGetDelta(new ItemWithAmount(itemReference, amountToAdd));
        }

        public ItemWithAmount RemoveAndGetDelta(ItemWithAmount itemWithAmount)
        {
            ItemWithAmount existingQuantity = GetItem(itemWithAmount);
            return existingQuantity - RemoveItem(itemWithAmount);
        }

        public ItemWithAmount RemoveAndGetDelta(ItemReference itemReference, int amountToRemove)
        {
            return RemoveAndGetDelta(new ItemWithAmount(itemReference, amountToRemove));
        }

        public int SetZero(ItemReference itemReference)
        {
            int previousAmount = CountItem(itemReference);
            SetItem(itemReference, 0);
            return previousAmount;
        }

        /// <inheritdoc />
        /// <exception cref="T:System.ArgumentException">Fails if the amount of items to add is negative.</exception>
        public ItemWithAmount AddItem(ItemWithAmount itemWithAmount)
        {
            if (itemWithAmount.Amount < 0)
            {
                throw new ArgumentException($"Cannot add a negative amount ({itemWithAmount.Amount}) of item {itemWithAmount.Item.Name}.");
            }

            if (itemWithAmount.Amount == 0)
            {
                Debug.LogWarning($"Attempting to add 0 of item. {itemWithAmount.Item.Name}");
            }

            return ModifyItem(itemWithAmount);
        }

        /// <inheritdoc />
        /// <exception cref="ArgumentException">Fails if the amount of items to add is negative.</exception>
        public ItemWithAmount AddItem(ItemReference itemReference, int amountToAdd)
        {
            return AddItem(new ItemWithAmount(itemReference, amountToAdd));
        }

        /// <inheritdoc />
        /// <exception cref="ArgumentException">Fails if the amount of items to add is negative.</exception>
        public ItemWithAmount RemoveItem(ItemWithAmount itemWithAmount)
        {
            if (itemWithAmount.Amount < 0)
            {
                throw new ArgumentException($"Cannot remove a negative amount ({itemWithAmount.Amount}) of item {itemWithAmount.Item.Name}.");
            }

            if (itemWithAmount.Amount == 0)
            {
                Debug.LogWarning($"Attempting to remove 0 of item. {itemWithAmount.Item.Name}");
            }

            return ModifyItem(-itemWithAmount);
        }

        /// <inheritdoc />
        /// <exception cref="ArgumentException">Fails if the amount of items to add is negative.</exception>
        public ItemWithAmount RemoveItem(ItemReference itemReference, int amountToRemove)
        {
            return RemoveItem(new ItemWithAmount(itemReference, amountToRemove));
        }

        #endregion //Item Access

        IEnumerator<ItemWithAmount> IEnumerable<ItemWithAmount>.GetEnumerator()
        {
            return m_itemStacks.Values.GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)m_itemStacks.Values).GetEnumerator();
        }

        #region ISerializationCallbackReceiver implementation
        public void OnAfterDeserialize()
        {
            m_itemStacks = new Dictionary<ItemReference, ItemWithAmount>();

            foreach (ItemWithAmount stack in m_itemStacksSerialized)
            {
                // Skip invalid items.
                if (stack.ItemReference.IsInvalid)
                    continue;

                ItemWithAmount stackToInsert = stack;
                // Skip items with nonpositive amount.
                if (stack.Amount <= 0)
                {
                    Debug.LogWarning($"{stack.Item.Name} had non-positive amount ({stack.Amount}). Setting to 1.");
                    stackToInsert = new ItemWithAmount(stack.ItemReference, 1);
                }

                if (m_itemStacks.TryGetValue(stackToInsert.ItemReference, out ItemWithAmount existing))
                {
                    // Can merge stacks on conflict.
                    m_itemStacks[stackToInsert.ItemReference] = existing.WithAmount(existing.Amount + stackToInsert.Amount);
                }
                else
                {
                    m_itemStacks[stackToInsert.ItemReference] = stackToInsert;
                }
            }

            // The serialized data doesn't need to be kept in memory after this point.
            m_itemStacksSerialized = null;
        }

        public void OnBeforeSerialize()
        {
            m_itemStacksSerialized = m_itemStacks?.Values.ToList() ?? new List<ItemWithAmount>();
        }

        [SerializeField]
        private List<ItemWithAmount> m_itemStacksSerialized = new List<ItemWithAmount>();

        #endregion

        private Dictionary<ItemReference, ItemWithAmount> m_itemStacks = new Dictionary<ItemReference, ItemWithAmount>();
        
        private CharacterReference m_characterReference;
    }
}
