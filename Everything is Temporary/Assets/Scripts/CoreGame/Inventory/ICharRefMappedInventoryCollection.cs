using System.Collections.Generic;
using Characters;

namespace GameInventory
{
    public interface ICharRefMappedInventoryCollection
    {
        IEnumerable<KeyValuePair<IModifiableInventory, CharacterReference>> InventoryCharacterReferences { get; }
    }
}