﻿using System.Linq;
using UnityEngine;

using GameInventory;
using Characters;

/// <summary>
/// A test script that gives a character items on keypress.
/// Change CharacterIndex to specify the character.
/// </summary>
public class TestGiveCharacterItemsOnKeypress : MonoBehaviour
{
    [SerializeField]
    private ItemReference itemToGive = default;

    public int CharacterIndex;

    private void Update()
    {
        CharacterReference targetCharRef = null;
        if (CharacterManager.Instance.CurrentParty.Count() - 1 >= CharacterIndex)
        {
            targetCharRef = CharacterManager.Instance.CurrentParty.ElementAt(CharacterIndex);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            targetCharRef.Character.Inventory.ModifyItem(new ItemWithAmount(itemToGive, 1));
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            targetCharRef.Character.Inventory.ModifyItem(new ItemWithAmount(itemToGive, -1));
        }

    }
}
