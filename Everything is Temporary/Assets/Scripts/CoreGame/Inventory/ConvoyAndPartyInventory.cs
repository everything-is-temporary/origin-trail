using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Characters;
using GameInventory.Utility;

namespace GameInventory
{
    public class ConvoyAndPartyInventory : IModifiableInventory, IDisposable, ICharRefMappedInventoryCollection
    {

        public IEnumerable<KeyValuePair<IModifiableInventory, CharacterReference>> InventoryCharacterReferences =>
            m_partyInventory.InventoryCharacterReferences.Union(new List<KeyValuePair<IModifiableInventory, CharacterReference>>
                {new KeyValuePair<IModifiableInventory, CharacterReference>(m_convoyInventory, null)});

        public IEnumerable<KeyValuePair<IModifiableInventory, CharacterReference>>
            PartyOnlyInventoryCharacterReferences => m_partyInventory.InventoryCharacterReferences;

        public ConvoyAndPartyInventory(Inventory convoyItems, IEnumerable<Inventory> partyItems = null)
        {
            m_convoyInventory = convoyItems;
            m_partyInventory = partyItems == null ? new InventoryCollection() : new InventoryCollection(partyItems);
            m_jointInventory = new InventoryCollection(new List<IModifiableInventory>{ConvoyInventory, m_partyInventory});
            m_jointInventory.OnItemAmountChanged += ItemAmountChangedHandler;
        }
        
        public void AddPartyInventory(IModifiableInventory modifiableInventory, CharacterReference characterReference = null)
        {
            m_partyInventory.AddInventory(modifiableInventory, characterReference);
        }

        public bool RemovePartyInventory(IModifiableInventory modifiableInventory)
        {
            return m_partyInventory.RemoveInventory(modifiableInventory);
        }
        
        public void ItemAmountChangedHandler(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            OnItemAmountChanged?.Invoke(oldAmount, newAmount);
        }
        public event ItemAmountChangedHandler OnItemAmountChanged;

        #region Convoy Inventory Access

        public bool ContainsItem(ItemReference itemReference)
        {
            return m_jointInventory.ContainsItem(itemReference);
        }

        public bool ContainsItem(ItemTag itemTag)
        {
            return m_jointInventory.ContainsItem(itemTag);
        }

        public bool ContainsItemQuantity(ItemReference itemReference, int minQuantity)
        {
            return m_jointInventory.ContainsItemQuantity(itemReference, minQuantity);
        }

        public bool ContainsItemQuantity(ItemTag itemTag, int minQuantity)
        {
            return m_jointInventory.ContainsItemQuantity(itemTag, minQuantity);
        }

        public ItemWithAmount GetItem(ItemReference itemReference)
        {
            return m_jointInventory.GetItem(itemReference);
        }

        public int CountItem(ItemReference itemReference)
        {
            return m_jointInventory.CountItem(itemReference);
        }

        public int CountItem(ItemTag itemTag)
        {
            return m_jointInventory.CountItem(itemTag);
        }

        public ItemWithAmount GetItemConvoyOnly(ItemReference itemReference)
        {
            return ConvoyInventory.GetItem(itemReference);
        }
        
        public int CountItemConvoyOnly(ItemReference itemReference)
        {
            return ConvoyInventory.CountItem(itemReference);
        }
        
        public int CountItemConvoyOnly(ItemTag itemTag)
        {
            return ConvoyInventory.CountItem(itemTag);
        }
        
        #endregion //Convoy Inventory Access
        
        #region Convoy Inventory Modification

        public ItemWithAmount ModifyItem(ItemReference itemReference, int deltaChange)
        {
            return deltaChange < 0 ? RemoveItem(itemReference, -deltaChange) : AddItem(itemReference, deltaChange);
        }

        public ItemWithAmount AddAndGetDelta(ItemReference itemReference, int amountToAdd)
        {
            ItemWithAmount current = GetItem(itemReference);
            return ConvoyInventory.AddAndGetDelta(itemReference, amountToAdd) + current;
        }

        /// <summary>
        /// Attempts to remove the given quantity of an item from the convoy.
        /// </summary>
        /// <param name="itemReference">Item Reference of item to remove.</param>
        /// <param name="amountToRemove">Amount of the item to remove.</param>
        /// <returns>Actual amount of the item removed. If this is 0, the player has no such items.</returns>
        public ItemWithAmount RemoveAndGetDelta(ItemReference itemReference, int amountToRemove)
        {
            return ConvoyInventory.RemoveAndGetDelta(itemReference, amountToRemove);
            // FIXME: Clean up commented out code
//            ItemWithAmount removed = ConvoyInventory.RemoveAndGetDelta(itemReference, amountToRemove);
//            return removed.Amount > 0 ? removed : m_partyInventory.RemoveAndGetDelta(itemReference, amountToRemove);
        }

        #region Modify Special
        
        private int RemoveAndGetDelta(List<ItemWithAmount> itemsWithAmounts)
        {
            return itemsWithAmounts.Select(itemWithAmount => this.RemoveAndGetDelta(itemWithAmount).Amount).Sum();
        }
        
        public int RemoveAndGetDelta(ItemFilter filter, int quantityToRemove)
        {
            return RemoveAndGetDelta(filter.ApplyFilter(ConvoyInventory, quantityToRemove));
        }
        
        /// <summary>
        /// Removes a subset of items with the given task from <see cref="ConvoyInventory"/>, filtered as desired and returns the number of items removed.
        /// </summary>
        /// <param name="tag">Tag by which to initially filter out items.</param>
        /// <param name="filter">Filter by which to sort the tag-filtered items.</param>
        /// <param name="quantityToRemove">Quantity of item to remove.</param>
        /// <returns>Integer representing the actual number of items removed in range [0, quantityToRemove]</returns>
        public int RemoveAndGetDelta(ItemTag tag, ItemFilter filter, int quantityToRemove)
        {
            return RemoveAndGetDelta(
                filter.ApplyFilter(ConvoyInventory.Where(itemWithAmount => itemWithAmount.Item.Tags.Contains(tag)),
                    quantityToRemove));
        }
        
        #endregion //Modify Special

        public int SetZero(ItemReference itemReference)
        {
            return m_jointInventory.SetZero(itemReference);
        }

        public ItemWithAmount AddItem(ItemReference itemReference, int amountToAdd)
        {
            ConvoyInventory.AddItem(itemReference, amountToAdd);
            return GetItem(itemReference);
        }

        public ItemWithAmount RemoveItem(ItemReference itemReference, int amountToRemove)
        {
            RemoveAndGetDelta(itemReference, amountToRemove);
            return GetItem(itemReference);
        }
        
        #endregion //Convoy Inventory Modification
        
        public IEnumerator<ItemWithAmount> GetEnumerator()
        {
            return m_jointInventory.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) m_jointInventory).GetEnumerator();
        }

        public IEnumerator<ItemWithAmount> GetConvoyEnumerator()
        {
            return (IEnumerator<ItemWithAmount>) ConvoyInventory.GetEnumerator();
        }
        
        public void Dispose()
        {
            m_jointInventory.OnItemAmountChanged -= ItemAmountChangedHandler;
        }

        public Inventory ConvoyInventory => m_convoyInventory;
        private Inventory m_convoyInventory;
        
        [NonSerialized] private InventoryCollection m_partyInventory;
        [NonSerialized] private InventoryCollection m_jointInventory;

    }
}