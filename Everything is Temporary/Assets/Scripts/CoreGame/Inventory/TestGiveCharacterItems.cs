﻿using System.Collections.Generic;
using UnityEngine;

using GameInventory;
using Characters;

/// <summary>
/// A temporary script, to give player some items,
/// so they can show up in the inventory page,
/// for the purpose of sprint 2.
/// </summary>
public class TestGiveCharacterItems : MonoBehaviour
{
    [SerializeField]
    private List<ItemReference> itemsToGive = default;

    private void Start()
    {
        CharacterManager.Instance.OnAddNewPartyMember += (newCharRef) => GiveCharacterItems(newCharRef);

        GiveCharacterItems(CharacterManager.Instance.Player);
    }

    private void GiveCharacterItems(CharacterReference charRef)
    {
        for (int i = 0; i < 5; i++)
        {
            ItemWithAmount itemWithAmount = new ItemWithAmount(itemsToGive[Random.Range(0, itemsToGive.Count - 1)], Random.Range(1, 5));

            charRef.Character.Inventory.ModifyItem(itemWithAmount);
        }
    }
}
