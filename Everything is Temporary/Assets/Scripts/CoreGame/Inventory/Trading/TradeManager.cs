﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameUI;
using UnityEngine;
using GameUtility;

namespace GameInventory
{
    public static class TradeManager
    {
        /// <summary>
        /// Opens a trade between the player inventory and the shop.
        /// </summary>
        /// <param name="convoyAndPartyInventory">Player inventory.</param>
        /// <param name="shop">Shop.</param>
        public static void PerformTrade(ConvoyAndPartyInventory convoyAndPartyInventory, Shop shop)
        {
            PerformTradeWithShopAsync(convoyAndPartyInventory, shop).FireAndForget();
        }

        public static async Task<TradeResult> PerformTradeWithShopAsync(ConvoyAndPartyInventory convoyAndPartyInventory, Shop shop)
        {
            return await GameUIHelper.Instance.DisplayTradeMenuAsync(convoyAndPartyInventory, shop);
        }

//        /// <summary>
//        /// Opens a trade between the player and the shop, and returns the result
//        /// of the trade.
//        /// </summary>
//        /// <returns>The result of the trade.</returns>
//        /// <param name="playerInventory">Player inventory.</param>
//        /// <param name="shop">Shop.</param>
//        public static async Task<TradeResult> PerformTradeAsync(Inventory playerInventory, Shop shop)
//        {
//            TradeResult result = await GameUIHelper.Instance.DisplayTradeMenuAsync(playerInventory, shop);
//
//            if (result.Result == TradeResult.ResultType.Finished)
//            {
//                List<ItemWithAmount> giveToShop = new List<ItemWithAmount>();
//                List<ItemWithAmount> giveToPlayer = new List<ItemWithAmount>();
//
//                foreach (var stack in result.Info.Giving)
//                {
//                    int removed = playerInventory.RemoveAndGetDelta(stack).Amount;
//                    
//                    giveToShop.Add(stack.WithAmount(removed));
//
//                    if (removed < stack.Amount)
//                        Debug.LogWarning($"Tried to sell {stack.Amount} of {stack.Item.Name} but player only had {removed}.");
//                }
//
//                foreach (var stack in result.Info.Getting)
//                {
//                    //FIXME: Refactor Shop to use new Inventory Code
//                    int removed = shop.Inventory.RemoveAndGetDelta(stack).Amount;
//                    giveToPlayer.Add(stack.WithAmount(removed));
//
//                    if (removed < stack.Amount)
//                        Debug.LogWarning($"Tried to buy {stack.Amount} of {stack.Item.Name} but shop only had {removed}.");
//                }
//
//                foreach (var stack in giveToShop)
//                    shop.Inventory.ModifyItem(stack);
//
//                foreach (var stack in giveToPlayer)
//                    playerInventory.ModifyItem(stack);
//            }
//
//            return result;
//        }
    }
}
