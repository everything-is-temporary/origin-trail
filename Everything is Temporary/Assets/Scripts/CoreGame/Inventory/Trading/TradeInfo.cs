﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInventory
{

    //ADD FIRST CHARACTER REFERENCE SECOND CHARACTER REFERENCE 
    //TO GET INVENTORIES 


    /// <summary>
    /// A class that represents a trade. The interpretation of who is giving
    /// and getting is left up to implementation.
    /// </summary>
    public class TradeInfo
    {
        public IEnumerable<ItemWithAmount> Giving => m_giving;
        public IEnumerable<ItemWithAmount> Getting => m_getting;
        public Inventory FirstCharacterInventory => m_firstInventory;
        public Inventory SecondCharacterInventory => m_secondInventory;

        public TradeInfo(IEnumerable<ItemWithAmount> giving, IEnumerable<ItemWithAmount> getting)
        {
            m_giving = giving?.ToList() ?? new List<ItemWithAmount>();
            m_getting = getting?.ToList() ?? new List<ItemWithAmount>();
        }

        public TradeInfo(IEnumerable<ItemWithAmount> giving, IEnumerable<ItemWithAmount> getting, Inventory firstInventory, Inventory secondInventory)
        {
            m_giving = giving?.ToList() ?? new List<ItemWithAmount>();
            m_getting = getting?.ToList() ?? new List<ItemWithAmount>();
            m_firstInventory = firstInventory;
            m_secondInventory = secondInventory;
        }

        private List<ItemWithAmount> m_giving;
        private List<ItemWithAmount> m_getting;
        private Inventory m_firstInventory;
        private Inventory m_secondInventory;
    }
}