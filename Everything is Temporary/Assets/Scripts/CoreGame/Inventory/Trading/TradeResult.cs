﻿using System.Text;
namespace GameInventory
{
    public class TradeResult
    {
        public enum ResultType
        {
            Finished, Cancelled
        }

        public ResultType Result { get; private set; }
        public TradeInfo Info { get; private set; }

        public static TradeResult CancelledTrade()
        {
            return new TradeResult { Result = ResultType.Cancelled };
        }

        public static TradeResult FinishedTrade(TradeInfo info)
        {
            return new TradeResult
            {
                Result = ResultType.Finished,
                Info = info
            };
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            switch (Result)
            {
                case ResultType.Cancelled:
                    str.Append("Cancelled trade.");
                    break;
                case ResultType.Finished:
                    str.AppendLine($"Finished trade: gave ({string.Join(", ", Info.Giving)})" +
                    	$" for ({string.Join(", ", Info.Getting)}).");
                    break;
            }
            return str.ToString();
        }
    }
}
