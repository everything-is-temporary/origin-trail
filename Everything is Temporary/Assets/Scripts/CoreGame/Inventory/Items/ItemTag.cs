﻿using System;
using UnityEngine;

namespace GameInventory
{
    // TODO: Use Strongly Typed (typesafe) Enum Pattern (if a feasible implementation) for easier use in function signatures.
    [Serializable]
    public class ItemTag
    {

        [SerializeField] private string m_tagName;
        public string TagName => m_tagName;
        
        public ItemTag(string tagName)
        {
            m_tagName = tagName;
        }
        
        public override string ToString()
        {
            return m_tagName;
        }

        public override bool Equals(object other)
        {
            switch (other)
            {
                case ItemTag itemTag:
                    return m_tagName.Equals(itemTag.TagName);
                case string s:
                    return m_tagName.Equals(s);
                default:
                    return false;
            }
        }

        public override int GetHashCode()
        {
            return TagName.GetHashCode();
        }

        public static implicit operator string(ItemTag itemTag)
        {
            return itemTag.TagName;
        }
    }
}
