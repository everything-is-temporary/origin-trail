using System;
using System.Collections.Generic;
using System.Linq;

namespace GameInventory.Utility
{
    public class ItemFilter
    {

        #region Typesafe Enumerations
        
        public static readonly ItemFilter LeastValueFilter = new ItemFilter(LeastValue);

        #endregion        
        
        private Func<IEnumerable<ItemWithAmount>, int, List<ItemWithAmount>> m_filterFunc;

        // Constructor is private since it is only used here. If it is necessary elsewhere, make it public.
        private ItemFilter(Func<IEnumerable<ItemWithAmount>, int, List<ItemWithAmount>> filterFunc)
        {
            m_filterFunc = filterFunc;
        }
        
        public List<ItemWithAmount> ApplyFilter(IEnumerable<ItemWithAmount> allItems, int desiredQuantity = 1)
        {
            return m_filterFunc.Invoke(allItems, desiredQuantity);
        }
        
        #region Static Functions to Filter Items
        
        private static List<ItemWithAmount> LeastValue(IEnumerable<ItemWithAmount> stacks, int numberToSelect)
        {
            List<ItemWithAmount> retStacks = new List<ItemWithAmount>();
            using (var orderedByValue = stacks.OrderBy(stack => stack.Item.EconomyValue).GetEnumerator())
            {
                orderedByValue.MoveNext();
                while (numberToSelect > 0 && orderedByValue.Current != null)
                {
                    int takenAmount = Math.Min(numberToSelect, orderedByValue.Current.Amount);
                    retStacks.Add(orderedByValue.Current.WithAmount(takenAmount));
                    orderedByValue.MoveNext();
                    numberToSelect -= takenAmount;
                }
            }
            return retStacks;
        }
        
        #endregion //Static Functions to Filter Items
    }
}