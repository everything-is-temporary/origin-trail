﻿using System;
using System.Collections.Generic;
using GameUtility;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GameInventory
{
    /// <summary>
    /// An item definition list. This stores items with their associated IDs.
    /// </summary>
    [CreateAssetMenu(fileName = "Item List", menuName = "Item Definition List")]
    public class ItemDefinitionList : ScriptableObject, ISerializationCallbackReceiver
    {
        /// <summary>
        /// Gets the ItemDefinitionList singleton instance. In the editor, this
        /// may return null if it is run during serialization.
        /// </summary>
        /// <value>The instance.</value>
        public static ItemDefinitionList Instance
        {
#if UNITY_EDITOR
            get
            {
                if (_instance == null)
                {
                    try
                    {
                        _instance = AssetDatabase.LoadAssetAtPath<ItemDefinitionList>("Assets/Prefabs/Items/Item List.asset");
                    }
                    catch (UnityException)
                    {
                        // This happens if the Instance property is accessed
                        // during serialization. It can be ignored, but _instance
                        // will remain null.
                    }
                }

                return _instance;
            }
#else
            get => _instance;
#endif
        }
        private static ItemDefinitionList _instance;

        [SerializeField]
        private List<ItemWithId> m_serializedItems = default;

        /// <summary>
        /// The list of duplicate items found in <see cref="m_serializedItems"/>.
        /// This exists just so that items with duplicate IDs in m_serializedItems
        /// are not lost.
        /// </summary>
        [SerializeField]
        private List<ItemWithId> m_duplicateItems = default;

        /// <summary>
        /// Gets the definition for an item. If the item ID is not found in
        /// this list, returns null.
        /// </summary>
        /// <returns>The item with the ID or null.</returns>
        /// <param name="itemId">Item identifier.</param>
        public InventoryItem GetItem(long itemId)
        {
            return m_items[itemId];
        }

        /// <summary>
        /// Checks whether an item with the given ID exists.
        /// </summary>
        /// <returns><c>true</c>, if item exists, <c>false</c> otherwise.</returns>
        /// <param name="itemId">Item identifier.</param>
        public bool HasItem(long itemId)
        {
            return m_items.ContainsKey(itemId);
        }

        private void OnEnable()
        {
            if (_instance == null || _instance == this)
                _instance = this;
            else
                Debug.LogWarning("Duplicate ItemDefinitionList detected. The original one" +
                    $" was {Instance.name} and the duplicate one is {name}.");
        }

#if UNITY_EDITOR

        /// <summary>
        /// Generates an item ID that has not yet been used within this list.
        /// </summary>
        /// <returns>A unique (within this list) item identifier.</returns>
        public long GenerateUniqueId()
        {
            return GenerateUniqueId(new System.Random());
        }

        /// <summary>
        /// Generates an item ID that has not yet been used within this list.
        /// </summary>
        /// <param name="rand">The random number generator to use.</param>
        /// <returns>A unique (within this list) item identifier.</returns>
        public long GenerateUniqueId(System.Random rand)
        {
            long id;

            do
            {
                id = rand.NextLong();
            } while (id == 0 || m_items.ContainsKey(id));

            return id;
        }

        /// <summary>
        /// Adds a new item, if an item with the same ID is not in the list.
        /// </summary>
        /// <returns><c>true</c>, if new item was added, <c>false</c> otherwise.</returns>
        /// <param name="id">The item ID.</param>
        /// <param name="item">The item.</param>
        public bool AddNewItem(long id, InventoryItem item)
        {
            if (!m_items.ContainsKey(id))
            {
                m_items[id] = item;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Enumerates the item IDs of all items.
        /// </summary>
        /// <returns>The item identifiers.</returns>
        public IEnumerable<long> EnumerateItemIds()
        {
            foreach (var pair in m_items)
                yield return pair.Key;
        }
#endif

        public void OnBeforeSerialize()
        {
            m_serializedItems = new List<ItemWithId>();

            if (m_items != null)
            {
                foreach (var pair in m_items)
                {
                    m_serializedItems.Add(new ItemWithId(pair.Key, pair.Value));
                }
            }
        }

        public void OnAfterDeserialize()
        {
            m_items = new Dictionary<long, InventoryItem>();

            foreach (ItemWithId itemAndId in m_serializedItems)
            {
                if (!m_items.ContainsKey(itemAndId.Id))
                    m_items[itemAndId.Id] = itemAndId.Item;
                else
                {
                    Debug.LogWarning($"Detected duplicate item with ID {itemAndId.Id}" +
                            $" in item list. The original was {m_items[itemAndId.Id].Name} and" +
                            $" the duplicate is {itemAndId.Item.Name}. The duplicate will not" +
                            $" be accessible by its ID and will be moved to the duplicate list.");
                    m_duplicateItems.Add(itemAndId);
                }
            }
        }

        private Dictionary<long, InventoryItem> m_items;

        [Serializable]
#if UNITY_EDITOR
        public
#else
        private
#endif
        struct ItemWithId
        {
            /*
                Implementation note:
                A simpler and more appropriate way of doing this would be
                to make a "readonly struct". Unfortunately, Unity cannot
                serialize readonly fields, so this is the best we got.               
            */

            public long Id => m_id;
            public InventoryItem Item => m_item;

            public ItemWithId(long id, InventoryItem item)
            {
                m_id = id;
                m_item = item;
            }

            [SerializeField]
            private long m_id;

            [SerializeField]
            private InventoryItem m_item;
        }
    }
}