using System;
using UnityEngine;

namespace GameInventory
{
    /// <summary>
    /// Lightweight immutable class representing an item with an amount.
    /// </summary>
    [Serializable]
	public class ItemWithAmount
	{
        public InventoryItem Item => ItemReference.Item;
        public int Amount => m_amount;

        public ItemReference ItemReference => m_itemReference;

        public ItemWithAmount(ItemReference itemReference, int amount)
        {
            m_itemReference = itemReference;
            m_amount = amount;
        }

        public ItemWithAmount(ItemReference itemReference)
        {
            m_itemReference = itemReference;
            m_amount = 0;
        }

        /// <summary>
        /// Creates a new itemWithAmount for the same item but with a different amount.
        /// </summary>
        /// <returns>The new itemWithAmount.</returns>
        /// <param name="newAmount">New amount.</param>
        public ItemWithAmount WithAmount(int newAmount)
        {
            return new ItemWithAmount(ItemReference, newAmount);
        }
        
        /// <summary>
        /// Creates a new itemWithAmount with the same item but an inverted amount value.
        /// </summary>
        /// <returns>The new itemWithAmount.</returns>
        public ItemWithAmount InvertAmount()
        {
            return -this;
        }

        public ItemWithAmount Copy()
        {
            return new ItemWithAmount(m_itemReference, m_amount);
        }

        public override string ToString()
        {
            return $"({Amount} {ItemReference})";
        }

        [SerializeField]
        private ItemReference m_itemReference;

        [SerializeField]
        private int m_amount;

        public override bool Equals(object obj)
        {
            if (obj is ItemWithAmount stack)
            {
                return stack.ItemReference == ItemReference && stack.Amount == Amount;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ItemReference.GetHashCode();
        }

        public static ItemWithAmount operator +(ItemWithAmount first, ItemWithAmount second)
        {
            Debug.Assert(first.ItemReference.Equals(second.ItemReference));
            return new ItemWithAmount(first.ItemReference, first.Amount+second.Amount);
        }

        public static ItemWithAmount operator -(ItemWithAmount thisItemWithAmount)
        {
            return new ItemWithAmount(thisItemWithAmount.ItemReference, -thisItemWithAmount.Amount);
        }
        
        public static ItemWithAmount operator -(ItemWithAmount first, ItemWithAmount second)
        {
            Debug.Assert(first.ItemReference.Equals(second.ItemReference));
            return new ItemWithAmount(first.ItemReference, first.Amount-second.Amount);
        }

        public static implicit operator ItemReference(ItemWithAmount itemWithAmount)
        {
            return itemWithAmount.ItemReference;
        }

        public static implicit operator InventoryItem(ItemWithAmount itemWithAmount)
        {
            return itemWithAmount.Item;
        }

        public static explicit operator int(ItemWithAmount itemWithAmount)
        {
            return itemWithAmount.Amount;
        }
    }
}
