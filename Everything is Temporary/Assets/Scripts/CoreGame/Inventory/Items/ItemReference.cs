﻿using System;
using UnityEngine;

namespace GameInventory
{
    /// <summary>
    /// A reference to an item in the item database. Use this class to represent
    /// items. In general, it is more convenient than a string. It also has a
    /// custom inspector, so if any MonoBehaviour you write needs to know about
    /// a particular item, use this class.
    /// </summary>
    [Serializable]
    public class ItemReference
    {
        #region Public Interface
        /// <summary>
        /// The referenced item. Could be null.
        /// </summary>
        /// <value>The item.</value>
        public InventoryItem Item
        {
            get
            {
                if (!m_queriedItem)
                {
                    m_item = ItemDefinitionList.Instance.GetItem(m_id);
                    m_queriedItem = true;
                }

                return m_item;
            }
        }

        public long Id => m_id;

        /// <summary>
        /// True if this item reference does not refer to a valid item.
        /// False if the item definition list isn't set or if the reference does
        /// point to a valid item.
        /// </summary>
        public bool IsInvalid
        {
            get => ItemDefinitionList.Instance != null && !ItemDefinitionList.Instance.HasItem(m_id);
        }
        #endregion

        /// <summary>
        /// Creates an ItemReference at runtime from an ID. You should generally
        /// not need to use this.
        /// </summary>
        /// <returns>The identifier.</returns>
        /// <param name="id">Identifier.</param>
        public static ItemReference FromId(long id)
        {
            return new ItemReference(id);
        }

        /// <summary>
        /// Converts the reference to an InventoryItem implicitly, allowing ItemReferences
        /// to be passed to methods expecting InventoryItems.
        /// </summary>
        /// <returns>The item.</returns>
        /// <param name="itemRef">The item reference.</param>
        public static implicit operator InventoryItem(ItemReference itemRef)
        {
            return itemRef.Item;
        }

        private ItemReference(long id)
        {
            m_id = id;
        }

        #region Standard Overrides
        public override string ToString()
        {
            return Item?.Name ?? "BAD_ITEM_ID";
        }

        public override int GetHashCode()
        {
            // GetHashCode() will not be called until after OnAfterDeserialize()
            // or after the non-default constructor.
#pragma warning disable RECS0025 // Non-readonly field referenced in 'GetHashCode()'
            return (int)m_id;
#pragma warning restore RECS0025 // Non-readonly field referenced in 'GetHashCode()'
        }

        public override bool Equals(object obj)
        {
            if (obj is ItemReference itemRef)
                return itemRef.Id == Id;

            return false;
        }
        #endregion

        private InventoryItem m_item;
        private bool m_queriedItem = false;

        [SerializeField] private long m_id;
    }
}
