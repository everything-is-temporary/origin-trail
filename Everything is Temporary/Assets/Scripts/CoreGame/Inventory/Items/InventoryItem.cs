﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInventory
{
    [System.Serializable]
    public class InventoryItem
    {
        public string Name => m_name;
        public string DescriptionShort => m_descriptionShort;
        public string FlavorText => m_flavorText;
        public float EconomyValue => m_economyValue;
        public List<ItemTag> Tags => m_tags;
        public Sprite Icon => m_icon;
        public Characters.Stats Stats => m_stats;

        [SerializeField] private string m_name = default;
        [SerializeField] private string m_descriptionShort = default;
        [SerializeField] private string m_flavorText = default;
        [SerializeField] private float m_economyValue = default;
        [SerializeField] private List<ItemTag> m_tags = default;
        [SerializeField] private Sprite m_icon = default;
        [SerializeField] private Characters.Stats m_stats = default;
    }
}
