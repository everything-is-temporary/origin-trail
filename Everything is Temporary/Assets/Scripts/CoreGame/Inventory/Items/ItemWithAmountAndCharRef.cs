using System;
using Characters;
using GameUtility;
using UnityEngine;

namespace GameInventory
{
    public class ItemWithAmountAndCharRef : IKeyable<int>
    {
        public ItemWithAmount ItemWithAmount => m_itemWithAmount;
        public CharacterReference CharacterReference => m_characterReference;

        public ItemWithAmountAndCharRef(CharacterReference characterReference, ItemWithAmount itemWithAmount)
        {
            m_characterReference = characterReference;
            m_itemWithAmount = itemWithAmount;
        }

        /// <summary>
        /// Modifies the contained <see cref="CharacterReference"/>'s inventory by the contained <see cref="ItemWithAmount"/>.
        /// </summary>
        /// <returns>An <see cref="ItemWithAmount"/> representing the quantity of the item the character now owns.</returns>
        public ItemWithAmount ModifyCharacterInventory()
        {
            Debug.Assert(m_characterReference.Character.Inventory != null && ItemWithAmount != null);
            return m_characterReference.Character.Inventory.ModifyItem(ItemWithAmount);
        }

        /// <summary>
        /// Sets the contained <see cref="CharacterReference"/>'s inventory to the contained <see cref="ItemWithAmount"/>.
        /// </summary>
        /// <returns>An <see cref="ItemWithAmount"/> representing the quantity of the item the character now owns.</returns>
        public ItemWithAmount SetCharacterInventory()
        {
            Debug.Assert(m_characterReference.Character.Inventory != null && ItemWithAmount != null);
            return m_characterReference.Character.Inventory.SetItem(ItemWithAmount);
        }

        /// <summary>
        /// Returns a new <see cref="ItemWithAmountAndCharRef"/> with an inverted amount in its <see cref="ItemWithAmount"/>.
        /// </summary>
        /// <returns>A new <see cref="ItemWithAmountAndCharRef"/>.</returns>
        public ItemWithAmountAndCharRef Invert()
        {
            return new ItemWithAmountAndCharRef(m_characterReference, ItemWithAmount.InvertAmount());
        }

        /// <summary>
        /// Returns a new <see cref="ItemWithAmountAndCharRef"/> with the <see cref="m_characterReference"/> set to <paramref name="characterReference"/>.
        /// </summary>
        /// <param name="characterReference">A reference to a character.</param>
        /// <returns>Returns a new <see cref="ItemWithAmountAndCharRef"/>.</returns>
        public ItemWithAmountAndCharRef WithReference(CharacterReference characterReference)
        {
            return new ItemWithAmountAndCharRef(characterReference, m_itemWithAmount);
        }
        
        /// <summary>
        /// Returns a new <see cref="ItemWithAmountAndCharRef"/> with the <see cref="m_itemWithAmount"/> set to <paramref name="itemWithAmount"/>.
        /// </summary>
        /// <param name="itemWithAmount">The new item and amount for the returned <see cref="ItemWithAmountAndCharRef"/>.</param>
        /// <returns>Returns a new <see cref="ItemWithAmountAndCharRef"/>.</returns>
        public ItemWithAmountAndCharRef WithItem(ItemWithAmount itemWithAmount)
        {
            return new ItemWithAmountAndCharRef(m_characterReference, itemWithAmount);
        }

        /// <summary>
        /// Returns a new <see cref="ItemWithAmountAndCharRef"/> with the <see cref="m_itemWithAmount"/>'s Amount set to the requested value, keeping the internal item and <see cref="m_characterReference"/> identical.
        /// </summary>
        /// <param name="newAmount">The new item amount for the <see cref="ItemWithAmountAndCharRef"/> returned.</param>
        /// <returns>Returns a new <see cref="ItemWithAmountAndCharRef"/>.</returns>
        public ItemWithAmountAndCharRef WithAmount(int newAmount)
        {
            return WithItem(m_itemWithAmount.WithAmount(newAmount));
        }

        /// <summary>
        /// Adds two <see cref="ItemWithAmountAndCharRef"/> together. Note that the CharacterReferences must be equal.
        /// </summary>
        /// <param name="first">First argument to addition.</param>
        /// <param name="second">Second argument to addition.</param>
        /// <returns>A new <see cref="ItemWithAmountAndCharRef"/> containing the sum of the <see cref="ItemWithAmount"/> of <paramref name="first"/> abd <paramref name="second"/>.</returns>
        /// <exception cref="ArgumentException">Throws an exception when CharacterReferences are not equivalent.</exception>
        public static ItemWithAmountAndCharRef operator +(ItemWithAmountAndCharRef first,
            ItemWithAmountAndCharRef second)
        {
            if (!first.CharacterReference.Equals(second.CharacterReference))
            {
                throw new ArgumentException("Character references must be equal.");
            }
            return new ItemWithAmountAndCharRef(first.CharacterReference, first.ItemWithAmount + second.ItemWithAmount);
        }
        
        /// <summary>
        /// Takes two <see cref="ItemWithAmountAndCharRef"/> and subtracts the second from the first. Note that the CharacterReferences must be equal.
        /// </summary>
        /// <param name="first">First argument to subtraction.</param>
        /// <param name="second">Second argument to subtraction.</param>
        /// <returns>A new <see cref="ItemWithAmountAndCharRef"/> containing the difference of the <see cref="ItemWithAmount"/> of <paramref name="first"/> abd <paramref name="second"/>.</returns>
        /// <exception cref="ArgumentException">Throws an exception when CharacterReferences are not equivalent.</exception>
        public static ItemWithAmountAndCharRef operator -(ItemWithAmountAndCharRef first,
            ItemWithAmountAndCharRef second)
        {
            if (!first.CharacterReference.Equals(second.CharacterReference))
            {
                throw new ArgumentException("Character references must be equal.");
            }
            return new ItemWithAmountAndCharRef(first.CharacterReference, first.ItemWithAmount - second.ItemWithAmount);
        }
        
        public static implicit operator CharacterReference(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            return itemWithAmountAndCharRef.CharacterReference;
        }

        public static implicit operator ItemWithAmount(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            return itemWithAmountAndCharRef.ItemWithAmount;
        }
        
        public static explicit operator ItemReference(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            return itemWithAmountAndCharRef.ItemWithAmount;
        }
        
        public static explicit operator int(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            return itemWithAmountAndCharRef.ItemWithAmount.Amount;
        }
        
        public int GetKey()
        {
            return Tuple.Create(m_characterReference.GetHashCode(), m_itemWithAmount.ItemReference.GetHashCode()).GetHashCode();
        }

        private readonly CharacterReference m_characterReference;
        private readonly ItemWithAmount m_itemWithAmount;
    }
}