using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using GameInventory;
using UnityEngine;

namespace GameInventory
{
    public delegate void ItemAmountChangedHandler(ItemWithAmount oldItemWithAmount, ItemWithAmount newItemWithAmount);

    public interface IInventory : IEnumerable<ItemWithAmount>
    {   
        /// <summary>
        /// Event called when an item is added or removed from the inventory.
        /// </summary>
        event ItemAmountChangedHandler OnItemAmountChanged;
        
        /// <summary>
        /// Determines if the inventory contains the specified item.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to check for.</param>
        /// <returns><c>true</c>, if at least one of the item exists in the inventory, <c>false</c> otherwise.</returns>
        bool ContainsItem(ItemReference itemReference);

        /// <summary>
        /// Determines if the inventory contains at least one item with the specified tag.
        /// </summary>
        /// <param name="itemTag">An <see cref="ItemTag"/> to filter by.</param>
        /// <returns><c>true</c>, if at least one of the item with the given tag exists in the inventory, <c>false</c> otherwise.</returns>
        bool ContainsItem(ItemTag itemTag);
        
        /// <summary>
        /// Determines if the inventory contains at least <paramref name="minQuantity"/> of the specified item.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to check for.</param>
        /// <param name="minQuantity">The minimum number of the item to check for.</param>
        /// <returns><c>true</c>, if at least <paramref name="minQuantity"/> of the item exists in the inventory, <c>false</c> otherwise.</returns>
        bool ContainsItemQuantity(ItemReference itemReference, int minQuantity);

        /// <summary>
        /// Determines if the inventory contains at least <paramref name="minQuantity"/> of the specified item.
        /// </summary>
        /// <param name="itemTag">An <see cref="ItemTag"/> to filter by.</param>
        /// <param name="minQuantity">The minimum number of the item to check for.</param>
        /// <returns><c>true</c>, if at least <paramref name="minQuantity"/> items with the specified tag exist in the inventory, <c>false</c> otherwise.</returns>
        bool ContainsItemQuantity(ItemTag itemTag, int minQuantity);
        
        /// <summary>
        /// Gets a <see cref="ItemWithAmount"/> representing the amount of the specified item in the inventory.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to get.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        ItemWithAmount GetItem(ItemReference itemReference);

        /// <summary>
        /// Counts the quantity of the specified item in the inventory.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to count.</param>
        /// <returns>A <c>int</c> representing the quantity of the item in the inventory.</returns>
        int CountItem(ItemReference itemReference);
        
        /// <summary>
        /// Counts the quantity of the specified item in the inventory.
        /// </summary>
        /// <param name="itemTag">The <see cref="itemTag"/> by which to filter items by.</param>
        /// <returns>A <c>int</c> representing the quantity of the item in the inventory.</returns>
        int CountItem(ItemTag itemTag);
    }

    public static class InventoryExt
    {
        /// <summary>
        /// Determines if the inventory contains at least <paramref name="itemWithAmount"/> of the specified item.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount of the item to check for.</param>
        /// <returns><c>true</c>, if at least <paramref name="itemWithAmount"/> of the item exists in the inventory, <c>false</c> otherwise.</returns>
        public static bool ContainsItemQuantity(this IInventory inventory, ItemWithAmount itemWithAmount)
        {
            return inventory.ContainsItemQuantity(itemWithAmount, itemWithAmount.Amount);
        }
    }
}