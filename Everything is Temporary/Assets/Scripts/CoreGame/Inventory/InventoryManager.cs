﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Characters;
using GameInventory.Utility;
using GameUtility;
using GameUtility.DataPersistence;
using GameUtility.Serialization;

namespace GameInventory
{
    public class InventoryManager : SingletonMonoBehavior<InventoryManager>
    {
        protected void Start()
        {
//            base.Start();
            LoadPartyInventory(CharacterManager.Instance.CurrentParty);
        }

        #region Inventory Method Access

        public int CountItem(ItemReference itemReference)
        {
            return m_convoyAndPartyInventory.CountItem(itemReference);
        }

        public int CountItem(ItemTag itemTag)
        {
            return m_convoyAndPartyInventory.CountItem(itemTag);
        }

        public int CountItemConvoyOnly(ItemReference itemReference)
        {
            return m_convoyAndPartyInventory.CountItemConvoyOnly(itemReference);
        }

        public int CountItemConvoyOnly(ItemTag itemTag)
        {
            return m_convoyAndPartyInventory.CountItemConvoyOnly(itemTag);
        }
        
        #endregion Inventory Method Access

        public void AddCharacterToParty(CharacterReference characterReference)
        {
             m_convoyAndPartyInventory.AddPartyInventory(GetInventoryScriptableObjectForCharacter(characterReference), characterReference);
        }

        public bool RemoveCharacterFromParty(CharacterReference characterReference)
        {
            return m_convoyAndPartyInventory.RemovePartyInventory(characterReference.Character.Inventory);
        }        
        public void LoadPartyInventory(IEnumerable<CharacterReference> partyCharacters)
        {
            foreach (CharacterReference characterReference in partyCharacters)
            {
                m_convoyAndPartyInventory.AddPartyInventory(characterReference.Character.Inventory, characterReference);
            }
        }

        private Inventory GetInventoryScriptableObjectForCharacter(CharacterReference characterReference)
        {
            return characterReference.Character.Inventory ?? new Inventory();
        }

        public ItemWithAmount AddInventoryItem(ItemWithAmount itemWithAmount)
        {
            return m_convoyAndPartyInventory.AddItem(itemWithAmount);
        }

        public ItemWithAmount RemoveInventoryItem(ItemWithAmount itemWithAmount)
        {
            return m_convoyAndPartyInventory.RemoveItem(itemWithAmount);
        }

        public ItemWithAmount ModifyInventoryItem(ItemWithAmount itemWithAmount)
        {
            if (itemWithAmount.Amount < 0)
            {
                return RemoveInventoryItem(-itemWithAmount);
            }

            return AddInventoryItem(itemWithAmount);
        }

        /// <summary>
        /// Attempts to remove items with the given tag from the convoy.
        /// </summary>
        /// <param name="tagToRemove">Item tag to filter items by.</param>
        /// <param name="quantityToRemove">The quantity of the item to remove. If this is -1 or empty, it removes all items with the specified tag.</param>
        /// <returns>An integer representing the actual number of items removed in range [0, quantityToRemove]</returns>
        public int RemoveInventoryItemsByTagGetDelta(ItemTag tagToRemove, int quantityToRemove = -1)
        {
            if (quantityToRemove == -1)
            {
                quantityToRemove = m_convoyAndPartyInventory.CountItemConvoyOnly(tagToRemove);
            }

            return m_convoyAndPartyInventory.RemoveAndGetDelta(tagToRemove, ItemFilter.LeastValueFilter,
                quantityToRemove);
        }

        public IEnumerable<ItemWithAmount> GetConvoyEnumerator()
        {
            return (IEnumerable<ItemWithAmount>) m_convoyAndPartyInventory.GetConvoyEnumerator();
        }
        
        public Inventory GetConvoyInventory()
        {
            return m_convoyAndPartyInventory.ConvoyInventory;
        }

        public Inventory ConvoyInventory => m_convoyAndPartyInventory.ConvoyInventory;
        public ConvoyAndPartyInventory ConvoyAndPartyInventory => m_convoyAndPartyInventory;
        
                
        [NonSerialized]
        private ConvoyAndPartyInventory m_convoyAndPartyInventory = new ConvoyAndPartyInventory(new Inventory());
    }
}
