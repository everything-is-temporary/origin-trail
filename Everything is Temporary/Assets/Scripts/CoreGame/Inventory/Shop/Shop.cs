﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameInventory.Utility;
using UnityEngine;
using UnityEditor;
using GameUtility.Serialization;

// TODO: Shop: Write buying and selling functions, here or in Inventory?

namespace GameInventory
{
    /// <summary>
    /// Shop by default has a multiplier of 1.0 for all tags.
    /// </summary>
    [Serializable]
    [CreateAssetMenu(fileName = "New Shop", menuName = "Create New Shop")]
    public class Shop : ScriptableObject
    {
        [SerializeField]
        private string m_name = default;

        [SerializeField] private bool m_useSimpleValidation = true;

        // FIXME: This is a quick implementation of Trade Validation functions. Validations are to be serialized in the Shop Editor serialization as a list already.
        public List<TradeValidator> ShopTradeValidators => m_useSimpleValidation
            ? new List<TradeValidator>
            {
                TradeValidator.AtLeastEqualInValue
            }
            : new List<TradeValidator>();

        [SerializeField]
        private Inventory m_inventory = new Inventory();

        [SerializeField, SerializableDictionary(false)]
        private ItemPreference m_itemPreference = default;

        [SerializeField, SerializableDictionary(false)]
        private TagPreference m_tagPreference = default;

        // Shop's overall multipliers, which apply to all items
        [SerializeField] private float m_buyFromOthersMultiplier = 1;
        [SerializeField] private float m_sellToOthersMultiplier = 1;

        // Value below which a shop will reject a trade
        [SerializeField] private float m_minimalAcceptedTradeValue = 0;

        public Inventory Inventory => m_inventory;

        public string Name => m_name;

        //// 2 functions to get multipliers for preferences.

        public float GetItemPreferenceMultiplier(ItemReference itemReference)
        {
            if (m_itemPreference.Dictionary.ContainsKey(itemReference))
                return m_itemPreference.Dictionary[itemReference];

            return 1f;
        }

        public float GetTagPreferenceMultiplier(ItemTag queriedTag)
        {
            if (m_tagPreference.Dictionary.ContainsKey(queriedTag))
                return m_tagPreference.Dictionary[queriedTag];

            return 1f;
        }

        // TODO: Discuss: Will there be a need to procedurally change those multipliers through code?
        public void SetPriceMultiplier(string targetTag, float newMultiplier)
        {

        }

        /// <summary>
        /// Get the sell price of the item.
        /// HasItem must be called and checked before calling this function!
        /// </summary>
        // TODO: Discuss: Here, itemReference will be from Shop's Inventory.
        //                Will whoever's asking the price be able to access that?
        public float GetSellItemPrice(ItemReference itemReference)
        {
            InventoryItem item = itemReference.Item;

            // TODO: Discuss: Is checking null necessary here?
            if (item != null)
            {
                // 1. Base price
                float price = item.EconomyValue;

                // 2. Item preference multiplier
                price *= GetItemPreferenceMultiplier(itemReference);

                // 3. Tag preference multipliers
                foreach (ItemTag itemTag in item.Tags)
                {
                    float tagMultiplier = GetTagPreferenceMultiplier(itemTag);

                    price *= tagMultiplier;
                }

                // 4. Shop's sell multiplier
                price *= m_sellToOthersMultiplier;

                return price;
            }
            else
            {
                Debug.LogError("SHOP000: Shop.HasItem should be called and checked before getting price. " + item.Name + " doesn't exist in Shop " + Name + ". Aborted.");
                return -1;
            }
        }

        /// <summary>
        /// Get the buy price of an item.
        /// The other party should provide the item so HasItem doesn't need to be called and checked here!
        /// </summary>
        public float GetBuyItemPrice(ItemReference itemReference)
        {
            // TODO: Discuss: Is checking null necessary here?
            if (itemReference != null)
            {
                InventoryItem item = itemReference.Item;

                // 1. Base price
                float price = item.EconomyValue;

                // 2. Item preference multiplier
                price *= GetItemPreferenceMultiplier(itemReference);

                // 3. Tag preference multipliers
                foreach (ItemTag itemTag in item.Tags)
                {
                    float tagMultiplier = GetTagPreferenceMultiplier(itemTag);

                    price *= tagMultiplier;
                }

                // 4. Shop's buy multiplier
                price *= m_buyFromOthersMultiplier;

                return price;
            }
            else
            {
                Debug.LogError("SHOP100: Shop.GetBuyPrice received a null ItemWithAmount.");
                return -1;
            }
        }

        public bool IsTradeAccepted(IEnumerable<ItemWithAmount> itemsOffered, IEnumerable<ItemWithAmount> itemsRequested)
        {
            return this.GetTradeNetValueForStore(itemsOffered, itemsRequested) >= this.m_minimalAcceptedTradeValue;
        }

        protected float GetTradeNetValueForStore(IEnumerable<ItemWithAmount> itemsOffered, IEnumerable<ItemWithAmount> itemsRequested)
        {
            return itemsOffered.Sum((ItemWithAmount buyStack) => buyStack.Amount * this.GetBuyItemPrice(buyStack.ItemReference)) - itemsRequested.Sum((ItemWithAmount sellStack) => sellStack.Amount * this.GetSellItemPrice(sellStack.ItemReference));
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            string assetPath = AssetDatabase.GetAssetPath(this);

            if (!string.IsNullOrEmpty(assetPath))
            {
                // This ScriptableObject is attached to an asset. Update the
                // shop name to match the asset name.

                string assetFileName = assetPath.Substring(assetPath.LastIndexOf('/') + 1);
                string assetName = assetFileName.Substring(0, assetFileName.IndexOf('.'));

                m_name = assetName;
            }
        }
#endif

        /* These definitions exist so that Unity can serialize preferences. */

        [Serializable]
        private class ItemPreference : SerializableDictionary<ItemReference, float>
        { }

        [Serializable]
        private class TagPreference : SerializableDictionary<ItemTag, float>
        { }
    }

}
