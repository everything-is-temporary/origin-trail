using System;
using System.Collections.Generic;
using GameUtility;
using GameUtility.Serialization;
using UnityEngine;
using UnityEditor;

namespace GameInventory
{
    [CreateAssetMenu(fileName = "Shop List", menuName = "Shop Definition List")]
    public class ShopDefinitionList : ScriptableObject
    {
        public static ShopDefinitionList Instance
        {
#if UNITY_EDITOR
            get
            {
                if (_instance == null)
                {
                    _instance = AssetDatabase.LoadAssetAtPath<ShopDefinitionList>("Assets/Prefabs/_Shop List.asset");
                }

                return _instance;
            }
#else
            get => _instance;
#endif
        }
        private static ShopDefinitionList _instance;

        public Shop GetShop(long id)
        {
            if (m_shopDict.Dictionary.TryGetValue(id, out Shop shop))
                return shop;
            return null;
        }

        private void OnEnable()
        {
            if (Instance == null || Instance == this)
                _instance = this;
            else
                Debug.LogError("Detected duplicate ShopDefinitionList.");
        }

#if UNITY_EDITOR
        public void AddScriptableShop(Shop shop)
        {
            if (m_shopDict.Dictionary.ContainsValue(shop))
                return;

            long newId = GenerateUniqueId(_rand);
            m_shopDict.Dictionary[newId] = shop;
        }

        public void RemoveNullShops()
        {
            var keys = new List<long>();

            foreach (var pair in m_shopDict.Dictionary)
            {
                if (pair.Value == null)
                    keys.Add(pair.Key);
            }

            foreach (var key in keys)
                m_shopDict.Dictionary.Remove(key);
        }

        public long GenerateUniqueId(System.Random rand)
        {
            long id;

            do
            {
                id = rand.NextLong();
            } while (id == 0 || m_shopDict.Dictionary.ContainsKey(id));

            return id;
        }

        private ShopDefinitionList()
        {
            m_shopDict = new ShopDictionary
            {
                CreateNewElement = () => new KeyValuePair<long, Shop>(GenerateUniqueId(_rand), null)
            };
        }

        private static readonly System.Random _rand = new System.Random();

#endif

        [Tooltip("To change elements, create shops in Prefabs/Shops or reimport existing ones if they are not showing up.")]
        [SerializeField]
        [SerializableDictionary(showAddButton: true, showDeleteButton: true, keyIsLongId: true, readonlyKey: false)]
        private ShopDictionary m_shopDict;


        [Serializable]
        private class ShopDictionary : SerializableDictionary<long, Shop> { }
    }
}