﻿using UnityEngine;

namespace GameInventory
{
    [System.Serializable]
    public class ShopReference
    {
        public Shop Shop
        {
            get
            {
                if (!m_didQueryShop)
                {
                    m_shop = ShopDefinitionList.Instance.GetShop(m_id);
                    m_didQueryShop = true;
                }

                return m_shop;
            }
        }

        public static ShopReference FromId(long id)
        {
            return new ShopReference(id);
        }

        private ShopReference(long id)
        {
            m_id = id;
        }

        #region Standard Overrides
        public override string ToString()
        {
            return Shop != null ? $"(Shop '{Shop.Name}')" : "BAD_SHOP_ID";
        }

        public override int GetHashCode()
        {
#pragma warning disable RECS0025 // Non-readonly field referenced in 'GetHashCode()'
            return (int)m_id;
#pragma warning restore RECS0025 // Non-readonly field referenced in 'GetHashCode()'
        }

        public override bool Equals(object obj)
        {
            if (obj is ShopReference shop)
                return shop.m_id == m_id;
            return false;
        }
        #endregion

        [SerializeField, GameUtility.IdField]
        private long m_id;

        private bool m_didQueryShop = false;
        private Shop m_shop = null;
    }
}