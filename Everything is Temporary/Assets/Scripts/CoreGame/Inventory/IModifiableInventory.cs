namespace GameInventory
{
    public interface IModifiableInventory : IInventory
    {
                
        /// <summary>
        /// Increments or Decrements the current quantity of the item in inventory.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to modify.</param>
        /// <param name="deltaChange">The amount to modify the specified item by.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        ItemWithAmount ModifyItem(ItemReference itemReference, int deltaChange);

        /// <summary>
        /// A function to add and return the actual change in quantity of a specified item.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to add.</param>
        /// <param name="amountToAdd">The number of the item to add.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the number of item added.</returns>
        ItemWithAmount AddAndGetDelta(ItemReference itemReference, int amountToAdd);

        /// <summary>
        /// A function to remove and return the actual change in quantity of a specified item.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to remove.</param>
        /// <param name="amountToRemove">The number of the item to remove.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the number of item removed.</returns>
        ItemWithAmount RemoveAndGetDelta(ItemReference itemReference, int amountToRemove);

        /// <summary>
        /// Removes the specified item completely from the inventory. Zero of the item remains afterward.
        /// </summary>
        /// <param name="itemReference"></param>
        /// <returns>The amount of the item previously contained in the inventory.</returns>
        int SetZero(ItemReference itemReference);

        /// <summary>
        /// Adds the specified quantity of an item to inventory.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to add.</param>
        /// <param name="amountToAdd">The number of the item to add.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        ItemWithAmount AddItem(ItemReference itemReference, int amountToAdd);

        /// <summary>
        /// Removes the specified quantity of an item from the inventory.
        /// </summary>
        /// <param name="itemReference">The <see cref="ItemReference"/> of the item to remove.</param>
        /// <param name="amountToRemove">The number of the item to remove.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        ItemWithAmount RemoveItem(ItemReference itemReference, int amountToRemove);
        
    }
    
    public static class ModifiableInventoryExt
    {
        /// <summary>
        /// Increments or Decrements the current quantity of the item in inventory.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount by which to change it by (additively).</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        public static ItemWithAmount ModifyItem(this IModifiableInventory modifiableInventory, ItemWithAmount itemWithAmount)
        {
            return modifiableInventory.ModifyItem(itemWithAmount, itemWithAmount.Amount);
        }

        /// <summary>
        /// A function to add and return the actual change in quantity of a specified item.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount of the item and the quantity to add. Amount must be a positive number.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the number of item added.</returns>
        public static ItemWithAmount AddAndGetDelta(this IModifiableInventory modifiableInventory,
            ItemWithAmount itemWithAmount)
        {
            return modifiableInventory.AddAndGetDelta(itemWithAmount, itemWithAmount.Amount);
        }

        /// <summary>
        /// A function to remove and return the actual change in quantity of a specified item.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount of the item and the quantity to remove. Amount must be a positive number.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the number of item removed.</returns>
        public static ItemWithAmount RemoveAndGetDelta(this IModifiableInventory modifiableInventory, ItemWithAmount itemWithAmount)
        {
            return modifiableInventory.RemoveAndGetDelta(itemWithAmount, itemWithAmount.Amount);
        }

        /// <summary>
        /// Adds the specified quantity of an item to inventory.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount of the item and the quantity to add. Amount must be a positive number.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        public static ItemWithAmount AddItem(this IModifiableInventory modifiableInventory, ItemWithAmount itemWithAmount)
        {
            return modifiableInventory.AddItem(itemWithAmount, itemWithAmount.Amount);
        }

        /// <summary>
        /// Removes the specified quantity of an item from the inventory.
        /// </summary>
        /// <param name="itemWithAmount">The item and amount of the item and the quantity to remove. Amount must be a positive number.</param>
        /// <returns>A new <see cref="ItemWithAmount"/> representing the currently held quantity of the item.</returns>
        public static ItemWithAmount RemoveItem(this IModifiableInventory modifiableInventory,
            ItemWithAmount itemWithAmount)
        {
            return modifiableInventory.RemoveItem(itemWithAmount, itemWithAmount.Amount);
        }
    }
}