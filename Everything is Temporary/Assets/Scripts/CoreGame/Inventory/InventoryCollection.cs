using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Characters;

namespace GameInventory
{
    // A collection of separate inventories which are <i>equally</i> affected by changes in item quantity.
    public class InventoryCollection : IModifiableInventory, IDisposable, ICharRefMappedInventoryCollection
    {
        public IEnumerable<KeyValuePair<IModifiableInventory, CharacterReference>> InventoryCharacterReferences => m_inventories;

        public InventoryCollection()
        {
            m_inventories = new Dictionary<IModifiableInventory, CharacterReference>();
        }
            
        public InventoryCollection(IEnumerable<IModifiableInventory> inventories)
        {
            m_inventories = new Dictionary<IModifiableInventory, CharacterReference>();
            foreach (IModifiableInventory modifiableInventory in inventories)
            {
                m_inventories.Add(modifiableInventory, null);
                modifiableInventory.OnItemAmountChanged += ItemAmountChangedHandler;
            }
        }

        public void AddInventory(IModifiableInventory modifiableInventory, CharacterReference associatedCharacterReference = null)
        {
            m_inventories.Add(modifiableInventory, associatedCharacterReference);
            modifiableInventory.OnItemAmountChanged += ItemAmountChangedHandler;
        }

        public bool RemoveInventory(IModifiableInventory modifiableInventory)
        {
            return m_inventories.Remove(modifiableInventory);
        }

        public void ItemAmountChangedHandler(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            OnItemAmountChanged?.Invoke(oldAmount, newAmount);
        }

        public event ItemAmountChangedHandler OnItemAmountChanged;

        public bool ContainsItem(ItemReference itemReference)
        {
            return m_inventories.Keys.Any(inventory => inventory.ContainsItem(itemReference));
        }

        public bool ContainsItem(ItemTag itemTag)
        {
            return m_inventories.Keys.Any(inventory => inventory.ContainsItem(itemTag));
        }

        public bool ContainsItemQuantity(ItemReference itemReference, int minQuantity)
        {
            return m_inventories.Keys.Select(inventory => inventory.CountItem(itemReference)).Sum() >= minQuantity;
        }

        public bool ContainsItemQuantity(ItemTag itemTag, int minQuantity)
        {
            return m_inventories.Keys.Select(inventory => inventory.CountItem(itemTag)).Sum() >= minQuantity;
        }

        public ItemWithAmount GetItem(ItemReference itemReference)
        {
            return m_inventories.Keys.Aggregate(new ItemWithAmount(itemReference, 0), (baseItemWithAmount, inventory) => baseItemWithAmount + inventory.GetItem(itemReference));
        }

        public int CountItem(ItemReference itemReference)
        {
            return m_inventories.Keys.Select(inventory => inventory.CountItem(itemReference)).Sum();
        }

        public int CountItem(ItemTag itemTag)
        {
            return m_inventories.Keys.Select(inventory => inventory.CountItem(itemTag)).Sum();
        }

        #region Modification

        public ItemWithAmount ModifyItem(ItemReference itemReference, int deltaChange)
        {
            return deltaChange < 0 ? RemoveItem(itemReference, -deltaChange) : AddItem(itemReference, deltaChange);
        }

        public ItemWithAmount AddAndGetDelta(ItemReference itemReference, int amountToAdd)
        {
            ItemWithAmount existingCount = GetItem(itemReference);
            // This could probably be sped up a bit by not requiring the random inventory to recalculate for the delta ans just use amount to add. I think this is cleaner though.
            return existingCount + SelectRandomInventory().AddAndGetDelta(itemReference, amountToAdd);
        }
        
        public ItemWithAmount RemoveAndGetDelta(ItemReference itemReference, int amountToRemove)
        {
            ItemWithAmount existingCount = GetItem(itemReference);
            return existingCount - SelectRandomInventory().RemoveAndGetDelta(itemReference, amountToRemove);
        }

        public int SetZero(ItemReference itemReference)
        {
            int previousCount = CountItem(itemReference);
            foreach (IModifiableInventory modifiableInventory in m_inventories.Keys)
            {
                modifiableInventory.SetZero(itemReference);
            }

            return previousCount;
        }

        public ItemWithAmount AddItem(ItemReference itemReference, int amountToAdd)
        {
            return SelectRandomInventory().AddItem(itemReference, amountToAdd);
        }

        public ItemWithAmount RemoveItem(ItemReference itemReference, int amountToRemove)
        {
            return SelectRandomInventory().RemoveItem(itemReference, amountToRemove);
        }

        #endregion //Modification
        
        public IEnumerator<ItemWithAmount> GetEnumerator()
        {
            Inventory baseInventory = new Inventory();
            m_inventories.Keys.ToList().ForEach(inventory => inventory.ToList().ForEach(itemWithAmount => baseInventory.ModifyItem(itemWithAmount)));
            return (IEnumerator<ItemWithAmount>) baseInventory.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public void Dispose()
        {
            foreach (IModifiableInventory modifiableInventory in m_inventories.Keys)
            {
                modifiableInventory.OnItemAmountChanged -= ItemAmountChangedHandler;
            }
        }

        private IModifiableInventory SelectRandomInventory()
        {
            return m_inventories.Keys.ToList()[m_random.Next(m_inventories.Count)];
        }

        private List<ItemWithAmountAndCharRef> EnumerateWithRefs()
        {
            var retList = new List<ItemWithAmountAndCharRef>();
            foreach (KeyValuePair<IModifiableInventory,CharacterReference> inventoryCharacterReference in m_inventories)
            {
                foreach (ItemWithAmount itemWithAmount in inventoryCharacterReference.Key)
                {
                    retList.Add(new ItemWithAmountAndCharRef(inventoryCharacterReference.Value, itemWithAmount));
                }
            }

            return retList;
        }
        
        private static Random m_random = new Random();
        
        private Dictionary<IModifiableInventory, CharacterReference> m_inventories;


    }
}