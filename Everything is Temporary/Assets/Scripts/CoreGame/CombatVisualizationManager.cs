﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Characters;
using CombatSim.CoreSim;
using CombatSim.CoreSim.History;
using GameUtility;
using UnityEngine;


public class CombatVisualizationManager : SingletonMonoBehavior<CombatVisualizationManager>, ICombatEventVisitor
{
    public delegate Task CombatStartedHandler(
        IEnumerable<CharacterReference> partyCharacters,
        IEnumerable<CharacterReference> enemyCharacters);
    public ICollection<CombatStartedHandler> OnCombatStarted = new List<CombatStartedHandler>();

    public delegate Task CharacterHandler(CharacterReference character);
    public ICollection<CharacterHandler> OnCharacterEntered = new List<CharacterHandler>();
    public ICollection<CharacterHandler> OnCharacterExited = new List<CharacterHandler>();

    /// <summary>
    /// Occurs when a character's combat state might have been modified. Does
    /// not occur at start of combat, but does occur when a character enters
    /// or exits combat. If the character exits combat, its state is null.
    /// </summary>
    public event System.Action<CharacterReference, CombatActorState> OnCharacterStateUpdated;

    public async Task EnterCombat()
    {
        m_previousScene = CoreGameManager.Instance.CurrentSceneName;

        await CoreGameManager.Instance.OpenGameSceneAsync(m_genericCombatScene);
    }

    public async Task ExitCombat()
    {
        await CoreGameManager.Instance.OpenGameSceneAsync(m_previousScene);
    }

    public async Task Visualize(CombatEvent combatEvent, CombatState combatState)
    {
        Debug.Assert(m_visualizationTask == null);

        combatEvent.ProcessWith(this);

        if (m_visualizationTask != null)
            await m_visualizationTask;

        m_visualizationTask = null;
    }

    private void CharacterStateChanged(CombatActorId actorId)
    {
        var charRef = CombatManager.Instance.GetCharacterReference(actorId);
        CharacterStateChanged(charRef);
    }

    private void CharacterStateChanged(CharacterReference charRef)
    {
        var characterState = CombatManager.Instance.GetCharacterState(charRef);
        OnCharacterStateUpdated?.Invoke(charRef, characterState);
    }

    void ICombatEventVisitor.Process(CombatStartEvent startEvent)
    {
        var partyMembers = startEvent.InitialTurnOrder
                              .Where(CombatManager.Instance.IsActorPartyMember)
                              .Select(CombatManager.Instance.GetCharacterReference);

        var enemyMembers = startEvent.InitialTurnOrder
                              .Where(CombatManager.Instance.IsActorEnemyMember)
                              .Select(CombatManager.Instance.GetCharacterReference);

        m_visualizationTask = Task.WhenAll(OnCombatStarted.Select(handler => handler(partyMembers, enemyMembers)));
    }

    void ICombatEventVisitor.Process(CombatEndEvent endEvent)
    {
        Debug.Log("COMBAT: Combat ended.");
    }

    void ICombatEventVisitor.Process(CombatNextGameTurnEvent combatNextGameTurnEvent)
    {
        Debug.Log("COMBAT: Next game turn.");
    }

    void ICombatEventVisitor.Process(CombatNextCharacterTurnEvent combatNextCharacterTurnEvent)
    {
        Debug.Log("COMBAT: Next character turn.");
    }

    void ICombatEventVisitor.Process(CombatChangeTurnOrderEvent changeTurnOrderEvent)
    {
        Debug.Log("COMBAT: New turn order.");
    }

    void ICombatEventVisitor.Process(CombatActorEnterCombatEvent combatActorEnterCombatEvent)
    {
        var characterState = combatActorEnterCombatEvent.NewActorInitialState;
        var character = CombatManager.Instance.GetCharacterReference(characterState.Identifier);

        OnCharacterStateUpdated?.Invoke(character, characterState);

        m_visualizationTask = Task.WhenAll(OnCharacterEntered.Select(handler => handler(character)));
    }

    void ICombatEventVisitor.Process(CombatActorExitCombatEvent combatActorExitCombatEvent)
    {
        var character = CombatManager.Instance.GetCharacterReference(combatActorExitCombatEvent.Actor);

        OnCharacterStateUpdated?.Invoke(character, null);

        m_visualizationTask = Task.WhenAll(OnCharacterExited.Select(handler => handler(character)));
    }

    void ICombatEventVisitor.Process(CombatItemUsedEvent combatItemUsedEvent)
    {
        Debug.Log($"COMBAT: {combatItemUsedEvent.Actor} used {combatItemUsedEvent.Item.Name} on" +
            $" {string.Join(", ", combatItemUsedEvent.Targets.Select(target => target.Name))}");

        // The targets' states don't change until further events.
        CharacterStateChanged(combatItemUsedEvent.Actor);
    }

    void ICombatEventVisitor.Process(CombatNoActionMadeEvent combatNoActionMadeEvent)
    {
        Debug.Log($"COMBAT: No action made by {combatNoActionMadeEvent.Actor}.");
    }

    void ICombatEventVisitor.Process(CombatAttemptedFleeBattleEvent combatAttemptedFleeBattleEvent)
    {
        Debug.Log($"COMBAT: {combatAttemptedFleeBattleEvent.Actor} attempted to flee.");
    }

    void ICombatEventVisitor.Process(CombatDamageTakenEvent combatDamageTakenEvent)
    {
        Debug.Log($"COMBAT: {combatDamageTakenEvent.DamagedActor} took {combatDamageTakenEvent.Damage} damage.");

        CharacterStateChanged(combatDamageTakenEvent.DamagedActor);
    }

    void ICombatEventVisitor.Process(CombatHealthRestoredEvent combatHealthRestoredEvent)
    {
        Debug.Log($"COMBAT: {combatHealthRestoredEvent.Target} regained {combatHealthRestoredEvent.RestoredAmount} health.");

        CharacterStateChanged(combatHealthRestoredEvent.Target);
    }

    void ICombatEventVisitor.Process(CombatEffectAppliedEvent combatEffectEvent)
    {
        Debug.Log($"COMBAT: Effect applied to {combatEffectEvent.Target}: {combatEffectEvent.Effect}");

        CharacterStateChanged(combatEffectEvent.Target);
    }

    void ICombatEventVisitor.Process(CombatPersistentEffectStartedEvent combatPersistentEffectStartedEvent)
    {
        Debug.Log($"COMBAT: Persistent effect added to {combatPersistentEffectStartedEvent.Target}: {combatPersistentEffectStartedEvent.Effect}");

        CharacterStateChanged(combatPersistentEffectStartedEvent.Target);
    }

    void ICombatEventVisitor.Process(CombatPersistentEffectEndedEvent combatPersistentEffectEndedEvent)
    {
        Debug.Log($"COMBAT: Persistent effect ended on {combatPersistentEffectEndedEvent.Target}: {combatPersistentEffectEndedEvent.Effect}");

        CharacterStateChanged(combatPersistentEffectEndedEvent.Target);
    }

    void ICombatEventVisitor.Process(CombatActorPositionUpdatedEvent combatActorPositionUpdatedEvent)
    {
        Debug.Log($"COMBAT: Position was updated.");

        // TODO CharacterStateChanged(combatActorPositionUpdatedEvent...);
    }

    [SerializeField]
    private string m_genericCombatScene = "Generic Combat Scene";

    private string m_previousScene = default;

    private Task m_visualizationTask;
}