﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Characters;
using Combat;
using CombatSim.CoreSim;
using CombatSim.CoreSim.Controller;
using CombatSim.CoreSim.History;
using GameInventory;
using GameUtility;
using UnityEngine;

public class CombatManager : SingletonMonoBehavior<CombatManager>, IActorController
{
    public Dictionary<CombatActor, Actor> Mapping = new Dictionary<CombatActor, Actor>();

    /// <summary>
    /// Performs combat against a collection of "test" enemies that are set
    /// through the inspector window on the Combat Manager object.
    /// </summary>
    public async Task PerformTestCombat()
    {
        await PerformCombat(m_enemyCharacters);
    }

    /// <summary>
    /// Performs combat from start to finish with the whole party against the
    /// given list of enemies.
    /// </summary>
    /// <param name="enemies">The enemies the party will fight against.</param>
    public async Task PerformCombat(IEnumerable<CharacterReference> enemies)
    {
        CombatGame game = new CombatGame(CreateAllActors(enemies));

        await CombatVisualizationManager.Instance.EnterCombat();

        CombatEvent lastEvent;

        do
        {
            (lastEvent, m_currentCombatState) = await game.Proceed();

            await CombatVisualizationManager.Instance.Visualize(lastEvent, m_currentCombatState);

        } while (!m_currentCombatState.IsEnded);

        await CombatVisualizationManager.Instance.ExitCombat();
    }

    public delegate Task<CombatPlayerInput> CombatPlayerInputProvider(CharacterReference currentCharacter);
    public CombatPlayerInputProvider PlayerInputProvider
    {
        get => m_playerInputProvider;
        set
        {
            Debug.Assert(m_playerInputProvider == null || value == null,
                "Tried to overwrite existing PlayerInputProvider. The previous one should have" +
                " unregistered itself first by setting this property to null.");

            m_playerInputProvider = value;
        }
    }

    /// <summary>
    /// Checks whether the given character is fighting on the player's side
    /// in combat.
    /// </summary>
    public bool IsCharacterPartyMember(CharacterReference charRef)
    {
        if (!m_characterRefToCombatActor.TryGetValue(charRef, out CombatActorId actorId))
            return false;

        return IsActorPartyMember(actorId);
    }

    /// <summary>
    /// Checks whether the given character is fighting against the player in combat.
    /// </summary>
    public bool IsCharacterEnemyMember(CharacterReference charRef)
    {
        if (!m_characterRefToCombatActor.TryGetValue(charRef, out CombatActorId actorId))
            return false;

        return IsActorEnemyMember(actorId);
    }

    public bool IsActorPartyMember(CombatActorId actorId)
    {
        if (!m_currentCombatState.TryGetActorState(actorId, out CombatActorState actorState))
            return false;

        return actorState.GroupIdentifier == PartyGroupId;
    }

    public bool IsActorEnemyMember(CombatActorId actorId)
    {
        if (!m_currentCombatState.TryGetActorState(actorId, out CombatActorState actorState))
            return false;

        return actorState.GroupIdentifier == EnemyGroupId;
    }

    public CharacterReference GetCharacterReference(CombatActorId actorId)
    {
        if (m_combatActorToCharacterRef.TryGetValue(actorId, out CharacterReference reference))
            return reference;

        return null;
    }

    /// <summary>
    /// Gets the character's current combat state. Returns null if the character
    /// is not in combat.
    /// </summary>
    /// <returns>The character's combat state, or null if the character is not in combat.</returns>
    /// <param name="charRef">The character reference.</param>
    public CombatActorState GetCharacterState(CharacterReference charRef)
    {
        if (!m_characterRefToCombatActor.TryGetValue(charRef, out var actorId))
            return null;

        if (!m_currentCombatState.TryGetActorState(actorId, out var actorState))
            return null;

        return actorState;
    }

    public ItemReference GetItemReference(CombatItemId itemId)
    {
        if (m_combatItemToItemRef.TryGetValue(itemId, out ItemReference itemRef))
            return itemRef;

        return null;
    }

    private IEnumerable<CombatActor> CreateAllActors(IEnumerable<CharacterReference> enemies)
    {
        return CreatePartyActors().Concat(CreateEnemyActors(enemies));
    }

    private IEnumerable<CombatActor> CreatePartyActors()
    {
        yield return new CombatActor(this, CreateCombatActorState(CharacterManager.Instance.Player, PartyGroupId));

        foreach (var charRef in CharacterManager.Instance.CurrentPartyExcludingPlayer)
        {
            yield return new CombatActor(this, CreateCombatActorState(charRef, PartyGroupId));
        }
    }

    private IEnumerable<CombatActor> CreateEnemyActors(IEnumerable<CharacterReference> enemies)
    {
        var enemyController = new TestRandomItemSelectionController();
        foreach (var charRef in enemies)
        {
            yield return new CombatActor(enemyController, CreateCombatActorState(charRef, EnemyGroupId));
        }
    }

    async Task<ActionCommand> IActorController.MakeMove(CombatActorId actor, CombatState combatState)
    {
        if (m_playerInputProvider == null)
            return ActionCommand.None;

        CombatPlayerInput playerInput = await PlayerInputProvider(GetCharacterReference(actor));

        switch (playerInput.Action)
        {
            case CombatPlayerInput.ActionType.None:
                return ActionCommand.None;
            case CombatPlayerInput.ActionType.Flee:
                return ActionCommand.Flee;
            case CombatPlayerInput.ActionType.Item:
                return new ActionCommand(
                    // TODO: Should we allow players to throw more than one item?
                    new CombatItem(GetOrCreateCombatItemId(playerInput.Item), 1),
                    playerInput.Targets.Select(charRef => m_characterRefToCombatActor[charRef]));
            default:
                throw new NotImplementedException();
        }
    }

    private CombatActorId GetOrCreateCombatActorId(CharacterReference charRef)
    {
        if (m_characterRefToCombatActor.TryGetValue(charRef, out CombatActorId actorId))
            return actorId;

        actorId = new CombatActorId(charRef.Character.Name);
        m_combatActorToCharacterRef[actorId] = charRef;
        m_characterRefToCombatActor[charRef] = actorId;

        return actorId;
    }

    private CombatItemId GetOrCreateCombatItemId(ItemReference itemRef)
    {
        if (m_itemRefToCombatItem.TryGetValue(itemRef, out CombatItemId itemId))
            return itemId;

        itemId = new CombatItemId(itemRef.Item.Name, itemRef.Item.Stats.GetCombatStats<int>());
        m_combatItemToItemRef[itemId] = itemRef;
        m_itemRefToCombatItem[itemRef] = itemId;

        return itemId;
    }

    private CombatActorState CreateCombatActorState(CharacterReference charRef, int groupId)
    {
        List<CombatItem> items = new List<CombatItem>();

        Inventory inventory = charRef.Character.Inventory;
        if (inventory != null)
        {
            foreach (ItemWithAmount itemWithAmount in inventory)
            {
                CombatItemId itemId = GetOrCreateCombatItemId(itemWithAmount.ItemReference);
                CombatItem item = new CombatItem(itemId, itemWithAmount.Amount);
                items.Add(item);
            }
        }

        CombatActorId id = GetOrCreateCombatActorId(charRef);

        return new CombatActorState(id, groupId, items);
    }

    [SerializeField]
    [Tooltip("Testing only. These are the enemy characters in all combat.")]
    private List<CharacterReference> m_enemyCharacters = default;

    private Dictionary<CombatActorId, CharacterReference> m_combatActorToCharacterRef = new Dictionary<CombatActorId, CharacterReference>();
    private Dictionary<CharacterReference, CombatActorId> m_characterRefToCombatActor = new Dictionary<CharacterReference, CombatActorId>();

    private Dictionary<CombatItemId, ItemReference> m_combatItemToItemRef = new Dictionary<CombatItemId, ItemReference>();
    private Dictionary<ItemReference, CombatItemId> m_itemRefToCombatItem = new Dictionary<ItemReference, CombatItemId>();

    private CombatState m_currentCombatState;
    private CombatPlayerInputProvider m_playerInputProvider;

    private const int PartyGroupId = 1;
    private const int EnemyGroupId = 2;
}
