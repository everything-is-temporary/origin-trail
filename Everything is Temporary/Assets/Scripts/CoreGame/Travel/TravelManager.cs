﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using GameUtility;
using Characters;
using CoreGame.Progression;

namespace Travel
{
    public class TravelManager : SingletonMonoBehavior<TravelManager>
    {
        [SerializeField]
        private Transform m_travelStartLocation = default;

        [SerializeField]
        [Tooltip("Units of 'units per second'.")]
        private float m_travelSpeed = 0.5f;

        [SerializeField]
        [Tooltip("Number of real-life seconds per in-game day.")]
        private float m_secondsPerDay = 30;

        [SerializeField]
        [Tooltip("The hour of the day at which the party sets up camp. '23' means 11:00 PM.")]
        [Range(0, 23.5f)]
        private int m_campingHour = 22;

        [SerializeField]
        [Tooltip("The hour of the day at which the party wakes up after camping. '23' means 11:00 PM.")]
        [Range(0, 23.5f)]
        private int m_dayStartHour = 8;

        [SerializeField]
        [Tooltip("The scene to open for camping. Must have a CampingManager inside.")]
        private string m_campingScene = "Forest Camping Scene";

        public float CurrentTravelSpeed => IsMoving ? m_travelSpeed : 0;
        public bool IsMoving { get; private set; }

        public async Task PerformRandomEvent(string inkKnot)
        {
            IsMoving = false;
            await CoreGameManager.Instance.PerformDialog(inkKnot);
            IsMoving = true;
        }

        private void Start()
        {
            SpawnParty();
            IsMoving = true;
        }

        private void Update()
        {
            if (IsMoving)
            {
                int previousHour = (int)(ProgressionManager.Instance.CurrentDayCompletionFraction * 24);

                ProgressionManager.Instance.ModifyCurrentDay(Time.deltaTime / m_secondsPerDay);

                int newHour = (int)(ProgressionManager.Instance.CurrentDayCompletionFraction * 24);

                if (previousHour < m_campingHour && newHour >= m_campingHour)
                {
                    BeginCamping();
                }
            }
        }

        private void BeginCamping()
        {
            IsMoving = false;

            CoreGameManager.Instance.PerformCamping(m_campingScene)
                .ContinueWith(_ =>
                {
                    // Update time of day to match day start hour.
                    float currentTime = ProgressionManager.Instance.CurrentDayCompletionFraction;
                    float targetTime = 1 + m_dayStartHour / 24f;
                    ProgressionManager.Instance.ModifyCurrentDay(targetTime - currentTime);

                    // This is necessary because the game clock resets the current
                    // fraction of the day whenever it progresses to a new day
                    // via ModifyCurrentDay().
                    // FIXME: Make ProgressionManager easier to use.
                    ProgressionManager.Instance.ModifyCurrentDay(targetTime);
                })
                .FireAndForget();
        }

        private void SpawnParty()
        {
            var playerChar = CharacterManager.Instance.Player;
            var playerActor = InstantiateCharacter(playerChar);
            playerActor.SetMovementBehavior(new TravelMovementBehavior());

            // This is used by ActorManager.AddPartyMember to make the new
            // party member follow the player.
            ActorManager.Instance.RegisterPlayer(playerActor);

            foreach (var partyChar in CharacterManager.Instance.CurrentPartyExcludingPlayer)
            {
                var partyActor = InstantiateCharacter(partyChar);
                partyActor.FollowOther(playerActor);
            }

            //add the wagon to the scene
            GameObject wagonObject = Instantiate(CharacterManager.Instance.Wagon, m_travelStartLocation.parent);
            wagonObject.transform.position = m_travelStartLocation.position;
            wagonObject.GetComponent<WagonActorScript>().FollowOther(playerActor);

            TownCamera.Instance.DefaultTarget = playerActor.transform;
        }

        private Actor InstantiateCharacter(CharacterReference characterReference)
        {
            GameObject actorObject = Instantiate(characterReference.Character.ScenePrefab, m_travelStartLocation.parent);

            actorObject.transform.position = m_travelStartLocation.position;

            return actorObject.GetComponent<Actor>();
        }
    }
}