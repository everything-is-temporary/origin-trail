﻿using System;
using UnityEngine;
using GameUtility;

namespace Travel
{
    public class TravelRandomEventStarterScript : MonoBehaviour
    {
        public void StartRandomEvent(string knotPath)
        {
            TravelManager.Instance.PerformRandomEvent(knotPath).FireAndForget();
        }
    }
}