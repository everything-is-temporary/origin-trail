﻿using System;
using Characters;
using UnityEngine;
using GameUtility;

namespace Travel
{
    public class TravelMovementBehavior : IMovementBehavior<Actor>
    {
        public MovementSummary StepMove(Actor obj, Rigidbody2D rigidbody)
        {
            rigidbody.velocity = new Vector2(TravelManager.Instance.CurrentTravelSpeed, rigidbody.velocity.y);

            return new MovementSummary(rigidbody.velocity, Direction.Right);
        }
    }
}