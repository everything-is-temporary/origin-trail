﻿using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using Characters;

/// <summary>
/// Script to place on a scene entry point. This will instantiate the party.
/// </summary>
public class SceneEntryPointScript : MonoBehaviour
{
    private void Awake()
    {
        CoreGameManager.Instance.SetupGameSceneActions.Add(InstantiateParty);
    }

    private void OnDestroy()
    {
        CoreGameManager.Instance?.SetupGameSceneActions.Remove(InstantiateParty);
    }

    private Task InstantiateParty()
    {
        // TODO: This can be done cleaner.
        Vector3 position = transform.position;
        float offset = 1;

        var player = InstantiateInScene(CharacterManager.Instance.Player.Character.ScenePrefab, position += offset * Vector3.left);
        var playerActor = player.GetComponent<Actor>();
        playerActor.SetControlledByPlayer();
        ActorManager.Instance.RegisterPlayer(playerActor);
        TownCamera.Instance.DefaultTarget = player.transform;

        foreach (var character in CharacterManager.Instance.CurrentPartyExcludingPlayer)
        {
            GameObject partyMember = InstantiateInScene(character.Character.ScenePrefab, position += offset * Vector3.left);

            // TODO: Make ScenePrefab type Actor?
            partyMember.GetComponent<Actor>().FollowOther(playerActor);
        }

        return Task.CompletedTask;
    }

    /// <summary>
    /// Instantiates the prefab in this script's scene, and sets its parent to the same
    /// parent as this object.
    /// </summary>
    /// <returns>The instantiated object.</returns>
    /// <param name="prefab">The prefab.</param>
    /// <param name="pos">The position at which to instantiate.</param>
    private GameObject InstantiateInScene(GameObject prefab, Vector3 pos)
    {
        var obj = Instantiate(prefab, pos, Quaternion.identity);
        UnityEngine.SceneManagement.SceneManager.MoveGameObjectToScene(obj, gameObject.scene);
        obj.transform.parent = transform.parent;
        return obj;
    }
}
