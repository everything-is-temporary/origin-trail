using System;
using System.Globalization;
using UnityEditor;
using UnityEngine;

namespace CoreGame.Progression
{
    [Serializable]
    public class GameProgressionClock
    {

        [SerializeField] private int m_hoursPerDay = 24;
        [SerializeField] private int m_daysTotal = default;
        [SerializeField] private int m_gameDaysRemaining = default;
        [SerializeField][Range(0, 1)] private float m_currentDayCompletionFraction = default;

        public int DaysTotal => m_daysTotal;
        public int GameDaysRemaining => m_gameDaysRemaining;
        public float CurrentDayCompletionFraction => m_currentDayCompletionFraction;
        public bool DayComplete => m_currentDayCompletionFraction >= 1.0;
        public int HourOfDay => (int)Math.Floor(m_hoursPerDay * m_currentDayCompletionFraction);

        /// <summary>
        /// Changes the number of remaining days and returns the resulting number of days remaining.
        /// </summary>
        /// <param name="dayDelta">The change in days to apply to the number of completed days.</param>
        /// <returns>A number in the set [0, m_daysTotal] representing the remaining number of days.</returns>
        public int ModifyDays(int dayDelta)
        {
            return m_gameDaysRemaining = Mathf.Clamp(m_gameDaysRemaining + dayDelta, 0, m_daysTotal);
        }

        /// <summary>
        /// Modifies the fraction of the current day completed and returns the resulting value.
        /// </summary>
        /// <param name="currentDayDelta">Change to apply to the current day completion fraction.</param>
        /// <returns>The fraction [0, 1.0] of the current day completed.</returns>
        public float ModifyCurrentDay(float currentDayDelta)
        {
            return m_currentDayCompletionFraction = Mathf.Clamp(m_currentDayCompletionFraction + currentDayDelta, 0f, 1f);
        }

        /// <summary>
        /// Resets the fraction of the day completed to 0.
        /// </summary>
        public void ResetFractionOfDayCompleted()
        {
            m_currentDayCompletionFraction = 0f;
        }

        /// <summary>
        /// Decrements the day counter and resets the fraction of completed day to 0.
        /// </summary>
        /// <returns>Number of days remaining.</returns>
        public int ProgressToNextDay()
        {
            ResetFractionOfDayCompleted();
            return Math.Min(--m_gameDaysRemaining, 0);
        }
        
        /// <summary>
        /// Returns the GameProgressionClock (representing fraction of game completed) as a float in range [0, 1]
        /// </summary>
        /// <returns>A float between 0 and 1 representing the elapsed time of the game.</returns>
        public double GameProgressionCompletionFraction()
        {
            // The Math.Max is not particularly necessary here, it is here in case a future operation will result in m_currentDayCompletionFraction exceeding 1.
            return Math.Min( (m_daysTotal-m_gameDaysRemaining) / (double)m_daysTotal + 1/(double)m_daysTotal * Math.Max(m_currentDayCompletionFraction, 0), 1.0);
        }
        
        public bool RemainingTimeEquals(GameProgressionClock gameProgressionClock)
        {
            if (gameProgressionClock == null) return false;
            return gameProgressionClock.GameDaysRemaining == GameDaysRemaining && Mathf.Approximately(gameProgressionClock.CurrentDayCompletionFraction, CurrentDayCompletionFraction);
        }
        
        public override string ToString()
        {
            return $"<{GameProgressionCompletionFraction().ToString(CultureInfo.CurrentCulture)}>";
        }

    }
}