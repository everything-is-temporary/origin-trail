using GameUtility.DataPersistence;
using GameUtility.Serialization;
using UnityEngine;

namespace CoreGame.Progression
{
    public class ProgressionManager : ISaveLoadableManager<ProgressionManager>
    {
        [SerializeField] private GameProgressionClock m_gameClock;

        #region Event and Delegate Definitions

        public delegate void GameDayChange(int remainingDays);
        public event GameDayChange OnGameDayChange;

        public delegate void GameTimeChange(float currentDayFraction);
        public event GameTimeChange OnGameTimeChange;

        #endregion

        #region Game Clock Access

        public int DaysTotal => m_gameClock.DaysTotal;
        public int GameDaysRemaining => m_gameClock.GameDaysRemaining;
        public float CurrentDayCompletionFraction => m_gameClock.CurrentDayCompletionFraction;
        public bool DayComplete => m_gameClock.DayComplete;
        public int HourOfDay => m_gameClock.HourOfDay;
        public double GameProgressionCompletionFraction => m_gameClock.GameProgressionCompletionFraction();

        public int ModifyDays(int dayDelta)
        {
            int result = m_gameClock.ModifyDays(dayDelta);
            OnGameDayChange?.Invoke(result);
            return result;
        }

        public float ModifyCurrentDay(float currentDayDelta)
        {
            float result = m_gameClock.ModifyCurrentDay(currentDayDelta);
            OnGameTimeChange?.Invoke(result);
            if (DayComplete)
            {
                ProgressToNextDay();
            }
            return result;
        }

        public int ProgressToNextDay()
        {
            int result = m_gameClock.ProgressToNextDay();
            OnGameDayChange?.Invoke(result);
            return result;
        }

        #endregion

        #region Serialization

        public override ISerializationCallbackReceiver GetSerializedData()
        {
            return new SerializableDataContainer<GameProgressionClock>(m_gameClock);
        }

        public override bool SetSerializedData(ISerializationCallbackReceiver data)
        {
            m_gameClock = (data as SerializableDataContainer<GameProgressionClock>)?.Data;
            if (m_gameClock == null)
            {
                Debug.LogWarning("Unable to retrieve game clock from deserialized data.");
                return false;
            }

            return true;
        }

        #endregion
    }
}