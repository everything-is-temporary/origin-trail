﻿using System.Collections;
using System.Collections.Generic;
using GameUI;
using GameUI.Abstractions;
using GameUI.Raycasting;
using GameUtility;
using GameUtility.ObserverPattern;
using UnityEngine;

/// <summary>
/// The main camera in any sidescroller scene that follows some object (usually
/// the player) and also registers itself to be drawn in the Game View.
/// </summary>
[RequireComponent(typeof(Camera))]
public class TownCamera : SingletonMonoBehavior<TownCamera>, IInteractiveView
{
    /// <summary>
    /// The camera will follow this object by default.
    /// </summary>
    public Transform DefaultTarget { get; set; }

    /// <summary>
    /// The camera will follow the DefaultTarget's position plus this offset.
    /// </summary>
    /// <value>The offset.</value>
    public float OffsetX
    {
        get => m_horizontalOffset;
        set => m_horizontalOffset = value;
    }

    /// <summary>
    /// Lets the TownCamera be implicitly converted to a Camera.
    /// </summary>
    public static implicit operator Camera(TownCamera cam)
    {
        return cam.m_camera;
    }

    /// <summary>
    /// Grabs the camera focus. The camera will follow the given transform.
    /// Call Unsubscribe() to release the focus.
    /// </summary>
    /// <returns>The camera focus.</returns>
    /// <param name="targetPos">Target position.</param>
    public IUnsubscriber GrabCameraFocus(Transform targetPos)
    {
        m_cameraFocusStack.Add(targetPos);

        return new ListUnsubscriber<Transform>(m_cameraFocusStack, targetPos);
    }

    protected override void PostAwake()
    {
        m_camera = GetComponent<Camera>();
        m_camera.enabled = false;
        m_camera.clearFlags = CameraClearFlags.Nothing;

        // TODO: Remove game view slot from TownCamera; use InteractiveViewSlotAcquirer component instead
        if (m_gameViewSlot != null)
            m_gameViewSlotUnsubscriber = m_gameViewSlot.SetView(this);

        m_cameraFocusStack = new List<Transform>();
    }

    private void Start()
    {
        if (DefaultTarget != null)
        {
            // Center camera on player.
            transform.position = new Vector3(DefaultTarget.position.x, transform.position.y, transform.position.z);
        }

        StartCoroutine(CatchUpWithPlayer());
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        m_gameViewSlotUnsubscriber?.Unsubscribe();
    }

    private IEnumerator CatchUpWithPlayer()
    {
        while (true)
        {
            Vector3 targetPos = GetTargetPosition();

            // Move toward targetPos smoothly.
            transform.position = Vector3.Lerp(transform.position, targetPos, 0.2f);

            yield return null;
        }
    }

    private Vector3 GetTargetPosition()
    {
        var cameraPosition = transform.position;

        if (m_cameraFocusStack.Count > 0)
        {
            // When focusing on something that's not the player, try to center
            // over it perfectly.
            cameraPosition.x = m_cameraFocusStack[m_cameraFocusStack.Count - 1].position.x;
        }
        else if (DefaultTarget != null)
        {
            // In this case, we want to focus on the player. When we follow
            // the player, we want some wiggle room: if the player is near the
            // center of the screen and moves a little bit horizontally, the
            // camera should not move until the player passes some threshold.
            float targetX = DefaultTarget.position.x + OffsetX;
            float cameraX = transform.position.x;

            if (cameraX < targetX - m_maxOffset)
            {
                targetX -= m_maxOffset;
            }
            else if (cameraX > targetX + m_maxOffset)
            {
                targetX += m_maxOffset;
            }
            else
            {
                targetX = cameraX;
            }

            cameraPosition.x = targetX;
        }

        return cameraPosition;
    }

    void IInteractiveView.RenderTo(RenderTexture target)
    {
        m_camera.targetTexture = target;
        m_camera.Render();
    }

    IRaycaster IInteractiveView.GetRaycaster()
    {
        if (m_raycaster != null)
            return (IRaycaster)m_raycaster;
        return null;
    }

    [SerializeField]
    [RequiresType(typeof(IRaycaster))]
    private Object m_raycaster = default;

    [SerializeField]
    private InteractiveViewSlot m_gameViewSlot = default;
    private IUnsubscriber m_gameViewSlotUnsubscriber;

    [SerializeField]
    private float m_horizontalOffset = default;

    private Camera m_camera;

    private const float m_maxOffset = 0.3f;

    /// <summary>
    /// The last entry in this list should be focused on. If the list
    /// is empty, the camera should focus on the player.
    /// </summary>
    private List<Transform> m_cameraFocusStack;
}
