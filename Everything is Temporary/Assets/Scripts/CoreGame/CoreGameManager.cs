﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GameUtility;
using UnityEngine.SceneManagement;
using GameUtility.DataPersistence;
using UnityEngine;
using CoreGame.Progression;

/// <summary>
/// Controls large-scale actions such as switching between top screen scenes and
/// determining win/ loss conditions to transition to the appropriate Unity scene.
/// </summary>
public class CoreGameManager : ISaveLoadableManager<CoreGameManager>
{
    public string CurrentSceneName => m_currentGameScene;

    /// <summary>
    /// Collection of asynchronous operations to run before unloading the game scene.
    /// These operations will be invoked simultaneously, and the current game scene will
    /// not be unloaded until all tasks complete. The list is cleared before a new
    /// game scene is loaded.
    /// </summary>
    /// <value>The unload actions.</value>
    public ICollection<Func<Task>> UnloadGameSceneActions { get; private set; } = new List<Func<Task>>();

    /// <summary>
    /// Collection of asynchronous operations to run to set up a newly loaded game
    /// scene. These operations will be invoked simultaneously, and the <see cref="OnGameSceneSetupFinished"/>
    /// event will not be raised until all operations complete. This list is NOT cleared
    /// when a new scene is loaded.
    /// <para>
    /// These actions will be invoked after Awake() is called on every object in the scene, but
    /// before Start(). Register actions in the Awake() phase.
    /// </para>
    /// </summary>
    /// <value>The setup game scene actions.</value>
    public ICollection<Func<Task>> SetupGameSceneActions { get; private set; } = new List<Func<Task>>();

    /// <summary>
    /// Occurs immediately after a new game scene is loaded and set up. See also
    /// <seealso cref="RunOnceWhenSceneLoads(Action)"/>.
    /// </summary>
    public event Action OnGameSceneSetupFinished;

    /// <summary>
    /// Runs the action once when the scene is loaded and set up. If the scene
    /// is already loaded, it will run the action immediately.
    /// </summary>
    /// <param name="action">The action to run.</param>
    public void RunOnceWhenSceneLoads(Action action)
    {
        if (m_sceneSetupFinished)
            action();
        else
            m_actionsWhenLoaded.Add(action);
    }

    /// <summary>
    /// Opens a new scene to display in the game view (aka sidescroller view).
    /// </summary>
    /// <returns>The game scene async.</returns>
    /// <param name="sceneName">Scene name.</param>
    public async Task OpenGameSceneAsync(string sceneName)
    {
        if (m_isOpeningScene)
        {
            Debug.LogError("Tried to open a scene while already in the" +
            	" process of opening a scene. Only the first scene will open.");
            return;
        }

        m_isOpeningScene = true;

        if (sceneName == m_currentGameScene)
            return;

        if (sceneName == "Art_TutorialScene")
        {
            GameInputManager.Instance.IsPageFlipEnabled = true;
        }

        // Unload previous scene if it isn't null.
        if (m_currentGameScene != null)
        {
            // Wait for all on-unload tasks to complete.
            await Task.WhenAll(from action in UnloadGameSceneActions select action());
            await SceneManager.UnloadSceneAsync(m_currentGameScene);
        }

        UnloadGameSceneActions.Clear();

        m_currentGameScene = sceneName;
        m_sceneSetupFinished = false;
        await SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

        // Wait to set up scene.
        await Task.WhenAll(from action in SetupGameSceneActions select action());

        m_sceneSetupFinished = true;
        OnGameSceneSetupFinished?.Invoke();

        m_isOpeningScene = false;
    }

    /// <summary>
    /// Runs the camping phase start-to-finish.
    /// </summary>
    /// <returns>
    /// A Task that is finished when camping completes.
    /// </returns>
    public async Task PerformCamping(string campingScene)
    {
        string previousScene = CurrentSceneName;

        await OpenGameSceneAsync(campingScene);

        try
        {
            await Camping.CampingManager.Instance.WaitForCampingToFinish();
        }
        catch (TaskCanceledException)
        {
            // This should never happen in the release build.
            // It can happen in the Unity editor if a developer uses an
            // editor tool to load a new scene.
            Debug.LogWarning($"Camping phase ended prematurely. This is probably not an issue.");

            return;
        }

        await OpenGameSceneAsync(previousScene);
    }


    public async Task PerformDialog(string knotPath)
    {
        if (m_dialogSemaphore.CurrentCount != 1)
            Debug.LogWarning($"Tried to start a new dialog at path '{knotPath}' during dialog.");

        // Enforce only one dialog at a time.
        await m_dialogSemaphore.WaitAsync();

        GameInputManager.Instance.ToggleMovementAndInteraction(false);

        Dialogue.StoryManager.Instance.LoadStoryKnot(knotPath);
        await GameUI.GameUIHelper.Instance.DisplayDialogueAsync();

        GameInputManager.Instance.ToggleMovementAndInteraction(true);

        m_dialogSemaphore.Release();
    }

    protected override void PostAwake()
    {
        OnGameSceneSetupFinished += delegate
        {
            foreach (var action in m_actionsWhenLoaded)
                action();

            m_actionsWhenLoaded.Clear();
        };
    }

    protected override void Start()
    {
        base.Start();
    }

    #region SaveLoad

    private static readonly string m_activeSceneKey = "activeScene";

    public override ISerializationCallbackReceiver GetSerializedData()
    {
        SerializableDict<string, object> serialization = new SerializableDict<string, object>();
        serialization.Add(m_activeSceneKey, m_currentGameScene);
        return serialization;
    }

    public override bool SetSerializedData(ISerializationCallbackReceiver data)
    {
        SerializableDict<string, object> deserialization = (SerializableDict<string, object>)data;
        if (!deserialization.ContainsKey(m_activeSceneKey))
        {
            //TODO: Also check if the scene exists in project befote trying to load it (doesn't seem like there is a clean solution for this)
            // Immediately fail loading if we can't find the scene in savedata
            return false;
        }
        string activeScene = (string)deserialization[m_activeSceneKey];
        if (activeScene != null)
        {
            OpenGameSceneAsync(activeScene).FireAndForget();
        }
        return true;
    }

    #endregion

    private bool m_isOpeningScene = false;
    private bool m_sceneSetupFinished = false;
    private string m_currentGameScene = null;

    private SemaphoreSlim m_dialogSemaphore = new SemaphoreSlim(1);

    private readonly IList<Action> m_actionsWhenLoaded = new List<Action>();
}
