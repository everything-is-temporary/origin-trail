﻿using System;
using System.Collections.Generic;
using GameUtility.DataPersistence;
using GameUtility.Serialization;
using UnityEngine;

public class SceneDataManager : ISaveLoadableManager<SceneDataManager>
{
    /// <summary>
    /// Sets the saveable object for the given key. This object's data will
    /// be saved when the game is saved. This will return true if this overwrote
    /// a previous object set for this key, and false otherwise.
    /// </summary>
    /// <exception cref="ArgumentException">Thrown when an object is already registered for <paramref name="key"/></exception>
    /// <param name="key">Key.</param>
    /// <param name="obj">Saveable object.</param>
    public bool SetSaveableObject(string key, SceneSaveableObject obj)
    {
        bool didOverwrite = m_saveableObjects.ContainsKey(key);
        m_saveableObjects[key] = obj;

        return didOverwrite;
    }

    /// <summary>
    /// Removes the saveable object from the key. Any saved data for this key
    /// will remain untouched, but it will not be updated unless another object
    /// is registered.
    /// </summary>
    /// <returns>Returns true if the object was registered for this key, and false
    /// otherwise (in which case nothing happened).</returns>
    /// <param name="key">Key.</param>
    public bool RemoveSaveableObject(string key, SceneSaveableObject obj)
    {
        if (m_saveableObjects.TryGetValue(key, out var existingObj))
        {
            if (obj != existingObj)
                return false;
        }

        return m_saveableObjects.Remove(key);
    }

    /// <summary>
    /// Removes the data saved for a particular key.
    /// </summary>
    /// <param name="key">Key.</param>
    public void RemoveSavedData(string key)
    {
        m_savedData.Remove(key);
    }

    /// <summary>
    /// Updates the save data for this key.
    /// </summary>
    /// <exception cref="ArgumentException">Thrown if the key did not have a
    /// corresponding object.</exception>
    /// <param name="key">Key.</param>
    public void SaveObject(string key)
    {
        m_savedData[key] = m_saveableObjects[key].GetSaveData();
    }

    /// <summary>
    /// Retrieves the data saved for the key.
    /// </summary>
    /// <returns><c>true</c>, if the key had saved data, <c>false</c> otherwise.</returns>
    /// <param name="key">Key.</param>
    /// <param name="data">Output saved data. This is only valid if the method returns true.</param>
    public bool TryGetSaveData(string key, out object data)
    {
        return m_savedData.TryGetValue(key, out data);
    }

    #region ISaveLoadable
    public override ISerializationCallbackReceiver GetSerializedData()
    {
        SaveAll();
        return m_savedData;
    }

    public override bool SetSerializedData(ISerializationCallbackReceiver data)
    {
        if (data is SerializableDictionary<string, object> savedData)
        {
            m_savedData = savedData;
            return true;
        }

        return false;
    }
    #endregion

    private void SaveAll()
    {
        foreach (var keyAndObject in m_saveableObjects)
        {
            m_savedData[keyAndObject.Key] = keyAndObject.Value.GetSaveData();
        }
    }

    private Dictionary<string, SceneSaveableObject> m_saveableObjects = new Dictionary<string, SceneSaveableObject>();
    private SerializableDictionary<string, object> m_savedData = new SerializableDictionary<string, object>();
}

