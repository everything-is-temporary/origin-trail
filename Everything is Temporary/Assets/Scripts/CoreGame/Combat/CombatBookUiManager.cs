﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Characters;
using Combat.UI;
using GameUI;
using UnityEngine;
using GameInventory;
using GameUtility;

namespace Combat
{
    public class CombatBookUiManager : SingletonMonoBehavior<CombatBookUiManager>
    {
        protected override void PostAwake()
        {
            // This has to happen on Awake() because combat starts before Start() is called.
            CombatManager.Instance.PlayerInputProvider = WaitForPlayerInput;

            m_combatSection.OnEscape += HandleEscape;
            m_combatSection.OnItemUseAction += HandleItemUsed;
        }

        protected override void OnDestroy()
        {
            if (CombatManager.Instance != null)
                CombatManager.Instance.PlayerInputProvider -= WaitForPlayerInput;

            BookScript.Instance?.RemoveAndCloseSection(m_combatSection);
            BookUIManager.Instance?.FocusGame();
            base.OnDestroy();
        }

        private void Start()
        {
            CombatSceneManager.Instance.OnCharacterClicked += HandleCharacterClicked;

            BookScript.Instance.InsertSectionAtEnd(m_combatSection);
            BookScript.Instance.FlipTo(m_combatSection);
            BookUIManager.Instance.CombatMode();
        }

        private async Task<CombatPlayerInput> WaitForPlayerInput(CharacterReference charRef)
        {
            Debug.Assert(m_playerInputTask == null, "Already waiting for input, but called wait again.");

            m_combatSection.ShowActionsForCharacter(charRef);
            m_expectedSource = charRef;
            m_playerInputTask = new TaskCompletionSource<CombatPlayerInput>();

            var result = await m_playerInputTask.Task;
            m_playerInputTask = null;

            return result;
        }

        private void HandleEscape()
        {
            m_playerInputTask?.SetResult(CombatPlayerInput.Flee);
        }

        private void HandleItemUsed(CharacterReference source, ItemWithAmount itemWithAmount, CharacterReference target)
        {
            if (m_expectedSource == source)
                m_playerInputTask?.SetResult(new CombatPlayerInput(itemWithAmount.ItemReference, target));
        }

        private void HandleCharacterClicked(CharacterReference charRef)
        {
            if (CombatManager.Instance.IsCharacterPartyMember(charRef))
                m_combatSection.ShowDetailForPartyCharacter(charRef);
            else if (CombatManager.Instance.IsCharacterEnemyMember(charRef))
                m_combatSection.ShowDetailForEnemyCharacter(charRef);
        }

        private TaskCompletionSource<CombatPlayerInput> m_playerInputTask;
        private CharacterReference m_expectedSource;

        [SerializeField]
        private CombatSectionScript m_combatSection = default;
    }
}
