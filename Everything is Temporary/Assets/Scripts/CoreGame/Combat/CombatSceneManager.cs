﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;
using Characters;

namespace Combat
{
    public class CombatSceneManager : SingletonMonoBehavior<CombatSceneManager>
    {
        public delegate void CharacterHandler(CharacterReference characterReference);

        public event CharacterHandler OnCharacterClicked;

        protected override void PostAwake()
        {
            CombatVisualizationManager.Instance.OnCombatStarted.Add(HandleCombatStarted);
            CombatVisualizationManager.Instance.OnCharacterExited.Add(HandleCharacterExited);
        }

        protected override void OnDestroy()
        {
            CombatVisualizationManager.Instance?.OnCombatStarted.Remove(HandleCombatStarted);
            base.OnDestroy();
        }

        private Task HandleCombatStarted(
            IEnumerable<CharacterReference> partyCharacters,
            IEnumerable<CharacterReference> enemyCharacters)
        {
            m_partyActors = new List<BaseCombatActor>();
            m_enemyActors = new List<BaseCombatActor>();

            foreach (var partyChar in partyCharacters)
            {
                MakePartyActor(partyChar);
            }

            foreach (var enemyChar in enemyCharacters)
            {
                MakeEnemyActor(enemyChar);
            }

            return Task.CompletedTask;
        }

        private Task HandleCharacterExited(CharacterReference charRef)
        {
            if (m_characterToActor.TryGetValue(charRef, out BaseCombatActor actor))
            {
                Destroy(actor.gameObject);

                m_characterToActor.Remove(charRef);
                m_partyActors.Remove(actor);
                m_enemyActors.Remove(actor);
            }

            return Task.CompletedTask;
        }

        private void MakePartyActor(CharacterReference charRef)
        {
            Transform position = m_partyCharacterPositions[m_partyActors.Count];

            BaseCombatActor actor = CreateActor(charRef, position);
            actor.FacingDirection = Direction.Right;

            if (actor != null)
                m_partyActors.Add(actor);
        }

        private void MakeEnemyActor(CharacterReference charRef)
        {
            Transform position = m_enemyCharacterPositions[m_enemyActors.Count];

            BaseCombatActor actor = CreateActor(charRef, position);
            actor.FacingDirection = Direction.Left;

            if (actor != null)
                m_enemyActors.Add(actor);
        }

        private BaseCombatActor CreateActor(CharacterReference charRef, Transform position)
        {
            if (charRef.Character.CombatPrefab == null)
            {
                Debug.LogError($"Character {charRef.Character.Name} doesn't have a combat prefab.");
                return null;
            }

            BaseCombatActor actor = Instantiate(charRef.Character.CombatPrefab, position);
            m_characterToActor.Add(charRef, actor);

            actor.OnActorClicked += () => OnCharacterClicked?.Invoke(charRef);

            return actor;
        }

        [SerializeField]
        [Tooltip("Where party characters are placed for combat.")]
        private List<Transform> m_partyCharacterPositions = default;

        [SerializeField]
        [Tooltip("Where enemy characters are placed for combat.")]
        private List<Transform> m_enemyCharacterPositions = default;

        /// <summary>
        /// List of actors representing party characters, ordered by distance from enemy.
        /// </summary>
        private List<BaseCombatActor> m_partyActors;

        /// <summary>
        /// List of actors representing enemy characters, ordered by distance from party.
        /// </summary>
        private List<BaseCombatActor> m_enemyActors;

        /// <summary>
        /// Maps character references to their actors.
        /// </summary>
        private Dictionary<CharacterReference, BaseCombatActor> m_characterToActor = new Dictionary<CharacterReference, BaseCombatActor>();
    }
}
