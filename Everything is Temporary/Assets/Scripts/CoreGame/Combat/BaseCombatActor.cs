﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using GameUtility;

namespace Combat
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class BaseCombatActor : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public Direction FacingDirection
        {
            get => m_facingDirection;
            set
            {
                m_facingDirection = value;
                SetSpriteRendererDirection();
            }
        }

        public event Action OnActorClicked;

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            m_spriteRenderer.color = Color.gray;
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            m_spriteRenderer.color = Color.white;
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            OnActorClicked?.Invoke();
        }

        protected virtual void Awake()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            SetSpriteRendererDirection();
        }

        protected virtual void OnDestroy()
        {
        }

        /// <summary>
        /// If <see cref="m_spriteRenderer"/> isn't null, sets its flipX value
        /// to correctly represent <see cref="FacingDirection"/>.
        /// </summary>
        private void SetSpriteRendererDirection()
        {
            if (m_spriteRenderer != null)
                m_spriteRenderer.flipX = m_isSpriteFacingRight != (FacingDirection == Direction.Right);
        }

        protected SpriteRenderer m_spriteRenderer;
        private Direction m_facingDirection;


        [SerializeField]
        private bool m_isSpriteFacingRight = true;
    }
}