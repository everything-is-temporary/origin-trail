﻿using System;
using UnityEngine;

namespace Combat.UI
{
    using UI = UnityEngine.UI;
    public class CombatTargetIconScript : MonoBehaviour
    {
        public event Action OnIconClicked;

        public bool Highlighted
        {
            get => m_highlighted;
            set
            {
                m_highlighted = value;

                if (value)
                    Highlight();
                else
                    Unhighlight();
            }
        }

        public void DisplayIcon(Sprite sprite)
        {
            m_icon.sprite = sprite;
        }

        public void ButtonClick()
        {
            OnIconClicked?.Invoke();
        }

        private void Highlight()
        {
            m_outline.enabled = true;
        }

        private void Unhighlight()
        {
            m_outline.enabled = false;
        }

        private void Awake()
        {
            m_outline = gameObject.GetComponent<UI.Outline>();

            if (m_outline == null)
                m_outline = gameObject.AddComponent<UI.Outline>();

            m_outline.enabled = false;
        }

        private bool m_highlighted;

        private UI.Outline m_outline;

        [SerializeField]
        private UI.Image m_icon = default;
    }
}
