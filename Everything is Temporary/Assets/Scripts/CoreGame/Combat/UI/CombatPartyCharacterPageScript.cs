﻿using Characters;
using GameUI;
using UnityEngine;
using CombatSim.CoreSim;

namespace Combat.UI
{
    using UI = UnityEngine.UI;
    public class CombatPartyCharacterPageScript : SimpleInteractiveView
    {
        public void DisplayCharacter(CharacterReference charRef)
        {
            m_displayedCharacter = charRef;

            CharacterDefinition character = charRef.Character;
            CombatActorState characterState = CombatManager.Instance.GetCharacterState(charRef);

            m_characterName.text = character.Name;
            m_characterIcon.sprite = character.ScenePrefab.GetComponent<SpriteRenderer>()?.sprite;

            DisplayStats(characterState);
        }

        private void HandleCharacterStateChanged(CharacterReference charRef, CombatActorState charState)
        {
            if (!charRef.Equals(m_displayedCharacter))
                return;

            DisplayStats(charState);
        }

        private void DisplayStats(CombatActorState charState)
        {
            if (charState == null)
            {
                m_healthText.text = "N/A";
                m_attackText.text = "N/A";
                m_defenseText.text = "N/A";
            }
            else
            {
                m_healthText.text = $"{charState.StatsActual[CombatStatType.Health]:F2}";
                m_attackText.text = $"{charState.StatsActual[CombatStatType.Attack]:F2}";
                m_defenseText.text = $"{charState.StatsActual[CombatStatType.Defense]:F2}";
            }
        }

        private void Start()
        {
            CombatVisualizationManager.Instance.OnCharacterStateUpdated += HandleCharacterStateChanged;
        }

        private void OnDestroy()
        {
            if (CombatVisualizationManager.Instance != null)
                CombatVisualizationManager.Instance.OnCharacterStateUpdated -= HandleCharacterStateChanged;
        }

        private CharacterReference m_displayedCharacter;

        [SerializeField]
        private UI.Text m_characterName = default;

        [SerializeField]
        private UI.Image m_characterIcon = default;

        [SerializeField]
        private UI.Text m_healthText = default;

        [SerializeField]
        private UI.Text m_attackText = default;

        [SerializeField]
        private UI.Text m_defenseText = default;
    }
}