﻿using System;
using Characters;
using GameInventory;
using GameUI;
using GameUI.Abstractions;
using GameUtility;
using UnityEngine;

namespace Combat.UI
{
    using NumberedCombatPage = NumberedPage<SwappablePage>;

    public class CombatSectionScript : MonoBehaviour, IBookSection
    {
        /// <summary>
        /// Occurs when the player selects the escape action.
        /// </summary>
        public event Action OnEscape;


        public delegate void UseItemHandler(CharacterReference inventorySource, ItemWithAmount itemWithAmount, CharacterReference target);

        /// <summary>
        /// Occurs when the player selects an itemWithAmount from some character's inventory.
        /// </summary>
        public event UseItemHandler OnItemUseAction;

        public void ShowActionsForCharacter(CharacterReference charRef)
        {
            m_actionPage.DisplayActionsForCharacter(charRef);
        }

        public void ShowDetailForPartyCharacter(CharacterReference charRef)
        {
            m_partyPage.DisplayCharacter(charRef);

            m_currentLeftPage.PageScript.SetPage(m_partyPage);
        }

        public void ShowDetailForEnemyCharacter(CharacterReference charRef)
        {
            // TODO: Need to pass some info to the enemy page.

            m_currentLeftPage.PageScript.SetPage(m_enemyPage);
        }

        private void Awake()
        {
            m_currentLeftPage.PageScript.SetPage(m_partyPage);
            m_currentRightPage.PageScript.SetPage(m_actionPage);

            m_actionPage.OnEscape += () => OnEscape?.Invoke();
            m_actionPage.OnItemUseAction += (source, item, target) => OnItemUseAction?.Invoke(source, item, target);
        }

        #region IBookSection
        string IBookSection.SectionName => "Combat";

        Two<IInteractiveView> IBookSection.AcquirePages(int containingPage)
        {
            Debug.Assert(containingPage >= 0 && containingPage <= 1);

            return new Two<IInteractiveView>(m_currentLeftPage, m_currentRightPage);
        }

        int IBookSection.GetInitialPage() => 0;
        int IBookSection.GetMaxPage() => 1;
        int IBookSection.GetMinPage() => 0;

        int IBookSection.GetPageNumber(IInteractiveView page)
        {
            return ((NumberedCombatPage)page).PageNumber;
        }

        void IBookSection.ReleasePage(IInteractiveView page)
        {
            ((NumberedCombatPage)page).IsAcquired = false;
        }
        #endregion

        [SerializeField] private CombatPartyCharacterPageScript m_partyPage = default;
        [SerializeField] private CombatEnemyCharacterPageScript m_enemyPage = default;
        [SerializeField] private CombatActionPageScript m_actionPage = default;

        private NumberedCombatPage m_currentLeftPage = new NumberedCombatPage(new SwappablePage(), 0);
        private NumberedCombatPage m_currentRightPage = new NumberedCombatPage(new SwappablePage(), 1);
    }
}