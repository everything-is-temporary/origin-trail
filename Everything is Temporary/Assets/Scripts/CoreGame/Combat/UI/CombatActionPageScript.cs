﻿using System;
using System.Linq;
using System.Collections.Generic;
using Characters;
using GameInventory;
using GameUI;
using GameUI.Abstractions;
using GameUtility;
using UnityEngine;
using CombatSim.CoreSim;

namespace Combat.UI
{
    public class CombatActionPageScript : SimpleInteractiveView
    {
        /// <summary>
        /// Occurs when the player selects the escape action.
        /// </summary>
        public event Action OnEscape;

        public delegate void UseItemHandler(CharacterReference inventorySource, ItemWithAmount itemWithAmount, CharacterReference target);

        /// <summary>
        /// Occurs when the player selects the action to use an item on some character.
        /// </summary>
        public event UseItemHandler OnItemUseAction;


        public void DisplayActionsForCharacter(CharacterReference charRef)
        {
            if (m_displayedCharacter != null && m_displayedCharacter.Equals(charRef))
                return;

            m_displayedCharacter = charRef;
            m_displayedCombatInventory = new Inventory();

            UpdateDisplayedInventory();

            InventoryDisplay.DisplayedInventory = m_displayedCombatInventory;
        }


        protected override void Awake()
        {
            base.Awake();

            // TODO For now, every item has the player and the player's party as the targets.

            CreateTargetIconForCharacter(CharacterManager.Instance.Player);
            foreach (var charRef in CharacterManager.Instance.CurrentPartyExcludingPlayer)
            {
                CreateTargetIconForCharacter(charRef);
            }

            InventoryDisplay.OnItemDisplaySelected += HandleItemStackSelected;

            m_useItemButton.onClick.AddListener(HandleUseItemClicked);
            UpdateItemUseButton();

            CombatVisualizationManager.Instance.OnCharacterStateUpdated += HandleCharacterStateChanged;
        }

        private void OnDestroy()
        {
            if (CombatVisualizationManager.Instance != null)
                CombatVisualizationManager.Instance.OnCharacterStateUpdated -= HandleCharacterStateChanged;
        }

        private CombatTargetIconScript InstantiateActionTargetIcon()
        {
            CombatTargetIconScript iconScript = Instantiate(m_combatTargetIconPrefab, m_actionTargetArea);

            return iconScript;
        }

        private void UpdateDisplayedInventory()
        {
            Dictionary<ItemReference, int> newItemCounts = new Dictionary<ItemReference, int>();

            // Set all item counts to zero first, so that we know to remove them
            // if they do not show up in the new item list.
            foreach (ItemWithAmount stack in m_displayedCombatInventory)
            {
                newItemCounts[stack.ItemReference] = 0;
            }

            // Get new item counts.
            CombatActorState charState = CombatManager.Instance.GetCharacterState(m_displayedCharacter);
            if (charState != null)
            {
                foreach (CombatItem itemStack in charState.Items)
                {
                    var itemRef = CombatManager.Instance.GetItemReference(itemStack.Identifier);

                    newItemCounts[itemRef] = itemStack.Quantity;
                }
            }

            // Update item counts in the displayed inventory.
            foreach (var entry in newItemCounts)
            {
                ItemReference itemRef = entry.Key;
                int newCount = entry.Value;

                if (newCount == 0)
                {
                    m_displayedCombatInventory.SetZero(itemRef);
                }
                else
                {
                    int oldCount = m_displayedCombatInventory.CountItem(itemRef);

                    m_displayedCombatInventory.ModifyItem(itemRef, newCount - oldCount);
                }
            }
        }

        private void HandleCharacterStateChanged(CharacterReference charRef, CombatActorState charState)
        {
            if (charRef.Equals(m_displayedCharacter))
                UpdateDisplayedInventory();
        }

        private void HandleItemStackSelected(ItemWithAmount itemWithAmount)
        {
            if (m_highlightedItem != null)
                InventoryDisplay.UnhighlightItem(m_highlightedItem);

            InventoryDisplay.HighlightItem(itemWithAmount.ItemReference);
            m_highlightedItem = itemWithAmount.ItemReference;

            m_selectedItem = (m_displayedCharacter, itemWithAmount);
            UpdateItemUseButton();
        }

        private void HandleTargetSelected(CombatTargetIconScript icon, CharacterReference target)
        {
            if (m_highlightedTargetIcon != null)
                m_highlightedTargetIcon.Highlighted = false;

            icon.Highlighted = true;
            m_highlightedTargetIcon = icon;

            m_selectedTarget = target;
            UpdateItemUseButton();
        }

        private void HandleUseItemClicked()
        {
            if (CanPressUseButton())
            {
                OnItemUseAction?.Invoke(m_selectedItem.Item1, m_selectedItem.Item2, m_selectedTarget);
                m_selectedItem = (null, null);
                m_selectedTarget = null;
                UpdateItemUseButton();
            }
        }

        private void UpdateItemUseButton()
        {
            m_useItemButton.interactable = CanPressUseButton();
        }

        private bool CanPressUseButton()
        {
            if (m_selectedItem.Item1 == null)
                return false;
            if (m_selectedItem.Item2 == null)
                return false;
            if (m_selectedTarget == null)
                return false;
            return true;
        }

        private void CreateTargetIconForCharacter(CharacterReference charRef)
        {
            var charSprite = charRef.Character?.ScenePrefab?.GetComponent<SpriteRenderer>()?.sprite;

            if (charSprite != null)
            {
                var charIconDisplay = InstantiateActionTargetIcon();
                charIconDisplay.DisplayIcon(charSprite);

                var charIconWeakRef = new WeakReference<CombatTargetIconScript>(charIconDisplay);
                charIconDisplay.OnIconClicked += () =>
                {
                    if (charIconWeakRef.TryGetTarget(out CombatTargetIconScript charIcon))
                        HandleTargetSelected(charIcon, charRef);
                };
            }
        }

        #region UI Actions
        public void HandleEscapeAction()
        {
            OnEscape?.Invoke();
        }
        #endregion

        private IInventoryDisplay InventoryDisplay => (IInventoryDisplay)m_inventoryDisplay;

        private Inventory m_displayedCombatInventory;
        private CharacterReference m_displayedCharacter;

        private (CharacterReference, ItemWithAmount) m_selectedItem;
        private CharacterReference m_selectedTarget;

        private CombatTargetIconScript m_highlightedTargetIcon = null;
        private ItemReference m_highlightedItem = null;

        [SerializeField]
        [RequiresType(typeof(IInventoryDisplay))]
        private UnityEngine.Object m_inventoryDisplay = default;

        [SerializeField]
        [Tooltip("The place to show the targets at which an action may be directed.")]
        private RectTransform m_actionTargetArea = default;

        [SerializeField]
        [Tooltip("The type of thing to add to the Action Target Area.")]
        private CombatTargetIconScript m_combatTargetIconPrefab = default;

        [SerializeField]
        [Tooltip("The button to click to use the selected item on the selected target.")]
        private UnityEngine.UI.Button m_useItemButton = default;
    }
}