﻿using System;
using CombatSim.CoreSim;
using Characters;
using System.Collections.Generic;
using GameInventory;
using CombatSim.CoreSim.Stats;

namespace Combat
{
    public static class TranslationUtility
    {

        public static CombatItem CreateCombatItem(this ItemWithAmount itemWithAmount)
        {
            InventoryItem originalItem = itemWithAmount.Item;
            CombatItemId combatItemId = new CombatItemId(originalItem.Name, originalItem.Stats.GetCombatStats<int>());
            return new CombatItem(combatItemId, itemWithAmount.Amount);
        }

        public static CombatStats<T> GetCombatStats<T>(this Stats stats)
        {
            CombatStats<T> combatStats = new CombatStats<T>();
            foreach (StatEnum statType in Stats.AllStats)
            {
                //TODO: Convert CombatStatType to double?
                var a = ConvertToCombatStat(statType);
                if (a != null)
                {
                    combatStats.ModifyStat((CombatStatType)a, (T)Convert.ChangeType(stats.Get(statType), typeof(T)));
                }
            }

            return combatStats;
        }

        // TODO: Should we just use a single type of stat enumeration? Whee does it go in that case since CombatSim won't run standalone without it.
        private static CombatStatType? ConvertToCombatStat(StatEnum statEnum)
        {
            string eName = Enum.GetName(typeof(StatEnum), statEnum);
            foreach (CombatStatType combatStatEnum in Enum.GetValues(typeof(CombatStatType)))
            {
                if(Enum.GetName(typeof(CombatStatType), combatStatEnum) == eName)
                {
                    return combatStatEnum;
                }
            }
            return null;
        }

        private static StatEnum? ConvertFromCombatStat(CombatStatType combatStatType)
        {
            string eName = Enum.GetName(typeof(CombatStatType), combatStatType);
            foreach (StatEnum statEnum in Enum.GetValues(typeof(StatEnum)))
            {
                if (Enum.GetName(typeof(StatEnum), statEnum) == eName)
                {
                    return statEnum;
                }
            }
            return null;
        }
    }
}
