﻿using System.Collections.Generic;
using GameInventory;
using Characters;

namespace Combat
{
    public class CombatPlayerInput
    {
        public enum ActionType
        {
            None, Flee, Item
        }

        public ActionType Action { get; }
        public ItemReference Item { get; }
        public IReadOnlyList<CharacterReference> Targets { get; }

        public static readonly CombatPlayerInput None = new CombatPlayerInput(ActionType.None);
        public static readonly CombatPlayerInput Flee = new CombatPlayerInput(ActionType.Flee);

        public CombatPlayerInput(ItemReference item, CharacterReference target)
        {
            Action = ActionType.Item;
            Item = item;
            Targets = new[] { target };
        }

        public CombatPlayerInput(ItemReference item, IEnumerable<CharacterReference> targets)
        {
            Action = ActionType.Item;
            Item = item;
            Targets = new List<CharacterReference>(targets);
        }

        private CombatPlayerInput(ActionType actionType)
        {
            Action = actionType;
        }
    }
}
