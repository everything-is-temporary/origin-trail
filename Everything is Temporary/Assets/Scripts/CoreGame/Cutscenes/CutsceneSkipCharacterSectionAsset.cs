﻿using System.Linq;
using UnityEngine;
using UnityEngine.Playables;
using Characters;

/// <summary>
/// Can be used to skip a section of a Timeline if a character is not present
/// in the party.
/// </summary>
[System.Serializable]
public class CutsceneSkipCharacterSectionAsset : PlayableAsset
{
    [Tooltip("Set this to the PlayableDirector playing the Timeline.")]
    public ExposedReference<PlayableDirector> director;
    public CharacterReference requiredPartyMember;
    public bool skipInEditor;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<CutsceneSkipSectionBehaviour>.Create(graph);

        CutsceneSkipSectionBehaviour behaviour = playable.GetBehaviour();
        behaviour.director = director.Resolve(graph.GetResolver());

        CharacterManager characterManager = CharacterManager.Instance;

        if (characterManager == null || !Application.isPlaying)
            behaviour.shouldSkip = skipInEditor;
        else
            behaviour.shouldSkip = !characterManager.CurrentParty.Contains(requiredPartyMember);

        return playable;
    }
}
