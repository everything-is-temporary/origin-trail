﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

/// <summary>
/// A PlayableBehaviour that causes a PlayableDirector to skip forward by the
/// duration of a Playable. This is used by the <see cref="CutsceneSkipCharacterSectionAsset"/>
/// to optionally skip parts of a Timeline.
/// </summary>
[System.Serializable]
public class CutsceneSkipSectionBehaviour : PlayableBehaviour
{
    public PlayableDirector director;
    public bool shouldSkip;

    public override void PrepareFrame(Playable playable, FrameData info)
    {
        if (shouldSkip)
            director.time += playable.GetDuration();
    }
}
