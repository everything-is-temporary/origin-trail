﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Dialogue;
using System.Threading.Tasks;

namespace Cutscenes
{
    /// <summary>
    /// Runs a cutscene when a game command is triggered from an Ink file through
    /// the <see cref="StoryManager.GameCommand(string)"/> function.
    /// </summary>
    [RequireComponent(typeof(PlayableDirector))]
    public class CutsceneScript : MonoBehaviour
    {
        [Tooltip("The game command that will trigger this cutscene from an Ink file.")]
        public string gameCommand;

        [Tooltip("Can the cutscene be triggered multiple times?")]
        public bool onlyRunOnce = true;

        [Tooltip("Prevent dialogue from continuing to the next line until the cutscene finishes.")]
        public bool blockStory = true;

        private void Awake()
        {
            m_playableDirector = GetComponent<PlayableDirector>();
        }

        private void Start()
        {
            // Save the game command we use to register with the StoryManager
            // so that changing it through the inspector does not prevent us
            // from unregistering the handler.
            m_savedGameCommand = gameCommand;
            StoryManager.Instance.AddGameCommandHandler(m_savedGameCommand, PlayCutscene);
        }

        private void OnDisable()
        {
            StoryManager.Instance.RemoveGameCommandHandler(m_savedGameCommand, PlayCutscene);

            // Unblock the story (if it was blocked by this cutscene).
            // Users of this component are expected to disable its game object
            // when the cutscene is over.
            m_cutsceneTask?.SetResult(null);
            m_cutsceneTask = null;
        }

        private Task PlayCutscene()
        {
            if (onlyRunOnce)
                StoryManager.Instance.RemoveGameCommandHandler(m_savedGameCommand, PlayCutscene);

            m_playableDirector.Play();

            if (!blockStory)
                return Task.CompletedTask;

            // Block story until the playable director is stopped.
            m_cutsceneTask = new TaskCompletionSource<object>();
            return m_cutsceneTask.Task;
        }

        private string m_savedGameCommand;
        private PlayableDirector m_playableDirector;
        private TaskCompletionSource<object> m_cutsceneTask;
    }
}