﻿using System.Linq;
using Characters;
using UnityEngine;

namespace Cutscenes
{
    /// <summary>
    /// Disables own game object on Start() if the specified character is not
    /// present in the party.
    /// </summary>
    public class CutsceneOptionalPartyMember : MonoBehaviour
    {
        [Tooltip("If this character is not a party member, the game object" +
            " containing this script will be disabled.")]
        public CharacterReference character;

        private void Start()
        {
            if (!CharacterManager.Instance.CurrentParty.Contains(character))
                gameObject.SetActive(false);
        }
    }
}