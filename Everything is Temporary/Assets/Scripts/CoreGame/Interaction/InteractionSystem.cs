﻿using System.Collections.Generic;
using UnityEngine;

using GameUtility.ObserverPattern;

namespace Interaction {
    /// <summary>
    /// A class to keep track of all interactive objects and to send out events
    /// when they are created and destroyed.
    /// </summary>
    public class InteractionSystem : MonoBehaviour
    {
        public static InteractionSystem Instance
        {
            get
            {
                // The 'is' guarantees that we only create a new _singleton if
                // we have never done so before. Using '==' can yield null if
                // the _singleton was created and then destroyed with Destroy().
                if (_singleton is null)
                {
                    var go = new GameObject("Interaction System");
                    _singleton = go.AddComponent<InteractionSystem>();
                }

                return _singleton;
            }
        }

        public delegate void InteractiveObjectDelegate(IInteractiveObject obj);

        /// <summary>
        /// Occurs when an interactive object is registered.
        /// </summary>
        public event InteractiveObjectDelegate OnInteractiveObjectRegistered;

        /// <summary>
        /// Occurs when an interactive object is unregistered.
        /// </summary>
        public event InteractiveObjectDelegate OnInteractiveObjectUnregistered;

        /// <summary>
        /// Registers the interactive object.
        /// </summary>
        /// <returns>The interactive object.</returns>
        /// <param name="obj">Object.</param>
        public void RegisterInteractiveObject(IInteractiveObject obj)
        {
            m_currentInteractiveObjects.Add(obj);

            OnInteractiveObjectRegistered?.Invoke(obj);
        }

        /// <summary>
        /// Unregisters the interactive object.
        /// </summary>
        /// <param name="obj">Object.</param>
        public void UnregisterInteractiveObject(IInteractiveObject obj)
        {
            m_currentInteractiveObjects.Remove(obj);

            OnInteractiveObjectUnregistered?.Invoke(obj);
        }

        /// <summary>
        /// Returns an object that can be used to loop through all registered
        /// interactive objects.
        /// </summary>
        /// <returns>The objects.</returns>
        public IEnumerable<IInteractiveObject> InteractiveObjects()
        {
            return m_currentInteractiveObjects;
        }

        private void Awake()
        {
            if (_singleton == null)
            {
                _singleton = this;
            }
            else
            {
                Debug.LogWarning("Duplicate InteractionSystem script detected. " +
                                 "You don't need to create your own game object with an " +
                                 "InteractionSystem script.");
                Destroy(this);
            }
        }

        private static InteractionSystem _singleton;

        private List<IInteractiveObject> m_currentInteractiveObjects = new List<IInteractiveObject>();
    }
}