﻿using UnityEngine;

using GameUtility.ObserverPattern;

namespace Interaction
{
    /// <summary>
    /// Simple example script for how to implement the IInteractiveObject interface.
    /// </summary>
    public class TestInteractiveObject : MonoBehaviour, IInteractiveObject
    {
        public bool CanInteractWith(IInteractor interactor)
        {
            return enabled;
        }

        public bool TryInteractWith(IInteractor interactor)
        {
            return enabled;
        }

        public float GetDistanceTo(IInteractor interactor)
        {
            return Mathf.Abs(transform.position.x - interactor.GetPosition().x);
        }

        public void ShowSelectionVisual()
        {
            Debug.LogFormat("SHOW Visual for {0}.", gameObject.name);

            m_speechBubble?.SetActive(true);
        }

        public void HideSelectionVisual()
        {
            Debug.LogFormat("HIDE Visual for {0}.", gameObject.name);

            m_speechBubble?.SetActive(false);
        }

        private void Awake()
        {
            InteractionSystem.Instance.RegisterInteractiveObject(this);

            m_speechBubble?.SetActive(false);
        }

        private void OnDestroy()
        {
            InteractionSystem.Instance?.UnregisterInteractiveObject(this);
        }

        [SerializeField]
        [Tooltip("An object that should be enabled/disabled on ShowSelectionVisual()" +
                 " and ShowSelectionVisual().")]
        private GameObject m_speechBubble = default;
    }
}