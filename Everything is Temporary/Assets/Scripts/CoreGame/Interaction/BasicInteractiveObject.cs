﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Events;
using System.Collections.Generic;

using GameUtility;
using GameUtility.ObserverPattern;

namespace Interaction
{
    /// <summary>
    /// Simple example script for how to implement the IInteractiveObject interface.
    /// </summary>
    public class BasicInteractiveObject : MonoBehaviour, IInteractiveObject
    {
        [FormerlySerializedAs("m_maxInteractionDistance")]
        public float maxInteractionDistance = 1;

        [FormerlySerializedAs("m_onInteract")]
        public UnityEvent onInteract = new UnityEvent();

        [FormerlySerializedAs("m_selectionVisual")]
        public Object selectionVisualObject = default;

        public bool CanInteractWith(IInteractor interactor)
        {
            return GetDistanceTo(interactor) < maxInteractionDistance;
        }

        public bool TryInteractWith(IInteractor interactor)
        {
            onInteract.Invoke();

            return true;
        }

        public float GetDistanceTo(IInteractor interactor)
        {
            return Mathf.Abs(transform.position.x - interactor.GetPosition().x);
        }

        public void ShowSelectionVisual()
        {
            if (selectionVisualObject is ISelectionVisual selectionVisual)
                selectionVisual.ShowSelectionVisual();
            else if (selectionVisualObject is GameObject selectionGameObject)
                selectionGameObject.SetActive(true);
        }

        public void HideSelectionVisual()
        {
            if (selectionVisualObject is ISelectionVisual selectionVisual)
                selectionVisual.HideSelectionVisual();
            else if (selectionVisualObject is GameObject selectionGameObject)
                selectionGameObject.SetActive(false);
        }

        private void Awake()
        {
            InteractionSystem.Instance.RegisterInteractiveObject(this);

            if (selectionVisualObject is GameObject selectionGameObject)
                selectionGameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            InteractionSystem.Instance?.UnregisterInteractiveObject(this);
        }
    }
}
