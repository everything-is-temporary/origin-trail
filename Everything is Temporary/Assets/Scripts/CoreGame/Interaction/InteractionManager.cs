﻿using UnityEngine;
using System.Collections.Generic;
using GameUtility;
using System.Linq;

namespace Interaction
{
    public class InteractionManager : SingletonMonoBehavior<InteractionManager>
    {

        private Dictionary<IInteractor, IInteractiveObject> m_ongoingInteractions = new Dictionary<IInteractor, IInteractiveObject>();
        private HashSet<IInteractiveObject> m_subscribedInteractiveObjects = new HashSet<IInteractiveObject>();

        public bool RegisterInteractiveObject(IInteractiveObject interactiveObject)
        {
            return m_subscribedInteractiveObjects.Add(interactiveObject);
        }

        public bool UnregisterInteractiveObject(IInteractiveObject interactiveObject){
            foreach(IInteractor interactor in m_ongoingInteractions.Keys)
            {
                if(m_ongoingInteractions[interactor] == interactiveObject)
                {
                    m_ongoingInteractions.Remove(interactor);
                }
            }
            return m_subscribedInteractiveObjects.Remove(interactiveObject);
        }

        private IInteractiveObject GetClosestInteractiveObject(IInteractor interactor)
        {
            List<IInteractiveObject> canInteractWith = new List<IInteractiveObject>();
            foreach(IInteractiveObject interactiveObject in this.m_subscribedInteractiveObjects)
            {
                if(interactiveObject.CanInteractWith(interactor))
                {
                    canInteractWith.Add(interactiveObject);
                }
            }
            return canInteractWith.OrderBy(interactable => interactable.GetDistanceTo(interactor)).FirstOrDefault();
        }

        public void TryInteract(IInteractor interactor){
            if (m_ongoingInteractions.TryGetValue(interactor, out IInteractiveObject ongoingInteractable))
            {
                if(ongoingInteractable.TryInteractWith(interactor)){
                    return;
                }else{
                    m_ongoingInteractions.Remove(interactor);
                }
            }
            IInteractiveObject closest = this.GetClosestInteractiveObject(interactor);
            if(closest!=null)
            {
                if(closest.TryInteractWith(interactor))
                {
                    m_ongoingInteractions.Add(interactor, closest);
                }
            }
        }

        public bool EndInteraction(IInteractor interactor, IInteractiveObject interactiveObject)
        {
            if (m_ongoingInteractions[interactor] == interactiveObject){
                return m_ongoingInteractions.Remove(interactor);
            }
            return false;
        }
    }
}