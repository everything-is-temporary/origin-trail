﻿using UnityEngine;

namespace Interaction {
    /// <summary>
    /// Something that can interact with IInteractiveObjects. For now, this just
    /// has a GetPosition() method so that IInteractiveObjects can compute their
    /// distance from it.
    /// </summary>
    public interface IInteractor
    {
        /// <summary>
        /// The position of this object, in the same units as transform.position.
        /// This is used by interactive objects to determine how far they are from
        /// this.
        /// </summary>
        /// <returns>The position.</returns>
        Vector3 GetPosition();
    }
}