﻿using UnityEngine;

namespace Interaction {
    /// <summary>
    /// Simple script that calls ShowSelectionVisual() on the closest interactive
    /// object, and prints the name of the closest object when the Space key
    /// is pressed.
    /// </summary>
    public class PlayerInteractor : MonoBehaviour, IInteractor
    {
        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                InteractionManager.Instance.TryInteract(this);
            }
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }
    }
}