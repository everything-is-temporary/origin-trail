﻿using Dialogue;
using UnityEngine;

namespace Interaction
{
    [RequireComponent((typeof(BoxCollider2D)))]
    public class InvisibleWall : MonoBehaviour, System.IComparable
    {
        [UnityEngine.Serialization.FormerlySerializedAs("name")]
        public string wallName;
        public bool enabledOnStart = false;

        // Start is called before the first frame update
        void Start()
        {
            if (string.IsNullOrEmpty(wallName))
            {
                wallName = this.GetHashCode().ToString();
            }
            StoryManager.Instance.RegisterWall(this);
            if (enabledOnStart)
            {
                Enable();
            }
            else
            {
                Disable();
            }
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            StoryManager.Instance.UnregisterWall(this);
        }

        public int CompareTo(object obj)
        {
            InvisibleWall otherWall = obj as InvisibleWall;
            if (otherWall == null)
            {
                return 0;
            }
            if (this < otherWall)
            {
                return -1;
            }
            else if (this > otherWall)
            {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Determines whether one specified <see cref="Interaction.InvisibleWall"/> is less than another specfied <see cref="Interaction.InvisibleWall"/>.
        /// </summary>
        /// <param name="first">The first <see cref="Interaction.InvisibleWall"/> to compare.</param>
        /// <param name="second">The second <see cref="Interaction.InvisibleWall"/> to compare.</param>
        /// <returns><c>true</c> if the x coordinate of the center of the <see cref="BoxCollider2D"/> of <c>first</c> is less than the corresponding x coordinate of <c>second</c>; otherwise, <c>false</c>.</returns>
        public static bool operator <(InvisibleWall first, InvisibleWall second)
        {
            // Using BoxCollider2D transform here since it is possible to move BoxCollider2D bounds so that it does not overlap raw transform center.
            return first.GetComponent<BoxCollider2D>().bounds.center.x < second.GetComponent<BoxCollider2D>().bounds.center.x;
        }

        /// <summary>
        /// Determines whether one specified <see cref="Interaction.InvisibleWall"/> is greater than another specfied <see cref="Interaction.InvisibleWall"/>.
        /// </summary>
        /// <param name="first">The first <see cref="Interaction.InvisibleWall"/> to compare.</param>
        /// <param name="second">The second <see cref="Interaction.InvisibleWall"/> to compare.</param>
        /// <returns><c>true</c> if the x coordinate of the center of the <see cref="BoxCollider2D"/> of <c>first</c> is greater than the corresponding x coordinate of <c>second</c>; otherwise, <c>false</c>.</returns>
        public static bool operator >(InvisibleWall first, InvisibleWall second)
        {
            // Using BoxCollider2D transform here since it is possible to move BoxCollider2D bounds so that it does not overlap raw transform center.
            return first.GetComponent<BoxCollider2D>().bounds.center.x > second.GetComponent<BoxCollider2D>().bounds.center.x;
        }
    }
}