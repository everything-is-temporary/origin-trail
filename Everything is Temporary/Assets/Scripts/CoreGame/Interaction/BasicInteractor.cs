﻿using UnityEngine;

using GameUtility;

namespace Interaction
{
    /// <summary>
    /// Simple script that properly uses the InteractionSystem.
    /// </summary>
    public class BasicInteractor : MonoBehaviour, IInteractor
    {
        // Use this for initialization
        private void Awake()
        {
            m_objectTracker = new InteractiveObjectTracker(this);

            InputManager.RegisterAsInputHandler(InputType.InteractKey, HandleInteractKeyInput);
        }

        // Update is called once per frame
        private void Update()
        {
            if (GameInputManager.Instance.IsInteractionEnabled)
            {
                m_objectTracker.Update();

                m_nearest = m_objectTracker.GetClosest();

                if (m_nearest != null && m_nearest.GetDistanceTo(this) > m_maximumInteractionDistance)
                    m_nearest = null;

                if (m_nearest != m_previousNearest)
                {
                    m_nearest?.ShowSelectionVisual();
                    m_previousNearest?.HideSelectionVisual();
                }

                m_previousNearest = m_nearest;
            }
            else
            {
                m_previousNearest?.HideSelectionVisual();
                m_previousNearest = null;
            }
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        /// <summary>
        /// For the moment, this handler is not unregistered anywhere.
        /// So it serves as a base handler.
        /// If nothing else consumes the input, the player can always try to interact.
        /// </summary>
        /// <returns>Is the input handled?</returns>
        private bool HandleInteractKeyInput()
        {
            if (InputManager.IsInteractionEnabled)
            {
                if (m_nearest != null)
                {
                    return m_nearest.TryInteractWith(this);
                }
            }

            return false;
        }

        [SerializeField]
        private float m_maximumInteractionDistance = 0.5f;

        private InteractiveObjectTracker m_objectTracker;
        private IInteractiveObject m_nearest;
        private IInteractiveObject m_previousNearest;

        private GameInputManager InputManager => GameInputManager.Instance;
    }
}