﻿namespace Interaction
{

    public interface ISelectionVisual
    {
        void ShowSelectionVisual();

        void HideSelectionVisual();
    }

}
