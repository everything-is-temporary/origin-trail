﻿using UnityEngine;
using System.Collections.Generic;

namespace Interaction
{
    public class TriggerVisualIndicators : MonoBehaviour, ISelectionVisual
    {
        [SerializeField]
        private List<GameObject> m_visualIndicators = default;

        void ISelectionVisual.ShowSelectionVisual()
        {
            foreach(GameObject visualIndicator in m_visualIndicators)
            {
                visualIndicator.SetActive(true);
            }
        }

        void ISelectionVisual.HideSelectionVisual()
        {
            foreach(GameObject visualIndicator in m_visualIndicators)
            {
                visualIndicator.SetActive(false);
            }
        }
    }

}
