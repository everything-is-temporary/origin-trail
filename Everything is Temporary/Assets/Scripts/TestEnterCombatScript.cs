﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;

public class TestEnterCombatScript : MonoBehaviour
{
    public void EnterCombat()
    {
        CombatManager.Instance.PerformTestCombat().FireAndForget();
    }
}
