﻿using System;
using UnityEngine;
using GameUtility;

namespace VFX
{
    /// <summary>
    /// Makes a mesh completely fill a camera's viewport.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter))]
    public class QuadBackgroundCameraFitter : MonoBehaviour
    {
        public Camera fitCamera;

        private void OnEnable()
        {
            m_meshFilter = GetComponent<MeshFilter>();
            m_mesh = new Mesh
            {
                vertices = new Vector3[4],
                triangles = new int[] { 0, 2, 1, 0, 3, 2 },
                uv = new Vector2[]
                {
                    new Vector2(0, 0),
                    new Vector2(1, 0),
                    new Vector2(1, 1),
                    new Vector2(0, 1)
                }
            };

            m_mesh.MarkDynamic();
            m_meshFilter.mesh = m_mesh;
        }

        private void LateUpdate()
        {
            if (fitCamera == null)
                return;

            transform.parent = fitCamera.transform;
            FitMeshToCamera(fitCamera);
        }

        private void FitMeshToCamera(Camera cam)
        {
            Vector4 clipCorner1 = new Vector4(-1, -1, 1, 1);
            Vector4 clipCorner2 = new Vector4(1, -1, 1, 1);
            Vector4 clipCorner3 = new Vector4(1, 1, 1, 1);
            Vector4 clipCorner4 = new Vector4(-1, 1, 1, 1);

            Matrix4x4 projInv = cam.projectionMatrix.inverse;

            Vector4 corner1 = projInv * clipCorner1;
            Vector4 corner2 = projInv * clipCorner2;
            Vector4 corner3 = projInv * clipCorner3;
            Vector4 corner4 = projInv * clipCorner4;

            // Multiplying by 2 so that we're not exactly at the far plane.
            corner1 /= corner1.w * 2;
            corner2 /= corner2.w * 2;
            corner3 /= corner3.w * 2;
            corner4 /= corner4.w * 2;

            Vector4 center = (corner1 + corner2 + corner3 + corner4) / 4;
            center.w = 0;

            transform.localPosition = new Vector3(center.x, center.y, -center.z);
            corner1 -= center;
            corner2 -= center;
            corner3 -= center;
            corner4 -= center;

            m_mesh.vertices = new Vector3[]
            {
                corner1,
                corner2,
                corner3,
                corner4
            };
        }

        private Mesh m_mesh;
        private MeshFilter m_meshFilter;
    }
}