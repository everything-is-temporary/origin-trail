using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using GameUtility;
using GameUtility.Serialization;
using UnityEngine;

namespace VFX.TweenAnimations
{
    [CreateAssetMenu]
    public class SpriteTweenAnimation : ScriptableObject //TODO: Make generic base for this class if possible
    {
        [SerializeField]
        [SerializableDictionary(false)]
        public TweenAndAnimationSettingsDict m_tweenAndAnimationSettingsDict = default;

        public List<TweenApplicator> GetTweensForRenderer(SpriteRenderer spriteRenderer)
        {
            return m_tweenAndAnimationSettingsDict
                .Select(stringSettingsPair => GetApplicatorForPair(spriteRenderer, stringSettingsPair))
                .Where(sequence => sequence!=null).ToList();
        }

        private TweenApplicator GetApplicatorForPair(SpriteRenderer spriteRenderer,
            KeyValuePair<string, TweenAnimationSettings> pair)
        {
            return SpriteTweenLibrary.FromEnumNameString(pair.Key)?.Apply(spriteRenderer, pair.Value);
        }

        [Serializable] public class TweenAndAnimationSettingsDict : SerializableDictionary<string, TweenAnimationSettings>
        {
        }
    }
}