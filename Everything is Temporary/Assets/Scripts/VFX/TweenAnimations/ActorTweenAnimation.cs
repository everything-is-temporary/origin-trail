using System;
using System.Collections.Generic;
using System.Linq;
using Characters;
using DG.Tweening;
using GameUtility;
using GameUtility.Serialization;
using UnityEngine;

namespace VFX.TweenAnimations
{
    [CreateAssetMenu]
    public class ActorTweenAnimation : ScriptableObject //TODO: Make generic base for this class if possible
    {
        [SerializeField]
        [SerializableDictionary(false)]
        public TweenAndAnimationSettingsDict m_tweenAndAnimationSettingsDict = default;

        public List<TweenApplicator> GetTweensForActor(Actor actor)
        {
            return m_tweenAndAnimationSettingsDict
                .Select(stringSettingsPair => GetApplicatorForPair(actor, stringSettingsPair))
                .Where(sequence => sequence!=null).ToList();
        }

        private TweenApplicator GetApplicatorForPair(Actor actor,
            KeyValuePair<string, TweenAnimationSettings> pair)
        {
            return ActorTweenLibrary.FromEnumNameString(pair.Key)?.Apply(actor, pair.Value);
        }

        [Serializable] public class TweenAndAnimationSettingsDict : SerializableDictionary<string, TweenAnimationSettings>
        {
        }
    }
}