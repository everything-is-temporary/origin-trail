using System.Threading.Tasks;
using DG.Tweening;
using GameUtility;

namespace VFX.TweenAnimations
{
    public class TweenApplicator
    {
        public TweenApplicator(Sequence sequenceToApply, float delay)
        {
            m_internalSequence = sequenceToApply;
            m_delay = delay;
        }

        public void Begin()
        {
            PlaySequence().FireAndForget();
        }

        public void End()
        {
            m_internalSequence.Complete();
            m_internalSequence.Kill();
        }

        private async Task PlaySequence()
        {
            await Task.Delay((int)(1000*m_delay));
            m_internalSequence.Play();
        }

        private Sequence m_internalSequence = default;
        private float m_delay = 0;
    }
}