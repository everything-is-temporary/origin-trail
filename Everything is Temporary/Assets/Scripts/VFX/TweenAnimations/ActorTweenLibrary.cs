using System;
using System.Reflection;
using Characters;
using DG.Tweening;
using UnityEngine;

namespace VFX.TweenAnimations
{
    public class ActorTweenLibrary : TweenLibrary<Actor>
    {
        
        #region Enumerations
        
        public static readonly ActorTweenLibrary ShortHop = new ActorTweenLibrary(ShortHopFunc);
        public static readonly ActorTweenLibrary SpinAround = new ActorTweenLibrary(SpinAroundFunc);
        public static readonly ActorTweenLibrary FlipAround = new ActorTweenLibrary(FlipAroundFunc);
        public static readonly ActorTweenLibrary WalkLeft = new ActorTweenLibrary(WalkLeftFunc);
        
        #endregion //Enumerations
        
        #region Predefined Functions

        private static Sequence ShortHopFunc(Actor actor, TweenAnimationSettings tweenAnimationSettings)
        {
            float multiplier = 0.5f;
            Sequence shortHopSeq = DOTween.Sequence();
            var srTransform = actor.gameObject.GetComponent<SpriteRenderer>().transform;
            shortHopSeq.Append(srTransform.DOJump(srTransform.position, multiplier * tweenAnimationSettings.intensity,
                1, tweenAnimationSettings.duration));
            return shortHopSeq;
        }

        /// <summary>
        /// Note: not affected by intensity value.
        /// </summary>
        private static Sequence SpinAroundFunc(Actor actor, TweenAnimationSettings tweenAnimationSettings)
        {
            int maxIntensitySpins = 10;
            Sequence spinAroundSeq = DOTween.Sequence();
            var srTransform = actor.gameObject.GetComponent<SpriteRenderer>().transform;
            int spins = Mathf.FloorToInt(tweenAnimationSettings.intensity * maxIntensitySpins);
            spinAroundSeq.Append(srTransform.DOLocalRotate(Vector3.down * (360f*spins), tweenAnimationSettings.duration,
                RotateMode.LocalAxisAdd));
            spinAroundSeq.SetEase(Ease.InOutQuad);
            return spinAroundSeq;
        }

        private static Sequence FlipAroundFunc(Actor actor, TweenAnimationSettings tweenAnimationSettings)
        {
            float maxSpinDuration = 2f; // 2 seconds
            float spinDuration = maxSpinDuration * Mathf.Clamp(1.0f - tweenAnimationSettings.intensity, 0.0001f, 1f);
            Sequence flipAroundSeq = DOTween.Sequence();
            var srTransform = actor.gameObject.GetComponent<SpriteRenderer>().transform;
            flipAroundSeq.Append(srTransform.DOLocalRotate(Vector3.down * 180f, spinDuration, RotateMode.LocalAxisAdd));
            flipAroundSeq.AppendInterval(tweenAnimationSettings.duration);
            flipAroundSeq.Append(srTransform.DOLocalRotate(Vector3.up * 180f, spinDuration, RotateMode.LocalAxisAdd));
            return flipAroundSeq;
        }

        private static Sequence WalkLeftFunc(Actor actor, TweenAnimationSettings tweenAnimationSettings)
        {
            Sequence walkLeftSeq = DOTween.Sequence();
            //TODO: Implement
//            walkLeftSeq.Append(DOTween.To());
            return walkLeftSeq;
        }
        
        #endregion //Predefined Functions
        
        public static ActorTweenLibrary FromEnumNameString(string s)
        {
            foreach (FieldInfo fieldInfo in typeof(ActorTweenLibrary).GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                if (String.Equals(fieldInfo.Name, s, StringComparison.CurrentCultureIgnoreCase))
                {
                    return (ActorTweenLibrary) fieldInfo.GetValue(null);
                }
            }
            Debug.LogWarning($"ActorTweenLibrary Enumeration for {s} not found!");
            return null;
        }
        
        public ActorTweenLibrary(Func<Actor, TweenAnimationSettings, Sequence> tweenApplicator) : base(tweenApplicator)
        {
        }
    }
}