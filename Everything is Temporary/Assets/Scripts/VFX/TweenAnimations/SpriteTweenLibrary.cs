using System;
using System.Reflection;
using DG.Tweening;
using UnityEngine;

namespace VFX.TweenAnimations
{
    public class SpriteTweenLibrary : TweenLibrary<SpriteRenderer>
    {
        #region Enumerations

        public static readonly SpriteTweenLibrary ShakeX = new SpriteTweenLibrary(ShakeXFunc);
        public static readonly SpriteTweenLibrary ShortHop = new SpriteTweenLibrary(ShortHopFunc);
        public static readonly SpriteTweenLibrary PunchRotateZ = new SpriteTweenLibrary(PunchRotateZFunc);
        public static readonly SpriteTweenLibrary BobY = new SpriteTweenLibrary(BobYFunc);
        public static readonly SpriteTweenLibrary PuffXY = new SpriteTweenLibrary(PuffXYFunc);
        
        #endregion //Enumerations

        #region Predefined Functions

        private static Sequence ShakeXFunc(SpriteRenderer spriteRenderer, TweenAnimationSettings tweenAnimationSettings)
        {
            float suppressionFactor = 0.25f;
            Sequence shakeXSeq = DOTween.Sequence();
            shakeXSeq.Append(spriteRenderer.transform.DOShakePosition(tweenAnimationSettings.duration,
                suppressionFactor * tweenAnimationSettings.intensity * Vector3.right).SetEase(Ease.InOutCubic));
            return shakeXSeq;
        }

        private static Sequence ShortHopFunc(SpriteRenderer spriteRenderer, TweenAnimationSettings tweenAnimationSettings)
        {
            float suppressionFactor = 0.1f;
            Sequence shortHopSeq = DOTween.Sequence();
            shortHopSeq.Append(spriteRenderer.transform.DOJump(spriteRenderer.transform.position,
                suppressionFactor * tweenAnimationSettings.intensity, 1, tweenAnimationSettings.duration));
            return shortHopSeq;
        }

        private static Sequence PunchRotateZFunc(SpriteRenderer spriteRenderer, TweenAnimationSettings tweenAnimationSettings)
        {
            float suppressionFactor = 30.0f;
            Sequence punchRotateZSeq = DOTween.Sequence();
            punchRotateZSeq.Append(spriteRenderer.transform.DOPunchRotation(
                Vector3.forward * suppressionFactor * tweenAnimationSettings.intensity,
                tweenAnimationSettings.duration, 3));
            return punchRotateZSeq;
        }

        private static Sequence BobYFunc(SpriteRenderer spriteRenderer, TweenAnimationSettings tweenAnimationSettings)
        {
            float suppressionFactor = 0.1f;
            Sequence bobYSeq = DOTween.Sequence();
            bobYSeq.Append(spriteRenderer.transform.DOPunchPosition(
                Vector3.down * suppressionFactor * tweenAnimationSettings.intensity, tweenAnimationSettings.duration,
                1, 1.2f));
            return bobYSeq;
        }

        private static Sequence PuffXYFunc(SpriteRenderer spriteRenderer,
            TweenAnimationSettings tweenAnimationSettings)
        {
            float suppressionFactor = 0.5f;
            Sequence puffXYSeq = DOTween.Sequence();
            puffXYSeq.Append(spriteRenderer.transform.DOPunchScale(
                Vector3.one * suppressionFactor * tweenAnimationSettings.intensity, tweenAnimationSettings.duration, 
                1));
            return puffXYSeq;
        }
        
        #endregion
        
        public static SpriteTweenLibrary FromEnumNameString(string s)
        {
            foreach (FieldInfo fieldInfo in typeof(SpriteTweenLibrary).GetFields(BindingFlags.Static | BindingFlags.Public))
            {
                if (String.Equals(fieldInfo.Name, s, StringComparison.CurrentCultureIgnoreCase))
                {
                    return (SpriteTweenLibrary) fieldInfo.GetValue(null);
                }
            }
            return null;
        }
        
        public SpriteTweenLibrary(Func<SpriteRenderer, TweenAnimationSettings, Sequence> tweenApplicator) : base(tweenApplicator)
        {
        }
    }
}