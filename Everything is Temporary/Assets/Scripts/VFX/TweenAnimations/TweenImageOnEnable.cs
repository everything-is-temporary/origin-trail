﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TweenImageOnEnable : MonoBehaviour
{
    [SerializeField] private float m_strength;
    
    private void Awake()
    {
        m_image = gameObject.GetComponent<Image>();
    }

    private void OnEnable()
    {
        m_image.transform.DOPunchScale(Vector3.one * m_strength, 0.2f, 1);
    }
    
    private Image m_image;

}
