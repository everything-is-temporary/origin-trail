using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

namespace VFX.TweenAnimations
{
    public class TweenEnsemble
    {
        public TweenEnsemble(IEnumerable<TweenApplicator> applicators)
        {
            m_applicators = applicators.ToList();
        }

        public void Start()
        {
            m_applicators.ForEach(applicator => applicator.Begin());
        }

        public void Stop()
        {
            m_applicators.ForEach(applicator => applicator.End());
        }

        private List<TweenApplicator> m_applicators;
    }
}