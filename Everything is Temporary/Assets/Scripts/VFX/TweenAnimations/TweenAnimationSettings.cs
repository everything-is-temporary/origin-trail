using System;
using UnityEngine;

namespace VFX.TweenAnimations
{
    [Serializable]
    public class TweenAnimationSettings
    {
        /// <summary>
        /// A container class for settings that change the functionality of an animation. Most of these are up to the implementor to enforce, no guarantee is given for a particular TweenLibrary tween to actually use the values specified here.
        /// </summary>
        /// <param name="duration">The duration of the animation in seconds.<remarks>Default: 1 second.</remarks></param>
        /// <param name="intensity">The intensity (range 0 to 1) of the animation. Implementation depends on the Tween.<remarks>Default: 0.5</remarks></param>
        /// <param name="loops">The amount of times the animation needs to loop. -1 means loop infinitely.<remarks>Default: -1</remarks></param>
        /// <param name="startDelay">The delay in seconds before the start of the animation. Useful for applying multiple animations simultaneously.<remarks>Default: 0</remarks></param>
        /// <param name="loopDelay">The delay in seconds before the animation loop restarts (provided the animation loops in the first place).<remarks>Default: 0</remarks></param>
        public TweenAnimationSettings(float duration = 1.0f, float intensity = 0.5f, int loops = -1, float startDelay = 0f, float loopDelay = 0f)
        {
            this.duration = duration;
            this.intensity = intensity;
            this.loops = loops;
            this.startDelay = startDelay;
            this.loopDelay = loopDelay;
            this.easeCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }

        public float duration;
        public float startDelay;
        public float loopDelay;   
        [Range(0, 1)]
        public float intensity;
        public int loops;
        public AnimationCurve easeCurve;
    }
}