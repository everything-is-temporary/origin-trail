using System;
using DG.Tweening;

namespace VFX.TweenAnimations
{
    public abstract class TweenLibrary<TInput>
    {
        public TweenLibrary(Func<TInput, TweenAnimationSettings, Sequence> tweenApplicator)
        {
            m_tweenApplicator = tweenApplicator;
        }

        public virtual TweenApplicator Apply(TInput inputObject, TweenAnimationSettings tweenAnimationSettings)
        {
            var producedSequence = m_tweenApplicator.Invoke(inputObject, tweenAnimationSettings);
            producedSequence.SetDelay(tweenAnimationSettings.startDelay);
            producedSequence.AppendInterval(tweenAnimationSettings.loopDelay);
            producedSequence.SetLoops(tweenAnimationSettings.loops, LoopType.Restart);
            if (tweenAnimationSettings.easeCurve != null && tweenAnimationSettings.easeCurve.length > 0)
            {
                producedSequence.SetEase(tweenAnimationSettings.easeCurve);
            }
            return new TweenApplicator(producedSequence, tweenAnimationSettings.startDelay);
        }

        private Func<TInput, TweenAnimationSettings, Sequence> m_tweenApplicator;
    }
}