﻿Shader "Hidden/Vertical Gradient"
{
    Properties
    {
        _BottomColor ("Bottom Color", Color) = (0.5, 0.5, 1, 1)
        _TopColor ("Top Color", Color) = (0.2, 0.2, 0.5, 1)
    }
    SubShader
    {
        Tags
        {
            // Makes this work with sprites.
            "Queue" = "Transparent"
        }
    
        Pass
        {
            ZTest Off
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                fixed4 color : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
            
            fixed4 _BottomColor;
            fixed4 _TopColor;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = lerp(_BottomColor, _TopColor, v.uv.y);
                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        }
    }
}
