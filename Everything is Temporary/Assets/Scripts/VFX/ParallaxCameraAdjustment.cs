﻿using System;
using UnityEngine;

namespace VFX
{
    [RequireComponent(typeof(Camera))]
    [ExecuteInEditMode]
    public class ParallaxCameraAdjustment : MonoBehaviour
    {
        private void Awake()
        {
            m_camera = GetComponent<Camera>();
        }

        private void LateUpdate()
        {
            float theta = Mathf.Deg2Rad * m_camera.fieldOfView / 2;
            float aspect = m_camera.aspect;
            float near = m_camera.nearClipPlane;
            float far = m_camera.farClipPlane;

            float h = near * Mathf.Tan(theta);
            float w = aspect * h;

            /*
             * Results in a frustum where the bottom plane is horizontal.
             * */

            Vector4 row1 = new Vector4(2 * near / w, 0, 0, 0);
            Vector4 row2 = new Vector4(0, 2 * near / h, 1, 0);
            Vector4 row3 = new Vector4(0, 0, -(far + near) / (far - near), -2 * far * near / (far - near));
            Vector4 row4 = new Vector4(0, 0, -1, 0);

            m_camera.projectionMatrix = new Matrix4x4(row1, row2, row3, row4).transpose;
        }

        private Camera m_camera;
    }
}