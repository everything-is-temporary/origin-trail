﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;

namespace VFX
{
    [ExecuteInEditMode]
    public class ParallaxLayer : MonoBehaviour
    {
        [SerializeField]
        private Camera m_camera = default;

        [SerializeField]
        [Tooltip("The parallax layer that will contain colliders and should not move. There can only be one per scene.")]
        private bool m_isNeutral = default;

        /// <summary>
        /// Ensures all children are at the correct Z level.
        /// </summary>
        public void EnsureCorrect()
        {
            RecurseEnsureCorrect(transform);
        }

        private void RecurseEnsureCorrect(Transform tr)
        {
            float myZ = transform.position.z;

            foreach (Transform child in tr)
            {
                child.SetZ(myZ);
                RecurseEnsureCorrect(child);
            }
        }

        private void OnValidate()
        {
            if (m_camera == null)
            {
                Debug.LogWarning($"No camera set on {name}'s Parallax Layer component.");
                return;
            }

            // Without this, assigning a new camera can trigger a Reposition()
            m_cameraPreviousZ = m_camera.transform.position.z;
            m_cameraPreviousFov = m_camera.fieldOfView;

            if (m_camera.orthographic)
            {
                Debug.LogWarning($"{nameof(ParallaxLayer)} only works with perspective camera. Changing camera to perspective mode.");
                m_camera.orthographic = false;
            }

            if (m_camera.transform.position.z >= transform.position.z)
            {
                Debug.LogWarning($"Parallax Layer should be in front of the camera. Moving {name}.");

                var camZ = m_camera.transform.position.z;
                transform.SetZ(camZ + 1);
            }

            if (m_isNeutral)
            {
                // Ensure this is the only neutral layer for this camera, and also
                // inform all the other layers that this is the neutral layer.
                foreach (var parallaxLayer in FindObjectsOfType<ParallaxLayer>())
                {
                    if (parallaxLayer == this)
                        continue;

                    if (parallaxLayer.m_camera == m_camera)
                    {
                        if (parallaxLayer.m_isNeutral)
                        {
                            Debug.LogWarning($"The parallax layer '{parallaxLayer.gameObject.name}' was previously" +
                                $" set to neutral, but the new neutral layer is '{gameObject.name}'.");
                            parallaxLayer.m_isNeutral = false;
                        }
                    }

                    parallaxLayer.m_neutralLayer = this;
                }
            }
            else
            {
                foreach (var parallaxLayer in FindObjectsOfType<ParallaxLayer>())
                {
                    // We may have been the neutral layer previously.
                    if (parallaxLayer.m_neutralLayer == this)
                        parallaxLayer.m_neutralLayer = null;

                    // We may also be a newly added layer, so we should search for the
                    // neutral layer.
                    if (parallaxLayer.m_isNeutral && parallaxLayer.m_camera == m_camera)
                        m_neutralLayer = parallaxLayer;
                }
            }
        }

        /// <summary>
        /// Adjusts the Y position so that the object has the same offset from the
        /// bottom part of the camera's view as it did before the camera moved.
        /// If this is the neutral layer, it will move the camera instead.
        /// </summary>
        private void Reposition()
        {
            float newFov = m_camera.fieldOfView;
            float newZDelta = transform.position.z - m_camera.transform.position.z;

            float oldFov = m_cameraPreviousFov;
            float oldZDelta = transform.position.z - m_cameraPreviousZ;

            float yAdjustment = ComputeYAdjustment(oldFov, oldZDelta, newFov, newZDelta);

            if (m_isNeutral)
                RepositionNeutral(yAdjustment);
            else
                RepositionUsual(yAdjustment);

            m_cameraPreviousFov = m_camera.fieldOfView;
            m_cameraPreviousZ = m_camera.transform.position.z;
        }

        private void RepositionNeutral(float yAdjustment)
        {
            m_camera.transform.SetY(m_camera.transform.position.y - yAdjustment);
        }

        private void RepositionUsual(float yAdjustment)
        {
            if (m_neutralLayer == null)
            {
                transform.SetY(transform.position.y + yAdjustment);
            }
            else
            {
                float newFov = m_camera.fieldOfView;
                float newZDeltaNeutral = m_neutralLayer.transform.position.z - m_camera.transform.position.z;

                float oldFov = m_cameraPreviousFov;
                float oldZDeltaNeutral = m_neutralLayer.transform.position.z - m_cameraPreviousZ;

                float neutralYAdjustment = ComputeYAdjustment(oldFov, oldZDeltaNeutral, newFov, newZDeltaNeutral);

                transform.SetY(transform.position.y + yAdjustment - neutralYAdjustment);
            }
        }

        private float ComputeYAdjustment(float oldFov, float oldZDelta, float newFov, float newZDelta)
        {
            return oldZDelta * Mathf.Tan(0.5f * oldFov * Mathf.Deg2Rad) - newZDelta * Mathf.Tan(0.5f * newFov * Mathf.Deg2Rad);
        }

        private void Update()
        {
            if (m_camera == null)
                return;

            if (DidFovChange() || DidZChange())
                Reposition();
        }

        private bool DidFovChange()
        {
            return !Mathf.Approximately(m_cameraPreviousFov, m_camera.fieldOfView);
        }

        private bool DidZChange()
        {
            return !Mathf.Approximately(m_cameraPreviousZ, m_camera.transform.position.z);
        }

        private void OnTransformChildrenChanged()
        {
            // Like EnsureCorrect(), but without recursion.
            foreach (Transform child in transform)
                child.transform.SetZ(transform.position.z);
        }

        [SerializeField, HideInInspector]
        private float m_cameraPreviousFov;

        [SerializeField, HideInInspector]
        private float m_cameraPreviousZ;

        /// <summary>
        /// The layer with respect to which all motion is done.
        /// </summary>
        [SerializeField, HideInInspector]
        private ParallaxLayer m_neutralLayer;
    }
}