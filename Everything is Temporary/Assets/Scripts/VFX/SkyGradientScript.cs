﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CoreGame.Progression;

namespace VFX
{
    /// <summary>
    /// Applies the vertical gradient shader to the <see cref="Renderer"/>
    /// component attached to the same object. Should be used with a quad
    /// renderer (i.e. there should be a <see cref="MeshFilter"/> with a quad
    /// mesh). Changes gradient based on time of day.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(Renderer))]
    public class SkyGradientScript : MonoBehaviour
    {
        public Gradient bottomColors;
        public Gradient topColors;

        [Range(0, 1)]
        public float interpolationTime;

        private void Awake()
        {
            m_material = new Material(Shader.Find("Hidden/Vertical Gradient"));

            m_renderer = GetComponent<Renderer>();
            m_renderer.material = m_material;
            m_renderer.sortingLayerID = SortingLayer.NameToID("True Background");
        }

        private void Update()
        {
            if (Application.isPlaying)
                interpolationTime = ProgressionManager.Instance.CurrentDayCompletionFraction;

            m_material.SetColor("_BottomColor", bottomColors.Evaluate(interpolationTime));
            m_material.SetColor("_TopColor", topColors.Evaluate(interpolationTime));
        }

        private Renderer m_renderer;
        private Material m_material;
    }
}