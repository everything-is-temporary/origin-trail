﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameInventory;
using GameUtility;
using UnityEngine;

namespace GameUI.Notifications
{
    /// <summary>
    /// Script to automatically display inventory-related notifications.
    /// </summary>
    public class ItemNotifications : MonoBehaviour
    {
        [SerializeField]
        [RequiresType(typeof(IItemNotificationDisplay))]
        private Component m_itemNotificationPrefab = default;

        private void HandleItemAmountChanged(ItemWithAmount oldItemWithAmount, ItemWithAmount newItemWithAmount)
        {
            int change = newItemWithAmount.Amount - oldItemWithAmount.Amount;
            ItemReference item = newItemWithAmount.ItemReference;
            m_notificationsToShow.Enqueue(new ItemWithAmount(item, change));

            if (!m_addingNotifications)
                StartAddingNotifications().FireAndForget();
        }

        private async Task StartAddingNotifications()
        {
            // No need to worry about concurrency because this all happens
            // on the same thread.
            m_addingNotifications = true;

            int delay = 200;

            while (m_notificationsToShow.Count > 0)
            {
                ItemWithAmount change = m_notificationsToShow.Dequeue();

                ShowAndAnimateItemChange(change.Amount, change.ItemReference)
                    .FireAndForget();

                if (delay > 0)
                {
                    await Task.Delay(delay);

                    // Keep reducing delay so that if a large number of changes
                    // happens simultaneously, only the first few are displayed
                    // with a delay and the rest are shown instantaneously.
                    delay -= 25;
                }
            }

            m_addingNotifications = false;
        }

        private async Task ShowAndAnimateItemChange(int change, ItemReference item)
        {
            Component popupObject = Instantiate(m_itemNotificationPrefab, transform);
            IItemNotificationDisplay popup = (IItemNotificationDisplay)popupObject;

            popup.AmountChange = change;
            popup.Item = item;

            await Task.Delay(2000);
            Destroy(popupObject.gameObject);
        }


        private void Start()
        {
            m_inventory = InventoryManager.Instance.ConvoyAndPartyInventory;
            m_inventory.OnItemAmountChanged += HandleItemAmountChanged;
        }

        private void OnEnable()
        {
            // OnEnable() is called before Start(), so m_inventory could be
            // null here if this is called on scene load.
            if (m_inventory != null)
                m_inventory.OnItemAmountChanged += HandleItemAmountChanged;
        }

        private void OnDisable()
        {
            m_inventory.OnItemAmountChanged -= HandleItemAmountChanged;
        }

        private IInventory m_inventory;
        private readonly Queue<ItemWithAmount> m_notificationsToShow = new Queue<ItemWithAmount>();
        private bool m_addingNotifications;
    }
}