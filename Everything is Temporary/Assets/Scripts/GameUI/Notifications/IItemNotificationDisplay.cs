﻿using System;
using GameInventory;

namespace GameUI.Notifications
{
    public interface IItemNotificationDisplay
    {
        int AmountChange { get; set; }
        ItemReference Item { get; set; }
    }
}