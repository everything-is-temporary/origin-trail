﻿using System.Collections;
using System.Collections.Generic;
using GameInventory;
using UnityEngine;

namespace GameUI.Notifications
{
    using UI = UnityEngine.UI;

    public class ItemNotificationDisplayScript : MonoBehaviour, IItemNotificationDisplay
    {
        public int AmountChange { get; set; }
        public ItemReference Item { get; set; }

        [SerializeField]
        private UI.Text m_itemAmountText;

        [SerializeField]
        private UI.Text m_itemNameText;

        [SerializeField]
        private UI.Image m_itemIcon;

        private void Start()
        {
            InventoryItem item = Item.Item;

            if (item != null && AmountChange != 0)
            {
                m_itemIcon.sprite = item.Icon;
                m_itemNameText.text = item.Name;

                // If AmountChange is positive, this will display a + sign.
                m_itemAmountText.text = AmountChange.ToString("+#;-#");
            }
        }
    }
}