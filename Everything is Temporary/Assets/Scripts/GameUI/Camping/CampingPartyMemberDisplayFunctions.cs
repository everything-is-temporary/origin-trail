﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUI.Camping
{
    /// <summary>
    /// UI functions for the <see cref="CampingPartyMemberDisplay"/>. These
    /// should be called by UI elements.
    /// </summary>
    public class CampingPartyMemberDisplayFunctions : MonoBehaviour
    {
        public event System.Action OnHide;
        public event System.Action OnFeed;
        public event System.Action OnTalk;

        public void Hide()
        {
            OnHide?.Invoke();
        }

        public void Feed()
        {
            OnFeed?.Invoke();
        }

        public void Talk()
        {
            OnTalk?.Invoke();
        }
    }
}