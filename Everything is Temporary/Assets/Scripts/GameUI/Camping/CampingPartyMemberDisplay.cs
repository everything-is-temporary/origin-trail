﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Characters;

namespace GameUI.Camping
{
    using UI = UnityEngine.UI;

    [RequireComponent(typeof(CampingPartyMemberDisplayFunctions))]
    public class CampingPartyMemberDisplay : MonoBehaviour
    {
        [SerializeField]
        private UI.Text m_characterName = default;

        [SerializeField]
        private UI.Image m_characterIcon = default;

        public event System.Action<CharacterReference> OnFeed;

        public event System.Action<CharacterReference> OnTalk;

        public void ShowForPartyMember(CharacterReference charRef)
        {
            m_shownCharacter = charRef;

            if (m_characterName != null)
                m_characterName.text = charRef.Character.Name;

            if (m_characterIcon != null)
                m_characterIcon.sprite = charRef.Character.Icon;

            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void Awake()
        {
            m_functions = GetComponent<CampingPartyMemberDisplayFunctions>();

            m_functions.OnHide += Hide;
            m_functions.OnTalk += () => OnTalk?.Invoke(m_shownCharacter);
            m_functions.OnFeed += () => OnFeed?.Invoke(m_shownCharacter);

            Hide();
        }

        private CharacterReference m_shownCharacter;
        private CampingPartyMemberDisplayFunctions m_functions;
    }
}