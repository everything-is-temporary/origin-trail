﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;

/// <summary>
/// Component that is placed on the game view (aka the sidescroller quad). This
/// exposes the RenderTexture used by the view, so that cameras may render into
/// it.
/// </summary>
public class SidescrollQuadScript : SingletonMonoBehavior<SidescrollQuadScript>
{
    /// <summary>
    /// The render texture that is being displayed on the quad.
    /// </summary>
    /// <value>The render texture.</value>
    public RenderTexture RenderTexture => m_renderTexture;

    [SerializeField]
    private RenderTexture m_renderTexture = null;
}
