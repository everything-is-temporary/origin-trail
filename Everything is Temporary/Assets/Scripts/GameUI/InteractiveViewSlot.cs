﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUI.Abstractions;
using GameUI.Raycasting;
using GameUtility;
using GameUtility.ObserverPattern;

namespace GameUI
{
    /// <summary>
    /// A scriptable object containing a reference to an IInteractiveView. The
    /// purpose is to decouple the view from the thing that renders it.
    /// </summary>
    [CreateAssetMenu(fileName = "Interactive View Slot", menuName = "Interactive View Slot")]
    public class InteractiveViewSlot : ScriptableObject, IInteractiveView
    {
        public IInteractiveView View { get; private set; }

        /// <summary>
        /// Set the view in the slot. This can only happen if View is null. This
        /// returns a token whose Unsubscribe() method can be called to release
        /// the slot.
        /// </summary>
        /// <returns>An IUnsubscriber that can be used to release the slot. If the slot
        /// is not released, it can never be used by anything else again.</returns>
        /// <param name="view">View.</param>
        public IUnsubscriber SetView(IInteractiveView view)
        {
            if (View != null)
            {
                Debug.LogError("Trying to call SetView() on an occupied slot. The previous" +
                    " occupant must first release the slot by calling Unsubscribe() on the" +
                    " given return value.");

                return null;
            }

            View = view;
            m_delegateRaycaster.Delegate = View?.GetRaycaster();

            return new ViewResetter(this);
        }

        private void ResetView()
        {
            View = null;
        }

        #region IInteractiveView implementation
        // A slot can act like a view, but anything that needs to use the
        // view functionalities should access them through View directly.

        void IInteractiveView.RenderTo(RenderTexture target)
        {
            View?.RenderTo(target);
        }

        IRaycaster IInteractiveView.GetRaycaster()
        {
            // Have to use a delegate raycaster to follow the rule that GetRaycaster()
            // should always return the same value.
            return m_delegateRaycaster;
        }
        #endregion

        private DelegateRaycaster m_delegateRaycaster = new DelegateRaycaster();

        /// <summary>
        /// Calls the private ResetView() method on the slot.
        /// </summary>
        private class ViewResetter : IUnsubscriber
        {
            public ViewResetter(InteractiveViewSlot slot)
            {
                m_slotRef = new WeakReference<InteractiveViewSlot>(slot);
                m_didReset = false;
            }

            public void Unsubscribe()
            {
                if (m_didReset)
                    return;

                if (m_slotRef.TryGetTarget(out var slot))
                {
                    slot.ResetView();
                    m_didReset = true;
                }
            }

            private WeakReference<InteractiveViewSlot> m_slotRef;
            private bool m_didReset;
        }
    }
}