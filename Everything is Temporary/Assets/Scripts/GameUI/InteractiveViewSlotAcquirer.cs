﻿using GameUI.Abstractions;
using GameUtility;
using UnityEngine;
using GameUtility.ObserverPattern;

namespace GameUI
{
    public class InteractiveViewSlotAcquirer : MonoBehaviour
    {
        [RequiresType(typeof(IInteractiveView))]
        public Object interactiveView;
        public InteractiveViewSlot viewSlot;

        private void Awake()
        {
            if (viewSlot != null && interactiveView != null)
            {
                m_viewSlotUnsubscriber = viewSlot.SetView((IInteractiveView)interactiveView);
            }
        }

        private void OnDestroy()
        {
            m_viewSlotUnsubscriber?.Unsubscribe();
        }

        private IUnsubscriber m_viewSlotUnsubscriber;
    }
}