﻿using System.Threading.Tasks;
using UnityEngine;

namespace GameUI
{
    /// <summary>
    /// Utility script for asynchronously adding an efficient fade effect to a camera.
    /// Meant to be used in code.
    /// </summary>
    public class CameraFadeHelper : MonoBehaviour
    {
        /// <summary>
        /// Gets the fade script on the object if there is one already, or adds
        /// a new one. Use this to avoid creating duplicate fade scripts on the
        /// same object.
        /// </summary>
        /// <returns>The fade script.</returns>
        /// <param name="obj">The game object for which to get the fade script.</param>
        public static CameraFadeHelper GetFadeScript(GameObject obj)
        {
            CameraFadeHelper script = obj.GetComponent<CameraFadeHelper>();

            if (script == null)
                script = obj.AddComponent<CameraFadeHelper>();

            return script;
        }

        /// <summary>
        /// Whether a fade effect is currently in progress.
        /// </summary>
        public bool IsFading { get; private set; }

        /// <summary>
        /// The current fade value. Between 0 and 1, with 0 meaning "don't change
        /// image at all" and 1 meaning "replace image by fade color."
        /// </summary>
        /// <value>The fade value.</value>
        public float FadeValue { get; private set; }


        /// <summary>
        /// Cancels the previous fading process, if any, and begins a new one.
        /// </summary>
        /// <returns><c>true</c>, if fade succeeded, <c>false</c> if it was cancelled.</returns>
        /// <param name="initial">Initial fade value. Should be between 0 (no fade) and 1 (full fade).</param>
        /// <param name="final">Final fade value. Should be between 0 (no fade) and 1 (full fade).</param>
        /// <param name="color">Fade color.</param>
        /// <param name="time">Total time in seconds to fade.</param>
        public async Task<bool> FadeAsync(float initial, float final, Color color, float time)
        {
            if (IsFading)
            {
                // Cancel the previous fading.
                m_fadeTask?.SetResult(false);
                m_fadeTask = null;
            }

            m_initialFade = initial;
            FadeValue = m_initialFade;
            m_finalFade = final;
            m_startTime = Time.fixedTime;
            m_fadeTime = time;
            m_fadeColor = color;

            m_material.SetColor("_Color", m_fadeColor);

            IsFading = true;

            m_fadeTask = new TaskCompletionSource<bool>();
            return await m_fadeTask.Task;
        }

        private void Awake()
        {
            Shader shader = Shader.Find("Hidden/FadeShader");
            m_material = new Material(shader);

            IsFading = false;
            m_fadeTask = null;
        }

        private void FixedUpdate()
        {
            if (!IsFading)
                return;

            float deltaTime = Time.fixedTime - m_startTime;

            FadeValue = Mathf.Lerp(m_initialFade, m_finalFade, deltaTime / m_fadeTime);

            if (deltaTime > m_fadeTime)
            {
                FinishFade();
            }
        }

        private void FinishFade()
        {
            IsFading = false;
            m_fadeTask?.SetResult(true);
            m_fadeTask = null;
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            m_material.SetFloat("_FadeValue", FadeValue);
            Graphics.Blit(source, destination, m_material);
        }

        private float m_initialFade;
        private float m_finalFade;
        private float m_startTime;
        private float m_fadeTime;
        private Color m_fadeColor;
        private TaskCompletionSource<bool> m_fadeTask;

        private Material m_material;
    }
}