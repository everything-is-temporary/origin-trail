using System.Collections.Generic;
using Characters;
using GameInventory;
using GameInventory.Utility;
using UnityEngine;

namespace GameUI
{
    public class TestTradeInitializer : MonoBehaviour
    {
        [SerializeField] private TradeCanvasPairController m_controller;
        [SerializeField] private Inventory m_inv1;
        [SerializeField] private string m_inv1Name;
        [SerializeField] private Inventory m_inv2;
        [SerializeField] private string m_inv2Name;

        [SerializeField] private CharacterReference charl1;
        [SerializeField] private CharacterReference charl2;
        [SerializeField] private CharacterReference charr1;
        [SerializeField] private CharacterReference charr2;

        public void Start()
        {
            Test2();
        }

        private void Test1()
        {
            m_controller.Setup(m_inv1, m_inv2);
            m_controller.SetTradeCanvasName(m_inv1Name, m_inv2Name);
            m_controller.AddTradeValidator(TradeCanvasDisplayLocation.Right, TradeValidator.AtLeastEqualInValue);
        }

        private void Test2()
        {
            m_controller.SetTradeCanvasName("Left Side", "Right Side");
            m_controller.Setup(new List<CharacterReference>{charl1, charl2}, new List<CharacterReference>{charr1, charr2});
            m_controller.SetupMainInventory(charl1.Character.Inventory, charr1.Character.Inventory);
        }
    }
}