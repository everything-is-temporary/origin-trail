using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameUI
{
    // Created this class in case a generic version of this is needed. Remind me to remove it if unused.
    public class TextImageContentUIElement : CollapsiblePanel
    {
        [SerializeField] private List<Text> m_texts = new List<Text>();
        [SerializeField] private List<Image> m_images = new List<Image>();
        
        public event Action<TextImageContentUIElement> OnTextImageElementSelected;

        public virtual void Select()
        {
            OnTextImageElementSelected?.Invoke(this);
        }

        public virtual void SetText(params string[] texts)
        {
            if (texts.Length != m_texts.Count)
            {
                Debug.LogWarning($"Length of arguments passed to SetText ({texts.Length}) are not equal in length to the accepting prefabs ({m_texts.Count}).");
            }

            for (int i = 0; i < Math.Min(m_texts.Count, texts.Length); ++i)
            {
                m_texts[i].text = texts[i];
            }
        }

        public virtual void SetImages(params Image[] images)
        {
            if (images.Length != m_images.Count)
            {
                Debug.LogWarning($"Length of arguments passed to SetImages ({images.Length}) are not equal in length to the accepting prefabs ({m_images.Count}).");
            }
            
            for (int i = 0; i < Math.Min(m_images.Count, images.Length); ++i)
            {
                m_images[i].sprite = images[i].sprite;
            }
        }
    }
}