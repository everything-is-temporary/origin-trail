namespace GameUI.TradingV2
{
    public class TradeHistory
    {
        public bool HasCompletedTrade => m_completedTrades > 0;
        public bool HasFailedTrade => m_failedTrades > 0;

        public int AddCompletedTrade()
        {
            return m_completedTrades += 1;
        }

        public int AddFailedTrade()
        {
            return m_failedTrades += 1;
        }
        
        private int m_completedTrades = 0;
        private int m_failedTrades = 0;
    }
}