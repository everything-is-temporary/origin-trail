﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Characters;
using DG.Tweening;
using GameInventory;
using GameInventory.Utility;
using GameUtility;
using UnityEngine;
using UnityEngine.UI;

namespace GameUI
{
    public class TradeCanvasDisplay : MonoBehaviour
    {
        [SerializeField] private TradeOfferVisualizationDisplay m_tradeOfferVisualizationDisplay;
        [SerializeField] private OptionsSelectInteractiveContentPanel m_optionsSelectInteractiveContentPanel;
        [SerializeField] private TradeItemsSelectInteractiveContentPanel m_itemsSelectInteractiveContentPanel;

        [SerializeField] private static int m_maxAllowedOfferedUnits = 6; // Due to screen-space limitations
        
        public IModifiableInventory MainInventory {
            get
            {
                if (m_mainInventory != null)
                {
                    return m_mainInventory;
                }

                if (m_inventoryMapping.Count == 1)
                {
                    return m_inventoryMapping.Values.First();
                }
                Debug.LogWarning($"Cannot determine which inventory to receive traded items with {m_inventoryMapping.Count} possible inventories and m_defaultInventory not set.");
                return null;
            }
        }
        
        public bool TradeButtonEnabled
        {
            get => m_itemsSelectInteractiveContentPanel.TradeButtonEnabled;
            set => m_itemsSelectInteractiveContentPanel.TradeButtonEnabled = value;
        }

        public float AggregateContainedItemsValue => m_tradeOfferVisualizationDisplay.AggregateContainedItemsValue;
        
        public delegate void TradeInitiatedHandler(TradeCanvasDisplay sender);
        public event TradeInitiatedHandler OnTradeInitiated;

        public delegate void VisualizationUpdatedHandler(TradeCanvasDisplay sender);

        public event VisualizationUpdatedHandler OnVisualizationUpdated;
        
        public void InitiateTrade()
        {
            OnTradeInitiated?.Invoke(this);
        }

        private void HandleItemSelectionInventory(SelectableUIContainer<ItemWithAmountAndCharRef> itemWithAmountAndCharRefContainer)
        {
            // TODO: Modify this behavior to transfer more than 1 if desired
            InventoryToOffer(itemWithAmountAndCharRefContainer.Element.WithAmount(1));
        }
        
        private void HandleItemSelectionOffer(SelectableUIContainer<ItemWithAmountAndCharRef> itemWithAmountAndCharRefContainer)
        {
            // TODO: Modify this behavior to transfer more than 1 if desired
            OfferToInventory(itemWithAmountAndCharRefContainer.Element.WithAmount(1));
        }

        private bool InventoryToOffer(ItemWithAmountAndCharRef itemToTransfer)
        {
            // Check items held, subtract the above amount, add the amount subtracted to offer.
            if (m_tradeOfferVisualizationDisplay.Count >= m_maxAllowedOfferedUnits &&
                m_tradeOfferVisualizationDisplay.GetElement(itemToTransfer) == null)
            {
                // Validation check to make sure not too many items are added to offer bar.
                var element = m_itemsSelectInteractiveContentPanel.GetElement(itemToTransfer).gameObject.GetComponent<RectTransform>();
                element.DOComplete();
                element.DOShakePosition(0.5f, Vector3.right*15, randomness: 0f);
                return false;
            }
            return TransferItem(m_itemsSelectInteractiveContentPanel, m_tradeOfferVisualizationDisplay, itemToTransfer);
        }

        private bool OfferToInventory(ItemWithAmountAndCharRef itemToTransfer)
        {
            // Check items held, subtract the above amount, add the amount subtracted to offer.
            return TransferItem(m_tradeOfferVisualizationDisplay, m_itemsSelectInteractiveContentPanel, itemToTransfer);
        }

        private bool TransferItem(LayoutUpdater<ItemWithAmountAndCharRef> source,
            LayoutUpdater<ItemWithAmountAndCharRef> destination, ItemWithAmountAndCharRef itemToTransfer)
        {
            ItemWithAmountAndCharRef existingInSource =
                source.GetElement(itemToTransfer)?.Element;
            if (existingInSource == null)
            {
                return false;
            }
            // Get the maximum possible to remove
            ItemWithAmountAndCharRef removedFromSource = itemToTransfer.WithAmount(Math.Min(itemToTransfer.ItemWithAmount.Amount, existingInSource.ItemWithAmount.Amount));
            ItemWithAmountAndCharRef existingInDest = destination.GetElement(itemToTransfer)?.Element;
            if (existingInDest != null)
            {
                destination.SetElement(existingInDest + removedFromSource);
            }
            else
            {
                if (!destination.TryAddElement(removedFromSource))
                {
                    return false;
                }
            }

            source.SetElement(existingInSource - removedFromSource);
            OnVisualizationUpdated?.Invoke(this);
            return true;
        }

        public void HandleCharacterInventorySelected(CharacterReference selectedCharacter)
        {
            IModifiableInventory retrievedInventory = selectedCharacter.CharacterOrNull?.Inventory;
            //FIXME: How to handle Convoy Inventory?
            // Likely solution, modify the GetCharInventoryFunction to return the convoy inventory, code in the Convoy's CharRef as readonly in InventoryManager or ConvoyAndPartyInventory
            if (retrievedInventory == null)
            {
                retrievedInventory = InventoryManager.Instance.GetConvoyInventory();
            }
            AddTradeInventory(retrievedInventory);
        }

        public void StartCharacterSelection(List<CharacterReference> characterSelections)
        {
            m_tradeMenuMode = TradeMenuMode.Characters;
            m_optionsSelectInteractiveContentPanel.ClearTradeOptions();
            m_optionsSelectInteractiveContentPanel.AddTradeOption(characterSelections);
            m_itemsSelectInteractiveContentPanel.Hide();
            m_optionsSelectInteractiveContentPanel.Show();
        }

        public void StartItemSelection()
        {
            m_tradeMenuMode = TradeMenuMode.Items;

            RefreshItemSelectionDisplay();
            
            m_optionsSelectInteractiveContentPanel.Hide();
            m_itemsSelectInteractiveContentPanel.Show();      
        }

        public void RefreshItemSelectionDisplay()
        {
            m_itemsSelectInteractiveContentPanel.Clear();
            
            // Get an ItemWithAmountAndCharRef object 
            List<ItemWithAmountAndCharRef> selectableItems = new List<ItemWithAmountAndCharRef>();
            foreach (KeyValuePair<CharacterReference,IModifiableInventory> referenceInventoryPair in m_inventoryMapping)
            {
                // Items selected are copies and will not modify the source inventory
                // The source inventory is only modified on trade confirmation
                selectableItems.AddRange(referenceInventoryPair.Value.Select(itemWithAmount => new ItemWithAmountAndCharRef(referenceInventoryPair.Key, itemWithAmount)));
            }

            // Remove any items in the offered display
            var itemsOnOffer = m_tradeOfferVisualizationDisplay.InternalElementStorage;
            for(int i = 0; i < selectableItems.Count; ++i)
            {
                if (itemsOnOffer.TryGetValue(selectableItems[i].GetKey(), out SelectableUIContainer<ItemWithAmountAndCharRef> offeredItem))
                {
                    selectableItems[i] -= offeredItem.Element;
                }
            }
            
            // Order by item name first, then by owner name with convoy first.
            m_itemsSelectInteractiveContentPanel.AddElements(
                selectableItems.OrderBy(itemWithAmountAndCharRef => itemWithAmountAndCharRef.ItemWithAmount.Item.Name)
                    .ThenBy(itemWithAmountAndCharRef => itemWithAmountAndCharRef.CharacterReference == null)
                    .ThenBy(itemWithAmountAndCharRef => itemWithAmountAndCharRef.CharacterReference?.CharacterOrNull?.Name)
                    .ThenBy(itemWithAmountAndCharRef => itemWithAmountAndCharRef.ItemWithAmount.Item.Name)
                    .ToList()
            );
            
            //FIXME: Remove items from top
        }

        public void HandleRefreshSingleItem(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            // This just refreshes all items, ideally we want to also pass in CharRef as argument here so we know which item to refresh.
            RefreshItemSelectionDisplay();
        }

        public void ClearTradeInventories()
        {
            foreach (IModifiableInventory modifiableInventory in m_inventoryMapping.Values)
            {
                modifiableInventory.OnItemAmountChanged -= HandleRefreshSingleItem;
            }
            m_inventoryMapping.Clear();
            m_tradeOfferVisualizationDisplay.Clear(); // Clear the visualization so items aren't there on the second shop opening.
        }

        public void AddTradeInventory(IModifiableInventory modifiableInventory, CharacterReference characterReference=null)
        {
            if (characterReference == null)
            {
                do
                {
                    characterReference = CharacterReference.FromId(CharacterDefinitionList.Instance.GenerateUniqueId());
                } while (m_inventoryMapping.ContainsKey(characterReference));
            }
            m_inventoryMapping.Add(characterReference, modifiableInventory);
            modifiableInventory.OnItemAmountChanged += HandleRefreshSingleItem;
        }
        
        private void Start()
        {
            m_itemsSelectInteractiveContentPanel.OnLayoutItemSelected += HandleItemSelectionInventory;
            m_tradeOfferVisualizationDisplay.OnLayoutItemSelected += HandleItemSelectionOffer;
            m_optionsSelectInteractiveContentPanel.OnCharacterSelectionMade += HandleCharacterInventorySelected;
        }

        private void OnDestroy()
        {
            m_itemsSelectInteractiveContentPanel.OnLayoutItemSelected -= HandleItemSelectionInventory;
            m_tradeOfferVisualizationDisplay.OnLayoutItemSelected -= HandleItemSelectionOffer;
            m_optionsSelectInteractiveContentPanel.OnCharacterSelectionMade -= HandleCharacterInventorySelected;
        }

        /// <summary>
        /// Modifies the internal inventory by the specified amounts.
        /// </summary>
        /// <returns></returns>
        public bool ApplyActualInventoryModification(List<ItemWithAmountAndCharRef> modifications)
        {
            bool allSuccessful = true;
            foreach (ItemWithAmountAndCharRef modification in modifications)
            {
                if (m_inventoryMapping.TryGetValue(modification, out IModifiableInventory modifiableInventory))
                {
                    modifiableInventory.ModifyItem(modification.ItemWithAmount);
                }
                else if (MainInventory != null)
                {
                    MainInventory.ModifyItem(modification.ItemWithAmount);
                }
                else
                {
                    allSuccessful = false;
                    Debug.LogWarning("Attempted to apply impossible modification to actual inventory.");
                }
            }

            return allSuccessful;
        }

        public List<ItemWithAmountAndCharRef> GetOfferedItemsList()
        {
            return m_tradeOfferVisualizationDisplay.ToList();
        }

        public void SetMainInventory(IModifiableInventory receivingInventory)
        {
            m_mainInventory = receivingInventory;
        }
        
        /// <summary>
        /// Removes the offered items from the internal inventory and clears the <see cref="m_tradeOfferVisualizationDisplay"/>
        /// </summary>
        public void TradeRemoveOfferedItems()
        {
            ApplyActualInventoryModification(m_tradeOfferVisualizationDisplay.Select(itemWithAmountAndCharRef => itemWithAmountAndCharRef.Invert()).ToList());
            m_tradeOfferVisualizationDisplay.Clear();
        }

        public void SetItemSelectionNameAndImage(string name, Image image)
        {
            m_itemsSelectInteractiveContentPanel.SetHeader(name, image);
        }
        
        public void SetItemSelectionName(string newName)
        {
            if (newName == null)
            {
                m_itemsSelectInteractiveContentPanel.HeaderEnabled = false;
                return;
            }
            m_itemsSelectInteractiveContentPanel.SetHeaderName(newName);
        }

        public void UpdateVisualizationValues(float newVisualizationValue)
        {
            m_tradeOfferVisualizationDisplay.UpdateVisualizationValues(newVisualizationValue);
        }

        public void AddTradeValidator(TradeValidator tradeValidator)
        {
            m_validationFunctions.Add(tradeValidator);
        }

        public void ClearTradeValidation()
        {
            m_validationFunctions.Clear();
        }

        public bool ValidateTrade(TradeCanvasDisplay other)
        {
            return m_validationFunctions.TrueForAll(validator => validator.IsValid(this, other));
        }

        private IModifiableInventory m_mainInventory = null; //explicitly initialized to null, this field should NOT be serialized
        private Dictionary<CharacterReference, IModifiableInventory> m_inventoryMapping = new Dictionary<CharacterReference, IModifiableInventory>();
        private TradeMenuMode m_tradeMenuMode;
        private List<TradeValidator> m_validationFunctions = new List<TradeValidator>();
    }
}

