﻿using System;
using System.Collections.Generic;
using Characters;
using GameInventory;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI
{
    public class OptionsSelectInteractiveContentPanel : CollapsiblePanel
    {
        [SerializeField, NotNull] private RectTransform m_optionsSelectInteractiveContentRoot;
        [SerializeField] private CharacterAndNameUIElement m_tradePartnerOptionPrefab;

        public event Action<CharacterReference> OnCharacterSelectionMade;
        
        public void CharacterSelectionMadeHandler(CharacterReference characterReference)
        {
            OnCharacterSelectionMade?.Invoke(characterReference);
        }
        
        public void AddTradeOption(IEnumerable<CharacterReference> characterReferences)
        {
            foreach (CharacterReference characterReference in characterReferences)
            {
                AddTradeOption(characterReference);
            }
        }
        
        public void AddTradeOption(CharacterReference characterReference)
        {
            CharacterAndNameUIElement partnerSelectOption =
                Instantiate(m_tradePartnerOptionPrefab, m_optionsSelectInteractiveContentRoot);
            partnerSelectOption.Setup(characterReference);
            m_tradePartners.Add(characterReference, partnerSelectOption);
            partnerSelectOption.OnCharacterSelectionMade += CharacterSelectionMadeHandler;
        }

        public bool RemoveTradeOption(CharacterReference characterReference)
        {
            if (m_tradePartners.TryGetValue(characterReference, out CharacterAndNameUIElement partner))
            {
                partner.OnCharacterSelectionMade -= CharacterSelectionMadeHandler;
                DestroyImmediate(partner.gameObject);
                m_tradePartners.Remove(characterReference);
            }

            return false;
        }

        public void ClearTradeOptions()
        {
            foreach (CharacterAndNameUIElement tradePartner in m_tradePartners.Values)
            {
                DestroyImmediate(tradePartner.gameObject);
                m_tradePartners.Clear();
            }
        }
        
        //FIXME: Why does this not run?
        private void Start()
        {
            Debug.Assert(m_optionsSelectInteractiveContentRoot != null, "OptionsSelectInteractiveContentPanel must have a linked component of type RectTransform (to serve as the content root under which to add prefab instances).");
            Debug.Assert(m_tradePartnerOptionPrefab != null, "OptionsSelectInteractiveContentPanel must have a linked component of type RectTransform (to add prefabs for selecting trade partner).");
        }

        private Dictionary<CharacterReference, CharacterAndNameUIElement> m_tradePartners = new Dictionary<CharacterReference, CharacterAndNameUIElement>();
    }

}
