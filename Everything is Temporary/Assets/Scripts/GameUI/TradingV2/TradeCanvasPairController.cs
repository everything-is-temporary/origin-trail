using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Characters;
using DG.Tweening;
using GameInventory;
using GameInventory.Utility;
using GameUI.TradingV2;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI
{
    public class TradeCanvasPairController : UIBehaviour, ITradeDisplay
    {
        [SerializeField] private TradeCanvasDisplay m_tradeCanvasDisplayLeft;
        [SerializeField] private TradeCanvasDisplay m_tradeCanvasDisplayRight;
        //[SerializeField] private GenericButtonPopupScript m_confirmationPopup; //TODO: Implement confirmation popup to verify trade

        [SerializeField] private static float maxVisualizationValueFactor = 5f; //If offered items are 10 times more valuable on one side than the other, the scales will not change further.
        public TradeHistory CurrentTradeHistory => m_currentTradeHistory;
        
        public async Task<TradeResult> DisplayTrade(ConvoyAndPartyInventory convoyAndPartyInventory, Shop shop)
        {
            Setup(convoyAndPartyInventory, shop.Inventory);
            foreach (TradeValidator shopTradeValidator in shop.ShopTradeValidators)
            {
                AddTradeValidator(TradeCanvasDisplayLocation.Right, shopTradeValidator);
            }
            SetTradeCanvasName(TradeCanvasDisplayLocation.Left, "Convoy"); //FIXME: Remove Hardcoded value.
            SetTradeCanvasName(TradeCanvasDisplayLocation.Right, shop.Name);
            
            gameObject.SetActive(true);
            var result = await TradeForUserClosedTradeMenu();
            gameObject.SetActive(false);

            return result;
        }

        private async Task<TradeResult> TradeForUserClosedTradeMenu()
        {
            TaskCompletionSource<object> tradeMenuClosed = new TaskCompletionSource<object>();
            void tradeMenuClosedHandler() => tradeMenuClosed.SetResult(null);

            OnTradeMenuClosed += tradeMenuClosedHandler;

            Task result = await Task.WhenAny(tradeMenuClosed.Task);

            OnTradeMenuClosed -= tradeMenuClosedHandler;

            return m_currentTradeHistory.HasCompletedTrade
                ? TradeResult.FinishedTrade(null) //TODO: Modify TradeResult to not require this info or have more useful info.
                : TradeResult.CancelledTrade();
        }
        
        public void Setup(List<CharacterReference> leftPartners, List<CharacterReference> rightPartners)
        {
            Setup(
                leftPartners.Select(characterReference => new KeyValuePair<IModifiableInventory, CharacterReference>(characterReference.CharacterOrNull?.Inventory, characterReference)).Where(pair => pair.Key != null).ToList(),
                rightPartners.Select(characterReference => new KeyValuePair<IModifiableInventory, CharacterReference>(characterReference.CharacterOrNull?.Inventory, characterReference)).Where(pair => pair.Key != null).ToList()
            );
        }

        public void Setup(ConvoyAndPartyInventory convoyAndPartyInventory)
        {
            throw new NotImplementedException();
            foreach (KeyValuePair<IModifiableInventory,CharacterReference> inventoryCharacterReference in convoyAndPartyInventory.InventoryCharacterReferences)
            {
                m_tradeCanvasDisplayLeft.AddTradeInventory(inventoryCharacterReference.Key, inventoryCharacterReference.Value);
                m_tradeCanvasDisplayRight.AddTradeInventory(inventoryCharacterReference.Key, inventoryCharacterReference.Value);
            }
            m_tradeCanvasDisplayLeft.SetMainInventory(convoyAndPartyInventory);
            m_tradeCanvasDisplayRight.SetMainInventory(convoyAndPartyInventory);
            m_tradeCanvasDisplayLeft.TradeButtonEnabled = false;
            m_tradeCanvasDisplayRight.TradeButtonEnabled = true;
            // Enable selecting option
        }

        public void Setup(ConvoyAndPartyInventory convoyAndPartyInventory, IModifiableInventory tradedInventory)
        {
            Setup(
                convoyAndPartyInventory.InventoryCharacterReferences,
                new Dictionary<IModifiableInventory, CharacterReference>{{tradedInventory, null}}
            );
            
            m_tradeCanvasDisplayLeft.SetMainInventory(convoyAndPartyInventory);
        }

        public void Setup(IModifiableInventory firstInventory, IModifiableInventory secondInventory)
        {
            Setup(
                new Dictionary<IModifiableInventory, CharacterReference>{{firstInventory, null}},
                new Dictionary<IModifiableInventory, CharacterReference>{{secondInventory, null}}
            );
        }

        /// <summary>
        /// The main setup function, does all the work of adding inventories, called by every other setup function.
        /// </summary>
        /// <param name="leftSide"></param>
        /// <param name="rightSide"></param>
        public void Setup(IEnumerable<KeyValuePair<IModifiableInventory, CharacterReference>> leftSide, IEnumerable<KeyValuePair<IModifiableInventory, CharacterReference>> rightSide)
        {
            m_tradeCanvasDisplayLeft.ClearTradeInventories();
            foreach (KeyValuePair<IModifiableInventory,CharacterReference> inventoryReference in leftSide)
            {
                m_tradeCanvasDisplayLeft.AddTradeInventory(inventoryReference.Key, inventoryReference.Value);
            }
            
            m_tradeCanvasDisplayRight.ClearTradeInventories();
            foreach (KeyValuePair<IModifiableInventory,CharacterReference> inventoryReference in rightSide)
            {
                m_tradeCanvasDisplayRight.AddTradeInventory(inventoryReference.Key, inventoryReference.Value);
            }

            StartItemSelection();
            m_tradeCanvasDisplayLeft.TradeButtonEnabled = false;
            m_tradeCanvasDisplayRight.TradeButtonEnabled = true;
        }

        //TODO: Make the above constructors reference a single constructor at the end.
        
        public void Reset()
        {
            m_currentTradeHistory = new TradeHistory();
        }

        public void SetupMainInventory(IModifiableInventory leftInventory, IModifiableInventory rightInventory)
        {
            m_tradeCanvasDisplayLeft.SetMainInventory(leftInventory);
            m_tradeCanvasDisplayRight.SetMainInventory(rightInventory);
        }


        public void SetTradeCanvasName(string leftName, string rightName)
        {
            m_tradeCanvasDisplayLeft.SetItemSelectionName(leftName);
            m_tradeCanvasDisplayRight.SetItemSelectionName(rightName);
        }

        public void SetTradeCanvasName(TradeCanvasDisplayLocation tradeCanvasDisplayLocation, string tradeCanvasName)
        {
            GetTradeCanvas(tradeCanvasDisplayLocation).SetItemSelectionName(tradeCanvasName);
        }

        public void AddTradeValidator(TradeCanvasDisplayLocation tradeCanvasDisplayLocation, TradeValidator tradeValidator)
        {
            GetTradeCanvas(tradeCanvasDisplayLocation).AddTradeValidator(tradeValidator);
        }

        public void ClearTradeValidation(TradeCanvasDisplayLocation tradeCanvasDisplayLocation)
        {
            GetTradeCanvas(tradeCanvasDisplayLocation).ClearTradeValidation();            
        }

        private void StartItemSelection()
        {
            m_tradeCanvasDisplayLeft.StartItemSelection();
            m_tradeCanvasDisplayRight.StartItemSelection();
        }
        
        public void ExitTradeMenu()
        {
            // TODO: See if there's anything else that needs to be here.
            OnTradeMenuClosed?.Invoke();
        }

        private TradeCanvasDisplay GetTradeCanvas(TradeCanvasDisplayLocation canvasDisplayLocation)
        {
            return canvasDisplayLocation == TradeCanvasDisplayLocation.Left
                ? m_tradeCanvasDisplayLeft
                : m_tradeCanvasDisplayRight;
        }

        private void HandleTradeInvoked(TradeCanvasDisplay callee)
        {
            // TODO: Add Confirmation Popup Here
            PerformTrade();
        }

        private void HandleVisualizationValuesUpdate(TradeCanvasDisplay tradeCanvasDisplay)
        {
            UpdateVisualizationValues();
        }

        private void UpdateVisualizationValues()
        {
            float lhsValue = m_tradeCanvasDisplayLeft.AggregateContainedItemsValue;
            float rhsValue = m_tradeCanvasDisplayRight.AggregateContainedItemsValue;


            
            if (Mathf.Approximately(lhsValue, rhsValue))
            {
                m_tradeCanvasDisplayLeft.UpdateVisualizationValues(0.5f);
                m_tradeCanvasDisplayRight.UpdateVisualizationValues(0.5f);
                return;
            }
            
            if (Mathf.Approximately(lhsValue, 0f))
            {
                m_tradeCanvasDisplayLeft.UpdateVisualizationValues(0f);
                m_tradeCanvasDisplayRight.UpdateVisualizationValues(1f);
                return;
            }
            else if (Mathf.Approximately(rhsValue, 0f))
            {
                m_tradeCanvasDisplayLeft.UpdateVisualizationValues(1f);
                m_tradeCanvasDisplayRight.UpdateVisualizationValues(0f);
                return;
            }
            
            float differenceFactor = Math.Max(lhsValue, rhsValue) / Math.Min(lhsValue, rhsValue);

            float valChange = 1 - 1f / (2f*differenceFactor); //0.5 to 1

            if (rhsValue < lhsValue)
            {
                m_tradeCanvasDisplayLeft.UpdateVisualizationValues(valChange);
                m_tradeCanvasDisplayRight.UpdateVisualizationValues(1.0f-valChange);
            }
            else
            {
                m_tradeCanvasDisplayLeft.UpdateVisualizationValues(1.0f-valChange);
                m_tradeCanvasDisplayRight.UpdateVisualizationValues(valChange);
            }
        }

        private void PerformTrade()
        {
            // Have both canvas displays validate one-another
            if (!m_tradeCanvasDisplayRight.ValidateTrade(m_tradeCanvasDisplayLeft))
            {
                HandleValidationFailed(m_tradeCanvasDisplayRight);
                return;
            }
            if(!m_tradeCanvasDisplayLeft.ValidateTrade(m_tradeCanvasDisplayRight))
            {
                HandleValidationFailed(m_tradeCanvasDisplayLeft);
                return;
            }
            
            var leftOfferedItems = m_tradeCanvasDisplayLeft.GetOfferedItemsList();
            var rightOfferedItems = m_tradeCanvasDisplayRight.GetOfferedItemsList();
            if (m_tradeCanvasDisplayRight.ApplyActualInventoryModification(leftOfferedItems)
                && m_tradeCanvasDisplayLeft.ApplyActualInventoryModification(rightOfferedItems))
            {
                m_tradeCanvasDisplayRight.TradeRemoveOfferedItems();
                m_tradeCanvasDisplayLeft.TradeRemoveOfferedItems();
                m_tradeCanvasDisplayRight.RefreshItemSelectionDisplay();
                m_tradeCanvasDisplayLeft.RefreshItemSelectionDisplay();
                m_currentTradeHistory.AddCompletedTrade();
                OnValidItemsTraded?.Invoke();
                return;
            }
            Debug.LogError("Trade process has failed.");
        }

        protected void HandleValidationFailed(TradeCanvasDisplay validationFailedCanvasDisplay)
        {
            m_currentTradeHistory.AddFailedTrade();
            var rt = validationFailedCanvasDisplay.gameObject.GetComponent<RectTransform>();
            rt.DOComplete();
            rt.DOShakePosition(1.0f, Vector3.right*10f);
        }

        protected void Start()
        {
            m_tradeCanvasDisplayRight.TradeButtonEnabled = true;
            m_tradeCanvasDisplayLeft.TradeButtonEnabled = false;
            m_tradeCanvasDisplayLeft.OnTradeInitiated += HandleTradeInvoked;
            m_tradeCanvasDisplayRight.OnTradeInitiated += HandleTradeInvoked;
            m_tradeCanvasDisplayLeft.OnVisualizationUpdated += HandleVisualizationValuesUpdate;
            m_tradeCanvasDisplayRight.OnVisualizationUpdated += HandleVisualizationValuesUpdate;
        }

        private void OnDestroy()
        {
            m_tradeCanvasDisplayLeft.OnTradeInitiated -= HandleTradeInvoked;
            m_tradeCanvasDisplayRight.OnTradeInitiated -= HandleTradeInvoked;
            m_tradeCanvasDisplayLeft.OnVisualizationUpdated -= HandleVisualizationValuesUpdate;
            m_tradeCanvasDisplayRight.OnVisualizationUpdated -= HandleVisualizationValuesUpdate;
        }

        private TradeCanvasDisplay GetTradeCanvasDisplay(TradeCanvasDisplayLocation tradeCanvasDisplayLocation)
        {
            if (tradeCanvasDisplayLocation == TradeCanvasDisplayLocation.Left)
            {
                return m_tradeCanvasDisplayLeft;
            }
            else if (tradeCanvasDisplayLocation == TradeCanvasDisplayLocation.Right)
            {
                return m_tradeCanvasDisplayRight;
            }

            throw new NotSupportedException("Unsupported TradeCanvasDisplayLocation provided.");
        }

        private TradeHistory m_currentTradeHistory = new TradeHistory(); //FIXME: creating a new object is not needed here, create default instead and make sure to reset this in setup.;
        
        private event Action OnTradeMenuClosed;
        private event Action OnValidItemsTraded;
    }
}