﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameInventory;
using GameUI;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CollapsiblePanel))]
public class TradeItemsSelectInteractiveContentPanel : LayoutUpdater<ItemWithAmountAndCharRef>
{
    [SerializeField] private TextImageContentUIElement m_tradeHeaderPanel;
    [SerializeField] private CollapsiblePanel m_tradeFooterPanel;

    public bool TradeButtonEnabled
    {
        get => m_tradeFooterPanel.IsEnabled;
        set => m_tradeFooterPanel.IsEnabled = value;
    }

    public bool HeaderEnabled
    {
        get => m_tradeHeaderPanel.IsEnabled;
        set => m_tradeHeaderPanel.IsEnabled = value;
    }

    public void Hide()
    {
        gameObject.GetComponent<CollapsiblePanel>().Hide();
    }

    public void Show()
    {
        gameObject.GetComponent<CollapsiblePanel>().Show();
    }
    
    public void SetHeader(string nameToSet, params Image[] imagesToSet)
    {
        SetHeaderName(nameToSet);
        m_tradeHeaderPanel.SetImages(imagesToSet);
    }

    public void SetHeaderName(string nameToSet)
    {
        m_tradeHeaderPanel.SetText(nameToSet);
    }

    private GridInventoryDisplayItem m_selectedItem;
}
