using System;
using System.Collections.Generic;
using System.Linq;
using GameUI;

namespace GameInventory.Utility
{
    public class TradeValidator
    {

        #region Typesafe Enumerations
        
        public static readonly TradeValidator AtLeastEqualInValue = new TradeValidator(AtLeastEqualInValueFunc);

        #endregion        
        
        private Func<TradeCanvasDisplay, TradeCanvasDisplay, bool> m_validationFunc;

        // Constructor is private since it is only used here. If it is necessary elsewhere, make it public.
        private TradeValidator(Func<TradeCanvasDisplay, TradeCanvasDisplay, bool> validationFunc)
        {
            m_validationFunc = validationFunc;
        }
        
        public bool IsValid(TradeCanvasDisplay myTradeCanvas, TradeCanvasDisplay otherTradeCanvas)
        {
            return m_validationFunc.Invoke(myTradeCanvas, otherTradeCanvas);
        }
        
        #region Static Functions to Validate Trade

        private static bool AtLeastEqualInValueFunc(TradeCanvasDisplay myTradeCanvas, TradeCanvasDisplay otherTradeCanvas)
        {
            return otherTradeCanvas.AggregateContainedItemsValue >= myTradeCanvas.AggregateContainedItemsValue;
        }
        
        #endregion //Static Functions to Validate Trade
    }
}