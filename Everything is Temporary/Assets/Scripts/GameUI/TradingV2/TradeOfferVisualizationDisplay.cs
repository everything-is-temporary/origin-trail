﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using GameInventory;
using UnityEngine;

namespace GameUI
{
    public class TradeOfferVisualizationDisplay : LayoutUpdater<ItemWithAmountAndCharRef>
    {
        [SerializeField] private RectTransform m_moveableVisualizationContainer;
        [SerializeField] private RectTransform m_moveableVisualizationBounds;
                
        public float VerticalPosition
        {
            get => (m_moveableVisualizationContainer.anchoredPosition.y - m_minAnchoredY) /
                   (m_maxAnchoredY - m_minAnchoredY);
            set => UpdateVerticalPosition(value);
        }

        public float AggregateContainedItemsValue => this.Select(itemWithAmountAndCharRef => itemWithAmountAndCharRef.ItemWithAmount.Amount * itemWithAmountAndCharRef.ItemWithAmount.Item.EconomyValue).Sum();

        public override void Clear()
        {
            base.Clear();
            VerticalPosition = 0.5f;
        }

        public override bool SetElement(ItemWithAmountAndCharRef elementToSet)
        {
            if (elementToSet.ItemWithAmount.Amount <= 0)
            {
                return RemoveElement(elementToSet);
            }

            return base.SetElement(elementToSet);
        }

        public void UpdateVisualizationValues(float newVisualizationValue)
        {
            UpdateVerticalPosition(newVisualizationValue);
        }
        
        private void Start()
        {
            //Assumes the container starts at the bottom (0)
            m_minAnchoredY = m_moveableVisualizationContainer.anchoredPosition.y;
            m_maxAnchoredY = m_moveableVisualizationContainer.anchoredPosition.y +
                             (m_moveableVisualizationBounds.rect.height - m_moveableVisualizationContainer.rect.height);
            UpdateVerticalPosition(0.5f);
        }

        /// <summary>
        /// Updated Vertical Position of Containing Element. 0 at the top, 1 at the bottom.
        /// </summary>
        /// <param name="newPositionFraction"></param>
        private void UpdateVerticalPosition(float newPositionFraction)
        {
            var newPos = m_minAnchoredY + (m_maxAnchoredY - m_minAnchoredY) * (Mathf.Clamp(newPositionFraction, 0, 1));
            DOTween.To(() => m_moveableVisualizationContainer.anchoredPosition.y,
                yval => m_moveableVisualizationContainer.anchoredPosition =
                    new Vector2(m_moveableVisualizationContainer.anchoredPosition.x, yval),
                newPos,
                1.0f
            ).SetEase(Ease.OutSine);
        }

        private float m_minAnchoredY;
        private float m_maxAnchoredY;

    }
}
