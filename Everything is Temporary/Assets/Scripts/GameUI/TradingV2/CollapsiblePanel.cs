using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI
{
    public class CollapsiblePanel : UIBehaviour
    {

        public bool IsEnabled
        {
            get => m_isEnabled;
            set
            {
                if (!value && m_isEnabled)
                {
                    Hide();
                }

                if (value && !m_isEnabled)
                {
                    Show();
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            m_isEnabled = gameObject.activeSelf;
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }   
        
        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private bool m_isEnabled;

    }
    
}