using System;
using GameUtility;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameUI
{
    public abstract class SelectableUIContainer<TContainedElement> : Selectable where TContainedElement : IKeyable<int>
    {
        
        public event Action<SelectableUIContainer<TContainedElement>> OnElementSelected;
        public event Action<SelectableUIContainer<TContainedElement>> OnElementDeselected;


        public TContainedElement Element => m_containedElement;
        public bool Selected => m_isSelected;

        public void Setup(TContainedElement containedElement)
        {
            m_containedElement = containedElement;
            SetupElement(containedElement);
        }

        protected abstract void SetupElement(TContainedElement containedElement);

        public virtual void Teardown()
        {
            // do nothing, but this method is here if needed
        }
        
        public override void OnPointerDown(PointerEventData pointerEventData)
        {
            m_isSelected = true;
            OnElementSelected?.Invoke(this);
        }

        public override void OnPointerUp(PointerEventData pointerEventData)
        {
            Deselect();
        }

        public virtual void Deselect()
        {
            m_isSelected = false;
            OnElementDeselected?.Invoke(this);
        }

        private TContainedElement m_containedElement;
        private bool m_isSelected = false;
    }
}