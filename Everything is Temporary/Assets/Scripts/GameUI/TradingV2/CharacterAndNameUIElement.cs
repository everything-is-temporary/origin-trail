using System;
using Characters;
using UnityEngine;
using UnityEngine.UI;

namespace GameUI
{
    [RequireComponent(typeof(Button))]
    public class CharacterAndNameUIElement : CollapsiblePanel
    {
        [SerializeField] private Text m_characterTitle;
        [SerializeField] private Image m_characterImage;
        private CharacterReference m_characterReference;
        
        public event Action<CharacterReference> OnCharacterSelectionMade;
        
        public void CharacterSelectionMade(CharacterReference characterReference)
        {
            OnCharacterSelectionMade.Invoke(characterReference);
        }
        
        public void Setup(CharacterReference characterReference)
        {
            gameObject.GetComponent<Button>().onClick.AddListener(() => OnCharacterSelectionMade?.Invoke(m_characterReference));
            m_characterImage.sprite = characterReference.Character.ScenePrefab.GetComponent<SpriteRenderer>()?.sprite;
            m_characterTitle.text = characterReference.Character.Name;
        }
    }
}