﻿using System;
using Characters;
using GameInventory;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameUI
{
    public class GridInventoryDisplayItem : SelectableUIContainer<ItemWithAmountAndCharRef>
    {

        [SerializeField] private CollapsiblePanel m_characterImagePanel;
        [SerializeField] private Image m_characterSpriteImage;
        [SerializeField] private Image m_itemSpriteImage;
        [SerializeField] private Text m_itemAmountText;

        protected override void Awake()
        {
            base.Awake();
            Debug.Assert(m_characterImagePanel != null, "GridInventoryDisplayItem must have a linked component of type CollapsiblePanel (to enable hiding of the character display panel when not applicable).");
            Debug.Assert(m_characterSpriteImage != null, "GridInventoryDisplayItem must have a linked component of type Image (to display the character sprite of the item's owner when applicable).");
            Debug.Assert(m_itemSpriteImage != null, "GridInventoryDisplayItem must have a linked component of type Image (to display the item sprite).");
            Debug.Assert(m_itemAmountText != null,
                "GridInventoryDisplayItem must have a linked component of type Text (to display items remaining).");
        }

        protected override void SetupElement(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            SetItem(itemWithAmountAndCharRef);
            var charSprite =
                itemWithAmountAndCharRef.CharacterReference?.CharacterOrNull?.Icon != null
                    ? itemWithAmountAndCharRef.CharacterReference.Character.Icon
                    : itemWithAmountAndCharRef.CharacterReference?.CharacterOrNull?.ScenePrefab?.GetComponent<SpriteRenderer>()
                        ?.sprite;
            // TODO: Add a sprite property to CharacterReference.Character
            if (itemWithAmountAndCharRef.CharacterReference?.CharacterOrNull != null && (m_characterSpriteImage.sprite = charSprite) != null)
            {
                m_characterImagePanel.Show();
            }
            else
            {
                m_characterImagePanel.Hide();
            }
        }

        public void SetItem(ItemWithAmount itemWithAmount)
        {
            m_itemSpriteImage.sprite = itemWithAmount.Item.Icon;
            m_itemAmountText.text = itemWithAmount.Amount.ToString();
        }

        /// <summary>
        /// Decrements the contained
        /// </summary>
        /// <param name="itemWithAmount"></param>
        /// <returns>The number of items removed.</returns>
        public int DecrementAndGetDelta(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            if (CheckPositiveAmountAndEqualRefs(itemWithAmountAndCharRef))
            {
                int removedQuantity = Element.ItemWithAmount.Amount - itemWithAmountAndCharRef.ItemWithAmount.Amount;
                ItemWithAmountAndCharRef updatedItemWithAmountAndCharRef = new ItemWithAmountAndCharRef(Element.CharacterReference, new ItemWithAmount(Element.ItemWithAmount.ItemReference, removedQuantity));
                return removedQuantity;
            }
            throw new ArgumentException("itemWithAmountAndCharRef passed as argument failed validation.");
        }

        public ItemWithAmountAndCharRef IncrementAndGetResult(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            if (CheckPositiveAmountAndEqualRefs(itemWithAmountAndCharRef))
            {
                ItemWithAmountAndCharRef updatedItemWithAmountAndCharRef = new ItemWithAmountAndCharRef(Element.CharacterReference, Element.ItemWithAmount + itemWithAmountAndCharRef.ItemWithAmount);
                Setup(updatedItemWithAmountAndCharRef);
                return updatedItemWithAmountAndCharRef;
            }
            throw new ArgumentException("itemWithAmountAndCharRef passed as argument failed validation.");
        }

        private bool CheckPositiveAmountAndEqualRefs(ItemWithAmountAndCharRef itemWithAmountAndCharRef)
        {
            if (itemWithAmountAndCharRef.CharacterReference != Element.CharacterReference ||
                itemWithAmountAndCharRef.ItemWithAmount.ItemReference != Element.ItemWithAmount.ItemReference ||
                itemWithAmountAndCharRef.ItemWithAmount.Amount < 0)
            {
                Debug.LogError("Validity check for provided change in itemWithAmountAndCharRef has failed.");
                return false;
            }

            return true;
        }
        
    }
}