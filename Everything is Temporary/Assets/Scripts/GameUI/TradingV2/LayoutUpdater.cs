using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameUtility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI
{
    public abstract class LayoutUpdater<TContainedElement> : UIBehaviour, IEnumerable<TContainedElement> where TContainedElement : IKeyable<int>
    {
        [SerializeField] private UIBehaviour m_uiElementClassPrefab; //Unity does not support generic serialization
        [SerializeField] private RectTransform m_layoutContentRoot;

        public Dictionary<int, SelectableUIContainer<TContainedElement>> InternalElementStorage =>
            m_internalElementStorage;

        public int Count => m_internalElementStorage.Count;
        
        public event Action<SelectableUIContainer<TContainedElement>> OnLayoutItemSelected;
        public event Action<SelectableUIContainer<TContainedElement>> OnLayoutItemDeselected;

        public virtual void HandleElementSelected(SelectableUIContainer<TContainedElement> selectableUiContainer)
        {
            OnLayoutItemSelected?.Invoke(selectableUiContainer);
        }

        public virtual void HandleElementDeselected(SelectableUIContainer<TContainedElement> selectableUiContainer)
        {
            OnLayoutItemDeselected?.Invoke(selectableUiContainer);
        }

        public void AddElements(IEnumerable<TContainedElement> elementsToAdd)
        {
            foreach (TContainedElement elementToAdd in elementsToAdd)
            {
                TryAddElement(elementToAdd);
            }
        }
        
        public virtual bool TryAddElement(TContainedElement elementToAdd)
        {
            var elementKey = elementToAdd.GetKey();
            if (m_internalElementStorage.ContainsKey(elementKey)) return false;
            SelectableUIContainer<TContainedElement> addedElement =
                Instantiate((SelectableUIContainer<TContainedElement>) m_uiElementClassPrefab, m_layoutContentRoot);
            addedElement.Setup(elementToAdd);
            addedElement.OnElementSelected += HandleElementSelected;
            addedElement.OnElementDeselected += HandleElementDeselected;
            m_internalElementStorage.Add(addedElement.Element.GetKey(), addedElement);
            return true;
        }

        public void SetElements(IEnumerable<TContainedElement> elementsToAdd)
        {
            foreach (TContainedElement elementToSet in elementsToAdd)
            {
                SetElement(elementToSet);
            }
        }

        public virtual bool SetElement(TContainedElement elementToSet)
        {
            if (m_internalElementStorage.TryGetValue(elementToSet.GetKey(),
                out SelectableUIContainer<TContainedElement> elementToUpdate))
            {
                elementToUpdate.Setup(elementToSet);
                return false;
            }

            return TryAddElement(elementToSet);
        }

        public virtual SelectableUIContainer<TContainedElement> GetElement(TContainedElement elementToGet)
        {
            if (m_internalElementStorage.TryGetValue(elementToGet.GetKey(),
                out SelectableUIContainer<TContainedElement> retrievedElement))
            {
                return retrievedElement;
            }

            return null;
        }
        
        public virtual bool RemoveElement(TContainedElement elementToRemove)
        {
            var elementKey = elementToRemove.GetKey();
            if (m_internalElementStorage.TryGetValue(elementKey,
                out SelectableUIContainer<TContainedElement> selectableUIContainer))
            {
                m_internalElementStorage.Remove(elementKey);
                selectableUIContainer.OnElementSelected -= HandleElementSelected;
                selectableUIContainer.OnElementDeselected -= HandleElementDeselected;
                selectableUIContainer.Teardown();
                Destroy(selectableUIContainer.gameObject);
                return true;
            }

            return false;
        }

        public virtual void Clear()
        {
            foreach (SelectableUIContainer<TContainedElement> selectableUiContainer in m_internalElementStorage.Values.ToList()) //ToList required here since the collection changes during iteration.
            {
                RemoveElement(selectableUiContainer.Element);
            }
        }

        protected override void OnDestroy()
        {
            Clear();
            base.OnDestroy();
        }

        IEnumerator<TContainedElement> IEnumerable<TContainedElement>.GetEnumerator()
        {
            return m_internalElementStorage.Values.Select(container => container.Element).GetEnumerator();
        }

        public IEnumerator GetEnumerator()
        {
            return m_internalElementStorage.Values.Select(container => container.Element).GetEnumerator();
        }
        
        private Dictionary<int, SelectableUIContainer<TContainedElement>> m_internalElementStorage = new Dictionary<int, SelectableUIContainer<TContainedElement>>();
    }
}