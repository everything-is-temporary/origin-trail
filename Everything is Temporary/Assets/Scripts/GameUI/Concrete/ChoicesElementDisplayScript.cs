﻿using System.Collections;
using System.Collections.Generic;
using Dialogue;
using UnityEngine;
using UnityEngine.EventSystems;
using GameUtility;
using GameUI.Abstractions;

namespace GameUI.Concrete
{
    using UI = UnityEngine.UI;

    /// <summary>
    /// Basic implementation of IChoicesElementDisplay that instantiates choices
    /// as children of a RectTransform, allowing the RectTransform to perform
    /// layouting.
    /// </summary>
    public class ChoicesElementDisplayScript : MonoBehaviour, IChoicesElementDisplay
    {
        [SerializeField]
        private RectTransform m_choicesArea = default;

        [SerializeField]
        [RequiresType(typeof(IChoiceDisplay))]
        private Component m_choiceDisplayPrefab = default;

        public event System.Action<int> OnChoice;

        public void Display(ChoicesElement element)
        {
            ClearChoices();
            DisplayNewChoices(element);
            SetupNavigationForButtons();

            m_earliestTimeForChoices = Time.time + m_choiceDelay;
        }

        private void ClearChoices()
        {
            foreach (var obj in m_currentChoices)
            {
                Destroy(obj);
            }

            m_currentChoices.Clear();
        }

        private void DisplayNewChoices(ChoicesElement element)
        {
            int idx = 0;

            foreach (var choice in element.Choices)
            {
                IChoiceDisplay choiceDisplay = CreateChoice(choice.Key, choice.Value);

                if (idx++ == 0)
                    choiceDisplay.SetSelected();
            }
        }

        private IChoiceDisplay CreateChoice(int choiceIdx, DialogueElement choiceData)
        {
            Component choiceComponent = Instantiate(m_choiceDisplayPrefab, m_choicesArea) as Component;
            m_currentChoices.Add(choiceComponent.gameObject);

            IChoiceDisplay choice = choiceComponent as IChoiceDisplay;
            choice.DisplayChoice(choiceData);
            choice.OnChosen += () => OnChoice?.Invoke(choiceIdx);

            return choice;
        }

        private void SetupNavigationForButtons()
        {
            for (int i = 0; i < m_currentChoices.Count; i++)
            {
                IChoiceDisplay choiceDisplay = m_currentChoices[i].GetComponent<IChoiceDisplay>();

                UI.Navigation navigation = new UI.Navigation();

                navigation.mode = UI.Navigation.Mode.Explicit;

                if (i > 0)
                {
                    navigation.selectOnUp = m_currentChoices[i - 1].GetComponent<IChoiceDisplay>().Selectable;
                }

                if (i < m_currentChoices.Count - 1)
                {
                    navigation.selectOnDown = m_currentChoices[i + 1].GetComponent<IChoiceDisplay>().Selectable;
                }

                choiceDisplay.Navigation = navigation;
            }
        }

        private bool HandleInteractKeyInput()
        {
            if (Time.time < m_earliestTimeForChoices)
            {
                return false;
            }

            // Iterate through choice displays.
            for (int i = 0; i < m_currentChoices.Count; i++)
            {
                IChoiceDisplay choiceDisplay = m_currentChoices[i].GetComponent<IChoiceDisplay>();

                // Find the selected choice display, and submit.
                if (choiceDisplay.Selectable.gameObject == EventSystem.current.currentSelectedGameObject)
                {
                    choiceDisplay.SubmitChoice();

                    return true;
                }
            }

            return false;
        }

        private void OnEnable()
        {
            GameInputManager.Instance.RegisterAsInputHandler(InputType.InteractKey, HandleInteractKeyInput);
        }

        private void OnDisable()
        {
            GameInputManager.Instance.UnregisterAsInputHandler(InputType.InteractKey, HandleInteractKeyInput);
        }

        private List<GameObject> m_currentChoices = new List<GameObject>();

        private float m_earliestTimeForChoices;
        private readonly float m_choiceDelay = 0.5f;
    }
}