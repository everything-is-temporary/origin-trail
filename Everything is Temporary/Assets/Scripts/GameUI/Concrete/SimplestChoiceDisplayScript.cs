﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUI.Concrete
{
    using UI = UnityEngine.UI;
    public class SimplestChoiceDisplayScript : MonoBehaviour, Abstractions.IChoiceDisplay
    {
        [SerializeField]
        private UI.Text m_choiceText = default;

        [SerializeField]
        private UI.Selectable m_selectable = default;

        public UI.Selectable Selectable => m_selectable;

        public event System.Action OnChosen;

        public UI.Navigation Navigation
        {
            get
            {
                return m_selectable.navigation;
            }
            set
            {
                m_selectable.navigation = value;
            }
        }

        public void DisplayChoice(Dialogue.DialogueElement element)
        {
            if (m_choiceText != null)
                m_choiceText.text = element.Text;
        }

        public void SetSelected()
        {
            m_selectable?.Select();
        }

        /// <summary>
        /// Raises the OnChosen event. Meant to be called from a UI button.
        /// </summary>
        public void SubmitChoice()
        {
            OnChosen?.Invoke();
        }
    }
}