﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dialogue;
using DG.Tweening;
using GameUtility;

namespace GameUI.Concrete
{
    using UI = UnityEngine.UI;
    public class DialogueElementDisplayScript : MonoBehaviour, Abstractions.IDialogueElementDisplay
    {
        [SerializeField]
        private UI.Text m_subjectText = default;

        [SerializeField]
        private UI.Text m_dialogueText = default;

#pragma warning disable CS0414 // Variable initialized but not used
        [SerializeField]
        private UI.Image m_dialogueIcon = default;
#pragma warning restore CS0414 // Variable initialized but not used

        [SerializeField]
        [Tooltip("An object that is enabled only when the player can continue to the" +
            " next piece of dialogue.")]
        private GameObject m_continueIndicator = default;

        public bool CanContinue
        {
            get => m_canContinue;
            set
            {
                m_canContinue = value;
                m_continueIndicator?.SetActive(!IsAnimating && CanContinue);
            }
        }

        public bool IsAnimating
        {
            get => m_isAnimating;
            set
            {
                m_isAnimating = value;
                m_continueIndicator?.SetActive(!IsAnimating && CanContinue);
            }
        }

        public void StartDisplay(DialogueElement element)
        {
            if (element == null)
            {
                SetText("???", "...");
            }
            else
            {
                List<string> names = element.Aliases.Count > 0 ? element.Aliases : element.Sources;
                if (element.DialogueType == DialogueType.Narrator)
                {
                    SetText(string.Empty, $"{element.Text}");
                    m_dialogueText.fontStyle = FontStyle.Italic;
                }
                else
                {
                    SetText($"{string.Join(", ", names)}", $"{element.Text}");
                    m_dialogueText.fontStyle = FontStyle.Normal;
                }
            }
        }

        public void CompleteDisplay()
        {
            m_dialogueText.DOComplete();
            m_dialogueText.DOKill();
            IsAnimating = false;
        }

        private void SetText(string subject, string dialogue)
        {
            if (m_dialogueText != null)
            {
                IsAnimating = true;
                m_subjectText.text = subject;
                m_dialogueText.DOComplete();
                m_dialogueText.DOKill();
                m_dialogueText.text = string.Empty;
                m_dialogueText.DOText(dialogue, dialogue.Length / (PlayerSettings.TextSpeed ?? 25f))
                    .SetEase(Ease.OutSine)
                    .OnComplete(CompleteDisplay);
            }
        }

        private bool m_canContinue = true;
        private bool m_isAnimating;
    }
}