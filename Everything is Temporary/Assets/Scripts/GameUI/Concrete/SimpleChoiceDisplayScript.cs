﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using Dialogue;

using GameUI.Abstractions;
namespace GameUI.Concrete
{
    using UI = UnityEngine.UI;
    public class SimpleChoiceDisplayScript : MonoBehaviour, IChoiceDisplay,
        IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private UI.Text m_choiceText = default;

#pragma warning disable CS0414 // Variable assigned but unused
        [SerializeField]
        private UI.Image m_choiceIcon = default;
#pragma warning restore CS0414 // Variable assigned but unused

        [SerializeField]
        [Tooltip("The color of this will change when the player hovers their mouse over the choice.")]
        private UI.Image m_overlayImage = default;

        [SerializeField]
        private Color m_hoverColor = default;

        [SerializeField]
        private Color m_normalColor = default;

        public event System.Action OnChosen;

        public UI.Selectable Selectable
        {
            get { throw new System.NotImplementedException(); }
        }

        public UI.Navigation Navigation
        {
            get { throw new System.NotImplementedException(); }
            set { throw new System.NotImplementedException(); }
        }

        public void DisplayChoice(DialogueElement choice)
        {
            if (m_choiceText != null)
            {
                // TODO: Do choices ever have sources and aliases?
                m_choiceText.text = choice.Text;
            }
        }

        public void SetSelected()
        {
            // Does nothing.
        }

        public void SubmitChoice()
        {
            OnChosen?.Invoke();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            SubmitChoice();
            eventData.Use();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            m_overlayImage.color = m_hoverColor;
            eventData.Use();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            m_overlayImage.color = m_normalColor;
            eventData.Use();
        }
    }
}