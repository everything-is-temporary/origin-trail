﻿using System;
using System.Collections.Generic;
using UnityEngine;

using GameInventory;
using GameUI.Abstractions;
using GameUtility;
using Characters;

namespace GameUI.Concrete
{
    public class SimpleInventoryDisplayScript : MonoBehaviour, IInventoryDisplay
    {
        public event Action<ItemWithAmount> OnItemDisplaySelected;

        public Inventory DisplayedInventory
        {
            get
            {
                if (m_displayedInventory.TryGetTarget(out Inventory inventory))
                    return inventory;
                return null;
            }

            set
            {
                if (m_displayedInventory.TryGetTarget(out Inventory oldInventory))
                {
                    oldInventory.OnItemAmountChanged -= ItemAmountChanged;
                }

                m_displayedInventory = new WeakReference<Inventory>(value);

                if (value != null)
                {
                    value.OnItemAmountChanged += ItemAmountChanged;
                }

                UpdateDisplay(value);
            }
        }

        public void HighlightItem(ItemReference item)
        {
//             // TODO: We should ideally also scroll to this item.
//             if (m_mapItemToDisplay.TryGetValue(item, out DisplayObject display))
//             {
//                 display.Data.Highlighted = true;
//             }
         }

        public void UnhighlightItem(ItemReference item)
        {
//             if (m_mapItemToDisplay.TryGetValue(item, out DisplayObject display))
//             {
//                 display.Data.Highlighted = false;
//             }
        }

        private void UpdateDisplay(Inventory inventory)
        {
            Clear();

            if (inventory == null)
                return;

            foreach (ItemWithAmount itemWithAmount in inventory)
            {
                GridInventoryDisplayItem display = InstantiateDisplay();

                ItemWithAmountAndCharRef itemAndCharRef = new ItemWithAmountAndCharRef(null, itemWithAmount);
                display.Setup(itemAndCharRef);

                m_mapItemToDisplay.Add(itemWithAmount.ItemReference, display);
            }

        }

        private void Clear()
        {
            foreach (var obj in m_mapItemToDisplay.Values)
                Destroy(obj.gameObject);

            m_mapItemToDisplay.Clear();
        }

        private void ItemAmountChanged(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            GridInventoryDisplayItem display;
            if (newAmount.Amount > 0)
            {
                if (!m_mapItemToDisplay.TryGetValue(newAmount, out display))
                {
                    display = InstantiateDisplay();
                    m_mapItemToDisplay.Add(newAmount, display);
                }

                ItemWithAmountAndCharRef itemAndCharRef = new ItemWithAmountAndCharRef(null, newAmount);
                display.Setup(itemAndCharRef);
            }
            else
            {
                display = m_mapItemToDisplay[newAmount];

                Destroy(display.gameObject);

                m_mapItemToDisplay.Remove(newAmount);
            }
        }

        private GridInventoryDisplayItem InstantiateDisplay()
        {
            GridInventoryDisplayItem display = Instantiate(m_itemDisplayPrefab, m_itemDisplayArea) as GridInventoryDisplayItem;

            display.OnElementSelected += (_) => OnItemDisplaySelected?.Invoke(display.Element.ItemWithAmount);

            return display;
        }

        [SerializeField]
        private RectTransform m_itemDisplayArea = default;

        [SerializeField]
        [RequiresType(typeof(GridInventoryDisplayItem))]
        private UnityEngine.Object m_itemDisplayPrefab = default;

        private Dictionary<ItemReference, GridInventoryDisplayItem> m_mapItemToDisplay = new Dictionary<ItemReference, GridInventoryDisplayItem>();

        private WeakReference<Inventory> m_displayedInventory = new WeakReference<Inventory>(null);
    }
}
