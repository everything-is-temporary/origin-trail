﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dialogue;
using UnityEngine;

using GameUtility;
using GameInventory;
using GameUI.DataPersistence;

namespace GameUI
{
    public class GameUIHelper : SingletonMonoBehavior<GameUIHelper>
    {
        [SerializeField]
        private GenericButtonPopupScript m_GenericPopup = default;

        [SerializeField]
        private DialogueOverlayScript m_dialogueOverlay = default;

        [SerializeField]
        [RequiresType(typeof(ITradeDisplay))]
        private MonoBehaviour m_tradeDisplayPrefab = default;

        [SerializeField]
        private PartyTradeDisplayScript m_partyTradeDisplayPrefab = default;

        [SerializeField]
        private EscapeMenuScript m_escapeMenuPrefab = default;

        [SerializeField]
        private List<ItemReference> m_testInventory = default;

        [SerializeField]
        private List<ItemReference> m_testShop = default;

        [SerializeField]
        private SaveLoadUIModal m_SaveLoadUIModalPrefab = default;

        public void OpenSaveLoadModal()
        {
            UIOverlayManager.Instance.InstantiateOverlayOnTop<SaveLoadUIModal>(m_SaveLoadUIModalPrefab);
        }

        public async Task<bool> DisplayConfirmationAsync(string prompt, string okString = "Ok", string cancelString = "Cancel")
        {
            // Make the confirmation popup go on top of any other screen overlays
            // created by the GameUIHelper.
            m_GenericPopup.transform.SetAsLastSibling();
            return await m_GenericPopup.DisplayOkCancel(prompt, okString, cancelString);
        }

        public async Task<bool> DisplayPopupInfoAsync(string prompt, string okString = "Ok")
        {
            m_GenericPopup.transform.SetAsLastSibling();
            return await m_GenericPopup.DisplayOK(prompt, okString);
        }

        /// <summary>
        /// Displays a trade menu. This will return the user's choices, but will
        /// not actually perform the trade.
        /// </summary>
        /// <param name="playerInventory">Player inventory.</param>
        /// <param name="shop">Shop.</param>
        public async Task<TradeResult> DisplayTradeMenuAsync(ConvoyAndPartyInventory playerInventory, Shop shop)
        {
            TradeDisplay.GameObject.transform.SetAsLastSibling();
            var result = await TradeDisplay.Data.DisplayTrade(playerInventory, shop);

            return result;
        }

        public async Task<TradeResult> DisplayPartyTradeMenuAsync()
        {
            PartyTradeDisplay.GameObject.transform.SetAsLastSibling();
            var result = await PartyTradeDisplay.Data.InitializePartyTradeDisplay();

            return result;
        }

        /// <summary>
        /// Opens the dialogue overlay and begins a dialogue by running <see cref="Dialogue.StoryManager.Proceed(int?)"/>.
        /// This will return only when the dialogue is complete, or it will return
        /// immediately if there is already a dialogue going on.
        /// </summary>
        /// <returns>A task that completes when the dialogue is finished.</returns>
        public async Task DisplayDialogueAsync()
        {
            if (m_dialogueTask != null)
                return;

            m_dialogueTask = new TaskCompletionSource<object>();

            // Begin listening to story events.
            Dialogue.DialogueElement.OnDialogueElement += DialogueElementHandler;
            Dialogue.ChoicesElement.OnChoicesElement += ChoicesElementHandler;
            Dialogue.StoryManager.Instance.OnEndReached += DialogueFinishedHandler;

            // Begin the story.
            Dialogue.StoryManager.Instance.Proceed();

            // Wait for dialogue to complete now. The task may become null if
            // the Proceed() call immediately reached an end marker.
            if (m_dialogueTask != null)
                await m_dialogueTask.Task;

            return;
        }

        [ContextMenu("Test Confirmation")]
        public void TestConfirmation()
        {
            _ = TestConfirmationAsync();
        }

        [ContextMenu("Test Trading")]
        public void TestTrading()
        {
            TestTradeAsync();
        }

        private async Task TestConfirmationAsync()
        {
            bool result = await DisplayConfirmationAsync("This is a test. Or is it?", "I agree.", "I disagree.");
            Debug.Log($"Got result: {result}");
        }

        private async void TestTradeAsync()
        {
            if (m_testInventoryObj == null)
            {
                m_testInventoryObj = new Inventory();
                foreach (var item in m_testInventory)
                    m_testInventoryObj.SetItem(item, 1);
            }

            if (m_testShopObj == null)
            {
                m_testShopObj = new Shop();
                foreach (var item in m_testShop)
                    m_testShopObj.Inventory.SetItem(item, 1);
            }
            
            // I modified this function to use the convoy of the ConvoyAndPartyInventory. Technically that isn't the original functionality of this method.
            var result = await TradeManager.PerformTradeWithShopAsync(new ConvoyAndPartyInventory(m_testInventoryObj), m_testShopObj);

            Debug.Log(result.ToString());
        }


        private GameObjectWith<ITradeDisplay> TradeDisplay
        {
            get
            {
                if (m_tradeDisplay == null)
                    m_tradeDisplay = GameObjectWith.FromComponentPrefab<ITradeDisplay>(m_tradeDisplayPrefab, transform);

                return m_tradeDisplay;
            }
        }

        private GameObjectWith<PartyTradeDisplayScript> PartyTradeDisplay
        {
            get
            {
                if (m_partyTradeDisplay == null)
                    m_partyTradeDisplay = GameObjectWith.FromComponentPrefab<PartyTradeDisplayScript>(m_partyTradeDisplayPrefab, transform);

                return m_partyTradeDisplay;
            }
        }

        /// <summary>
        /// This is triggered when <see cref="m_dialogueOverlay"/> raises its
        /// <see cref="DialogueOverlayScript.OnDialogueFinished"/> event or when
        /// StoryManager's Proceed() is called when the StoryManager is already
        /// at an end marker.
        /// </summary>
        private void DialogueFinishedHandler()
        {
            // Disable dialogue overlay when it reaches the end of a dialogue.
            m_dialogueOverlay.gameObject.SetActive(false);

            // Stop listening to story events.
            Dialogue.DialogueElement.OnDialogueElement -= DialogueElementHandler;
            Dialogue.ChoicesElement.OnChoicesElement -= ChoicesElementHandler;
            Dialogue.StoryManager.Instance.OnEndReached -= DialogueFinishedHandler;

            // Allow async dialogue task to finish now.
            m_dialogueTask?.SetResult(null);
            m_dialogueTask = null;
        }

        private void DialogueElementHandler(Dialogue.DialogueElement element)
        {
            m_dialogueOverlay.gameObject.SetActive(true);
            m_dialogueOverlay.ShowDialogue(element);
        }

        private void ChoicesElementHandler(Dialogue.ChoicesElement element)
        {
            m_dialogueOverlay.gameObject.SetActive(true);
            m_dialogueOverlay.ShowChoices(element);
        }

        protected override void PostAwake()
        {
            m_uiOverlayManager = GetComponent<UIOverlayManager>();
            m_dialogueOverlay.gameObject.SetActive(false);
        }

        private void Start()
        {
            GameInputManager.Instance.RegisterAsInputHandler(InputType.EscapeKey, HandleEscapeKeyInput);
            StoryManager.Instance.OnEndReached += DialogueFinishedHandler;

            StoryManager.Instance.OnBlockedChanged += HandleStoryBlockedChanged;
        }

        /// <summary>
        /// This isn't unregistered anywhere yet.
        /// So this will always handle Escape Key input,
        /// If nothing else handles it.
        /// </summary>
        /// <returns>Is the input handled?</returns>
        private bool HandleEscapeKeyInput()
        {
            if (m_escapeMenu)
            {
                Destroy(m_escapeMenu.gameObject);
            }
            else
            {
                m_escapeMenu = UIOverlayManager.Instance.InstantiateOverlayOnTop<EscapeMenuScript>(m_escapeMenuPrefab);
            }

            return true;
        }

        private void HandleStoryBlockedChanged(bool blocked)
        {
            m_dialogueOverlay.CanContinueDialogue = !blocked;
        }

        private UIOverlayManager m_uiOverlayManager;

        private EscapeMenuScript m_escapeMenu;

        private Inventory m_testInventoryObj;
        private Shop m_testShopObj;

        private GameObjectWith<ITradeDisplay> m_tradeDisplay;
        private GameObjectWith<PartyTradeDisplayScript> m_partyTradeDisplay;
        private TaskCompletionSource<object> m_dialogueTask = null;
    }
}