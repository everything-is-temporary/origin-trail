﻿using System.Threading.Tasks;
using UnityEngine;
using GameUtility;

namespace GameUI
{
    /// <summary>
    /// Component that can be placed on a GameObject with a Camera to
    /// fade the camera in and out when a game scene is loaded and unloaded.
    /// </summary>
    public class FadeCameraScript : MonoBehaviour
    {
        [SerializeField]
        private bool m_fadeInOnStart = default;

        [SerializeField]
        private bool m_fadeOutOnGameSceneUnloaded = default;

        [SerializeField]
        [Tooltip("Fade duration in seconds.")]
        private float m_fadeDuration = 1;

        // async void methods are safe to begin from synchronous code (they will
        // report exceptions properly)
        private async Task BeginFadeIn()
        {
            await m_fadeHelper.FadeAsync(1, 0, Color.black, m_fadeDuration);
        }

        private async Task FadeOutAsync()
        {
            await m_fadeHelper.FadeAsync(m_fadeHelper.FadeValue, 1, Color.black, m_fadeDuration);
        }

        private void Awake()
        {
            m_fadeHelper = CameraFadeHelper.GetFadeScript(gameObject);
        }

        private void Start()
        {
            if (m_fadeOutOnGameSceneUnloaded)
                CoreGameManager.Instance.UnloadGameSceneActions.Add(FadeOutAsync);

            if (m_fadeInOnStart)
                CoreGameManager.Instance.RunOnceWhenSceneLoads(() => BeginFadeIn().FireAndForget());
        }

        private void OnDestroy()
        {
            if (m_fadeOutOnGameSceneUnloaded)
                // The instance may be destroyed before this script, hence the ?
                CoreGameManager.Instance?.UnloadGameSceneActions.Remove(FadeOutAsync);
        }

        private CameraFadeHelper m_fadeHelper;
    }
}