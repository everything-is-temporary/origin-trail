﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using GameUI.Abstractions;
using GameUtility;

namespace GameUI
{
    public class DialogueOverlayScript : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_dialogueContinueButton = default;

        [SerializeField]
        [RequiresType(typeof(IDialogueElementDisplay))]
        private Component m_currentDialogueDisplay = default;

        [SerializeField]
        [RequiresType(typeof(IChoicesElementDisplay))]
        private Component m_currentChoicesDisplay = default;

        public bool CanContinueDialogue
        {
            get => DialogueElementDisplay.CanContinue;
            set => DialogueElementDisplay.CanContinue = value;
        }

        /// <summary>
        /// Displays the next dialogue element.
        /// </summary>
        /// <param name="element">Element.</param>
        public void ShowDialogue(Dialogue.DialogueElement element)
        {
            DialogueElementDisplay.StartDisplay(element);
        }

        /// <summary>
        /// Displays the given choices element.
        /// </summary>
        /// <param name="choices">Choices.</param>
        public void ShowChoices(Dialogue.ChoicesElement choices)
        {
            m_dialogueContinueButton.SetActive(false);
            ChoicesElementObject.SetActive(true);
            ChoicesElementDisplay.Display(choices);
        }

        /// <summary>
        /// Continues the dialogue. This is meant to be invoked from a UI
        /// element belonging to this dialogue overlay.
        /// </summary>
        public void ButtonContinueDialogue()
        {
            if (DialogueElementDisplay != null && DialogueElementDisplay.IsAnimating)
            {
                DialogueElementDisplay.CompleteDisplay();
                return;
            }
            Dialogue.StoryManager.Instance.Proceed();
        }

        private void SelectChoice(int choice)
        {
            // This should be done before the Proceed() call, as the Proceed()
            // call can result in another set of choices.
            ChoicesElementObject.SetActive(false);
            m_dialogueContinueButton.SetActive(true);

            Dialogue.StoryManager.Instance.Proceed(choice);
        }

        // Function ButtonContinueDialogue needs to be void,
        // So we have to have this Handle function to wrap it.
        private bool HandleInteractKeyInput()
        {
            // Let the choice container handle the input to select choices.
            if (ChoicesElementObject.activeSelf == true)
            {
                return false;
            }

            ButtonContinueDialogue();

            return true;
        }

        private void OnEnable()
        {
            InputManager.RegisterAsInputHandler(InputType.InteractKey, HandleInteractKeyInput);
        }

        private void Awake()
        {
            ChoicesElementObject.SetActive(false);
            ChoicesElementDisplay.OnChoice += SelectChoice;
        }

        private void Start()
        {
            RegisterFinishDialogueOnUnload();
        }

        private void RegisterFinishDialogueOnUnload()
        {
            CoreGameManager.Instance.UnloadGameSceneActions.Add(() =>
            {
                return Task.CompletedTask;
            });
        }

        private void OnDisable()
        {
            InputManager?.UnregisterAsInputHandler(InputType.InteractKey, HandleInteractKeyInput);
        }

        private void OnDestroy()
        {
            ChoicesElementDisplay.OnChoice -= SelectChoice;
        }

        private IDialogueElementDisplay DialogueElementDisplay => m_currentDialogueDisplay as IDialogueElementDisplay;
        private IChoicesElementDisplay ChoicesElementDisplay => m_currentChoicesDisplay as IChoicesElementDisplay;
        private GameObject ChoicesElementObject => m_currentChoicesDisplay.gameObject;

        private GameInputManager InputManager => GameInputManager.Instance;
    }
}