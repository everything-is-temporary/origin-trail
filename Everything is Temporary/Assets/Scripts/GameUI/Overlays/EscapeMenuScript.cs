﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeMenuScript : MonoBehaviour
{
    public void ResumeGame()
    {
        Destroy(gameObject);
    }
}
