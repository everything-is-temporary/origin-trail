﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameInventory;

namespace GameUI
{
    using UI = UnityEngine.UI;

    public class ItemDetailDisplayScript : MonoBehaviour
    {
        [SerializeField] private UI.Image m_icon = default;
        [SerializeField] private UI.Text m_amount = default;
        [SerializeField] private UI.Text m_name = default;
        [SerializeField] private UI.Text m_flavorText = default;

        // These fields are not removed to avoid breaking references.
#pragma warning disable CS0414 // Variable initialized but not used
        [SerializeField] private UI.Image m_attackStatIcon = default;
        [SerializeField] private UI.Text m_attackStatText = default;
        [SerializeField] private UI.Image m_defenseStatIcon = default;
        [SerializeField] private UI.Text m_defenseStatText = default;
        [SerializeField] private UI.Image m_luckStatIcon = default;
        [SerializeField] private UI.Text m_luckStatText = default;
        [SerializeField] private UI.Image m_speedStatIcon = default;
        [SerializeField] private UI.Text m_speedStatText = default;
#pragma warning restore CS0414 // Variable initialized but not used

        public void UpdateItemDetail(ItemWithAmount itemWithAmount)
        {
            // Setup.
            m_displayedItemWithAmount = itemWithAmount;
            InventoryItem item = itemWithAmount.Item;

            // Draw item info.
            if (m_icon != null) m_icon.sprite = item.Icon;
            SetAmountText(itemWithAmount.Amount);
            if (m_name != null) m_name.text = item.Name;
            if (m_flavorText != null) m_flavorText.text = item.FlavorText;

            // Draw stats.
            if (m_attackStatText != null) m_attackStatText.text = $"{item.Stats.Get(StatEnum.Attack)}";
            if (m_defenseStatText != null) m_defenseStatText.text = $"{item.Stats.Get(StatEnum.Defense)}";
            //if (m_luckStatText != null) m_luckStatText.text = $"{item.Stats.Get(StatEnum.luck)}";
            //if (m_speedStatText != null) m_speedStatText.text = $"{item.Stats.Get(StatEnum.speed)}";
        }

        // TODO: Inventory UI: Implement.
        public void UseItem()
        {

        }

        public void DestroyItem()
        {
            InventoryManager.Instance.RemoveInventoryItem(m_displayedItemWithAmount);
        }
        
        /// <summary>
        /// Currently it's used by CloseButton.
        /// </summary>
        public void DestroyMyself()
        {
            Destroy(gameObject);
        }

        private void SetAmountText(int amount)
        {
            if (m_amount != null) m_amount.text = $"{amount}";
        }

        private ItemWithAmount m_displayedItemWithAmount;
    }
}
