﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility.DataPersistence;
using UnityEngine.UI;

namespace GameUI.DataPersistence
{
    public class SaveLoadUISaveFileSelection : MonoBehaviour
    {
        // FIXME: Using a WeakReference here is unnecessary.
        private WeakReference<SaveLoadUIModal> parentModalRef;
        public GameSaveMetadata SaveMetadata { get; protected set; }

        [SerializeField]
        private Text m_dateText = default;

        public void SetUpElement(SaveLoadUIModal controllingParent, GameSaveMetadata gameSaveMetadata)
        {
            parentModalRef = new WeakReference<SaveLoadUIModal>(controllingParent);
            SaveMetadata = gameSaveMetadata;
            m_dateText.text = gameSaveMetadata.CreationDate.ToString("f"); //Time format: universal, short
        }

        public void TryDelete()
        {
            if (parentModalRef.TryGetTarget(out SaveLoadUIModal saveLoadUIModal))
            {
                saveLoadUIModal.DeleteSaveFileSelection(this);
            }
        }

        public void TrySelect()
        {
            if (parentModalRef.TryGetTarget(out SaveLoadUIModal saveLoadUIModal))
            {
                saveLoadUIModal.SetActiveSavefileSelection(this);
            }
        }

        public void VisuallySelect()
        {
            if (this != null)
            {
                gameObject.GetComponent<Image>().color = Color.green;
            }
        }

        public void VisuallyDeselect()
        {
            if (this != null)
            {
                gameObject.GetComponent<Image>().color = Color.white;
            }
        }
    }
}