﻿using System.Collections;
using System.Collections.Generic;
using GameUtility.DataPersistence;
using UnityEngine;
using UnityEngine.UI;

namespace GameUI.DataPersistence
{
    public class SaveLoadUIModal : MonoBehaviour
    {
        [SerializeField]
        private SaveLoadUISaveFileSelection m_saveSelectionPrefab = default;

        [SerializeField]
        private HorizontalOrVerticalLayoutGroup m_saveSelectionContainer = default;

        private SaveLoadUISaveFileSelection m_activeSelection;

        private List<SaveLoadUISaveFileSelection> m_allSavefileSelections = new List<SaveLoadUISaveFileSelection>();

        private void Start()
        {
            this.SetUpSaveSelections();
            this.gameObject.SetActive(true);
        }

        public void SetActiveSavefileSelection(SaveLoadUISaveFileSelection selection)
        {
            m_activeSelection?.VisuallyDeselect();
            m_activeSelection = selection;
            selection.VisuallySelect();
        }

        public void DeleteSaveFileSelection(SaveLoadUISaveFileSelection selection)
        {
            if(SaveManager.Instance.DeleteGameSave(selection.SaveMetadata))
            {
                ClearSelectionItem(selection);
            }
        }

        public void TryLoadSelectedSave()
        {
            if (m_activeSelection == null)
            {
                ShowNoElementSelected();
                return;
            }
            if(!SaveManager.Instance.LoadGame(m_activeSelection.SaveMetadata))
            {
                ShowFailedLoad();
            }
        }

        public void TryCreateNewSave()
        {
            GameSaveMetadata gameSaveMetadata = SaveManager.Instance.SaveGame();
            if(gameSaveMetadata == null) // Currently it will never be null, but just in case, we should indicate a possible failed save
            {
                ShowFailedSave();
                return;
            }
            AddSaveSelection(gameSaveMetadata);
        }

        public void TryExitModal()
        {
            gameObject.SetActive(false);
            //FIXME: Temporary measure, normally let parent handle this.
            Destroy(gameObject);
        }

        private void SetUpSaveSelections()
        {
            foreach(GameSaveMetadata gameSaveMetadata in SaveManager.Instance.GetGameSaveMetadata())
            {
                AddSaveSelection(gameSaveMetadata);
            }
        }

        private void AddSaveSelection(GameSaveMetadata gameSaveMetadata)
        {
            SaveLoadUISaveFileSelection selection = Instantiate(m_saveSelectionPrefab, m_saveSelectionContainer.transform);
            selection.transform.SetAsFirstSibling();
            selection.SetUpElement(this, gameSaveMetadata);
        }

        private void ClearAllSelectionItems()
        {
            for (int i = m_allSavefileSelections.Count; i >= 0; --i)
            {
                ClearSelectionItem(m_allSavefileSelections[i]);
            }
        }

        private void ClearSelectionItem(SaveLoadUISaveFileSelection selection)
        {
            Destroy(selection.gameObject);
            m_allSavefileSelections.Remove(selection);
        }

        private void ShowFailedSave()
        {
            //TODO: Implement with popups?
        }

        private void ShowFailedLoad()
        {
            //TODO: Implement with popups?
        }

        private void ShowNoElementSelected()
        {
            //TODO: Implement with tweens?
        }



    }
}
