﻿using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GameUI
{
    using UI = UnityEngine.UI;
    public class GenericButtonPopupScript : MonoBehaviour
    {
        [SerializeField]
        protected UI.Text m_promptText = default;

        [SerializeField]
        private UI.Button m_selectableButtonPrefab = default;

        [SerializeField]
        private UI.HorizontalOrVerticalLayoutGroup m_buttonContainingLayoutGroup = default;

        public async Task<bool> DisplayOkCancel(string promptText, string okString = "OK", string cancelString = "Cancel")
        {
            return await DisplayPopup(promptText,
                                     new Dictionary<string, bool>{
                                        {cancelString, false},
                                        {okString, true}
                                     }
            );
        }

        public async Task<bool> DisplayOK(string promptText, string okString = "OK")
        {
            return await DisplayPopup(promptText,
                                     new Dictionary<string, bool>{
                                        {okString, true}
                                     }
            );
        }

        public async Task<ButtonAsyncReturnValue> DisplayPopup<ButtonAsyncReturnValue>(string promptText, Dictionary<string, ButtonAsyncReturnValue> options)
        {
            this.m_promptText.text = promptText;

            gameObject.SetActive(true);

            ButtonAsyncReturnValue returnValue = default;

            SemaphoreSlim responseSema = new SemaphoreSlim(0);

            foreach(KeyValuePair<string, ButtonAsyncReturnValue> buttonOptions in options)
            {
                CreateButton(buttonOptions.Key).onClick.AddListener(() => { responseSema.Release(); returnValue = buttonOptions.Value; });
            }

            await responseSema.WaitAsync();

            gameObject.SetActive(false);

            foreach (Transform child in m_buttonContainingLayoutGroup.transform)
            {
                Destroy(child.gameObject);
            }

            return returnValue;
        }

        protected UI.Button CreateButton(string buttonText)
        {
            UI.Button button = Instantiate(m_selectableButtonPrefab, m_buttonContainingLayoutGroup.transform);
            button.gameObject.GetComponentInChildren<UI.Text>().text = buttonText;
            button.gameObject.SetActive(true);
            return button;
        }
    }
}