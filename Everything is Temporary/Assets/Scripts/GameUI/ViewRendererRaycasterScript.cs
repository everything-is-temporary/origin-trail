﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using GameUI.Raycasting;

namespace GameUI
{
    /// <summary>
    /// Raycaster associated with a <see cref="ViewRendererScript"/>. This forwards
    /// raycasts to the <see cref="ViewRendererScript"/>'s interactive view.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    [RequireComponent(typeof(ViewRendererScript))]
    public class ViewRendererRaycasterScript : BaseRaycaster
    {
        public override Camera eventCamera => m_camera;

        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            Vector2 oldPosition = eventData.position;

            // Convert to viewport coordinates because that is the requirement for IRaycaster.
            eventData.position = eventCamera.ScreenToViewportPoint(oldPosition);
            m_raycaster.Raycast(eventData, resultAppendList);
            eventData.position = oldPosition;
        }

        protected override void Awake()
        {
            m_camera = GetComponent<Camera>();
            Debug.Assert(m_camera != null);
        }

        protected override void Start()
        {
            m_raycaster = GetComponent<ViewRendererScript>().InteractiveView.GetRaycaster();
        }

        private Camera m_camera;
        private IRaycaster m_raycaster;
    }
}