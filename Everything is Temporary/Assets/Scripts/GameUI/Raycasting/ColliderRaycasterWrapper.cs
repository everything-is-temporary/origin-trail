﻿using GameUtility;
using UnityEngine;

namespace GameUI.Raycasting
{
    /// <summary>
    /// Registers an IRaycaster to receive Raycast() messages from a <see cref="SceneRaycaster"/>
    /// when a particular collider is hit by a raycast. The coordinates for the Raycast() come
    /// from the texture coordinates of the hit. Unregisters when script is destroyed.
    /// </summary>
    public class ColliderRaycasterWrapper : MonoBehaviour
    {
        private void Awake()
        {
            Debug.Assert(DelegateRaycaster != null);
            Debug.Assert(m_sceneRaycaster != null);
            Debug.Assert(m_collider != null);

            m_sceneRaycaster.RegisterColliderRaycastReceiver(m_collider, DelegateRaycaster);
        }

        private void OnDestroy()
        {
            m_sceneRaycaster.RemoveColliderRaycastReceiver(m_collider);
            m_sceneRaycaster = null;
            m_collider = null;
        }

        private IRaycaster DelegateRaycaster => (IRaycaster)m_delegateRaycaster;

        [SerializeField]
        private SceneRaycaster m_sceneRaycaster = default;

        [SerializeField]
        private Collider m_collider = default;

        [SerializeField]
        [RequiresType(typeof(IRaycaster))]
        private Object m_delegateRaycaster = default;
    }
}