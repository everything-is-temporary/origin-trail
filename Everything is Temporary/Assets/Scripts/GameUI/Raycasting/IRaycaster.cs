﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI.Raycasting
{
    /// <summary>
    /// Represents something that can raycast. The Raycast method accepts a
    /// PointerEventData to make this easy to use with Unity's BaseRaycaster.
    /// </summary>
    public interface IRaycaster
    {
        /// <summary>
        /// Raycasts with the specified eventData and fills resultAppendList.
        /// </summary>
        /// <param name="eventData">The pointer data. Should usually be in viewport coordinates,
        /// that is [0,1] x [0,1]. Note that Unity implementations of <see cref="BaseRaycaster"/>
        /// tend to use screen coordinates rather than normalized coordinates.</param>
        /// <param name="resultAppendList">The result append list.</param>
        void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList);
    }
}
