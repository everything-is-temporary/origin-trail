﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace GameUI.Raycasting
{
    /// <summary>
    /// An IRaycaster that forwards messages to other IRaycasters in one of two
    /// ways. The first way is to forward with unmodified coordinates, and the
    /// second way is to forward only when a certain collider is hit and to use
    /// the hit's texture coordinates.
    /// </summary>
    public class SceneRaycaster : MonoBehaviour, IRaycaster
    {
        /// <summary>
        /// Adds a raycaster to delegate to when the given collider is hit
        /// by a physics raycast during this object's <see cref="Raycast(PointerEventData, List{RaycastResult})"/>.
        /// The delegate raycaster will receive the texture coordinates of the hit.
        /// </summary>
        public void RegisterColliderRaycastReceiver(Collider collider, IRaycaster raycaster)
        {
            // This will throw if the collider already has an associated raycaster.
            // If you have multiple raycasters associated with one collider, you
            // might be doing something wrong (if not, it's possible to allow
            // a collider to have multiple associated raycasters).
            m_colliderReceivers.Add(collider, raycaster);
        }

        /// <summary>
        /// Adds a raycaster to invoke on every Raycast() invocation.
        /// </summary>
        /// <param name="raycaster">Raycaster.</param>
        public void RegisterPlainRaycastReceiver(IRaycaster raycaster)
        {
            m_plainReceivers.Add(raycaster);
        }

        public void RemoveColliderRaycastReceiver(Collider collider)
        {
            m_colliderReceivers.Remove(collider);
        }

        public void RemovePlainRaycastReceiver(IRaycaster raycaster)
        {
            m_plainReceivers.Remove(raycaster);
        }

        public void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            // TODO: Measure how frequently this method gets invoked.

            foreach (var raycastReceiver in m_plainReceivers)
                raycastReceiver.Raycast(eventData, resultAppendList);

            if (m_colliderReceivers.Count <= 0)
                return;

            (Collider hitCollider, Vector2 texCoords) = PhysicsRaycast(eventData.position);

            if (hitCollider == null)
                return;

            if (m_colliderReceivers.TryGetValue(hitCollider, out IRaycaster raycaster))
            {
                var oldPosition = eventData.position;
                eventData.position = texCoords;
                raycaster.Raycast(eventData, resultAppendList);
                eventData.position = oldPosition;
            }
        }

        /// <summary>
        /// Performs a physics raycast toward the given screen position (with
        /// respect to <see cref="m_eventCamera"/>). Ensures that a raycast
        /// only happens at most once per screen position per frame.
        /// </summary>
        /// <returns>The result of the raycast.</returns>
        /// <param name="viewportPosition">Screen position specified in viewport coordinates.</param>
        private (Collider, Vector2) PhysicsRaycast(Vector2 viewportPosition)
        {
            // Ensure we only raycast at most once per frame unless the screen
            // position changes.
            if (m_lastScreenPosition != viewportPosition || m_lastRaycastTick != Time.frameCount)
            {
                Ray ray = m_eventCamera.ViewportPointToRay(viewportPosition);
                int layerMask = m_useLayerMask ? m_layerMask.value : Physics.AllLayers;

                if (m_usePhysicsScene)
                {
                    if (gameObject.scene.GetPhysicsScene().Raycast(ray.origin, ray.direction,
                        out RaycastHit hit, layerMask: layerMask))
                    {
                        m_lastRaycastCollider = hit.collider;
                        m_lastRaycastTexCoords = hit.textureCoord;
                    }
                    else
                    {
                        m_lastRaycastCollider = null;
                    }
                }
                else
                {
                    if (Physics.Raycast(ray, out RaycastHit hit, float.PositiveInfinity, layerMask))
                    {
                        m_lastRaycastCollider = hit.collider;
                        m_lastRaycastTexCoords = hit.textureCoord;
                    }
                    else
                    {
                        m_lastRaycastCollider = null;
                    }
                }

                m_lastScreenPosition = viewportPosition;
                m_lastRaycastTick = Time.frameCount;
            }

            return (m_lastRaycastCollider, m_lastRaycastTexCoords);
        }

        private int m_lastRaycastTick;
        private Vector2 m_lastScreenPosition;
        private Collider m_lastRaycastCollider;
        private Vector2 m_lastRaycastTexCoords;

        private Dictionary<Collider, IRaycaster> m_colliderReceivers = new Dictionary<Collider, IRaycaster>();
        private List<IRaycaster> m_plainReceivers = new List<IRaycaster>();

        [SerializeField]
        private Camera m_eventCamera = default;

        [SerializeField]
        private bool m_usePhysicsScene = default;

        [SerializeField]
        private bool m_useLayerMask = default;

        [SerializeField]
        private LayerMask m_layerMask = default;
    }
}