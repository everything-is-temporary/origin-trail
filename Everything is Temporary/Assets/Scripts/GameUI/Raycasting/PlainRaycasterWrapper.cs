﻿using GameUtility;
using UnityEngine;

namespace GameUI.Raycasting
{
    /// <summary>
    /// Registers an IRaycaster to receive Raycast() messages from a <see cref="SceneRaycaster"/>
    /// with unmodified coordinates. Unregisters when script is destroyed.
    /// </summary>
    public class PlainRaycasterWrapper : MonoBehaviour
    {
        private void Awake()
        {
            Debug.Assert(DelegateRaycaster != null);
            Debug.Assert(m_sceneRaycaster != null);

            m_sceneRaycaster.RegisterPlainRaycastReceiver(DelegateRaycaster);
        }

        private void OnDestroy()
        {
            m_sceneRaycaster.RemovePlainRaycastReceiver(DelegateRaycaster);
            m_sceneRaycaster = null;
        }

        private IRaycaster DelegateRaycaster => (IRaycaster)m_delegateRaycaster;

        [SerializeField]
        private SceneRaycaster m_sceneRaycaster = default;

        [SerializeField]
        [RequiresType(typeof(IRaycaster))]
        private Object m_delegateRaycaster = default;
    }
}
