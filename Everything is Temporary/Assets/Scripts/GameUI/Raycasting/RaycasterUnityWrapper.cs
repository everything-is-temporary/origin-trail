﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI.Raycasting
{
    /// <summary>
    /// A lightweight <see cref="IRaycaster"/> that delegates to a Unity raycaster.
    /// Meant to be created in code, as opposed to <see cref="RaycasterUnityWrapperScript"/>.
    /// </summary>
    public class RaycasterUnityWrapper : IRaycaster
    {
        public bool Enabled
        {
            get => m_raycaster.enabled;
            set => m_raycaster.enabled = value;
        }

        public void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            Vector2 oldPosition = eventData.position;

            // Transform coordinates from viewport to screen coordinates, since that
            // is expected by Unity's BaseRaycaster implementations.
            if (m_raycaster.eventCamera == null)
                eventData.position = Vector2.Scale(oldPosition, new Vector2(Screen.width, Screen.height));
            else
                eventData.position = m_raycaster.eventCamera.ViewportToScreenPoint(oldPosition);

            m_raycaster.Raycast(eventData, resultAppendList);

            eventData.position = oldPosition;
        }

        public RaycasterUnityWrapper(BaseRaycaster raycaster)
        {
            m_raycaster = raycaster;
        }

        private BaseRaycaster m_raycaster;
    }
}
