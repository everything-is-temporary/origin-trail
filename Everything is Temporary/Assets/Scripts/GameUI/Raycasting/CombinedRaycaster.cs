﻿using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace GameUI.Raycasting
{
    /// <summary>
    /// A raycaster that aggregates raycast results from a list of raycasters.
    /// </summary>
    public class CombinedRaycaster : IRaycaster
    {
        public List<IRaycaster> Raycasters { get; }

        public CombinedRaycaster()
        {
            Raycasters = new List<IRaycaster>();
        }

        public void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            foreach (IRaycaster raycaster in Raycasters)
            {
                raycaster.Raycast(eventData, resultAppendList);
            }
        }
    }
}
