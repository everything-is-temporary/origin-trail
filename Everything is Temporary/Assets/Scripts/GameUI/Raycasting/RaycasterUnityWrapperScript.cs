﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI.Raycasting
{
    /// <summary>
    /// Used to allow a BaseRaycaster to be used like an IRaycaster. Automatically
    /// disables the base raycaster. Meant to be used like a component, as
    /// opposed to <see cref="RaycasterUnityWrapper"/>.
    /// </summary>
    public class RaycasterUnityWrapperScript : MonoBehaviour, IRaycaster
    {
        public void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            Vector2 oldPosition = eventData.position;

            // Transform coordinates from viewport to screen coordinates, since that
            // is expected by Unity's BaseRaycaster implementations.
            if (m_baseRaycaster.eventCamera == null)
                eventData.position = Vector2.Scale(oldPosition, new Vector2(Screen.width, Screen.height));
            else
                eventData.position = m_baseRaycaster.eventCamera.ViewportToScreenPoint(oldPosition);

            m_baseRaycaster.Raycast(eventData, resultAppendList);

            eventData.position = oldPosition;
        }

        private void Awake()
        {
            m_baseRaycaster.enabled = false;
        }

        [SerializeField]
        private BaseRaycaster m_baseRaycaster = default;
    }
}
