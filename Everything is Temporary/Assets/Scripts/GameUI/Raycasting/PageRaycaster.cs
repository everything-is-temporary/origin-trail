﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using GameUI.Raycasting;

/// <summary>
/// Replacement for GraphicRaycaster that should be used on book page canvases.
/// Automatically disables self.
/// </summary>
[RequireComponent(typeof(Canvas))]
public class PageRaycaster : GraphicRaycaster, IRaycaster
{
    protected override void Awake()
    {
        enabled = false;
    }

    void IRaycaster.Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
    {
        Vector2 oldPosition = eventData.position;

        // Transform coordinates from viewport to screen coordinates, since that
        // is expected by GraphicRaycaster.
        if (eventCamera == null)
            eventData.position = Vector2.Scale(oldPosition, new Vector2(Screen.width, Screen.height));
        else
            eventData.position = eventCamera.ViewportToScreenPoint(oldPosition);

        base.Raycast(eventData, resultAppendList);

        eventData.position = oldPosition;
    }
}
