﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace GameUI.Raycasting
{
    /// <summary>
    /// An IRaycaster that simply forwards events to a delegate IRaycaster.
    /// </summary>
    public class DelegateRaycaster : IRaycaster
    {
        public void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            Delegate?.Raycast(eventData, resultAppendList);
        }

        public DelegateRaycaster()
        {
            Delegate = null;
        }

        public DelegateRaycaster(IRaycaster raycaster)
        {
            Delegate = raycaster;
        }

        public IRaycaster Delegate { get; set; }
    }
}