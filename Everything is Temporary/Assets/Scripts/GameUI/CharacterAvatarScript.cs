﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Characters;

namespace GameUI
{
    public class CharacterAvatarScript : MonoBehaviour
    {
        public event System.Action<CharacterAvatarScript> OnCharacterAvatarClicked;

        public void OnClicked()
        {
            OnCharacterAvatarClicked?.Invoke(this);
        }

        public CharacterReference CharRef
        {
            get => m_charRef;

            set
            {
                if (m_charRef == null)
                {
                    m_charRef = value;
                }
                else
                {
                    Debug.LogWarning("CharacterAvatarScript's CharacterReference is being reset!");
                }
            }
        }

        private CharacterReference m_charRef;
    }
}
