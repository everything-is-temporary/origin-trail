﻿using System.Collections.Generic;
using GameUI.Abstractions;
using GameUI.Raycasting;
using GameUtility;
using UnityEngine;

namespace GameUI
{
    // Exactly the same as InteractiveViewCombiner, but is a MonoBehaviour instead.
    public class InteractiveViewCombinerScript : MonoBehaviour, IInteractiveView
    {
        public IRaycaster GetRaycaster()
        {
            return m_raycaster;
        }

        public void RenderTo(RenderTexture target)
        {
            for (int i = 0; i < m_views.Count; ++i)
            {
                GraphicUtils.ClearDepthOnly(target);
                ((IInteractiveView)m_views[i]).RenderTo(target);
            }
        }

        private void OnEnable()
        {
            m_raycaster = new CombinedRaycaster();

            foreach (IInteractiveView view in m_views)
            {
                IRaycaster raycaster = view.GetRaycaster();

                if (raycaster != null)
                    m_raycaster.Raycasters.Add(raycaster);
            }
        }

        [SerializeField]
        [RequiresType(typeof(IInteractiveView))]
        [Tooltip("These views are rendered in order, with the last one appearing above all the rest.")]
        private List<Object> m_views = new List<Object>();

        private CombinedRaycaster m_raycaster;
    }
}