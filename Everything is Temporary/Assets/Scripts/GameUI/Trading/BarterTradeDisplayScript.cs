﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using GameInventory;
using GameUtility;

namespace GameUI
{
    using ItemDisplayDict = Dictionary<ItemReference, GameObjectWith<ISelectableItemStackDisplay>>;

    public class BarterTradeDisplayScript : MonoBehaviour, ITradeDisplay
    {
        [SerializeField]
        private RectTransform m_playerInventoryContent = default;

        [SerializeField]
        private RectTransform m_shopInventoryContent = default;

        [SerializeField]
        [Tooltip("Where to display the items the player is selling.")]
        private RectTransform m_givingAreaContent = default;

        [SerializeField]
        [Tooltip("Where to display the items the player is buying.")]
        private RectTransform m_gettingAreaContent = default;

        [SerializeField]
        [RequiresType(typeof(ISelectableItemStackDisplay))]
        [Tooltip("The " + nameof(ISelectableItemStackDisplay) + " to use for displaying items.")]
        private MonoBehaviour m_itemStackDisplayPrefab = default;

        [SerializeField]
        [RequiresType(typeof(ISelectableItemStackDisplay))]
        [Tooltip("The " + nameof(ISelectableItemStackDisplay) + " to use for displaying items" +
            " in the trading area.")]
        private MonoBehaviour m_itemStackTradingAreaPrefab = default;


        /// <summary>
        /// Button connection for accepting a trade. Should be called by a UI button.
        /// </summary>
        public void ButtonAcceptTrade()
        {
            if (ValidateCurrentTradeOfferWithShop())
            {
                OnAcceptedTrade?.Invoke();
            }
            else
            {
                HandleRejectedTrade().FireAndForget();
            }
        }

        /// <summary>
        /// Button connection for cancelling a trade. Should be called by a UI button.
        /// </summary>
        public void ButtonCancelTrade()
        {
            OnCancelledTrade?.Invoke();
        }


        public async Task<TradeResult> DisplayTrade(ConvoyAndPartyInventory convoyAndPartyInventory, Shop shop)
        {
            SetupNewTradeDisplay(convoyAndPartyInventory, shop);

            gameObject.SetActive(true);
            var result = await WaitForAcceptedOrCancelled();
            gameObject.SetActive(false);

            return result;
        }

        public (IEnumerable<ItemWithAmount> itemsOffered, IEnumerable<ItemWithAmount> itemsRequested) EnumerateTradeItems()
        {
            return (from pair in m_itemToGivingDisplay select pair.Value.Data.DisplayedItemWithAmount, from pair in m_itemToGettingDisplay select pair.Value.Data.DisplayedItemWithAmount);
        }

        private bool ValidateCurrentTradeOfferWithShop()
        {
            var (offeredItems, requestedItems) = this.EnumerateTradeItems();
            return this.m_currentShop.IsTradeAccepted(offeredItems, requestedItems);
        }

        private async Task HandleRejectedTrade()
        {
            await GameUIHelper.Instance.DisplayPopupInfoAsync("Trade was rejected!");
        }

        private async Task<TradeResult> WaitForAcceptedOrCancelled()
        {

            TaskCompletionSource<object> tradeCancelled = new TaskCompletionSource<object>();
            TaskCompletionSource<object> tradeAccepted = new TaskCompletionSource<object>();

            void tradeCancelledHandler() => tradeCancelled.SetResult(null);
            void tradeAcceptedHandler() => tradeAccepted.SetResult(null);

            OnAcceptedTrade += tradeAcceptedHandler;
            OnCancelledTrade += tradeCancelledHandler;

            Task result = await Task.WhenAny(
                tradeCancelled.Task,
                tradeAccepted.Task
            );

            OnCancelledTrade -= tradeCancelledHandler;
            OnAcceptedTrade -= tradeAcceptedHandler;

            if (result == tradeAccepted.Task
                    && await GameUIHelper.Instance.DisplayConfirmationAsync("Are you sure you want to do this trade?", "Yes", "No"))
            {
                var (givingStacks, gettingStacks) = EnumerateTradeItems();

                return TradeResult.FinishedTrade(new TradeInfo(givingStacks, gettingStacks));
            }

            return TradeResult.CancelledTrade();
        }

        private void SetupNewTradeDisplay(IModifiableInventory playerInventory, Shop shop)
        {
            CleanupTradeDisplay();
            m_currentShop = shop;
            m_currentPlayerInventory = playerInventory;

            foreach (ItemWithAmount stack in playerInventory)
                DisplayPlayerInventoryStack(stack);

            foreach (ItemWithAmount stack in shop.Inventory)
                DisplayShopInventoryStack(stack);


            // Listen to when the shop or player inventory changes.
            m_currentPlayerInventory.OnItemAmountChanged += UpdatePlayerInventoryStack;

            m_currentShop.Inventory.OnItemAmountChanged += UpdateShopInventoryStack;
        }

        private void CleanupTradeDisplay()
        {
            if (m_currentPlayerInventory != null)
            {
                m_currentPlayerInventory.OnItemAmountChanged -= UpdatePlayerInventoryStack;
            }

            if (m_currentShop != null)
            {
                m_currentShop.Inventory.OnItemAmountChanged -= UpdateShopInventoryStack;
            }

            // Clean all created objects.
            foreach (var dict in new List<ItemDisplayDict> {
                        m_itemToGivingDisplay,
                        m_itemToPlayerInventoryDisplay,
                        m_itemToShopInventoryDisplay,
                        m_itemToGettingDisplay })
            {
                foreach (var item in dict)
                    Destroy(item.Value.GameObject);

                dict.Clear();
            }

            // Cancel the previous trade, if any.
            OnCancelledTrade?.Invoke();

            m_currentShop = null;
            m_currentPlayerInventory = null;
        }

        private void ClearPlayerInventoryStack(ItemReference itemId)
        {
            if (m_itemToPlayerInventoryDisplay.TryGetValue(itemId, out var display))
            {
                Destroy(display.GameObject);
                m_itemToPlayerInventoryDisplay.Remove(itemId);
            }

            if (m_itemToGivingDisplay.TryGetValue(itemId, out display))
            {
                Destroy(display.GameObject);
                m_itemToGivingDisplay.Remove(itemId);
            }
        }

        private void ClearShopInventoryStack(ItemReference itemId)
        {
            if (m_itemToShopInventoryDisplay.TryGetValue(itemId, out var display))
            {
                Destroy(display.GameObject);
                m_itemToShopInventoryDisplay.Remove(itemId);
            }

            if (m_itemToGettingDisplay.TryGetValue(itemId, out display))
            {
                Destroy(display.GameObject);
                m_itemToGettingDisplay.Remove(itemId);
            }
        }

        private void DisplayPlayerInventoryStack(ItemWithAmount itemWithAmount)
        {
            ItemReference itemId = itemWithAmount.ItemReference;

            if (m_itemToPlayerInventoryDisplay.TryGetValue(itemId, out var display))
            {
                display.Data.DisplayedItemWithAmount = itemWithAmount;

                // If we now have fewer items than are in the giving area,
                // remove some items from the giving area.
                if (itemWithAmount.Amount + display.Data.AmountOffset < 0)
                {
                    // We're assuming that this method is not called if the amount
                    // drops below 0.
                    Debug.Assert(itemWithAmount.Amount > 0);

                    display.Data.AmountOffset = -itemWithAmount.Amount;

                    m_itemToGivingDisplay[itemId].Data.DisplayedItemWithAmount = itemWithAmount;
                }
            }
            else
            {
                display = CreateItemStackDisplay(m_playerInventoryContent, itemWithAmount, m_itemToPlayerInventoryDisplay);
                display.Data.OnSelectedAmount += (amt) => MoveToGivingArea(itemId, amt);
            }
        }

        private void DisplayShopInventoryStack(ItemWithAmount itemWithAmount)
        {
            ItemReference itemId = itemWithAmount.ItemReference;

            if (m_itemToShopInventoryDisplay.TryGetValue(itemId, out var display))
            {
                display.Data.DisplayedItemWithAmount = itemWithAmount;

                // If the shop has fewer items than the player is trying to buy,
                // reduce the number of items that the player is trying to buy.
                if (itemWithAmount.Amount + display.Data.AmountOffset < 0)
                {
                    Debug.Assert(itemWithAmount.Amount > 0);

                    display.Data.AmountOffset = -itemWithAmount.Amount;
                    m_itemToGettingDisplay[itemId].Data.DisplayedItemWithAmount = itemWithAmount;
                }
            }
            else
            {
                display = CreateItemStackDisplay(m_shopInventoryContent, itemWithAmount, m_itemToShopInventoryDisplay);
                display.Data.OnSelectedAmount += (amt) => MoveToGettingArea(itemId, amt);
            }
        }

        private void UpdatePlayerInventoryStack(ItemWithAmount old, ItemWithAmount itemWithAmount)
        {
            if (itemWithAmount.Amount <= 0)
            {
                // Remove the item from the display.
                ClearPlayerInventoryStack(itemWithAmount);
            }
            // Update or create the item.
            DisplayPlayerInventoryStack(itemWithAmount);
        }

        private void UpdateShopInventoryStack(ItemWithAmount oldAmount, ItemWithAmount itemWithAmount)
        {
            if (itemWithAmount.Amount <= 0)
            {
                // Remove the item from the display.
                ClearShopInventoryStack(itemWithAmount);
            }
            // Update or create the item.
            DisplayShopInventoryStack(itemWithAmount);
        }

        private void MoveToGivingArea(ItemReference id, int amount)
        {
            if (amount < 0)
                return;

            if (!m_itemToPlayerInventoryDisplay.TryGetValue(id, out var inventoryDisplay))
                return;

            int maxAmount = inventoryDisplay.Data.DisplayedItemWithAmount.Amount
                + inventoryDisplay.Data.AmountOffset;

            if (amount > maxAmount)
                amount = maxAmount;

            inventoryDisplay.Data.AmountOffset -= amount;
            ItemWithAmount newGivingItemWithAmount = new ItemWithAmount(id, -inventoryDisplay.Data.AmountOffset);

            if (!m_itemToGivingDisplay.TryGetValue(id, out var givingDisplay))
            {
                givingDisplay = CreateTradingAreaItemStackDisplay(m_givingAreaContent, newGivingItemWithAmount, m_itemToGivingDisplay);
                givingDisplay.Data.OnSelectedAmount += (amt) => UpdateGivingStack(id, amt);
            }
            else
            {
                givingDisplay.Data.DisplayedItemWithAmount = newGivingItemWithAmount;
            }
        }

        private void MoveToGettingArea(ItemReference id, int amount)
        {
            if (amount < 0)
                return;

            if (!m_itemToShopInventoryDisplay.TryGetValue(id, out var shopDisplay))
                return;

            int maxAmount = shopDisplay.Data.DisplayedItemWithAmount.Amount
                + shopDisplay.Data.AmountOffset;

            if (amount > maxAmount)
                amount = maxAmount;

            shopDisplay.Data.AmountOffset -= amount;
            ItemWithAmount newGettingItemWithAmount = new ItemWithAmount(id, -shopDisplay.Data.AmountOffset);

            if (!m_itemToGettingDisplay.TryGetValue(id, out var gettingDisplay))
            {
                gettingDisplay = CreateTradingAreaItemStackDisplay(m_gettingAreaContent, newGettingItemWithAmount, m_itemToGettingDisplay);
                gettingDisplay.Data.OnSelectedAmount += (amt) => UpdateGettingStack(id, amt);
            }
            else
            {
                gettingDisplay.Data.DisplayedItemWithAmount = newGettingItemWithAmount;
            }
        }

        private void UpdateGivingStack(ItemReference id, int amountChange)
        {
            var givingDisplay = m_itemToGivingDisplay[id];
            var inventoryDisplay = m_itemToPlayerInventoryDisplay[id];

            int newAmount = amountChange + givingDisplay.Data.DisplayedItemWithAmount.Amount;
            if (newAmount > inventoryDisplay.Data.DisplayedItemWithAmount.Amount)
                newAmount = inventoryDisplay.Data.DisplayedItemWithAmount.Amount;
            if (newAmount < 0)
                newAmount = 0;

            inventoryDisplay.Data.AmountOffset = -newAmount;

            if (newAmount <= 0)
            {
                Destroy(givingDisplay.GameObject);
                m_itemToGivingDisplay.Remove(id);
            }
            else
            {
                givingDisplay.Data.DisplayedItemWithAmount = new ItemWithAmount(id, newAmount);
            }
        }

        private void UpdateGettingStack(ItemReference id, int amountChange)
        {
            var gettingDisplay = m_itemToGettingDisplay[id];
            var shopDisplay = m_itemToShopInventoryDisplay[id];

            int newAmount = amountChange + gettingDisplay.Data.DisplayedItemWithAmount.Amount;

            if (newAmount > shopDisplay.Data.DisplayedItemWithAmount.Amount)
                newAmount = shopDisplay.Data.DisplayedItemWithAmount.Amount;
            if (newAmount < 0)
                newAmount = 0;

            shopDisplay.Data.AmountOffset = -newAmount;

            if (newAmount <= 0)
            {
                Destroy(gettingDisplay.GameObject);
                m_itemToGettingDisplay.Remove(id);
            }
            else
            {
                gettingDisplay.Data.DisplayedItemWithAmount = new ItemWithAmount(id, newAmount);
            }
        }

        private GameObjectWith<ISelectableItemStackDisplay> CreateItemStackDisplay(
            Transform parent, ItemWithAmount itemWithAmount, ItemDisplayDict dictionary)
        {
            var displayObj = Instantiate(m_itemStackDisplayPrefab, parent);

            var display = new GameObjectWith<ISelectableItemStackDisplay>
            {
                GameObject = displayObj.gameObject,
                Data = displayObj as ISelectableItemStackDisplay
            };

            display.Data.DisplayedItemWithAmount = itemWithAmount;
            dictionary[itemWithAmount.ItemReference] = display;

            return display;
        }

        private GameObjectWith<ISelectableItemStackDisplay> CreateTradingAreaItemStackDisplay(
            Transform parent, ItemWithAmount itemWithAmount, ItemDisplayDict dictionary)
        {
            var displayObj = Instantiate(m_itemStackTradingAreaPrefab, parent);

            var display = new GameObjectWith<ISelectableItemStackDisplay>
            {
                GameObject = displayObj.gameObject,
                Data = displayObj as ISelectableItemStackDisplay
            };

            display.Data.DisplayedItemWithAmount = itemWithAmount;
            dictionary[itemWithAmount.ItemReference] = display;

            return display;
        }

        private void OnDestroy()
        {
            CleanupTradeDisplay();
        }

        private event Action OnCancelledTrade;
        private event Action OnAcceptedTrade;

        private IModifiableInventory m_currentPlayerInventory;
        private Shop m_currentShop;

        private ItemDisplayDict m_itemToPlayerInventoryDisplay = new ItemDisplayDict();
        private ItemDisplayDict m_itemToShopInventoryDisplay = new ItemDisplayDict();
        private ItemDisplayDict m_itemToGivingDisplay = new ItemDisplayDict();
        private ItemDisplayDict m_itemToGettingDisplay = new ItemDisplayDict();
    }
}
