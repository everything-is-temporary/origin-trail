﻿using System;
using GameInventory;

namespace GameUI
{
    public interface ISelectableItemStackDisplay
    {
        /// <summary>
        /// Occurs when user selects an amount of the itemWithAmount.
        /// </summary>
        event Action<int> OnSelectedAmount;

        /// <summary>
        /// An offset by which the displayed number is modified.
        /// </summary>
        /// <value>The offset.</value>
        int AmountOffset { get; set; }

        /// <summary>
        /// The itemWithAmount that should be displayed.
        /// </summary>
        /// <value>The displayed itemWithAmount.</value>
        ItemWithAmount DisplayedItemWithAmount { get; set; }
    }
}
