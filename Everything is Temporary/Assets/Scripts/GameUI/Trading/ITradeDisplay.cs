﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using GameInventory;

namespace GameUI
{
    /// <summary>
    /// Interface for displaying a trade menu between a player and a shop.
    /// Implementations of this interface are expected only to figure out
    /// what trade the player wishes to make without actually moving around
    /// any items.
    /// </summary>
    public interface ITradeDisplay
    {
        /// <summary>
        /// Displays a trade menu where the inventory to be traded from is specified
        /// by <paramref name="convoyAndPartyInventory"/> and the shop to be traded with is
        /// specified by <paramref name="shop"/>.
        /// <para>
        /// If this is on a MonoBehaviour, this should activate the corresponding
        /// gameObject and deactivate it before returning.
        /// </para>
        /// </summary>
        /// <returns>The result of the trade.</returns>
        /// <param name="convoyAndPartyInventory">Inventory that the shop will trade with (generally the player's).</param>
        /// <param name="shop">Shop.</param>
        Task<TradeResult> DisplayTrade(ConvoyAndPartyInventory convoyAndPartyInventory, Shop shop);
    }
}