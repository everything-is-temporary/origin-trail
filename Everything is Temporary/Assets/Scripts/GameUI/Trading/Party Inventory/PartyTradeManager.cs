﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameUI;
using UnityEngine;

namespace GameInventory
{
    //i think i had a reason for seperating this into its own class apart from TradeManager but i honestly forgot why
    public static class PartyTradeManager
    {

        /// <summary>
        /// Opens trade menu for inter-party trading. 
        /// </summary>
        /// <returns>The result of the trade.</returns>
        /// <param name="playerInventory">Player inventory.</param>
        /// <param name="shop">Shop.</param>
        public static async Task<TradeResult> PerformPartyTrade()
        {
            TradeResult result = await GameUIHelper.Instance.DisplayPartyTradeMenuAsync();

            //keep trading between party members until user explicitly wants to stop
            //bc otherwise it's really annoying to have to keep opening the window to do a bunch of trades
            while (result.Result != TradeResult.ResultType.Cancelled)
            {
                if (result.Result == TradeResult.ResultType.Finished)
                {
                    Inventory firstInventory = result.Info.FirstCharacterInventory;
                    Inventory secondInventory = result.Info.SecondCharacterInventory;
                    List<ItemWithAmount> giveToSecondInventory = new List<ItemWithAmount>();
                    List<ItemWithAmount> giveToFirstInventory = new List<ItemWithAmount>();

                    foreach (var stack in result.Info.Giving)
                    {
                        int removed = firstInventory.RemoveAndGetDelta(stack).Amount;
                        giveToSecondInventory.Add(stack.WithAmount(removed));

                        if (removed < stack.Amount)
                            Debug.LogWarning($"Tried to give {stack.Amount} of {stack.Item.Name} but inventory one only had {removed}.");
                    }

                    foreach (var stack in result.Info.Getting)
                    {
                        int removed = secondInventory.RemoveAndGetDelta(stack).Amount;
                        giveToFirstInventory.Add(stack.WithAmount(removed));

                        if (removed < stack.Amount)
                            Debug.LogWarning($"Tried to get {stack.Amount} of {stack.Item.Name} but inventory two only had {removed}.");
                    }

                    foreach (var stack in giveToSecondInventory)
                        secondInventory.AddItem(stack);

                    foreach (var stack in giveToFirstInventory)
                        firstInventory.AddItem(stack);
                }

                result = await GameUIHelper.Instance.DisplayPartyTradeMenuAsync();
            }

            //once it's done, unfocus the book (close all inventory UI in user story)
            BookUIManager.Instance.FocusGame();

            return result;
        }
    }
}