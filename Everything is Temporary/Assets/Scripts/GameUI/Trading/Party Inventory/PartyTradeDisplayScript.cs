﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using GameInventory;
using GameUtility;
using UnityEngine.UI;
using Characters;

namespace GameUI
{
    using ItemDisplayDict = Dictionary<ItemReference, GameObjectWith<ISelectableItemStackDisplay>>;

    public class PartyTradeDisplayScript : MonoBehaviour
    {
        [SerializeField]
        private RectTransform m_firstInventoryContent = default;

        [SerializeField]
        private RectTransform m_secondInventoryContent = default;

        [SerializeField]
        [Tooltip("Where to display the items the first character is giving.")]
        private RectTransform m_givingAreaContent = default;

        [SerializeField]
        [Tooltip("Where to display the items the first character is getting.")]
        private RectTransform m_gettingAreaContent = default;

        [SerializeField]
        [Tooltip("Dropdown to select the first inventory.")]
        private Dropdown m_firstInventoryDropdown = default;

        [SerializeField]
        [Tooltip("Dropdown to select the second inventory.")]
        private Dropdown m_secondInventoryDropdown = default;

        [SerializeField]
        [RequiresType(typeof(ISelectableItemStackDisplay))]
        [Tooltip("The " + nameof(ISelectableItemStackDisplay) + " to use for displaying items.")]
        private MonoBehaviour m_itemStackDisplayPrefab = default;

        [SerializeField]
        [RequiresType(typeof(ISelectableItemStackDisplay))]
        [Tooltip("The " + nameof(ISelectableItemStackDisplay) + " to use for displaying items" +
            " in the trading area.")]
        private MonoBehaviour m_itemStackTradingAreaPrefab = default;


        public async Task<TradeResult> InitializePartyTradeDisplay()
        {
            CleanupTradeDisplay();

            List<string> partyMembers = new List<string>();
            partyMembers.Add("Convoy"); //this is for the convoy inventory
            foreach (var partyMember in CharacterManager.Instance.CurrentParty)
                partyMembers.Add(partyMember.Character.Name);

            m_firstInventoryDropdown.AddOptions(partyMembers);
            m_secondInventoryDropdown.AddOptions(partyMembers);

            //add handlers for dropdown selection
            m_firstInventoryDropdown.onValueChanged.AddListener(FirstInventoryValueChangedHandler);
            m_secondInventoryDropdown.onValueChanged.AddListener(SecondInventoryValueChangedHandler);

            Debug.Assert(CharacterManager.Instance.CurrentParty.Count() > 0);

            //make sure both inventories are initialized / displayed
            FirstInventoryValueChangedHandler(m_firstInventoryDropdown.value); //is this good practice....idk................
            if (m_firstInventoryDropdown.value == m_secondInventoryDropdown.value) //this only happens in initialization, first inventory always = 0
                m_secondInventoryDropdown.value = 1; //make sure it never displays the same as the first inventory
            SecondInventoryValueChangedHandler(m_secondInventoryDropdown.value);

            // Listen to when the shop or player inventory changes.
            m_firstInventory.OnItemAmountChanged += UpdateFirstInventoryStack;
            m_secondInventory.OnItemAmountChanged += UpdateSecondInventoryStack;

            gameObject.SetActive(true);
            var result = await WaitForAcceptedOrCancelled();
            gameObject.SetActive(false);

            //before returning result, reset dropdowns (or else things will fuck up)
            m_firstInventoryDropdown.ClearOptions();
            m_secondInventoryDropdown.ClearOptions();

            return result;
        }

        /// <summary>
        /// This handles selections in the dropdown menu. 
        /// The inventory that is selected will be appropriately updated. 
        /// </summary>
        /// <param name="target"></param>
        private void FirstInventoryValueChangedHandler(int target)
        {
            // Refresh first inventory (remove items from display)
            foreach (var dict in new List<ItemDisplayDict> {
                        m_itemToGivingDisplay,
                        m_itemToFirstInventoryDisplay})
            {
                foreach (var item in dict)
                    Destroy(item.Value.GameObject);

                dict.Clear();
            }

            m_firstInventory = GetInventoryBasedOnDropdownValue(target);

            //display inventory
            foreach (ItemWithAmount stack in m_firstInventory)
                DisplayFirstInventoryStack(stack);
        }

        private void SecondInventoryValueChangedHandler(int target)
        {
            //Refresh second inventory (remove previous items from display)
            foreach (var dict in new List<ItemDisplayDict> {
                        m_itemToSecondInventoryDisplay,
                        m_itemToGettingDisplay })
            {
                foreach (var item in dict)
                    Destroy(item.Value.GameObject);

                dict.Clear();
            }            

            m_secondInventory = GetInventoryBasedOnDropdownValue(target);

            //display inventory
            foreach (ItemWithAmount stack in m_secondInventory)
                DisplaySecondInventoryStack(stack);
        }

        public Inventory GetInventoryBasedOnDropdownValue(int value)
        {
            if (value == 0) //this is the convoy's inventory
                return InventoryManager.Instance.ConvoyInventory;
            else //party member - get from CurrentParty list (adjust index to account for convoy at beginning) 
            { 
                CharacterReference chosenCharacter = CharacterManager.Instance.CurrentParty.ElementAt(value - 1);
                return chosenCharacter.Character.Inventory;
            }
        }

        /// <summary>
        /// This determines whether the item in the dropdown is selectable. 
        /// 
        /// For example, it prevents Bel being selected in both dropdown menus. 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public bool IsCharacterInventorySelectable(int value, Dropdown parent)
        {
            //get value of other dropdown
            if (m_firstInventoryDropdown == parent)
            {
                if (m_secondInventoryDropdown.value == value)
                    return false;
            }
            else
            {
                if (m_firstInventoryDropdown.value == value)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Button connection for accepting a trade. Should be called by a UI button.
        /// </summary>
        public void ButtonAcceptTrade()
        {
                OnAcceptedTrade?.Invoke();
        }

        /// <summary>
        /// Button connection for cancelling a trade. Should be called by a UI button.
        /// </summary>
        public void ButtonCancelTrade()
        {
            OnCancelledTrade?.Invoke();
        }

        public (IEnumerable<ItemWithAmount> itemsOffered, IEnumerable<ItemWithAmount> itemsRequested) EnumerateTradeItems()
        {
            return (from pair in m_itemToGivingDisplay select pair.Value.Data.DisplayedItemWithAmount, from pair in m_itemToGettingDisplay select pair.Value.Data.DisplayedItemWithAmount);
        }

        private async Task<TradeResult> WaitForAcceptedOrCancelled()
        {

            TaskCompletionSource<object> tradeCancelled = new TaskCompletionSource<object>();
            TaskCompletionSource<object> tradeAccepted = new TaskCompletionSource<object>();

            void tradeCancelledHandler() => tradeCancelled.SetResult(null);
            void tradeAcceptedHandler() => tradeAccepted.SetResult(null);

            OnAcceptedTrade += tradeAcceptedHandler;
            OnCancelledTrade += tradeCancelledHandler;

            Task result = await Task.WhenAny(
                tradeCancelled.Task,
                tradeAccepted.Task
            );

            OnCancelledTrade -= tradeCancelledHandler;
            OnAcceptedTrade -= tradeAcceptedHandler;

            if (result == tradeAccepted.Task)
            {
                var (givingStacks, gettingStacks) = EnumerateTradeItems();

                return TradeResult.FinishedTrade(new TradeInfo(givingStacks, gettingStacks, m_firstInventory, m_secondInventory));
            }

            return TradeResult.CancelledTrade();
        }

        /// <summary>
        /// Remove event things from inventory 
        /// </summary>
        private void CleanupTradeDisplay()
        {
            if (m_firstInventory != null)
            {
                m_firstInventory.OnItemAmountChanged -= UpdateFirstInventoryStack;
            }

            if (m_secondInventory != null)
            {
                m_secondInventory.OnItemAmountChanged -= UpdateSecondInventoryStack;
            }

            // Clean all created objects.
            foreach (var dict in new List<ItemDisplayDict> {
                        m_itemToGivingDisplay,
                        m_itemToFirstInventoryDisplay,
                        m_itemToSecondInventoryDisplay,
                        m_itemToGettingDisplay })
            {
                foreach (var item in dict)
                    Destroy(item.Value.GameObject);

                dict.Clear();
            }

            // Cancel the previous trade, if any.
            OnCancelledTrade?.Invoke();

            m_secondInventory = null;
            m_firstInventory = null;
        }

        private void ClearFirstInventoryStack(ItemReference itemId)
        {
            if (m_itemToFirstInventoryDisplay.TryGetValue(itemId, out var display))
            {
                Destroy(display.GameObject);
                m_itemToFirstInventoryDisplay.Remove(itemId);
            }

            if (m_itemToGivingDisplay.TryGetValue(itemId, out display))
            {
                Destroy(display.GameObject);
                m_itemToGivingDisplay.Remove(itemId);
            }
        }

        private void ClearSecondInventoryStack(ItemReference itemId)
        {
            if (m_itemToSecondInventoryDisplay.TryGetValue(itemId, out var display))
            {
                Destroy(display.GameObject);
                m_itemToSecondInventoryDisplay.Remove(itemId);
            }

            if (m_itemToGettingDisplay.TryGetValue(itemId, out display))
            {
                Destroy(display.GameObject);
                m_itemToGettingDisplay.Remove(itemId);
            }
        }

        private void DisplayFirstInventoryStack(ItemWithAmount itemWithAmount)
        {
            ItemReference itemId = itemWithAmount.ItemReference;

            if (m_itemToFirstInventoryDisplay.TryGetValue(itemId, out var display))
            {
                display.Data.DisplayedItemWithAmount = itemWithAmount;

                // If we now have fewer items than are in the giving area,
                // remove some items from the giving area.
                if (itemWithAmount.Amount + display.Data.AmountOffset < 0)
                {
                    // We're assuming that this method is not called if the amount
                    // drops below 0.
                    Debug.Assert(itemWithAmount.Amount > 0);

                    display.Data.AmountOffset = -itemWithAmount.Amount;

                    m_itemToGivingDisplay[itemId].Data.DisplayedItemWithAmount = itemWithAmount;
                }
            }
            else
            {
                display = CreateItemStackDisplay(m_firstInventoryContent, itemWithAmount, m_itemToFirstInventoryDisplay);
                display.Data.OnSelectedAmount += (amt) => MoveToGivingArea(itemId, amt);
            }
        }


        private void DisplaySecondInventoryStack(ItemWithAmount itemWithAmount)
        {
            ItemReference itemRef = itemWithAmount.ItemReference;

            if (m_itemToSecondInventoryDisplay.TryGetValue(itemRef, out var display))
            {
                display.Data.DisplayedItemWithAmount = itemWithAmount;

                // If the shop has fewer items than the player is trying to buy,
                // reduce the number of items that the player is trying to buy.
                if (itemWithAmount.Amount + display.Data.AmountOffset < 0)
                {
                    Debug.Assert(itemWithAmount.Amount > 0);

                    display.Data.AmountOffset = -itemWithAmount.Amount;
                    m_itemToGettingDisplay[itemRef].Data.DisplayedItemWithAmount = itemWithAmount;
                }
            }
            else
            {
                display = CreateItemStackDisplay(m_secondInventoryContent, itemWithAmount, m_itemToSecondInventoryDisplay);
                display.Data.OnSelectedAmount += (amt) => MoveToGettingArea(itemRef, amt);
            }
        }

        private void UpdateFirstInventoryStack(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            if (newAmount.Amount <= 0)
            {
                // Remove the item from the display.
                ClearFirstInventoryStack(newAmount);
            }
            else // Update or create the item.
                DisplayFirstInventoryStack(newAmount);
        }

        private void UpdateSecondInventoryStack(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            if (newAmount.Amount <= 0)
            {
                // Remove the item from the display.
                ClearSecondInventoryStack(newAmount);
            }
            else // Update or create the item.
                DisplaySecondInventoryStack(newAmount);
        }

        private void MoveToGivingArea(ItemReference id, int amount)
        {
            if (amount < 0)
                return;

            if (!m_itemToFirstInventoryDisplay.TryGetValue(id, out var inventoryDisplay))
                return;

            int maxAmount = inventoryDisplay.Data.DisplayedItemWithAmount.Amount
                + inventoryDisplay.Data.AmountOffset;

            if (amount > maxAmount)
                amount = maxAmount;

            inventoryDisplay.Data.AmountOffset -= amount;
            ItemWithAmount newGivingItemWithAmount = new ItemWithAmount(id, -inventoryDisplay.Data.AmountOffset);

            if (!m_itemToGivingDisplay.TryGetValue(id, out var givingDisplay))
            {
                givingDisplay = CreateTradingAreaItemStackDisplay(m_givingAreaContent, newGivingItemWithAmount, m_itemToGivingDisplay);
                givingDisplay.Data.OnSelectedAmount += (amt) => UpdateGivingStack(id, amt);
            }
            else
            {
                givingDisplay.Data.DisplayedItemWithAmount = newGivingItemWithAmount;
            }
        }

        private void MoveToGettingArea(ItemReference id, int amount)
        {
            if (amount < 0)
                return;

            if (!m_itemToSecondInventoryDisplay.TryGetValue(id, out var shopDisplay))
                return;

            int maxAmount = shopDisplay.Data.DisplayedItemWithAmount.Amount
                + shopDisplay.Data.AmountOffset;

            if (amount > maxAmount)
                amount = maxAmount;

            shopDisplay.Data.AmountOffset -= amount;
            ItemWithAmount newGettingStack = new ItemWithAmount(id, -shopDisplay.Data.AmountOffset);

            if (!m_itemToGettingDisplay.TryGetValue(id, out var gettingDisplay))
            {
                gettingDisplay = CreateTradingAreaItemStackDisplay(m_gettingAreaContent, newGettingStack, m_itemToGettingDisplay);
                gettingDisplay.Data.OnSelectedAmount += (amt) => UpdateGettingStack(id, amt);
            }
            else
            {
                gettingDisplay.Data.DisplayedItemWithAmount = newGettingStack;
            }
        }

        private void UpdateGivingStack(ItemReference id, int amountChange)
        {
            var givingDisplay = m_itemToGivingDisplay[id];
            var inventoryDisplay = m_itemToFirstInventoryDisplay[id];

            int newAmount = amountChange + givingDisplay.Data.DisplayedItemWithAmount.Amount;
            if (newAmount > inventoryDisplay.Data.DisplayedItemWithAmount.Amount)
                newAmount = inventoryDisplay.Data.DisplayedItemWithAmount.Amount;
            if (newAmount < 0)
                newAmount = 0;

            inventoryDisplay.Data.AmountOffset = -newAmount;

            if (newAmount <= 0)
            {
                Destroy(givingDisplay.GameObject);
                m_itemToGivingDisplay.Remove(id);
            }
            else
            {
                givingDisplay.Data.DisplayedItemWithAmount = new ItemWithAmount(id, newAmount);
            }
        }

        private void UpdateGettingStack(ItemReference id, int amountChange)
        {
            var gettingDisplay = m_itemToGettingDisplay[id];
            var shopDisplay = m_itemToSecondInventoryDisplay[id];

            int newAmount = amountChange + gettingDisplay.Data.DisplayedItemWithAmount.Amount;

            if (newAmount > shopDisplay.Data.DisplayedItemWithAmount.Amount)
                newAmount = shopDisplay.Data.DisplayedItemWithAmount.Amount;
            if (newAmount < 0)
                newAmount = 0;

            shopDisplay.Data.AmountOffset = -newAmount;

            if (newAmount <= 0)
            {
                Destroy(gettingDisplay.GameObject);
                m_itemToGettingDisplay.Remove(id);
            }
            else
            {
                gettingDisplay.Data.DisplayedItemWithAmount = new ItemWithAmount(id, newAmount);
            }
        }

        private GameObjectWith<ISelectableItemStackDisplay> CreateItemStackDisplay(
            Transform parent, ItemWithAmount itemWithAmount, ItemDisplayDict dictionary)
        {
            var displayObj = Instantiate(m_itemStackDisplayPrefab, parent);

            var display = new GameObjectWith<ISelectableItemStackDisplay>
            {
                GameObject = displayObj.gameObject,
                Data = displayObj as ISelectableItemStackDisplay
            };

            display.Data.DisplayedItemWithAmount = itemWithAmount;
            dictionary[itemWithAmount.ItemReference] = display;

            return display;
        }

        private GameObjectWith<ISelectableItemStackDisplay> CreateTradingAreaItemStackDisplay(
            Transform parent, ItemWithAmount itemWithAmount, ItemDisplayDict dictionary)
        {
            var displayObj = Instantiate(m_itemStackTradingAreaPrefab, parent);

            var display = new GameObjectWith<ISelectableItemStackDisplay>
            {
                GameObject = displayObj.gameObject,
                Data = displayObj as ISelectableItemStackDisplay
            };

            display.Data.DisplayedItemWithAmount = itemWithAmount;
            dictionary[itemWithAmount.ItemReference] = display;

            return display;
        }

        private void OnDestroy()
        {
            CleanupTradeDisplay();
        }

        private event Action OnCancelledTrade;
        private event Action OnAcceptedTrade;

        private Inventory m_firstInventory;
        private Inventory m_secondInventory;

        private ItemDisplayDict m_itemToFirstInventoryDisplay = new ItemDisplayDict();
        private ItemDisplayDict m_itemToSecondInventoryDisplay = new ItemDisplayDict();
        private ItemDisplayDict m_itemToGivingDisplay = new ItemDisplayDict();
        private ItemDisplayDict m_itemToGettingDisplay = new ItemDisplayDict();
    }
}
