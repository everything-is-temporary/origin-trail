﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUI;
using GameInventory;

public class DisplayPartyTrade : MonoBehaviour
{

    [SerializeField] private PartyTradeDisplayScript m_partyTradeDisplayPrefab = default;

    public void Start()
    {
        //TODO: If convoy is added later in the game, then change the below check 
        //(since right now there is always the convoy in the party)
        //if (Characters.CharacterManager.Instance.CurrentPartyExcludingPlayer.Count > 0) //make sure that trade can only happen if there is a party!
        //    this.gameObject.SetActive(false);
    }

    public async void DisplayPartyTradeInterface()
    {
        await PartyTradeManager.PerformPartyTrade();
    }
}
