﻿ using GameUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//most of this script taken from both comments in https://answers.unity.com/questions/1142096/disable-a-single-dropdown-option.html
//bless them, love them, cherish them

/// <summary>
/// This basically disables options in the inventory dropdown selections. 
/// IsItemSelectable() asks PartyTradeDisplayScript whether this item is already selected in the other
/// inventory dropdown, and thus should not be able to be selected. 
/// 
/// (So player inventory can't be selected on both sides)
/// </summary>
public class DropdownItemToggleScript : MonoBehaviour
{
    [SerializeField]
    private PartyTradeDisplayScript m_partyTradeScript;
    [SerializeField]
    private Dropdown dropDownParent; //to identify which dropdown item comes from
    // Start is called before the first frame update
    void Start()
    {
        Toggle toggle = GetComponent<Toggle>();

        if (toggle != null && m_partyTradeScript != null)
        {
            toggle.interactable = m_partyTradeScript.IsCharacterInventorySelectable(transform.GetSiblingIndex()-1, dropDownParent);
        }

    }
}
