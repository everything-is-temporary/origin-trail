﻿using UnityEngine;
using System.Collections;
using GameInventory;

namespace GameUI
{
    using UI = UnityEngine.UI;

    /// <summary>
    /// An ItemWithAmount display that allows the user to "select" some amount of
    /// the itemWithAmount.
    /// </summary>
    public class SelectableItemStackDisplayScript : MonoBehaviour, ISelectableItemStackDisplay
    {
        // TODO: This could be implemented by making an IItemStackDisplay interface.

        [SerializeField]
        private UI.Text m_amountText = default;

        [SerializeField]
        private UI.Image m_itemIcon = default;

        [SerializeField]
        private UI.Text m_itemName = default;

        /// <summary>
        /// Invoked when the user "selects" some amount of the displayed item
        /// itemWithAmount. This can be interpreted as, for example, the user selecting
        /// some amount to add to a trade.
        /// </summary>
        public event System.Action<int> OnSelectedAmount;

        public int AmountOffset
        {
            get => m_amountOffset;
            set
            {
                m_amountOffset = value;
                UpdateAmount();
            }
        }

        public ItemWithAmount DisplayedItemWithAmount
        {
            get => m_currentItemWithAmount;
            set => DisplayItemStack(value);
        }

        public void DisplayItemStack(ItemWithAmount itemWithAmount)
        {
            m_currentItemWithAmount = itemWithAmount;

            if (m_currentItemWithAmount != null)
            {
                InventoryItem item = m_currentItemWithAmount.Item;

                if (m_itemIcon != null)
                    m_itemIcon.sprite = item.Icon;
                if (m_itemName != null)
                    m_itemName.text = item.Name;

                UpdateAmount();
            }
        }

        /// <summary>
        /// Button connection for changing the amount. Should be used by a UI button.
        /// </summary>
        /// <param name="changedAmount">Changed amount.</param>
        public void ButtonChangeAmount(int changedAmount)
        {
            OnSelectedAmount?.Invoke(changedAmount);
        }

        private void UpdateAmount()
        {
            if (m_amountText != null && m_currentItemWithAmount != null)
                m_amountText.text = $"{m_currentItemWithAmount.Amount + AmountOffset}";
        }

        private int m_amountOffset;
        private ItemWithAmount m_currentItemWithAmount;
    }
}