﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameUI.Abstractions;
using GameUtility;

namespace GameUI
{
    /// <summary>
    /// Renders an <see cref="IInteractiveView"/> on top of the output of a camera.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    [RequireComponent(typeof(ViewRendererRaycasterScript))]
    public class ViewRendererScript : MonoBehaviour
    {
        public IInteractiveView InteractiveView => (IInteractiveView)m_interactiveView;

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Graphics.Blit(source, destination);
            InteractiveView.RenderTo(destination);
        }

        private void Awake()
        {
            m_camera = GetComponent<Camera>();
        }

        [SerializeField]
        [RequiresType(typeof(IInteractiveView))]
        private Object m_interactiveView = default;

        private Camera m_camera;
    }
}