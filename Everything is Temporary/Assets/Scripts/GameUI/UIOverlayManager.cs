﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GameUtility;

/// <summary>
/// Manages full-screen overlays by adding them as children of the same Unity
/// object and controlling their order.
/// </summary>
public class UIOverlayManager : SingletonMonoBehavior<UIOverlayManager>
{
    [SerializeField]
    private Transform m_overlayContainer = default;

    [SerializeField]
    private Transform m_notificationContainer = default;

    /// <summary>
    /// Instantiates the prefab as the topmost overlay. These overlays appear
    /// over everything in the game but below notifications. To close the
    /// overlay, you should call <see cref="DestroyOverlay{T}(T)"/>.
    /// </summary>
    /// <returns>The instantiated overlay.</returns>
    /// <param name="overlayPrefab">Overlay prefab.</param>
    public T InstantiateOverlayOnTop<T>(T overlayPrefab) where T : Component
    {
        return InstantiateAsChild(overlayPrefab, m_overlayContainer);
    }

    /// <summary>
    /// Instantiates the prefab as the topmost notification overlay. These
    /// overlays appear above everything in the game and above regular overlays
    /// added with <see cref="InstantiateOverlayOnTop{T}(T)"/>. To close the
    /// overlay, you should call <see cref="DestroyOverlay{T}(T)"/>.
    /// </summary>
    /// <returns>The instantiated notification overlay.</returns>
    /// <param name="overlayPrefab">Overlay prefab.</param>
    public T InstantiateNotificationOverlay<T>(T overlayPrefab) where T : Component
    {
        return InstantiateAsChild(overlayPrefab, m_notificationContainer);
    }

    /// <summary>
    /// Removes the overlay from the overlay stack and destroys it.
    /// </summary>
    /// <param name="overlay">Overlay to destroy.</param>
    public void DestroyOverlay<T>(T overlay) where T : Component
    {
        Destroy(overlay.gameObject);
    }

    private T InstantiateAsChild<T>(T prefab, Transform container) where T : Component
    {
        T newObj = Instantiate(prefab, container);
        newObj.transform.SetAsLastSibling();

        return newObj;
    }
}
