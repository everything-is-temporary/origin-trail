using UnityEngine;

namespace GameUI.Book
{
    public class BookPageFlipHelperScript : MonoBehaviour
    {
        public void FlipLeftToRight()
        {
            BookScript.Instance.FlipToLaterPage();
        }

        public void FlipRightToLeft()
        {
            BookScript.Instance.FlipToEarlierPage();
        }
    }
}