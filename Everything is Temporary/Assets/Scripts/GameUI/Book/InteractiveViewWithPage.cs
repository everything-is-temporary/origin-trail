﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using GameUI.Abstractions;
using GameUI.Raycasting;

namespace GameUI
{
    /// <summary>
    /// A class that extends an IInteractiveView by adding a page number. Useful
    /// for IBookSection implementations.
    /// </summary>
    public class InteractiveViewWithPage : IInteractiveView
    {
        public InteractiveViewWithPage(IInteractiveView view, int page)
        {
            View = view;
            Page = page;
        }

        public void RenderTo(RenderTexture target) => View.RenderTo(target);
        public IRaycaster GetRaycaster() => View.GetRaycaster();


        public IInteractiveView View { get; private set; }
        public int Page { get; private set; }
    }
}
