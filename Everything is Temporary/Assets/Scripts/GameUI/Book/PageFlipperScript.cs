﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace GameUI
{
    public class PageFlipperScript : MonoBehaviour
    {
        public string triggerLeftToRight = "FlipL2R";
        public string triggerRightToLeft = "FlipR2L";

        /// <summary>
        /// Flips left to right if not currently flipping.
        /// </summary>
        /// <returns>True on success, false if nothing happened.</returns>
        public async Task<bool> FlipLeftToRight()
        {
            if (m_pageFlipSema.CurrentCount <= 0)
                return false;

            // Note: since Unity runs on one thread, we are guaranteed that
            // CurrentCount is still > 0 at this point!

            // Lower the semaphore to indicate that a flip is being performed.
            m_pageFlipSema.Wait();

            gameObject.SetActive(true);

            m_animator.SetTrigger(m_triggerL2R);
            await WaitForPageReady();

            gameObject.SetActive(false);

            return true;
        }

        /// <summary>
        /// Flips right to left if not currently flipping.
        /// </summary>
        /// <returns>True on success, false if nothing happened.</returns>
        public async Task<bool> FlipRightToLeft()
        {
            if (m_pageFlipSema.CurrentCount <= 0)
                return false;

            // Note: since Unity runs on one thread, we are guaranteed that
            // CurrentCount is still > 0 at this point!

            // Lower the semaphore to indicate that a flip is being performed.
            m_pageFlipSema.Wait();

            gameObject.SetActive(true);

            m_animator.SetTrigger(m_triggerR2L);

            await WaitForPageReady();

            gameObject.SetActive(false);

            return true;
        }

        /// <summary>
        /// Waits until the PageReady() signal. Raises the semaphore immediately
        /// after it is downed (so this simply detects when the semaphore was raised).
        /// </summary>
        /// <returns>The for page ready.</returns>
        private async Task WaitForPageReady()
        {
            await m_pageFlipSema.WaitAsync();

            // Raise the semaphore after waiting.
            m_pageFlipSema.Release();
        }

        private void PageReady()
        {
            m_pageFlipSema.Release();
        }

        private void Awake()
        {
            m_pageFlipSema = new SemaphoreSlim(1);
            m_animator = GetComponent<Animator>();

            m_triggerL2R = Animator.StringToHash(triggerLeftToRight);
            m_triggerR2L = Animator.StringToHash(triggerRightToLeft);

            gameObject.SetActive(false);
        }

        private Animator m_animator;
        private int m_triggerL2R;
        private int m_triggerR2L;
        private SemaphoreSlim m_pageFlipSema;
    }
}