﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUI.Book.Tabs
{
    public class TabManager : MonoBehaviour
    {
        [SerializeField]
        private RectTransform m_leftTabsArea = default;

        [SerializeField]
        private TabDisplayScript m_tabPrefab = default;

        public void ChangeSection(IBookSection section)
        {
            // TODO Highlight the corresponding tab.
            // TODO If the section doesn't have a tab, create a temporary tab.
        }

        public void InsertedBookSection(IBookSection section, int currentOverallIndex)
        {
            Debug.Assert(currentOverallIndex >= 0 && currentOverallIndex <= m_sectionList.Count);

            CreateTab(section, currentOverallIndex);
            m_sectionList.Insert(currentOverallIndex, section);
        }

        public void RemovedBookSection(IBookSection section)
        {
            if (m_sectionsToTabs.TryGetValue(section, out TabDisplayScript tabDisplay))
            {
                Destroy(tabDisplay.gameObject);
                m_sectionsToTabs.Remove(section);
                m_sectionList.Remove(section);
            }
        }

        private void CreateTab(IBookSection section, int newIndex)
        {
            TabDisplayScript tab = Instantiate(m_tabPrefab, m_leftTabsArea);

            tab.transform.SetSiblingIndex(newIndex);
            tab.TabName = section.SectionName;
            tab.OnSelect += () =>
            {
                BookScript.Instance.FlipTo(section);
            };

            m_sectionsToTabs[section] = tab;
        }

        private IBookSection m_currentSection;
        private int? m_currentTabIndex;

        private List<IBookSection> m_sectionList = new List<IBookSection>();
        private Dictionary<IBookSection, TabDisplayScript> m_sectionsToTabs = new Dictionary<IBookSection, TabDisplayScript>();
    }
}