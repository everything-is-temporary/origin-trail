﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUI.Book.Tabs
{
    using UI = UnityEngine.UI;
    public class TabDisplayScript : MonoBehaviour
    {
        [SerializeField]
        private UI.Text m_tabName = default;

        /// <summary>
        /// Occurs when the tab is selected.
        /// </summary>
        public event System.Action OnSelect;

        /// <summary>
        /// The name of the tab.
        /// </summary>
        /// <value>The name.</value>
        public string TabName
        {
            get
            {
                Debug.Assert(m_tabName != null);
                return m_tabName.text;
            }

            set
            {
                Debug.Assert(m_tabName != null);
                m_tabName.text = value;
            }
        }

        public void ButtonSelectTab()
        {
            OnSelect?.Invoke();
        }
    }
}