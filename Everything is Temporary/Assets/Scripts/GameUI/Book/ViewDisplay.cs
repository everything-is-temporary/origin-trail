﻿using GameUI.Abstractions;
using GameUtility;
using UnityEngine;

namespace GameUI.Book
{
    /// <summary>
    /// A <see cref="RenderTexture"/> paired with a <see cref="IInteractiveView"/>
    /// that draws into it. If the RenderTexture is null, this will not render
    /// to anything.
    /// </summary>
    public class ViewDisplay
    {
        public IInteractiveView View { get; set; }
        public RenderTexture RenderTexture { get; }

        public ViewDisplay(RenderTexture renderTexture)
        {
            RenderTexture = renderTexture;
        }

        /// <summary>
        /// If the <see cref="View"/> and <see cref="RenderTexture"/> are non-null,
        /// this clears the render texture and renders the view into it.
        /// </summary>
        public void Render()
        {
            if (View != null && RenderTexture != null)
            {
                Clear();
                View.RenderTo(RenderTexture);
            }
        }

        /// <summary>
        /// Clears the <see cref="RenderTexture"/> assuming it is non-null.
        /// </summary>
        public void Clear()
        {
            if (RenderTexture != null)
                GraphicUtils.ClearTexture(RenderTexture);
        }
    }
}
