﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameUI.Abstractions;
using GameUI.Raycasting;

using GameInventory;

namespace GameUI
{
    public class ScrollableInventoryPageScript : MonoBehaviour, IInteractiveView
    {
        [SerializeField]
        [Tooltip("The camera to use to render the page.")]
        private Camera m_camera = default;

        [SerializeField]
        [Tooltip("The raycaster from the page's canvas.")]
        private BaseRaycaster m_raycaster = default;

        [SerializeField]
        private ItemStackDisplayScript m_itemStackDisplayPrefab = default;

        [SerializeField]
        private RectTransform m_itemArea = default;

        public void DisplayInventory(Inventory inventory)
        {
            Clear();
            Setup(inventory);
        }

        #region IInteractiveView implementation
        public IRaycaster GetRaycaster()
        {
            return new RaycasterUnityWrapper(m_raycaster);
        }

        public void RenderTo(RenderTexture target)
        {
            m_camera.targetTexture = target;
            m_camera.Render();
        }
        #endregion

        private void Clear()
        {
            foreach (var pair in m_itemIdToDisplayScript)
                Destroy(pair.Value.gameObject);

            if (m_inventory != null)
            {
                m_inventory.OnItemAmountChanged -= ItemAmountChanged;
                m_inventory = null;
            }
        }

        private void Setup(Inventory inventory)
        {
            m_inventory = inventory;
            foreach (ItemWithAmount stack in m_inventory)
                DisplayStack(stack);

            m_inventory.OnItemAmountChanged += ItemAmountChanged;
        }

        private void DisplayStack(ItemWithAmount itemWithAmount)
        {
            ItemStackDisplayScript displayScript;

            if (!m_itemIdToDisplayScript.TryGetValue(itemWithAmount.ItemReference, out displayScript))
            {
                displayScript = Instantiate(m_itemStackDisplayPrefab, m_itemArea);
                displayScript.DisplayItemStack(itemWithAmount);
                m_itemIdToDisplayScript[itemWithAmount.ItemReference] = displayScript;
            }

            displayScript.DisplayItemStack(itemWithAmount);
        }

        private void ItemAmountChanged(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            if (newAmount.Amount > 0)
            {
                DisplayStack(newAmount);
                return;
            }

            if (m_itemIdToDisplayScript.TryGetValue(newAmount, out ItemStackDisplayScript displayScript))
            {
                Destroy(displayScript.gameObject);
                m_itemIdToDisplayScript.Remove(newAmount);
            }
        }

        private void Awake()
        {
            m_camera.enabled = false;
            m_camera.clearFlags = CameraClearFlags.Nothing;
        }

        private Inventory m_inventory;
        private Dictionary<ItemReference, ItemStackDisplayScript> m_itemIdToDisplayScript
            = new Dictionary<ItemReference, ItemStackDisplayScript>();
    }
}