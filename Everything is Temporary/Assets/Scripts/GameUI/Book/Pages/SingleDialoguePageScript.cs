﻿using Dialogue;
using GameUI.Abstractions;
using GameUI.Raycasting;
using GameUtility;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI
{
    /// <summary>
    /// A book page that can display dialogue and choice elements.
    /// </summary>
    public class SingleDialoguePageScript : MonoBehaviour, IInteractiveView
    {
        [SerializeField]
        private Camera m_camera = default;

        [SerializeField]
        private BaseRaycaster m_raycaster = default;

        [SerializeField]
        [RequiresType(typeof(IDialogueElementDisplay))]
        private UnityEngine.Object m_currentDialogueDisplay = default;

        [SerializeField]
        [Tooltip("A dialogue element displayer prefab for past dialogue elements.")]
        [RequiresType(typeof(IDialogueElementDisplay))]
        private UnityEngine.Object m_historyDialoguePrefab = default;

        [SerializeField]
        [Tooltip("A dialogue element displayer prefab for choices.")]
        [RequiresType(typeof(IChoiceDisplay))]
        private Object m_choiceDisplayPrefab = default;

        [SerializeField]
        private RectTransform m_historyArea = default;

        [SerializeField]
        private RectTransform m_choiceArea = default;

        [SerializeField]
        [Tooltip("The object used to continue the story. This will be disabled" +
        	" when the story should not be continue-able.")]
        private GameObject m_continueButton = default;

        /// <summary>
        /// Makes a new dialogue element the "current" dialogue element, and pushes
        /// the others into history.
        /// </summary>
        /// <param name="element">Element.</param>
        public void PushDialogueElement(DialogueElement element)
        {
            SwitchToHistoryMode();

            PushCurrentElementIntoHistory();

            m_currentDialogueElement = element;
            CurrentDialogueDisplay.StartDisplay(m_currentDialogueElement);
        }

        /// <summary>
        /// Displays a choice.
        /// </summary>
        /// <param name="element">Element.</param>
        public void PushChoicesElement(ChoicesElement element)
        {
            SwitchToChoiceMode();

            ClearChoices();

            foreach (var choice in element.Choices)
            {
                var choiceDisplay = Instantiate(m_choiceDisplayPrefab, m_choiceArea) as IChoiceDisplay;
                choiceDisplay.DisplayChoice(choice.Value);
                choiceDisplay.OnChosen += () => StoryManager.Instance.Proceed(choice.Key);
            }
        }

        public void BeginNewDialogue()
        {
            CurrentDialogueDisplay.StartDisplay(null);
            ClearHistory();
            ClearChoices();

            SwitchToHistoryMode();

            // Disable continue button until dialogue starts.
            m_continueButton?.SetActive(false);
        }

        /// <summary>
        /// Proceeds the story. This function should be called from a UI element
        /// on the page.
        /// </summary>
        public void ProceedStory()
        {
            StoryManager.Instance.Proceed();
        }

        private void SwitchToHistoryMode()
        {
            m_choiceArea.gameObject.SetActive(false);
            m_historyArea.gameObject.SetActive(true);

            m_continueButton?.SetActive(true);
        }

        private void SwitchToChoiceMode()
        {
            m_choiceArea.gameObject.SetActive(true);
            m_historyArea.gameObject.SetActive(false);

            m_continueButton?.SetActive(false);
        }

        /// <summary>
        /// Clears the dialogue history.
        /// </summary>
        private void ClearHistory()
        {
            foreach (Transform child in m_historyArea)
                Destroy(child.gameObject);
        }

        /// <summary>
        /// Clears the choices list.
        /// </summary>
        private void ClearChoices()
        {
            foreach (Transform child in m_choiceArea)
                Destroy(child.gameObject);
        }

        private void PushCurrentElementIntoHistory()
        {
            if (m_currentDialogueElement != null)
            {
                var newHistoryDisplay = Instantiate(m_historyDialoguePrefab, m_historyArea);
                (newHistoryDisplay as Component).transform.SetAsFirstSibling();
                (newHistoryDisplay as IDialogueElementDisplay).StartDisplay(m_currentDialogueElement);
            }
        }

        #region IInteractiveView implementation
        public IRaycaster GetRaycaster()
        {
            return new RaycasterUnityWrapper(m_raycaster);
        }

        public void RenderTo(RenderTexture target)
        {
            m_camera.targetTexture = target;
            m_camera.Render();
        }
        #endregion

        private void Awake()
        {
            m_camera.enabled = false;
            m_camera.clearFlags = CameraClearFlags.Nothing;
            BeginNewDialogue();
        }

        private DialogueElement m_currentDialogueElement;

        private IDialogueElementDisplay CurrentDialogueDisplay => m_currentDialogueDisplay as IDialogueElementDisplay;
    }
}