﻿using GameUI;
using GameUI.Abstractions;
using GameUI.Raycasting;
using GameUtility;
using GameUtility.ObserverPattern;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BookCameraScript : SingletonMonoBehavior<BookCameraScript>, IInteractiveView
{
    public Camera Camera { get; private set; }

    public FocusState TargetFocus
    {
        get => m_targetFocus;

        set
        {
            switch(value)
            {
                case FocusState.None:
                    Unfocus();
                    break;
                case FocusState.Book:
                    FocusBook();
                    break;
                case FocusState.Game:
                    FocusGame();
                    break;
                case FocusState.Unfocus:
                    Unfocus();
                    break;
                case FocusState.Combat:
                    CombatMode();
                    break;
                default:
                    break;
            }
        }
    }

    [SerializeField]
    private AudioClip m_soundOfBookSlidingUp = default;

    [SerializeField]
    private AudioClip m_soundOfBookSlidingDown = default;

    public void FocusBook()
    {
        if (m_targetFocus == FocusState.Book)
            return;
        m_targetFocus = FocusState.Book;

        if (m_moveCoroutine != null)
            StopCoroutine(m_moveCoroutine);

        m_moveCoroutine = StartCoroutine(MoveCameraCR(m_focusBookPosition, Quaternion.Euler(m_focusBookRotation)));

        InputManager.RegisterAsInputHandler(InputType.EscapeKey, HandleEscapeKeyInput);

        GetComponent<MakeSound>().PlayGivenSound(m_soundOfBookSlidingUp);
    }

    public void Unfocus()
    {
        if (m_targetFocus == FocusState.Unfocus)
            return;
        m_targetFocus = FocusState.Unfocus;

        if (m_moveCoroutine != null)
            StopCoroutine(m_moveCoroutine);

        m_moveCoroutine = StartCoroutine(MoveCameraCR(m_unfocusPosition, Quaternion.Euler(m_unfocusRotation)));
    }

    public void FocusGame()
    {
        if (m_targetFocus == FocusState.Game)
            return;
        m_targetFocus = FocusState.Game;

        if (m_moveCoroutine != null)
            StopCoroutine(m_moveCoroutine);

        m_moveCoroutine = StartCoroutine(MoveCameraCR(m_focusGamePosition, Quaternion.Euler(m_focusGameRotation)));

        InputManager.UnregisterAsInputHandler(InputType.EscapeKey, HandleEscapeKeyInput);

        GetComponent<MakeSound>().PlayGivenSound(m_soundOfBookSlidingDown);
    }

    public void CombatMode()
    {
        if (m_targetFocus == FocusState.Combat)
            return;
        m_targetFocus = FocusState.Combat;

        if (m_moveCoroutine != null)
            StopCoroutine(m_moveCoroutine);

        m_moveCoroutine = StartCoroutine(MoveCameraCR(m_combatModePosition, Quaternion.Euler(m_combatModeRotation)));
    }

#if UNITY_EDITOR
    [ContextMenu("Focus Game")]
    public void EditorFocusGame()
    {
        transform.localPosition = m_focusGamePosition;
        transform.localRotation = Quaternion.Euler(m_focusGameRotation);
    }
    [ContextMenu("Unfocus")]
    public void EditorUnfocus()
    {
        transform.localPosition = m_unfocusPosition;
        transform.localRotation = Quaternion.Euler(m_unfocusRotation);
    }
    [ContextMenu("Focus Book")]
    public void EditorFocusBook()
    {
        transform.localPosition = m_focusBookPosition;
        transform.localRotation = Quaternion.Euler(m_focusBookRotation);
    }
    [ContextMenu("Combat Mode")]
    public void EditorCombatMode()
    {
        transform.localPosition = m_combatModePosition;
        transform.localRotation = Quaternion.Euler(m_combatModeRotation);
    }
#endif

    protected override void PostAwake()
    {
        Camera = GetComponent<Camera>();

        Camera.enabled = false;
        Camera.clearFlags = CameraClearFlags.Nothing;
        m_bookViewSlotUnsubscriber = m_bookViewSlot.SetView(this);
    }

    private void Start()
    {
        InputManager.RegisterAsInputHandler(InputType.ToggleBookKey, HandleToggleBookInput);

        FocusBook();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        m_bookViewSlotUnsubscriber.Unsubscribe();
    }

    private IEnumerator MoveCameraCR(Vector3 targetPos, Quaternion targetRot, float duration = 1)
    {
        Vector3 startPos = transform.localPosition;
        Quaternion startRot = transform.localRotation;

        float startTime = Time.fixedTime;

        while (Time.fixedTime < startTime + duration)
        {
            float t = (Time.fixedTime - startTime) / duration;

            transform.localPosition = new Vector3
            { 
                x = Mathf.SmoothStep(startPos.x, targetPos.x, t),
                y = Mathf.SmoothStep(startPos.y, targetPos.y, t),
                z = Mathf.SmoothStep(startPos.z, targetPos.z, t),
            };

            transform.rotation = Quaternion.Slerp(startRot, targetRot, t);

            yield return new WaitForFixedUpdate();
        }

        transform.localPosition = targetPos;
        transform.rotation = targetRot;

        m_moveCoroutine = null;
    }

    /// <summary>
    /// Registered in FocusBook function,
    /// Unregistered in FocusGame function.
    /// </summary>
    /// <returns>Is the input handled?</returns>
    private bool HandleEscapeKeyInput()
    {
        FocusGame();

        return true;
    }

    /// <summary>
    /// Currently this is the only function that handles the Toggle Book input.
    /// This isn't unregistered anywhere,
    /// So it will always handle the Toggle Book input.
    /// </summary>
    /// <returns>Is the input handled?</returns>
    private bool HandleToggleBookInput()
    {
        if (TargetFocus == FocusState.Game)
        {
            FocusBook();
        }
        else
        {
            FocusGame();
        }

        return true;
    }

    void IInteractiveView.RenderTo(RenderTexture target)
    {
        Camera.targetTexture = target;
        Camera.Render();
    }

    IRaycaster IInteractiveView.GetRaycaster()
    {
        return (IRaycaster)m_raycaster;
    }

    [SerializeField]
    private InteractiveViewSlot m_bookViewSlot = default;

    [SerializeField]
    [RequiresType(typeof(IRaycaster))]
    private Object m_raycaster = default;

    [SerializeField] private Vector3 m_focusGamePosition = default;
    [SerializeField] private Vector3 m_focusGameRotation = default;

    [SerializeField] private Vector3 m_focusBookPosition = default;
    [SerializeField] private Vector3 m_focusBookRotation = default;

    [SerializeField] private Vector3 m_unfocusPosition = default;
    [SerializeField] private Vector3 m_unfocusRotation = default;

    [SerializeField] private Vector3 m_combatModePosition = default;
    [SerializeField] private Vector3 m_combatModeRotation = default;

    private IUnsubscriber m_bookViewSlotUnsubscriber;

    private Coroutine m_moveCoroutine;
    private FocusState m_targetFocus = FocusState.None;

    private GameInputManager InputManager => GameInputManager.Instance;

    public enum FocusState
    {
        None, Unfocus, Game, Book, Combat
    }
}
