﻿using System;
using UnityEngine;

namespace GameUI.Book
{
    public readonly struct PageCoordinates
    {
        public enum PageSide
        {
            Left, Right
        }

        public readonly PageSide side;

        /// <summary>
        /// Normalized page coordinates (range [0, 1]).
        /// </summary>
        public readonly Vector2 coords;

        public PageCoordinates(PageSide side, Vector2 pageCoords)
        {
            this.side = side;
            this.coords = pageCoords;
        }
    }
}