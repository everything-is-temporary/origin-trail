﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageFlipController : MonoBehaviour
{
    private bool HandleHorizontalAxisInput(float xInputRaw)
    {
        if (!InputManager.IsPageFlipEnabled) { return false; }

        if (BookCameraScript.Instance.TargetFocus == BookCameraScript.FocusState.Book)
        {
            if (xInputRaw == 1.0f)
            {
                GameUI.BookScript.Instance.FlipToLaterPage();

                return true;
            }
            else if (xInputRaw == -1.0f)
            {
                GameUI.BookScript.Instance.FlipToEarlierPage();

                return true;
            }
        }

        return false;
    }

    private void Start()
    {
        InputManager.RegisterAsHorizontalAxisInputHandler(HandleHorizontalAxisInput, false);
    }

    private void OnDestroy()
    {
        InputManager?.UnregisterAsHorizontalAxisInputHandler(HandleHorizontalAxisInput);
    }

    private GameInputManager InputManager => GameInputManager.Instance;
}
