﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameUI.Abstractions;
using GameUI.Book;
using GameUI.Book.Tabs;
using GameUtility;
using UnityEngine;

namespace GameUI
{
    /// <summary>
    /// The script for controlling the book. To add a section, implement
    /// <see cref="IBookSection"/> and insert it using <see cref="InsertSection(IBookSection, int)"/>.
    /// This section will then be accessible in the book using <see cref="FlipTo(IBookSection, int?)"/>
    /// and <see cref="FlipToPageInSection(int)"/>. A tab is created for the
    /// section automatically.
    /// </summary>
    [RequireComponent(typeof(TabManager))]
    public class BookScript : SingletonMonoBehavior<BookScript>
    {
        /// <summary>
        /// An object that is connected to receive raycasts when the left page
        /// is hit and that will forward the raycasts to the provided
        /// DelegateRaycaster.
        /// </summary>
        [SerializeField]
        private PageRaycastForwarderScript m_leftPageRaycastForwarder = default;

        /// <summary>
        /// An object that is connected to receive raycasts when the right page
        /// is hit and that will forward the raycasts to the provided
        /// DelegateRaycaster.
        /// </summary>
        [SerializeField]
        private PageRaycastForwarderScript m_rightPageRaycastForwarder = default;

        /// <summary>
        /// List of initial sections.
        /// </summary>
        [SerializeField]
        [Tooltip("List of initial sections to display in the book.")]
        [RequiresType(typeof(IBookSection))]
        private List<Object> m_initialSections = default;

        /// <summary>
        /// A script that encapsulates the details of how pages flip.
        /// </summary>
        [SerializeField]
        [Tooltip("Script to use to flip pages. If null, pages won't animate.")]
        private PageFlipperScript m_pageFlipper = null;

        [SerializeField]
        private bool m_animatePageFlipping = true;

        /// <summary>
        /// A prefabricated render texture that is used to create new
        /// render textures (with the same settings). In other words,
        /// this determines the resolution of the pages.
        /// </summary>
        [SerializeField]
        [Tooltip("This determines the resolution of the pages.")]
        private RenderTexture m_sampleRenderTexture = null;

        [SerializeField]
        [Tooltip("The material used by the static pages.")]
        private Material m_staticPageMaterial = null;

        [SerializeField]
        [Tooltip("The material used by the dynamic page.")]
        private Material m_dynamicPageMaterial = null;

        public IBookSection CurrentSection => m_currentSection;

        /// <summary>
        /// Get the total number of book sections.
        /// </summary>
        /// <returns></returns>
        public int GetTotalSections()
        {
            return m_sections.Count();
        }

        /// <summary>
        /// Inserts a new book section at the given index. The index is only
        /// used for ordering purposes. If the section was already in the book,
        /// it is first removed and then re-added at the given index.
        /// </summary>
        /// <param name="section">A book section.</param>
        /// <param name="sectionIndex">The desired index for the section.</param>
        public void InsertSection(IBookSection section, int sectionIndex)
        {
            m_sections.Remove(section);

            if (sectionIndex < 0)
                sectionIndex = 0;
            if (sectionIndex > m_sections.Count)
                sectionIndex = m_sections.Count;

            m_sections.Insert(sectionIndex, section);
            m_tabManager.InsertedBookSection(section, sectionIndex);
        }

        /// <summary>
        /// Inserts a new book section at the end.
        /// If the section was already in the book, it is first removed
        /// and then re-added at the given index.
        /// </summary>
        /// <param name="section">A book section.</param>
        public void InsertSectionAtEnd(IBookSection section)
        {
            m_sections.Remove(section);

            // Don't call m_sections.Count() twice below. It changes!
            int sectionIndex = m_sections.Count();

            m_sections.Insert(sectionIndex, section);
            m_tabManager.InsertedBookSection(section, sectionIndex);
        }

        /// <summary>
        /// Removes a section from the book. Note that if you will destroy this
        /// section immediately after, you probably want to use <see cref="RemoveAndCloseSection(IBookSection)"/>.
        /// </summary>
        /// <param name="section">Section to remove.</param>
        public void RemoveSection(IBookSection section)
        {
            m_sections.Remove(section);
            m_tabManager.RemovedBookSection(section);
        }

        /// <summary>
        /// Removes the section from the book and opens a different section.
        /// </summary>
        /// <param name="section">Section to remove.</param>
        public void RemoveAndCloseSection(IBookSection section)
        {
            int idx = m_sections.IndexOf(section);

            RemoveSection(section);

            if (idx == -1)
            {
                // Section wasn't in the section list, so it was a temporary section.
                FlipTo(m_sections.LastOrDefault());
            }
            else if (idx > 0)
            {
                // Section was in the section list and had at least one section come before it.
                FlipTo(m_sections[idx - 1]);
            }
            else if (m_sections.Count > 0)
            {
                // Section was first in the section list, and there was at least one section after it.
                FlipTo(m_sections[idx]);
            }

            if (m_sections.Count == 0)
                throw new System.NotImplementedException("BookScript does not currently handle having 0 sections.");
        }

        /// <summary>
        /// Flips backward in the book.
        /// </summary>
        public void FlipToEarlierPage()
        {
            if (m_currentSection == null)
                return;

            int page = m_currentSection.GetPageNumber(m_leftPage ?? m_rightPage);

            if (page > m_currentSection.GetMinPage())
                FlipToPageInSection(page - 1);
            else
            {
                // No section comes before this one.
                if (m_sections.Count == 0 || m_sections[0] == m_currentSection)
                    return;

                // Get last nonempty section before current one. If the current section
                // is not in the list of sections, this treats it as if it were the rightmost
                // section.
                IBookSection prevSection = null;
                for (int idx = 0; idx < m_sections.Count; ++idx)
                {
                    // Found current section
                    if (m_sections[idx] == m_currentSection)
                        break;

                    if (!m_sections[idx].IsEmpty())
                        prevSection = m_sections[idx];
                }

                if (prevSection == null)
                    return; // this means all sections were empty

                FlipTo(prevSection, prevSection.GetMaxPage());
            }
        }

        /// <summary>
        /// Flips forward in the book.
        /// </summary>
        public void FlipToLaterPage()
        {
            if (m_currentSection == null)
                return;

            int page = m_currentSection.GetPageNumber(m_rightPage ?? m_leftPage);
            if (page < m_currentSection.GetMaxPage())
                FlipToPageInSection(page + 1);
            else
            {
                // No section comes after this one.
                if (m_sections.Count == 0 || m_sections.Last() == m_currentSection)
                    return;

                // Get first nonempty section after the current one. If the current
                // section is not found in the sections list, it is treated as if
                // it were the rightmost section.
                IBookSection nextSection = null;
                for (int idx = 0; idx < m_sections.Count; ++idx)
                {
                    // Found current section
                    if (m_sections[idx] == m_currentSection)
                    {
                        // Try to find the next section that's not empty.
                        nextSection = m_sections.Skip(idx + 1).FirstOrDefault((section) => !section.IsEmpty());

                        break;
                    }
                }

                if (nextSection == null)
                    return; // this means that we are the right-most nonempty section

                FlipTo(nextSection, nextSection.GetMinPage());
            }
        }

        /// <summary>
        /// Flips to a page of the given section. If a page is not specified,
        /// flips to the "initial" page, which is specified by the section itself.
        /// </summary>
        /// <param name="section">The new section to open.</param>
        /// <param name="whichPage">The page number within the section.</param>
        public void FlipTo(IBookSection section, int? whichPage = null)
        {
            if (section == null || section == m_currentSection || m_isFlipping || section.IsEmpty())
                return;

            m_tabManager.ChangeSection(section);

            int currentPage = whichPage ?? section.GetInitialPage();

            Two<IInteractiveView> pages = section.AcquirePages(currentPage);


            bool flipLeftToRight;
            if (m_currentSection == null)
                flipLeftToRight = false;
            else
            {
                // Determine which is the leftmost section to know whether to flip
                // left-to-right or right-to-left.
                IBookSection leftmost = m_sections.FirstOrDefault((sec) => sec == section || sec == m_currentSection);

                flipLeftToRight = leftmost == section;
            }

            FlipAsync(pages.First, pages.Second, section, flipLeftToRight)
                .FireAndForget();
        }

        /// <summary>
        /// Flips to a page in the current section.
        /// </summary>
        /// <param name="page">The page index.</param>
        public void FlipToPageInSection(int page)
        {
            if (m_currentSection == null || m_isFlipping)
                return;

            Two<IInteractiveView> pages = m_currentSection.AcquirePages(page);

            int currentPage = m_currentSection.GetPageNumber(m_leftPage ?? m_rightPage);

            FlipAsync(pages.First, pages.Second, m_currentSection, leftToRight: page < currentPage)
                .FireAndForget();
        }

        private void Start()
        {
            m_currentLeftTarget = new ViewDisplay(new RenderTexture(m_sampleRenderTexture));
            m_currentRightTarget = new ViewDisplay(new RenderTexture(m_sampleRenderTexture));
            m_nextLeftTarget = new ViewDisplay(new RenderTexture(m_sampleRenderTexture));
            m_nextRightTarget = new ViewDisplay(new RenderTexture(m_sampleRenderTexture));

            // Present the initial page, if any.
            if (m_sections != null && m_sections.Count > 0)
            {
                IBookSection firstNonEmpty = m_sections.FirstOrDefault((section) => !section.IsEmpty());

                if (firstNonEmpty != null)
                    FlipTo(firstNonEmpty);
            }
        }


        /// <summary>
        /// Flip to the new pair of pages, animating left-to-right or right-to-left.
        /// If a flip is currently happening, does nothing.
        /// </summary>
        /// <param name="newLeftPage">New left page.</param>
        /// <param name="newRightPage">New right page.</param>
        /// <param name="newSection">New section. Can be the same as the old one.</param>
        /// <param name="leftToRight">If set to <c>true</c>, flips left to right.
        /// Otherwise flips right to left.</param>
        private async Task FlipAsync(IInteractiveView newLeftPage, IInteractiveView newRightPage,
            IBookSection newSection, bool leftToRight)
        {
            if (m_isFlipping)
                return;
            m_isFlipping = true;

            DisableInteraction();

            SetDrawToNext(newLeftPage, newRightPage);

            SetDynamicRight(leftToRight ? m_nextRightTarget.RenderTexture : m_currentRightTarget.RenderTexture);
            SetDynamicLeft(leftToRight ? m_currentLeftTarget.RenderTexture : m_nextLeftTarget.RenderTexture);

            // NOTE: If the static page changes texture before the dynamic page covers it, there will be trouble!
            if (leftToRight)
            {
                SetStaticLeft(m_nextLeftTarget.RenderTexture);
                if (m_pageFlipper != null && m_animatePageFlipping)
                    await m_pageFlipper.FlipLeftToRight();
                SetStaticRight(m_nextRightTarget.RenderTexture);
            }
            else
            {
                SetStaticRight(m_nextRightTarget.RenderTexture);
                if (m_pageFlipper != null && m_animatePageFlipping)
                    await m_pageFlipper.FlipRightToLeft();
                SetStaticLeft(m_nextLeftTarget.RenderTexture);
            }

            // NOTE: We do this REGARDLESS of whether the page flip was animated.

            SwapTargets();
            SetNewPages(newLeftPage, newRightPage, newSection);

            // Since flipping is done, the 'next' targets should no longer be rendered.
            m_nextLeftTarget.View = null;
            m_nextRightTarget.View = null;

            EnableInteraction();

            GetComponent<MakeSound>().PlaySound();

            m_isFlipping = false;
            return;
        }

        /// <summary>
        /// Disables mouse input to the book.
        /// </summary>
        private void DisableInteraction()
        {
            m_leftPageRaycastForwarder.DelegateRaycaster = null;
            m_rightPageRaycastForwarder.DelegateRaycaster = null;
        }

        /// <summary>
        /// Enables mouse interaction with the current m_leftPage and m_rightPage.
        /// </summary>
        private void EnableInteraction()
        {

            m_leftPageRaycastForwarder.DelegateRaycaster = m_leftPage?.GetRaycaster();
            m_rightPageRaycastForwarder.DelegateRaycaster = m_rightPage?.GetRaycaster();
        }

        private void SetDynamicLeft(RenderTexture texture)
        {
            m_dynamicPageMaterial.SetTexture("_LeftPageImage", texture);
        }

        private void SetDynamicRight(RenderTexture texture)
        {
            m_dynamicPageMaterial.SetTexture("_RightPageImage", texture);
        }

        private void SetStaticLeft(RenderTexture texture)
        {
            m_staticPageMaterial.SetTexture("_LeftPageImage", texture);
        }

        private void SetStaticRight(RenderTexture texture)
        {
            m_staticPageMaterial.SetTexture("_RightPageImage", texture);
        }

        /// <summary>
        /// Sets the render targets on the given views to the "next" render
        /// targets (<see cref="m_nextLeftTarget"/> and <see cref="m_nextRightTarget"/>).
        /// </summary>
        /// <param name="left">Left.</param>
        /// <param name="right">Right.</param>
        private void SetDrawToNext(IInteractiveView left, IInteractiveView right)
        {
            m_nextLeftTarget.View = left;
            m_nextRightTarget.View = right;

            if (left != null)
                m_nextLeftTarget.Clear();

            if (right == null)
                m_nextRightTarget.Clear();
        }

        /// <summary>
        /// Releases the old pages resetting their render targets, and sets
        /// <see cref="m_leftPage"/> and <see cref="m_rightPage"/> to
        /// <paramref name="left"/> and <paramref name="right"/> respectively.
        /// </summary>
        /// <param name="left">New left page.</param>
        /// <param name="right">New right page.</param>
        private void SetNewPages(IInteractiveView left, IInteractiveView right, IBookSection newSection)
        {
            // Release pages.
            if (m_leftPage != null) m_currentSection?.ReleasePage(m_leftPage);
            if (m_rightPage != null) m_currentSection?.ReleasePage(m_rightPage);

            m_leftPage = left;
            m_rightPage = right;
            m_currentSection = newSection;
        }

        /// <summary>
        /// Swaps the "current" ViewDisplays with the "next" ViewDisplays.
        /// </summary>
        private void SwapTargets()
        {
            ViewDisplay temp;

            temp = m_currentRightTarget;
            m_currentRightTarget = m_nextRightTarget;
            m_nextRightTarget = temp;

            temp = m_currentLeftTarget;
            m_currentLeftTarget = m_nextLeftTarget;
            m_nextLeftTarget = temp;
        }

        protected override void PostAwake()
        {
            m_tabManager = GetComponent<TabManager>();
            m_sections = new List<IBookSection>();

            // Initialize list of sections using initial sections provided.
            foreach (Object sectionObject in m_initialSections)
            {
                if (sectionObject is IBookSection section)
                    InsertSection(section, GetTotalSections());
            }
        }

        private void LateUpdate()
        {
            // Render pages in LateUpdate() to ensure all updates happen first.
            m_currentLeftTarget.Render();
            m_currentRightTarget.Render();
            m_nextLeftTarget.Render();
            m_nextRightTarget.Render();
        }

        /// <summary>
        /// List of all book sections.
        /// </summary>
        private List<IBookSection> m_sections;

        /// <summary>
        /// The current book section. Not necessarily in the list <see cref="m_sections"/>.
        /// </summary>
        private IBookSection m_currentSection;
        private IInteractiveView m_leftPage;
        private IInteractiveView m_rightPage;

        private ViewDisplay m_currentRightTarget;
        private ViewDisplay m_currentLeftTarget;
        private ViewDisplay m_nextRightTarget;
        private ViewDisplay m_nextLeftTarget;

        private bool m_isFlipping = false;

        private TabManager m_tabManager;
    }
}