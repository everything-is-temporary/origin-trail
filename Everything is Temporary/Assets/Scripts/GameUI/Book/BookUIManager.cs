﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUtility;
using Dialogue;
using GameInventory;
using System.Threading.Tasks;

namespace GameUI
{
    /// <summary>
    /// Singleton class for controlling what shows up in the book.
    /// </summary>
    public class BookUIManager : SingletonMonoBehavior<BookUIManager>
    {
        [SerializeField]
        private BookCameraScript m_bookCamera = default;

        [ContextMenu("Focus Book")]
        public void FocusBook() => m_bookCamera.FocusBook();

        [ContextMenu("Focus Game")]
        public void FocusGame() => m_bookCamera.FocusGame();

        [ContextMenu("Unfocus")]
        public void Unfocus() => m_bookCamera.Unfocus();

        [ContextMenu("Combat Mode")]
        public void CombatMode() => m_bookCamera.CombatMode();

        private void Start()
        {
            CoreGameManager.Instance.SetupGameSceneActions.Add(() =>
            {
                FocusGame();

                return Task.CompletedTask;
            });
        }
    }
}