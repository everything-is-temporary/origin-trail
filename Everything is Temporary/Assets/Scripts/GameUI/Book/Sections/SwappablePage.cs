﻿using UnityEngine;
using UnityEngine.EventSystems;
using GameUtility;
using GameUI.Abstractions;
using GameUI.Raycasting;

namespace GameUI
{
    /// <summary>
    /// An IInteractiveView implementation that can display other IInteractiveViews.
    /// </summary>
    public class SwappablePage : IInteractiveView
    {
        public SwappablePage()
        {
            m_raycaster = new DelegateRaycaster();
        }

        public SwappablePage(IInteractiveView page)
        {
            m_currentPage = page;
            m_raycaster = new DelegateRaycaster(m_currentPage.GetRaycaster());
        }

        public void SetPage(IInteractiveView newPage)
        {
            if (m_currentPage == newPage)
                return;

            m_currentPage = newPage;
            m_raycaster.Delegate = m_currentPage.GetRaycaster();
        }

        #region IInteractiveView implementation
        public IRaycaster GetRaycaster()
        {
            return m_raycaster;
        }

        public void RenderTo(RenderTexture target)
        {
            m_currentPage?.RenderTo(target);
        }
        #endregion

        private IInteractiveView m_currentPage;
        private DelegateRaycaster m_raycaster;
    }
}