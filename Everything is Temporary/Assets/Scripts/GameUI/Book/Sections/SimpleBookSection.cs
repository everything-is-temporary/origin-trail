﻿using UnityEngine;
using System.Collections;
using GameUtility;
using GameUI.Abstractions;

namespace GameUI
{
    /// <summary>
    /// A simple book section consisting of just two pages (left and right).
    /// Only allows acquiring one copy of any page, returning null if a page
    /// has not yet been released by the last acquirer.
    /// </summary>
    public class SimpleBookSection : MonoBehaviour, IBookSection
    {
        [SerializeField]
        [RequiresType(typeof(IInteractiveView))]
        private Object m_leftPageReference = default;

        [SerializeField]
        [RequiresType(typeof(IInteractiveView))]
        private Object m_rightPageReference = default;

        [SerializeField]
        private string m_sectionName = "Section";


        void Awake()
        {
            IInteractiveView leftPage = m_leftPageReference as IInteractiveView;
            IInteractiveView rightPage = m_rightPageReference as IInteractiveView;

            if (rightPage == leftPage)
            {
                Debug.LogWarning($"{gameObject.name} has the same object as" +
                        " both the left page and the right page. The right page" +
                        " will be set to null.");

                rightPage = null;
            }

            m_leftPageNumbered = new InteractiveViewWithPage(leftPage, 0);
            m_rightPageNumbered = new InteractiveViewWithPage(rightPage, 1);
        }

        public string SectionName => m_sectionName;

        public Two<IInteractiveView> AcquirePages(int containingPage)
        {
            Debug.Assert(containingPage == 0 || containingPage == 1);

            // Return null for pages that have already been acquired and have
            // not been released.
            var pages = new Two<IInteractiveView>(
                m_leftAcquired ? null : m_leftPageNumbered,
                m_rightAcquired ? null : m_rightPageNumbered);

            m_leftAcquired = true;
            m_rightAcquired = true;

            return pages;
        }

        public int GetInitialPage()
        {
            return 0;
        }

        public int GetMinPage()
        {
            return 0;
        }

        public int GetMaxPage()
        {
            return 1;
        }

        public int GetPageNumber(IInteractiveView page)
        {
            Debug.Assert(page is InteractiveViewWithPage);

            return (page as InteractiveViewWithPage).Page;
        }

        public void ReleasePage(IInteractiveView page)
        {
            if (page == m_leftPageNumbered)
                m_leftAcquired = false;
            else if (page == m_rightPageNumbered)
                m_rightAcquired = false;
        }

        private bool m_leftAcquired = false;
        private bool m_rightAcquired = false;

        private InteractiveViewWithPage m_leftPageNumbered;
        private InteractiveViewWithPage m_rightPageNumbered;
    }
}