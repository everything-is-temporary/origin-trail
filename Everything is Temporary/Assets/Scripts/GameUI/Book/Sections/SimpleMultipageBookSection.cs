﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameUtility;
using GameUI.Abstractions;

namespace GameUI
{
    /// <summary>
    /// A simple book section consisting of multiple predefined pages.
    /// </summary>
    public class SimpleMultipageBookSection : MonoBehaviour, IBookSection
    {
        [SerializeField]
        private string m_sectionName = "Section";

        [SerializeField]
        private List<PagePair> m_pages = default;

        void Awake()
        {
            m_numberedPages = new List<NumberedPage<IInteractiveView>>();

            int pageIdx = 0;
            foreach (var pair in m_pages)
            {
                m_numberedPages.Add(new NumberedPage<IInteractiveView>(pair.LeftPage, pageIdx++));
                m_numberedPages.Add(new NumberedPage<IInteractiveView>(pair.RightPage, pageIdx++));
            }
        }

        public string SectionName => m_sectionName;

        public Two<IInteractiveView> AcquirePages(int containingPage)
        {
            Debug.Assert(containingPage >= GetMinPage() && containingPage <= GetMaxPage());

            int leftPage = containingPage & (~1); // round down to even #
            int rightPage = leftPage + 1;

            return new Two<IInteractiveView>(
                m_numberedPages[leftPage].TryAcquire(),
                m_numberedPages[rightPage].TryAcquire());
        }

        public int GetInitialPage()
        {
            return 0;
        }

        public int GetMinPage()
        {
            return 0;
        }

        public int GetMaxPage()
        {
            return m_numberedPages.Count - 1;
        }

        public int GetPageNumber(IInteractiveView page)
        {
            Debug.Assert(page is NumberedPage<IInteractiveView>);

            return (page as NumberedPage<IInteractiveView>).PageNumber;
        }

        public void ReleasePage(IInteractiveView page)
        {
            (page as NumberedPage<IInteractiveView>).IsAcquired = false;
        }

        private List<NumberedPage<IInteractiveView>> m_numberedPages;

        [System.Serializable]
        private class PagePair
        {
            public IInteractiveView LeftPage => m_leftPageReference as IInteractiveView;
            public IInteractiveView RightPage => m_rightPageReference as IInteractiveView;

            [SerializeField]
            [RequiresType(typeof(IInteractiveView))]
            private Object m_leftPageReference = default;

            [SerializeField]
            [RequiresType(typeof(IInteractiveView))]
            private Object m_rightPageReference = default;
        }
    }
}