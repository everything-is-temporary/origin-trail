﻿using System;
using UnityEngine;
using GameUtility;
using GameUI.Abstractions;

namespace GameUI
{
    using NumberedSwappablePage = NumberedPage<SwappablePage>;

    /// <summary>
    /// A simple section with two pages that can be swapped out programmatically.
    /// </summary>
    public class SwappablePageSection : MonoBehaviour, IBookSection
    {
        [SerializeField]
        private string m_sectionName = default;

        public string SectionName => m_sectionName;

        public void SetLeftPage(IInteractiveView newPage)
        {
            m_leftPage.PageScript.SetPage(newPage);
        }

        public void SetRightPage(IInteractiveView newPage)
        {
            m_rightPage.PageScript.SetPage(newPage);
        }

        #region IBookSection implementation
        public Two<IInteractiveView> AcquirePages(int containingPage)
        {
            Debug.Assert(containingPage == 0 || containingPage == 1);

            // Return null for pages that have already been acquired and have
            // not been released.
            var pages = new Two<IInteractiveView>(
                m_leftPage.TryAcquire(),
                m_rightPage.TryAcquire());

            return pages;
        }

        public int GetInitialPage()
        {
            return 0;
        }

        public int GetMinPage()
        {
            return 0;
        }

        public int GetMaxPage()
        {
            return 1;
        }

        public int GetPageNumber(IInteractiveView page)
        {
            return (page as NumberedSwappablePage).PageNumber;
        }

        public void ReleasePage(IInteractiveView page)
        {
            (page as NumberedSwappablePage).IsAcquired = false;
        }
        #endregion

        private void Awake()
        {
            m_leftPage = new NumberedSwappablePage(new SwappablePage(), 0);
            m_rightPage = new NumberedSwappablePage(new SwappablePage(), 1);
        }

        private NumberedSwappablePage m_leftPage;
        private NumberedSwappablePage m_rightPage;
    }
}
