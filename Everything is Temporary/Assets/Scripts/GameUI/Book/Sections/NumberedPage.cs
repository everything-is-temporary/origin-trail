﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using GameUI.Abstractions;
using GameUI.Raycasting;

namespace GameUI
{
    /// <summary>
    /// A helper class for IBookSections. This is a numbered IInteractiveView
    /// that also knows whether or not it has been acquired. Use it to wrap
    /// your implementation of IInteractiveView when you store it in your
    /// IBookSection.
    /// </summary>
    public class NumberedPage<T> : IInteractiveView where T : IInteractiveView
    {
        public T PageScript { get; private set; }
        public int PageNumber { get; private set; }
        public bool IsAcquired { get; set; }

        public NumberedPage(T script, int number)
        {
            PageScript = script;
            PageNumber = number;
            IsAcquired = false;
        }

        /// <summary>
        /// Helper method to acquire this page if and only if it has not
        /// been acquired yet. This will return null if the page was
        /// previously acquired. It will also acquire the page.
        /// <para>This is useful for implementing <see cref="IBookSection.AcquirePages(int)"/>.</para>
        /// </summary>
        /// <returns>The page if it was not previously acquired, or null.</returns>
        public NumberedPage<T> TryAcquire()
        {
            if (IsAcquired)
                return null;
            else
            {
                IsAcquired = true;
                return this;
            }
        }

        public void RenderTo(RenderTexture target) => PageScript?.RenderTo(target);

        public IRaycaster GetRaycaster() => PageScript?.GetRaycaster();

    }
}
