﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using GameUI.Abstractions;
using GameUI.Raycasting;

namespace GameUI
{
    /// <summary>
    /// Simple implementation of IInteractiveView that uses a Camera and
    /// a BaseRaycaster directly. This is useful for creating interactive 
    /// views by hand in Unity.
    /// </summary>
    public class SimpleInteractiveView : MonoBehaviour, IInteractiveView
    {
        [SerializeField] private Camera m_camera = default;
        [SerializeField] private BaseRaycaster m_raycaster = default;

        public IRaycaster GetRaycaster()
        {
            return new RaycasterUnityWrapper(m_raycaster);
        }

        public void RenderTo(RenderTexture target)
        {
            m_camera.targetTexture = target;
            m_camera.Render();
        }

        protected virtual void Awake()
        {
            m_camera.enabled = false;
            m_camera.clearFlags = CameraClearFlags.Nothing;
        }
    }
}