﻿using GameUtility;
using GameUI.Abstractions;

namespace GameUI
{
    /// <summary>
    /// A section in the book. When implementing this interface, consider
    /// using <see cref="NumberedPage{T}"/> to help you keep track of the
    /// information needed for <see cref="AcquirePages(int)"/> and
    /// <see cref="GetPageNumber(IInteractiveView)"/>.
    /// </summary>
    public interface IBookSection
    {
        /// <summary>
        /// The name of the book section, to be displayed on its corresponding tab.
        /// </summary>
        string SectionName { get; }

        /// <summary>
        /// Acquires two interactive views that represent the pair of pages
        /// that contains <paramref name="containingPage"/>. The resources
        /// used by an interactive view that is returned will not be recycled
        /// or released until <see cref="ReleasePage(IInteractiveView)"/> is called.
        /// Those interactive views will also not be returned by any other call
        /// to AcquirePages until ReleasePage is called for them.
        /// 
        /// <para>This must not return null, but may return new Two(null, null).</para>
        /// </summary>
        /// <returns>The two pages (including <paramref name="containingPage"/>).</returns>
        /// <param name="containingPage">The index of one of the pages to be returned.</param>
        Two<IInteractiveView> AcquirePages(int containingPage);

        /// <summary>
        /// Allows the resources associated with the page to be released.
        /// </summary>
        /// <param name="page">The page whose resources may be released.</param>
        void ReleasePage(IInteractiveView page);

        /// <summary>
        /// This method is used by <see cref="BookScript"/> to determine which
        /// pair of pages to show when the user flips from a different section
        /// to this one.
        /// </summary>
        /// <returns>The initial page to show.</returns>
        int GetInitialPage();

        /// <summary>
        /// The smallest page number present in the section.
        /// </summary>
        /// <returns>The smallest page number.</returns>
        int GetMinPage();

        /// <summary>
        /// The largest page number present in the section.
        /// </summary>
        /// <returns>The largest page number.</returns>
        int GetMaxPage();

        /// <summary>
        /// Gets the page number of an acquired page.
        /// </summary>
        /// <returns>The page number.</returns>
        /// <param name="page">The page.</param>
        int GetPageNumber(IInteractiveView page);
    }

    public static class BookSectionExt
    {
        public static bool IsEmpty(this IBookSection section)
        {
            return section.GetMaxPage() < section.GetMinPage();
        }
    }
}
