﻿using System;
using UnityEngine;
using GameUI.Raycasting;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace GameUI.Book
{
    /// <summary>
    /// A placeholder for an <see cref="IRaycaster"/> that will forward the Raycast()
    /// call to a given raycaster. This is used by the <see cref="BookScript"/> to forward
    /// raycasts to page canvases.
    /// </summary>
    public class PageRaycastForwarderScript : MonoBehaviour, IRaycaster
    {
        // TODO: Maybe use slot pattern (like IInteractiveViewSlot) for PageRaycastForwarderScript?

        public IRaycaster DelegateRaycaster { get; set; }

        public void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            Vector2 oldPosition = eventData.position;

            // oldPosition is UV coordinates on the book
            PageCoordinates coords = ToPageCoordinates(oldPosition);

            eventData.position = coords.coords;
            DelegateRaycaster?.Raycast(eventData, resultAppendList);
            eventData.position = oldPosition;
        }

        private PageCoordinates ToPageCoordinates(Vector2 uvBook)
        {
            Vector2 pageSpace = new Vector2(2 * uvBook.x, uvBook.y);

            if (pageSpace.x < 1)
                return new PageCoordinates(PageCoordinates.PageSide.Left, pageSpace);
            else
                return new PageCoordinates(PageCoordinates.PageSide.Right, pageSpace - Vector2.right);
        }
    }
}
