﻿using System;
using GameInventory;
using GameUI.Abstractions;
using UnityEngine;

namespace GameUI
{
    using UI = UnityEngine.UI;

    /// <summary>
    /// A script that should be placed on prefab UI elements that can display
    /// item stacks. Any of the serialized fields may be null (which just means the
    /// corresponding information is not displayed).
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class ItemStackDisplayScript : MonoBehaviour, IItemStackDisplay
    {
        [SerializeField] private UI.Image m_icon = default;
        [SerializeField] private UI.Text m_amount = default;
        [SerializeField] private UI.Text m_name = default;
        [SerializeField] private UI.Text m_description = default;
        [SerializeField] private UI.Text m_flavorText = default;

        [SerializeField] private GameObject m_tagIcons = default;
        [SerializeField] private UnityEngine.Object m_tagIconPrefab = default;

        [SerializeField]
        [Tooltip("This is enabled or disabled to 'highlight' the itemWithAmount.")]
        private GameObject m_highlighter = default;

        /// <summary>
        /// Occurs when the itemWithAmount is selected by the player.
        /// </summary>
        /// <remarks>
        /// This can be in the form of clicking on the whole itemWithAmount display,
        /// clicking a button dedicated for selection, etc.
        /// </remarks>
        public event Action<ItemWithAmount> OnItemStackSelected;

        public bool Highlighted
        {
            get => m_highlighted;
            set
            {
                m_highlighted = value;

                if (m_highlighter != null)
                    m_highlighter.SetActive(m_highlighted);
            }
        }

        public RectTransform RectTransform { get; private set; }

        public void DisplayItemStack(ItemWithAmount itemWithAmount)
        {
            Clear();

            m_displayedItemWithAmount = itemWithAmount;

            InventoryItem item = itemWithAmount.Item;

            if (m_icon != null)
            {
                m_icon.sprite = item.Icon;
                m_icon.color = new Color(1f, 1f, 1f, 1f);
            }

            SetAmountText(itemWithAmount.Amount);

            if (m_name != null)
            {
                m_name.text = item.Name;
            }

            if (m_description != null)
            {
                m_description.text = item.DescriptionShort;
            }

            if (m_flavorText != null)
            {
                m_flavorText.text = item.FlavorText;
            }

            if (m_tagIcons != null)
            {
                foreach (ItemTag itemTag in item.Tags)
                {
                    Instantiate(m_tagIconPrefab, m_tagIcons.transform);
                }
            }
        }

        public void UpdateAmount(int amount)
        {
            // Update myself.
            SetAmountText(amount);
            m_displayedItemWithAmount = m_displayedItemWithAmount.WithAmount(amount);
        }

        public void Clear()
        {
            m_displayedItemWithAmount = null;

            if (m_icon != null)
            {
                m_icon.sprite = null;
                m_icon.color = new Color(1f, 1f, 1f, 0f);
            }

            if (m_name != null)
            {
                m_name.text = "";
            }

            if (m_amount != null)
            {
                m_amount.text = "";
            }

            if (m_description != null)
            {
                m_description.text = "";
            }

            if (m_flavorText != null)
            {
                m_flavorText.text = "";
            }

            if (m_tagIcons != null)
            {
                foreach (Transform child in m_tagIcons.transform)
                {
                    Destroy(child.gameObject);
                }
            }
        }

        /// <summary>
        /// Called by a UI component to raise the OnItemStackSelected event.
        /// </summary>
        public void SelectItemStack()
        {
            if (m_displayedItemWithAmount != null)
                OnItemStackSelected?.Invoke(m_displayedItemWithAmount);
        }

        private void SetAmountText(int amount)
        {
            if (m_amount != null)
                m_amount.text = $"{amount}";

            if (m_highlighter != null)
                m_highlighter.SetActive(false);
        }

        private void Awake()
        {
            RectTransform = GetComponent<RectTransform>();
            Clear();
        }

        private ItemWithAmount m_displayedItemWithAmount;
        private bool m_highlighted;
    }
}
