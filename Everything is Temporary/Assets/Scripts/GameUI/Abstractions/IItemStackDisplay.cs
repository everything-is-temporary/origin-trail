﻿using System;
using GameInventory;

namespace GameUI.Abstractions
{
    /// <summary>
    /// Something that can display an ItemWithAmount and possibly raise an event when
    /// selected.
    /// </summary>
    public interface IItemStackDisplay
    {
        /// <summary>
        /// Occurs when the itemWithAmount is selected by the player.
        /// </summary>
        /// <remarks>
        /// This can be in the form of clicking on the whole itemWithAmount display,
        /// clicking a button dedicated for selection, etc.
        /// </remarks>
        event Action<ItemWithAmount> OnItemStackSelected;

        /// <summary>
        /// Displays the given itemWithAmount.
        /// </summary>
        /// <param name="itemWithAmount">ItemWithAmount to display.</param>
        void DisplayItemStack(ItemWithAmount itemWithAmount);

        /// <summary>
        /// Whether the itemWithAmount should be highlighted in some way.
        /// </summary>
        bool Highlighted { get; set; }
    }

    public static class ItemStackDisplayExtensions
    {
        /// <summary>
        /// Displays the item with the amount.
        /// </summary>
        /// <param name="itemRef">Item reference.</param>
        /// <param name="amount">Amount.</param>
        public static void DisplayItemStack(this IItemStackDisplay display, ItemReference itemRef, int amount)
        {
            display.DisplayItemStack(new ItemWithAmount(itemRef, amount));
        }
    }
}
