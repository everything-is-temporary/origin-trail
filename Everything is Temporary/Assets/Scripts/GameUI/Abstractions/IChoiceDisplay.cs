﻿using System;
using Dialogue;

namespace GameUI.Abstractions
{
    using UI = UnityEngine.UI;

    public interface IChoiceDisplay
    {
        /// <summary>
        /// Occurs when the player selects this choice.
        /// </summary>
        event Action OnChosen;

        /// <summary>
        /// Displays the choice.
        /// </summary>
        /// <param name="choiceDescription">Choice description.</param>
        void DisplayChoice(DialogueElement choiceDescription);

        /// <summary>
        /// Sets the choice as the selected UI element.
        /// </summary>
        void SetSelected();

        void SubmitChoice();

        UI.Selectable Selectable { get; }

        UI.Navigation Navigation { get; set; }
    }
}
