﻿using System;
using GameUI.Raycasting;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameUI.Abstractions
{
    /// <summary>
    /// Interface implemented by anything that can draw to a RenderTexture
    /// and (optionally) perform raycasts.
    /// </summary>
    public interface IInteractiveView
    {
        /// <summary>
        /// Renders to the given texture. Implementations of this through a Camera
        /// object should set the Camera's clear flags to Nothing.
        /// </summary>
        /// <param name="target">Texture to which to render. If this is null, the
        /// object will render to the screen.</param>
        void RenderTo(RenderTexture target);

        /// <summary>
        /// Returns a raycaster or null. This can be a raycaster on a
        /// Canvas, for example. This should always return the same value.
        /// The position in the PointerEventData passed to <see cref="IRaycaster.Raycast"/>
        /// is given in the coordinates of the RenderTexture given to this
        /// interactive view ([0, width) x [0, height)).
        /// </summary>
        /// <returns>The raycaster to use to determine where to send
        /// mouse events, or null is this view shouldn't be interacted
        /// with.</returns>
        IRaycaster GetRaycaster();
    }
}
