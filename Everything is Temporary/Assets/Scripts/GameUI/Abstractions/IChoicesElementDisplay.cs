﻿using System;
using UnityEngine.UI;
using Dialogue;

namespace GameUI.Abstractions
{
    public interface IChoicesElementDisplay
    {
        event Action<int> OnChoice;

        void Display(ChoicesElement element);
    }
}
