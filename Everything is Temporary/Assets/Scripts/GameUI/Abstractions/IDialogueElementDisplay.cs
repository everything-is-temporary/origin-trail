﻿using System;
using Dialogue;

namespace GameUI.Abstractions
{
    public interface IDialogueElementDisplay
    {
        bool CanContinue { get; set; }

        bool IsAnimating { get; }
        
        void StartDisplay(DialogueElement element);

        void CompleteDisplay();
    }
}
