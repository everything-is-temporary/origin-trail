﻿using System;
using GameInventory;

namespace GameUI.Abstractions
{
    public interface IInventoryDisplay
    {
        /// <summary>
        /// Occurs when an itemWithAmount is selected by the user.
        /// </summary>
        event Action<ItemWithAmount> OnItemDisplaySelected;

        /// <summary>
        /// The inventory that is currently being displayed.
        /// </summary>
        /// <value>The displayed inventory, or null if no inventory is being displayed.</value>
        Inventory DisplayedInventory { get; set; }

        /// <summary>
        /// Highlights the item if it is in the inventory.
        /// </summary>
        /// <param name="item">Item to highlight.</param>
        void HighlightItem(ItemReference item);

        /// <summary>
        /// Unhighlights the item if it is in the inventory.
        /// </summary>
        /// <param name="item">Item to highlight.</param>
        void UnhighlightItem(ItemReference item);
    }
}
