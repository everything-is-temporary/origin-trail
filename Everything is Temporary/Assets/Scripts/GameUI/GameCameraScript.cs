﻿using GameUI.Abstractions;
using GameUI.Raycasting;
using GameUtility;
using GameUtility.ObserverPattern;
using UnityEngine;

namespace GameUI
{
    [RequireComponent(typeof(Camera))]
    public class GameCameraScript : MonoBehaviour, IInteractiveView
    {
        [SerializeField]
        private InteractiveViewSlot m_gameViewSlot = default;

        [SerializeField]
        [RequiresType(typeof(IRaycaster))]
        private Object m_raycaster = default;

        private void Start()
        {
            m_gameViewSlotToken = m_gameViewSlot.SetView(this);
        }

        private void OnDestroy()
        {
            m_gameViewSlotToken.Unsubscribe();
        }

        private void Awake()
        {
            m_camera = GetComponent<Camera>();

            Debug.Assert(m_camera != null);

            m_camera.enabled = false;
            m_camera.clearFlags = CameraClearFlags.Nothing;
        }


        private void OnValidate()
        {
            string title = $"{nameof(GameCameraScript)} '{name}'";

            if (m_gameViewSlot == null)
                Debug.LogError($"{title} doesn't have its Game View Slot property set.");
            if (m_raycaster == null)
                Debug.LogWarning($"{title} doesn't have its Raycaster property set. No mouse input will happen.");
        }

        public void RenderTo(RenderTexture target)
        {
            m_camera.targetTexture = target;
            m_camera.Render();
        }

        public IRaycaster GetRaycaster()
        {
            return (IRaycaster)m_raycaster;
        }

        private IUnsubscriber m_gameViewSlotToken;
        private Camera m_camera;
    }
}