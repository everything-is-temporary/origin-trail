﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameUI.Book;
using GameUI.Abstractions;
using GameUtility;
using GameInventory;
using Characters;

namespace GameUI.InventoryUI
{
    using UI = UnityEngine.UI;

    public class ScrollableInventorySectionController : MonoBehaviour
    {
        [SerializeField]
        [RequiresType(typeof(IBookSection))]
        private Object m_bookSection = default;

        [SerializeField]
        [RequiresType(typeof(IInventoryDisplay))]
        private Object m_inventoryDisplay = default;

        [SerializeField]
        [RequiresType(typeof(IItemStackDisplay))]
        private Object m_itemStackDisplay = default;

        [SerializeField]
        private CharacterAvatarScript m_characterAvatarPrefab = default;

        [SerializeField]
        private GameObject m_characterAvatars = default;

        private IBookSection BookSection => (IBookSection)m_bookSection;
        private IInventoryDisplay InventoryDisplay => (IInventoryDisplay)m_inventoryDisplay;
        private IItemStackDisplay ItemStackDisplay => (IItemStackDisplay)m_itemStackDisplay;

        public void DisplayConvoyInventory()
        {
            DisplayInventory(InventoryManager.Instance.ConvoyInventory);
        }
        
        private void Start()
        {
            // TODO: BookScript: Maybe we can have a InsertSectionBeforeMainMenu function in BookScript?
            BookScript.Instance.InsertSection(BookSection, BookScript.Instance.GetTotalSections() - 1);

            // Set up initial inventories.

            AddInventory(InventoryManager.Instance.ConvoyInventory);

            foreach (CharacterReference charRef in CharacterManager.Instance.CurrentParty)
            {
                HandleOnAddNewPartyMember(charRef);
            }

            // Character events

            CharacterManager.Instance.OnAddNewPartyMember += (newCharRef) => HandleOnAddNewPartyMember(newCharRef);

            // Input events

            InventoryDisplay.OnItemDisplaySelected += HandleOnItemStackSelected;
        }

        private void HandleOnAddNewPartyMember(CharacterReference newCharRef)
        {
            MakeNewCharacterAvatar(newCharRef);

            Inventory newCharInventory = newCharRef.Character.Inventory;
            if (newCharInventory != null)
            {
                AddInventory(newCharInventory);
            }
            else
            {
                Debug.LogError("The new character doesn't have an inventory!");
            }
        }

        private void HandleOnCharacterAvatarClicked(CharacterAvatarScript clickedAvatar)
        {
            Inventory charInventory = clickedAvatar.CharRef.Character.Inventory;
            if (charInventory != null)
            {
                DisplayInventory(charInventory);
            }
            else
            {
                Debug.LogError("This character doesn't have an inventory!");
            }
        }

        private void HandleOnItemStackSelected(ItemWithAmount selectedItem)
        {
            ItemStackDisplay.DisplayItemStack(selectedItem);

            m_displayedItem = selectedItem.ItemReference;
        }

        /// <summary>
        /// Take care of the ItemStackDisplay page when the displayed inventory gets OnItemAmountChanged.
        /// </summary>
        private void HandleOnItemAmountChanged(ItemWithAmount oldAmount, ItemWithAmount newAmount)
        {
            if (m_displayedItem == newAmount.ItemReference)
            {
                ItemStackDisplay.DisplayItemStack(newAmount);
            }
        }

        private void AddInventory(Inventory newInventory)
        {
            newInventory.OnItemAmountChanged += HandleOnItemAmountChanged;
        }

        private void DisplayInventory(Inventory inventoryToDisplay)
        {
            InventoryDisplay.DisplayedInventory = inventoryToDisplay;
        }

        private void MakeNewCharacterAvatar(CharacterReference newCharRef)
        {
            CharacterAvatarScript newAvatar = Instantiate(m_characterAvatarPrefab, m_characterAvatars.transform);

            newAvatar.CharRef = newCharRef;

            newAvatar.GetComponent<UI.Image>().sprite = newCharRef.Character.ScenePrefab.GetComponent<SpriteRenderer>()?.sprite;

            newAvatar.OnCharacterAvatarClicked += (clickedAvatar) => HandleOnCharacterAvatarClicked(clickedAvatar);
        }

        // The item being displayed on the right page
        private ItemReference m_displayedItem;
    }
}