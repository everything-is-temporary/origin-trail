﻿using UnityEngine;
using UnityEditor;

using GameUtility;

[CustomPropertyDrawer(typeof(RequiresTypeAttribute))]
public class RequiresTypeAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        RequiresTypeAttribute attrib = attribute as RequiresTypeAttribute;


        label = EditorGUI.BeginProperty(position, label, property);

        // TODO: don't always allow scene objects
        var result = EditorGUI.ObjectField(position, label, property.objectReferenceValue, typeof(Object), true);

        if (result != null)
        {
            if (attrib.requiredType.IsAssignableFrom(result.GetType()))
            {
                property.objectReferenceValue = result;
            }
            else if (result is GameObject)
            {
                var component = (result as GameObject).GetComponent(attrib.requiredType);

                if (component != null)
                    property.objectReferenceValue = component;
            }
        }
        else
        {
            property.objectReferenceValue = null;
        }

        EditorGUI.EndProperty();
    }
}
