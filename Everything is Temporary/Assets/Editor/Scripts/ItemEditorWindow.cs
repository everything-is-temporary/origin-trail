﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameInventory;
using GameUtility;

public class ItemEditorWindow : DefinitionListEditorWindow
{
    public static void Open(ItemDefinitionList itemList)
    {
        var window = GetWindow<ItemEditorWindow>("Item Editor");

        window.Instantiate(itemList);
    }

    protected override int CountObjects
    {
        get
        {
            SerializedProperty listProp = m_allItemsObject.FindProperty("m_serializedItems");
            return listProp.arraySize;
        }
    }

    protected override string[] GetSortingOptions()
    {
        return new[] { "Default", "Name" };
    }

    protected override MutableIntPermutation Sort(string sortingOption)
    {
        switch (sortingOption)
        {
            case "Default":
                return new MutableIntPermutation(CountObjects);
            case "Name":
                var indicesAndNames = EnumerateItems().Select((prop, idx) => (idx, GetItemName(prop))).ToList();

                // Put property indices in the order in which they should be displayed.
                indicesAndNames.Sort((a, b) => EditorUtility.NaturalCompare(a.Item2, b.Item2));

                return new MutableIntPermutation(indicesAndNames.Select(pair => pair.idx)).GetInverse();
            default:
                throw new NotSupportedException();
        }
    }

    protected override void DrawObject(int propIdx, Rect rect)
    {
        DrawItem(propIdx, rect);
    }

    protected override bool ShouldDeleteObject(int propIdx)
    {
        SerializedProperty itemProp = m_itemWithIdProps[propIdx];

        string itemName = GetItemName(itemProp);
        string itemId = IdConverter.ToReadableString(GetItemId(itemProp));

        return EditorUtility.DisplayDialog("Delete item?",
            $"Are you sure you want to delete '{itemName}' (id: '{itemId}')?",
            "Delete", "Cancel");
    }

    protected override void DeleteObject(int propIdx)
    {
        SerializedProperty listProp = m_allItemsObject.FindProperty("m_serializedItems");
        listProp.DeleteArrayElementAtIndex(propIdx);

        m_itemWithIdProps = EnumerateItems().ToList();
    }

    protected override void InsertNewObject(int propIdx)
    {
        SerializedProperty listProp = m_allItemsObject.FindProperty("m_serializedItems");
        listProp.InsertArrayElementAtIndex(propIdx);

        SerializedProperty newItemProp = listProp.GetArrayElementAtIndex(propIdx);
        SerializedProperty newItemNameProp = newItemProp.FindPropertyRelative("m_item.m_name");
        SerializedProperty newItemIdProp = newItemProp.FindPropertyRelative("m_id");

        newItemNameProp.stringValue = "New Item";
        newItemIdProp.longValue = ((ItemDefinitionList)m_allItemsObject.targetObject).GenerateUniqueId();

        m_itemWithIdProps.Add(newItemProp);
    }

    protected override void PrepareOnGUI()
    {
        m_allItemsObject.Update();
        m_itemWithIdProps = EnumerateItems().ToList();
    }

    protected override void SaveChanges()
    {
        if (m_allItemsObject.ApplyModifiedProperties())
        {
            EditorUtility.SetDirty(m_allItemsObject.targetObject);
            AssetDatabase.SaveAssets();
        }

    }

    protected override string GetDisplayName(int propIdx)
    {
        return GetItemName(m_itemWithIdProps[propIdx]);
    }

    private void Instantiate(ItemDefinitionList itemList)
    {
        if (itemList == null)
        {
            m_allItemsObject = null;
            return;
        }

        m_allItemsObject = new SerializedObject(itemList);
        m_itemWithIdProps = EnumerateItems().ToList();

        Initialize();
    }

    /// <summary>
    /// Draws the item at the given index in the given rect. Should only be
    /// called once per OnGUI() because it modifies <see cref="m_itemScroll"/>.
    /// </summary>
    /// <param name="idx">Item index.</param>
    /// <param name="rect">Item drawing area.</param>
    private void DrawItem(int idx, Rect rect)
    {
        SerializedProperty itemWithIdProp = m_itemWithIdProps[idx];

        float propHeight = EditorGUI.GetPropertyHeight(itemWithIdProp);

        bool shouldScroll = propHeight >= rect.height;

        if (shouldScroll)
        {
            Rect viewRect = new Rect(rect)
            {
                width = rect.width - 20,
                height = propHeight + 20
            };

            m_itemScroll = GUI.BeginScrollView(rect, m_itemScroll, viewRect);
            rect = viewRect;
        }

        EditorGUI.PropertyField(rect, itemWithIdProp, true);

        if (shouldScroll)
        {
            GUI.EndScrollView();
        }
    }

    private IEnumerable<SerializedProperty> EnumerateItems()
    {
        SerializedProperty itemsProp = m_allItemsObject.FindProperty("m_serializedItems");

        for (int i = 0; i < itemsProp.arraySize; ++i)
            yield return itemsProp.GetArrayElementAtIndex(i);
    }

    private string GetItemName(SerializedProperty itemWithIdProp)
    {
        return itemWithIdProp.FindPropertyRelative("m_item.m_name").stringValue;
    }

    private long GetItemId(SerializedProperty itemWithIdProp)
    {
        return itemWithIdProp.FindPropertyRelative("m_id").longValue;
    }

    [SerializeField]
    private Vector2 m_itemScroll;

    // Unity can't serialize SerializedObject and SerializedProperty, so these
    // are reset every time Unity recompiles.
    private SerializedObject m_allItemsObject;
    private List<SerializedProperty> m_itemWithIdProps;
}
