﻿using UnityEngine;
using UnityEditor;
using GameInventory;
using GameUtility;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(ItemDefinitionList.ItemWithId))]
public class ItemWithIdDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty itemTagProp = property.FindPropertyRelative("m_item.m_tags");
        SerializedProperty itemStatsProp = property.FindPropertyRelative("m_item.m_stats");
        return 185 + EditorGUI.GetPropertyHeight(itemStatsProp) + EditorGUI.GetPropertyHeight(itemTagProp);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        long id = property.FindPropertyRelative("m_id").longValue;
        SerializedProperty itemNameProp = property.FindPropertyRelative("m_item.m_name");
        SerializedProperty itemDescProp = property.FindPropertyRelative("m_item.m_descriptionShort");
        SerializedProperty itemFlavorTextProp = property.FindPropertyRelative("m_item.m_flavorText");
        SerializedProperty itemIconProp = property.FindPropertyRelative("m_item.m_icon");
        SerializedProperty itemTagProp = property.FindPropertyRelative("m_item.m_tags"); // store in variable b/c it's an expandable list, need numElements
        SerializedProperty itemValueProp = property.FindPropertyRelative("m_item.m_economyValue");
        SerializedProperty itemStatsProp = property.FindPropertyRelative("m_item.m_stats");

        Rect spriteRect = new Rect(position.x, position.y, 40, position.height);

        position = new Rect(position.x + 40, position.y, position.width - 40, position.height);
        Rect labelRect = Slice(position, 0, 20);
        Rect nameRect = Slice(position, 25, 20);
        Rect valRect = Slice(position, 50, 20);
        Rect descLabel = Slice(position, 70, 40);
        Rect descRect = Slice(position, 85, 40);
        Rect flavorLabel = Slice(position, 130, 40);
        Rect flavorTextRect = Slice(position, 145, 40);
        Rect tagRect = Slice(position, 185, EditorGUI.GetPropertyHeight(itemTagProp));
        Rect statsRect = Slice(position, 185 + EditorGUI.GetPropertyHeight(itemTagProp), EditorGUI.GetPropertyHeight(itemStatsProp));

        EditorGUI.SelectableLabel(labelRect, IdConverter.ToReadableString(id));
        EditorGUI.PropertyField(nameRect, itemNameProp);
        EditorGUI.PropertyField(valRect, itemValueProp);

        EditorGUI.LabelField(descLabel, "Short Description");
        itemDescProp.stringValue = TextArea(descRect, itemDescProp.stringValue);
        EditorGUI.LabelField(flavorLabel, "Flavor Text");
        itemFlavorTextProp.stringValue = TextArea(flavorTextRect, itemFlavorTextProp.stringValue);


        EditorGUI.PropertyField(tagRect, itemTagProp, true);

        EditorGUI.PropertyField(statsRect, itemStatsProp, true);

        GUISpriteDrawer.DrawSpriteProperty(spriteRect, itemIconProp);
    }

    private static string TextArea(Rect position, string value)
    {
        bool oldWrap = EditorStyles.textField.wordWrap;
        EditorStyles.textField.wordWrap = true;

        string result = EditorGUI.TextArea(position, value);

        EditorStyles.textField.wordWrap = oldWrap;

        return result;
    }

    private static Rect Slice(Rect rect, float top, float size)
    {
        return new Rect(rect.x, rect.y + top, rect.width, size);
    }
}
