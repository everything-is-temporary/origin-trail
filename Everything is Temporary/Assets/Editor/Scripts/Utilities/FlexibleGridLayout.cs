﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Utilities
{
    /// <summary>
    /// Helper class to display elements in a grid. Set the <see cref="Elements"/>
    /// array, then use <see cref="ComputePreferredHeight"/> and <see cref="OnGUI"/>
    /// to display.
    /// </summary>
    public class FlexibleGridLayout
    {
        public float ElementWidth { get; set; }
        public float ElementHeight { get; set; }

        public float Spacing { get; set; } = EditorGUIUtility.standardVerticalSpacing;

        public delegate void ElementDrawer(Rect position);
        public List<ElementDrawer> Elements { get; } = new List<ElementDrawer>();

        /// <summary>
        /// Computes the minimum height needed to display <paramref name="totalElements"/>
        /// elements with the current <see cref="ElementWidth"/>, <see cref="ElementHeight"/>
        /// and <see cref="Spacing"/> given the <paramref name="totalWidth"/> of the display
        /// area.
        /// </summary>
        /// <returns>The preferred height.</returns>
        /// <param name="totalWidth">Total width.</param>
        /// <param name="totalElements">Total elements.</param>
        public float ComputePreferredHeight(float totalWidth, int totalElements)
        {
            if (Elements.Count == 0)
                return 0;

            int elementsPerRow = (int)((totalWidth + Spacing) / (ElementWidth + Spacing));
            if (elementsPerRow < 1)
                elementsPerRow = 1;

            int desiredRows = 1 + (totalElements / elementsPerRow);

            return desiredRows * ElementHeight + (desiredRows - 1) * Spacing;
        }

        public void OnGUI(Rect position)
        {
            int numCols = (int)((position.width + Spacing) / (ElementWidth + Spacing));
            int numRows = (int)((position.height + Spacing) / (ElementHeight + Spacing));

            float elementWidth = ElementWidth;
            float elementHeight = ElementHeight;

            if (numCols < 1)
            {
                numCols = 1;
                elementWidth = position.width;
            }

            if (numRows < 1)
            {
                numRows = 1;
                elementHeight = position.height;
            }


            int elementIndex = 0;
            for (int rowIndex = 0; rowIndex < numRows && elementIndex < Elements.Count; ++rowIndex)
            {
                for (int colIndex = 0; colIndex < numCols && elementIndex < Elements.Count; ++colIndex, ++elementIndex)
                {
                    float x = position.x + (elementWidth + Spacing) * colIndex;
                    float y = position.y + (elementHeight + Spacing) * rowIndex;

                    Rect elementRect = new Rect(x, y, elementWidth, elementHeight);

                    Elements[elementIndex].Invoke(elementRect);
                }
            }
        }
    }
}