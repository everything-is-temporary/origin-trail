﻿using System;
using UnityEngine;
using UnityEditor;

namespace Utilities
{
    public static class GUIRectUtils
    {
        public static bool ClickableRect(Rect rect, MouseCursor cursor)
        {
            EditorGUIUtility.AddCursorRect(rect, cursor);
            return Event.current.type == EventType.MouseUp && rect.Contains(Event.current.mousePosition);
        }
    }
}
