﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Utilities
{
    [System.Serializable]
    public class ListView
    {
        public ListView(int numItems)
        {
            m_data = new GUIContent[numItems];
            for (int i = 0; i < numItems; ++i)
                m_data[i] = GUIContent.none;
        }

        public ListView(IEnumerable<string> data)
        {
            m_data = data.Select(label => new GUIContent(label)).ToArray();
        }

        public float PreferredHeight => m_data.Length * EditorGUIUtility.singleLineHeight
                                        + (m_data.Length - 1) * EditorGUIUtility.standardVerticalSpacing
                                        + 10;

        public GUIContent[] Entries => m_data;

        /// <summary>
        /// Updates the selected index and scrolls to it if it's not null.
        /// </summary>
        /// <value>The index of the selected entry.</value>
        public int? SelectedIndex
        {
            get
            {
                if (m_lastSelected >= 0)
                    return m_lastSelected;

                return null;
            }

            set
            {
                m_lastSelected = value ?? -1;
                NeedsRepaint = true;

                if (m_lastSelected >= 0)
                    m_scrollPosition = new Vector2(0,
                        m_lastSelected * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing));
            }
        }

        public bool NeedsRepaint { get; private set; }

        /// <summary>
        /// Draws the list in the given rectangle, possibly using a scrollview.
        /// </summary>
        /// <param name="position">The rectangle within which to draw the list.</param>
        public void OnGUI(Rect position)
        {
            NeedsRepaint = false;

            bool usingScrollview = position.height < PreferredHeight;

            if (usingScrollview)
            {
                Rect viewRect = new Rect(0, 0, position.width - 15, PreferredHeight);
                m_scrollPosition = GUI.BeginScrollView(position, m_scrollPosition, viewRect);

                position = viewRect;
            }
            else
            {
                m_scrollPosition = Vector2.zero;
            }

            for (int i = 0; i < m_data.Length; ++i)
                position = DrawElement(i, position);

            if (usingScrollview)
            {
                GUI.EndScrollView();
            }
        }

        private Rect DrawElement(int idx, Rect position)
        {
            Rect elementRect = new Rect(position) { height = EditorGUIUtility.singleLineHeight };

            if (DoesRectContainMouse(elementRect))
            {
                if (m_lastHovered != idx)
                {
                    NeedsRepaint = true;
                    m_lastHovered = idx;
                }

                if (IsMouseUpEvent() && m_lastSelected != idx)
                {
                    NeedsRepaint = true;
                    m_lastSelected = idx;
                }
            }

            Color color = m_defaultColor;
            if (m_lastSelected == idx)
                color = m_selectedColor;
            else if (m_lastHovered == idx)
                color = m_hoverColor;

            EditorGUI.DrawRect(elementRect, color);
            EditorGUI.LabelField(elementRect, m_data[idx]);

            return new Rect(position) { yMin = elementRect.yMax + EditorGUIUtility.standardVerticalSpacing };
        }

        private bool DoesRectContainMouse(Rect rect)
        {
            return rect.Contains(Event.current.mousePosition);
        }

        private bool IsMouseUpEvent()
        {
            return Event.current.type == EventType.MouseUp;
        }

        [SerializeField]
        private int m_lastSelected = -1;

        [SerializeField]
        private int m_lastHovered = -1;

        [SerializeField]
        private Vector2 m_scrollPosition;

        [SerializeField]
        private GUIContent[] m_data;

        private static Color m_selectedColor = new Color(0.5f, 0.5f, 0.5f);
        private static Color m_hoverColor = new Color(0.8f, 0.8f, 0.8f);
        private static Color m_defaultColor = new Color(0.9f, 0.9f, 0.9f);
    }
}