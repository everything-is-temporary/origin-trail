﻿using UnityEngine.Events;

namespace Utilities
{
    public static class UnityEventUtils
    {
        /// <summary>
        /// Adds a persistent (serialized) listener to the event. The action
        /// should have a valid UnityEngine.Object target.
        /// </summary>
        /// <param name="action">Action with a UnityEngine.Object target.</param>
        /// <param name="argument">Argument.</param>
        public static void AddPersistentListener(this UnityEventBase baseUnityEvent, UnityAction<string> action, string argument)
        {
            // FIXME: Fragile code. In Unity 2019, use UnityEventTools
            AddStringPersistentListenerMethod.Invoke(baseUnityEvent, new object[] { action, argument });
        }

        private static readonly System.Reflection.MethodInfo AddStringPersistentListenerMethod
            = typeof(UnityEventBase).GetMethod("AddStringPersistentListener",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
    }
}