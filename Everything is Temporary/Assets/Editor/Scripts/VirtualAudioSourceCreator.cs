﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Audio;

public static class VirtualAudioSourceCreator
{
    [MenuItem("GameObject/Origin Trail/Audio/Make Into Audio Source", false, 10)]
    private static void CreateVirtualAudioSource(MenuCommand menuCommand)
    {
        GameObject target = menuCommand.context as GameObject;

        if (target == null)
            target = Selection.activeGameObject;

        if (target == null)
            return;

        // Add the VirtualAudioSourceScript.
        var virtualSource = Undo.AddComponent<VirtualAudioSourceScript>(target);

        // Set scene to match that of virtual audio listener. This is a good
        // default (if there's no virtual audio listener, it just adds an
        // extra step to the user).
        virtualSource.virtualScene = Object.FindObjectOfType<VirtualAudioListenerScript>()?.virtualScene;

        // The VirtualAudioSourceScript configures its audioSource property
        // when it's created.
        EditorGUIUtility.PingObject(virtualSource.audioSource);
        Selection.activeObject = virtualSource.audioSource;
    }

    [MenuItem("GameObject/Origin Trail/Audio/Make Into Audio Source", true, 10)]
    private static bool ValidateCreateVirtualAudioSource(MenuCommand menuCommand)
    {
        return menuCommand.context is GameObject || Selection.activeGameObject != null;
    }
}
