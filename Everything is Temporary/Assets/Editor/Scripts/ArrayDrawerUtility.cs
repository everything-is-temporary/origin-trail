﻿using UnityEngine;
using UnityEditor;

using System;


/// <summary>
/// Helper class for drawing arrays in a pretty way. Needs to keep track of state for
/// dragging, so only re-initialize this object when displaying a new property.
/// </summary>
[Serializable]
public class ArrayDrawerUtility
{

    /// <summary>
    /// Returns how much height is needed to render the array.
    /// </summary>
    public float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        Debug.Assert(property != null && property.isArray);

        float totalHeight = 25 + 25;

        int arraySize = property.arraySize;

        for (int idx = 0; idx < arraySize; ++idx)
            totalHeight += EditorGUI.GetPropertyHeight(property.GetArrayElementAtIndex(idx), true) + 10;

        return totalHeight;
    }

    /// <summary>
    /// Draws the array. Call every time in OnGUI().
    /// <param name="initializer">This is invoked on new array elements after they are created. Null means do nothing.</param>
    /// </summary>
    public void DrawArray(Rect position, SerializedProperty property, GUIContent label,
        Action<SerializedProperty> initializer = null)
    {
        Debug.Assert(property != null && property.isArray);

        // Avoid drag-related glitches.
        if (m_lastProperty == null || !SerializedProperty.EqualContents(m_lastProperty, property))
        {
            m_lastProperty = property;
            m_selectedIdx = -1;
            m_dragPosition = -1;
        }

        label = EditorGUI.BeginProperty(position, label, property);
        EditorGUI.LabelField(new Rect(position.x, position.y, position.width, 20), label);
        position.y += 25;


        int newDragPosition = m_dragPosition;

        int idxOffset = 0;
        int arraySize = property.arraySize;

        m_deleteIdx = -1;

        for (int slotIdx = 0; slotIdx < arraySize; ++slotIdx)
        {

            // Draw the dragged item.
            if (slotIdx == m_dragPosition)
            {
            
                position.y += DrawElement(position, property.GetArrayElementAtIndex(m_selectedIdx), m_selectedIdx);

                if (Event.current.type == EventType.MouseUp)
                {
                    property.MoveArrayElement(m_selectedIdx, m_dragPosition);
                    m_selectedIdx = -1;
                    m_dragPosition = -1;
                    Event.current.Use();
                }

                --idxOffset; // slotIdx will now be one more than the real drawn index
            }
            else
            {
                int idxToDraw = slotIdx + idxOffset;

                // Skip the selected item.
                if (idxToDraw == m_selectedIdx)
                {
                    ++idxToDraw;
                    ++idxOffset;
                }

                float startY = position.y;
                position.y += DrawElement(position, property.GetArrayElementAtIndex(idxToDraw), idxToDraw);


                // If something is being dragged, update the drag position.
                if (m_selectedIdx >= 0)
                {
                    float mouseY = Event.current.mousePosition.y;
                    if (mouseY > startY && mouseY <= position.y)
                        newDragPosition = slotIdx;
                }
            }

            EditorGUI.LabelField(new Rect(position.x, position.y, position.width, 20), "", GUI.skin.horizontalSlider);

            position.y += 10;
        }

        if (m_deleteIdx >= 0)
        {
            if (EditorUtility.DisplayDialog("Delete item?", "Are you sure you want to delete this?", "Delete", "Cancel"))
                property.DeleteArrayElementAtIndex(m_deleteIdx);

            m_deleteIdx = -1;
        }

        if (m_selectedIdx >= 0)
        {
            // Exit text-editing mode, or else the text that is selected will
            // not move even when its corresponding item is dragged, and this
            // might freak people out.
            EditorGUIUtility.editingTextField = false;

            m_dragPosition = newDragPosition;
        }


        // Use mouse drag events when we are dragging.
        if (m_selectedIdx >= 0 && Event.current.type == EventType.MouseDrag)
            Event.current.Use();


        var buttonRect = new Rect(position.x, position.y + 5, position.width, 20);
        if (GUI.Button(buttonRect, "New Element"))
        {
            property.InsertArrayElementAtIndex(property.arraySize);

            initializer?.Invoke(property.GetArrayElementAtIndex(property.arraySize - 1));
        }

        EditorGUI.EndProperty();
    }

    /// <summary>
    /// Draws the array element. Returns the height used up.
    /// </summary>
    /// <returns>The height used up.</returns>
    /// <param name="position">The remaining rectangle.</param>
    /// <param name="element">The element to be drawn.</param>
    private float DrawElement(Rect position, SerializedProperty element, int index)
    {
        float necessaryHeight = EditorGUI.GetPropertyHeight(element, true);

        var draggerRect = new Rect(position.x, position.y, 15, necessaryHeight);
        var deleterRect = new Rect(position.xMax - 20, position.y + necessaryHeight / 2.0f - 10f, 20, 20);
        var elementRect = new Rect(draggerRect.xMax, position.y, deleterRect.xMin - draggerRect.xMax, necessaryHeight);

        // Draw!
        DrawCenteredText(draggerRect, "::", FontStyle.Bold);
        EditorGUI.PropertyField(elementRect, element, true);

        Color prevColor = GUI.color;
        GUI.color = Color.red;
        if (GUI.Button(deleterRect, "X"))
            m_deleteIdx = index;
        GUI.color = prevColor;

        // If nothing is being dragged, make this draggable.
        if (m_selectedIdx == -1)
        {
            EditorGUIUtility.AddCursorRect(draggerRect, MouseCursor.Pan);

            if (Event.current.type == EventType.MouseDown && draggerRect.Contains(Event.current.mousePosition))
            {
                m_selectedIdx = index;
                m_dragPosition = index;
                Event.current.Use();
            }
        }

        return necessaryHeight;
    }

    private void DrawCenteredText(Rect position, string text, FontStyle style = FontStyle.Normal)
    {
        var centeredStyle = GUI.skin.GetStyle("Label");
        centeredStyle.alignment = TextAnchor.MiddleCenter;
        centeredStyle.fontStyle = style;

        GUI.Label(position, text, centeredStyle);
    }

    private float GetDraggedElementHeight(SerializedProperty array)
    {
        Debug.Assert(m_selectedIdx >= 0);
        return EditorGUI.GetPropertyHeight(array.GetArrayElementAtIndex(m_selectedIdx), true);
    }


    /// <summary>
    /// The index of the array element that should be deleted. Reset at the start of OnGUI()
    /// every time, and should equal -1 if nothing should be deleted.
    /// </summary>
    private int m_deleteIdx = -1;

    /// <summary>
    /// The index of the item currently being dragged. -1 if none.
    /// </summary>
    [SerializeField]
    private int m_selectedIdx = -1;

    /// <summary>
    /// The current position of the dragged item, as an index into the array. -1 if nothing is dragged.
    /// </summary>
    [SerializeField]
    private int m_dragPosition = -1;

    /// <summary>
    /// The property for which this drawer was last used. If this changes, the selection & drag
    /// parameters should be reset.
    /// </summary>
    [SerializeField]
    private SerializedProperty m_lastProperty;
}