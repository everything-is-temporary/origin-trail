﻿using UnityEngine;
using UnityEditor;
using Interaction;

public class InvisibleWallCreator : ScriptableWizard
{
    public GameObject parent;
    public string wallName;
    public bool isEnabledAtStart;

    [MenuItem("GameObject/Origin Trail/Invisible Wall", false, 10)]
    private static void CreateInvisibleWall(MenuCommand menuCommand)
    {
        var wizard = DisplayWizard<InvisibleWallCreator>("Create Invisible Wall");

        if (menuCommand.context is GameObject parent)
            wizard.parent = parent;
    }

    private void OnWizardCreate()
    {
        // FIXME: Don't load from hardcoded path.
        GameObject invisibleWallPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(
            "Assets/Prefabs/SceneInteraction/InvisibleWall.prefab");

        GameObject invisibleWall = Instantiate(invisibleWallPrefab);
        invisibleWall.name = wallName;

        if (parent != null)
            GameObjectUtility.SetParentAndAlign(invisibleWall, parent);

        InvisibleWall invisibleWallScript = invisibleWall.GetComponent<InvisibleWall>();
        invisibleWallScript.wallName = wallName;
        invisibleWallScript.enabledOnStart = isEnabledAtStart;

        Selection.activeObject = invisibleWall;
        EditorGUIUtility.PingObject(invisibleWall);

        Undo.RegisterCreatedObjectUndo(invisibleWall, $"Created Invisible Wall '{wallName}'");
    }
}
