﻿using GameUtility;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(IdFieldAttribute))]
public class IdFieldDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label = EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, label);
        string updated = EditorGUI.DelayedTextField(position, IdConverter.ToReadableString(property.longValue));

        try
        {
            property.longValue = IdConverter.FromReadableString(updated);
        }
        catch (System.ApplicationException ex)
        {
            Debug.LogWarning($"{ex}");
        }

        EditorGUI.EndProperty();
    }
}
