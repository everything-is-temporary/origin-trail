﻿
using UnityEngine;
using UnityEditor;

/// <summary>
/// Utilities for drawing sprites in editor scripts. If using this from
/// inside a PropertyDrawer, you may want to override
/// <see cref="PropertyDrawer.CanCacheInspectorGUI(UnityEditor.SerializedProperty)"/>
/// and set it to false, or else the sprite may flicker. See this link
/// https://answers.unity.com/questions/377207/drawing-a-texture-in-a-custom-propertydrawer.html
/// </summary>
public static class GUISpriteDrawer
{
    /// <summary>
    /// Draw a tinted sprite filling the given rectangle.
    /// </summary>
    /// <param name="location">Location.</param>
    /// <param name="sprite">Sprite.</param>
    /// <param name="color">Tint color.</param>
    public static void Draw(Rect location, Sprite sprite, Color? color)
    {
        var prevColor = GUI.color;
        if (color.HasValue)
            GUI.color = color.Value;

        if (sprite != null)
        {
            var spriteRect = sprite.rect;

            spriteRect.x /= sprite.texture.width;
            spriteRect.width /= sprite.texture.width;

            spriteRect.y /= sprite.texture.height;
            spriteRect.height /= sprite.texture.height;

            GUI.DrawTextureWithTexCoords(location, sprite.texture, spriteRect);
        }
        else
        {
            GUI.Box(location, "(no image)");
        }

        GUI.color = prevColor;
    }

    /// <summary>
    /// Draw a tinted sprite in the given rectangle, preserving aspect.
    /// </summary>
    /// <param name="location">Location.</param>
    /// <param name="sprite">Sprite.</param>
    /// <param name="color">Tint color.</param>
    public static void DrawCorrectAspect(Rect location, Sprite sprite, Color? color = null)
    {
        if (sprite != null)
        {
            var height = location.height;
            var width = location.width;

            var spriteSize = sprite.bounds.size;
            float spriteAspect = spriteSize.x / spriteSize.y;
            float rectAspect = width / height;

            if (rectAspect > spriteAspect)
            {
                var xCenter = location.center.x;

                location.xMin = xCenter - spriteAspect * height / 2;
                location.xMax = xCenter + spriteAspect * height / 2;
            }
            else
            {
                var yCenter = location.center.y;

                location.yMin = yCenter - (width / spriteAspect) / 2;
                location.yMax = yCenter + (width / spriteAspect) / 2;
            }
        }

        Draw(location, sprite, color);
    }

    /// <summary>
    /// Draws the sprite in the rectangle, preserving aspect, and allows the
    /// user to drag new sprites into this rectangle to change the property.
    /// </summary>
    /// <param name="spriteRect">Sprite rect.</param>
    /// <param name="spriteProp">Sprite property.</param>
    public static void DrawSpriteProperty(Rect spriteRect, SerializedProperty spriteProp)
    {
        Debug.Assert(spriteProp.objectReferenceValue == null || spriteProp.objectReferenceValue is Sprite);

        Sprite sprite = spriteProp.objectReferenceValue as Sprite;

        if (sprite != null)
            DrawCorrectAspect(spriteRect, sprite);
        else
            EditorGUI.HelpBox(spriteRect, "drag & drop", MessageType.Info);

        ProcessSpriteDrag(spriteRect, spriteProp);
    }

    private static void ProcessSpriteDrag(Rect spriteRect, SerializedProperty spriteProp)
    {
        if (spriteRect.Contains(Event.current.mousePosition))
        {
            switch (Event.current.type)
            {
                case EventType.DragUpdated:

                    if (DragAndDrop.objectReferences[0] is Sprite)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Link;
                        DragAndDrop.AcceptDrag();
                        Event.current.Use();
                    }
                    else
                        DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;

                    break;

                case EventType.DragPerform:

                    if (DragAndDrop.objectReferences[0] is Sprite)
                    {
                        spriteProp.objectReferenceValue = DragAndDrop.objectReferences[0];
                        Event.current.Use();
                    }

                    break;
            }
        }
    }
}
