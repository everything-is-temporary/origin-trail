﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using GameUtility;
using Utilities;

public class TriggerZoneCreator : ScriptableWizard
{
    [MenuItem("GameObject/Origin Trail/Trigger Zone", false, 10)]
    private static void CreateTriggerZone(MenuCommand menuCommand)
    {
        var wizard = DisplayWizard<TriggerZoneCreator>("Create Trigger Zone", "Create");

        if (menuCommand.context is GameObject gameObject)
        {
            wizard.m_parent = gameObject;
        }
    }

    private void OnWizardCreate()
    {
        GameObject triggerZone = new GameObject(m_name);

        if (m_parent != null)
            GameObjectUtility.SetParentAndAlign(triggerZone, m_parent);

        BoxCollider2D collider = triggerZone.AddComponent<BoxCollider2D>();
        collider.isTrigger = true;

        TriggerEventScript triggerEventScript = triggerZone.AddComponent<TriggerEventScript>();
        triggerEventScript.destroyOnEnter = m_destroyOnEnter;
        triggerEventScript.destroyOnExit = m_destroyOnExit;

        if (m_opensInkKnot)
        {
            var dialogueStarterScript = triggerZone.AddComponent<DialogueStarterScript>();
            triggerEventScript.onTriggerEnter2d.AddPersistentListener(dialogueStarterScript.StartDialogue, m_inkKnot);
        }

        Selection.activeObject = triggerZone;
        EditorGUIUtility.PingObject(triggerZone);

        Undo.RegisterCreatedObjectUndo(triggerZone, $"Created Trigger Zone '{m_name}'");
    }

    [SerializeField] private GameObject m_parent = default;
    [SerializeField] private string m_name = "Trigger Zone";
    [SerializeField] private bool m_destroyOnEnter = default;
    [SerializeField] private bool m_destroyOnExit = default;
    [SerializeField] private bool m_opensInkKnot = default;
    [SerializeField] private string m_inkKnot = "";
}
