﻿using UnityEngine;
using UnityEditor;
using GameInventory;

[CustomEditor(typeof(ItemDefinitionList))]
public class ItemDefinitionListInspector : Editor
{
    public override void OnInspectorGUI()
    {
        ItemDefinitionList definitionList = target as ItemDefinitionList;

        if (GUILayout.Button("Open Item Editor"))
        {
            ItemEditorWindow.Open(definitionList);
        }
    }
}
