﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public static class SidescrollerSceneVerifier
{
    // The [InitializeOnLoad] attribute makes this run as soon as Unity loads.
    static SidescrollerSceneVerifier()
    {
        EditorSceneManager.sceneSaving += OnSceneSaving;
    }

    static void OnSceneSaving(Scene scene, string path)
    {
        if (IsSidescrollerScenePath(path))
            VerifyScene(scene);
    }

    static void VerifyScene(Scene scene)
    {
        CheckTownCamera(scene);
        CheckCamerasOverwritingScreen(scene);
        CheckRaycastersAreWrapped(scene);
        CheckNoEventSystems(scene);
        CheckParallax(scene);
    }

    static bool IsSidescrollerScenePath(string path)
    {
        return path.Contains("Scenes/Sidescroller Scenes");
    }

    static void CheckTownCamera(Scene scene)
    {
        List<TownCamera> townCameras = EnumerateInScene<TownCamera>(scene).ToList();

        if (townCameras.Count <= 1)
        {
            Camera camera;

            if (townCameras.Count == 1)
            {
                camera = townCameras[0].GetComponent<Camera>();
            }
            else
            {
                camera = GetSceneMainCamera(scene);
                if (camera != null)
                {
                    Debug.Log($"Scene {scene.name} didn't have a TownCamera. Adding one to the camera {camera.gameObject.name}.");
                    camera.gameObject.AddComponent<TownCamera>();
                }
                else
                {
                    Debug.Log($"Scene {scene.name} didn't have a camera. Creating a camera with the TownCamera component.");

                    GameObject camGo = new GameObject("Town Camera");
                    camGo.tag = "MainCamera";
                    camera = camGo.AddComponent<Camera>();
                    camera.orthographic = false;
                    camera.fieldOfView = 60;
                    camGo.AddComponent<TownCamera>();

                    SceneManager.MoveGameObjectToScene(camGo, scene);
                }
            }

            if ((camera.cullingMask & LayerMask.GetMask("Book")) != 0)
            {
                Debug.Log($"Removing 'Book' layer from camera {camera.gameObject.name}'s culling mask.");
                camera.cullingMask &= ~LayerMask.GetMask("Book");
            }
        }
        else if (townCameras.Count > 1)
        {
            Debug.LogError($"Scene {scene.name} has multiple TownCamera scripts. It must only have one!");
        }
    }

    static void CheckCamerasOverwritingScreen(Scene scene)
    {
        foreach (Camera cam in EnumerateInScene<Camera>(scene))
        {
            bool isTownCamera = cam.GetComponent<TownCamera>() != null;
            if (!isTownCamera && cam.enabled && cam.targetTexture == null)
                Debug.LogWarning($"Camera {cam.gameObject.name} in scene {scene.name} may overwrite scene contents.");
        }
    }

    static void CheckRaycastersAreWrapped(Scene scene)
    {
        foreach (BaseRaycaster raycaster in EnumerateInScene<BaseRaycaster>(scene))
        {
            if (raycaster.enabled)
            {
                Debug.LogWarning($"Found enabled raycaster in a sidescroller scene." +
                    $" Disabling {raycaster.gameObject.name} in scene {scene.name}. This means" +
                	" that the raycaster will not be used.");
                raycaster.enabled = false;
            }
        }
    }

    static void CheckNoEventSystems(Scene scene)
    {
        foreach (EventSystem es in EnumerateInScene<EventSystem>(scene))
        {
            if (es.enabled)
            {
                Debug.Log($"Scene {scene.name} has an event system ({es.gameObject.name}), but this is not" +
                    " allowed in a sidescroller scene. This EventSystem component will be disabled.");
                es.enabled = false;
            }
        }

        foreach (BaseInputModule inputModule in EnumerateInScene<BaseInputModule>(scene))
        {
            if (inputModule.enabled)
            {
                Debug.Log($"Scene {scene.name} has an input module ({inputModule.gameObject.name}), but this" +
                    " is not allowed in a sidescroller scene. This component will be disabled.");
                inputModule.enabled = false;
            }
        }
    }

    static void CheckParallax(Scene scene)
    {
        foreach (var parallax in EnumerateInScene<VFX.ParallaxLayer>(scene))
            parallax.EnsureCorrect();
    }

    static IEnumerable<T> EnumerateInScene<T>(Scene scene) where T : Component
    {
        foreach (GameObject go in scene.GetRootGameObjects())
        {
            var components = go.GetComponentsInChildren<T>();

            foreach (T component in components)
                yield return component;
        }
    }

    static Camera GetSceneMainCamera(Scene scene)
    {
        foreach (Camera cam in EnumerateInScene<Camera>(scene))
        {
            if (cam.CompareTag("MainCamera"))
                return cam;
        }

        return null;
    }

}
