﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameInventory;
using System.IO;
using Characters.Emotes;

public class EmoteAssetPostprocessor : AssetPostprocessor
{
    private static readonly string m_emotePathPrefix = "Assets/Prefabs/Emotes";
    
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        bool listWasModified = false;

        foreach (string path in importedAssets.Concat(movedAssets))
        {
            if (IsEmotePath(path))
                listWasModified |= HandleNewItemInEmoteFolder(path);
        }

        foreach (string path in deletedAssets)
        {
            if (IsEmotePath(path))
                listWasModified |= HandleItemRemovedFromEmoteFolder(path);
        }

        if (listWasModified)
        {
            EditorUtility.SetDirty(EmoteDefinitionList.Instance);
            AssetDatabase.SaveAssets();
        }
    }

    /// <summary>
    /// Quick check to determine whether this path is inside the Emotes folder.
    /// </summary>
    /// <returns><c>true</c>, if the path leads to the emote prefab folder, <c>false</c> otherwise.</returns>
    /// <param name="path">Path relative to project folder.</param>
    static bool IsEmotePath(string path)
    {
        string pathPrefix = m_emotePathPrefix.Replace('/', Path.DirectorySeparatorChar);
        return path.StartsWith(pathPrefix, System.StringComparison.InvariantCulture);
    }

    static bool HandleNewItemInEmoteFolder(string path)
    {
        EmoteDefinition obj = GetEmoteAsset(path);

        if (obj != null)
        {
            EmoteDefinitionList.Instance.AddEmoteDefinition(obj);
            return true;
        }

        return false;
    }

    static bool HandleItemRemovedFromEmoteFolder(string path)
    {
        EmoteDefinitionList.Instance.CleanNullEmotes();
        return true;
    }

    static EmoteDefinition GetEmoteAsset(string path)
    {
        return AssetDatabase.LoadAssetAtPath<EmoteDefinition>(path);
    }
}
