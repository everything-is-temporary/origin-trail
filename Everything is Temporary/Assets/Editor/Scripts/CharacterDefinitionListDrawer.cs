﻿using System;
using UnityEditor;
using Characters;
using UnityEngine;

[CustomEditor(typeof(CharacterDefinitionList))]
public class CharacterDefinitionListDrawer : Editor
{
    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Open Character Editor"))
        {
            CharacterEditorWindow.Open(target as CharacterDefinitionList);
        }
    }
}
