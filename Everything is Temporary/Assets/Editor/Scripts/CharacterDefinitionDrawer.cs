﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Characters;

[CustomPropertyDrawer(typeof(CharacterDefinition))]
public class CharacterDefinitionDrawer : PropertyDrawer
{
    [SerializeField]
    private float m_totalHeight;

    public override bool CanCacheInspectorGUI(SerializedProperty property)
    {
        // False because we draw a sprite icon.
        return false;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return m_totalHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float initialY = position.y;

        position = DrawIcon(position, property);
        position = DrawName(position, property);
        position = DrawCampingInteractionBoolean(position, property);
        position = DrawCampingKnot(position, property);
        position = DrawCampingKnotR2(position, property);
        position = DrawGiveItemKnot(position, property);
        position = DrawCampingPrefab(position, property);
        position = DrawScenePrefab(position, property);
        position = DrawStats(position, property);
        position = DrawInventory(position, property);

        m_totalHeight = position.y - initialY;
    }

    private Rect DrawIcon(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty iconProp = charProp.FindPropertyRelative("m_icon");

        Rect iconRect = new Rect(remainingSpace) { width = iconSize, height = iconSize };
        Rect iconLabelRect = new Rect(remainingSpace) { xMin = iconRect.xMax + spacing, height = lineHeight };

        GUISpriteDrawer.DrawSpriteProperty(iconRect, iconProp);
        EditorGUI.LabelField(iconLabelRect, "Character icon");

        return new Rect(remainingSpace) { yMin = iconRect.yMax + spacing };
    }

    private Rect DrawName(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty nameProp = charProp.FindPropertyRelative("m_name");
        return DrawSingleLineProp(remainingSpace, nameProp);
    }

    private Rect DrawCampingInteractionBoolean(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty campingInteractionProp = charProp.FindPropertyRelative("m_canInteractWithDuringCamping");
        return DrawSingleLineProp(remainingSpace, campingInteractionProp);
    }
    
    private Rect DrawCampingKnot(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty campingKnotProp = charProp.FindPropertyRelative("m_campingKnotPath");
        return DrawSingleLineProp(remainingSpace, campingKnotProp);
    }
    
    private Rect DrawCampingKnotR2(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty campingKnotProp = charProp.FindPropertyRelative("m_campingKnotPathR2");
        return DrawSingleLineProp(remainingSpace, campingKnotProp);
    }
    
    private Rect DrawGiveItemKnot(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty campingKnotProp = charProp.FindPropertyRelative("m_giveItemKnotPath");
        return DrawSingleLineProp(remainingSpace, campingKnotProp);
    }

    private Rect DrawCampingPrefab(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty campingPrefabProp = charProp.FindPropertyRelative("m_campingPrefab");

        if (campingPrefabProp.objectReferenceValue != null)
            return DrawSingleLineProp(remainingSpace, campingPrefabProp);

        remainingSpace = DrawSingleLineProp(remainingSpace, campingPrefabProp);

        Rect warningRect = new Rect(remainingSpace) { height = lineHeight };
        Rect fixRect = new Rect(remainingSpace) { yMin = warningRect.yMax + spacing, height = lineHeight };

        EditorGUI.HelpBox(warningRect, "No camping prefab set.", MessageType.Warning);
        if (GUI.Button(fixRect, "Create camping prefab"))
        {
            Sprite sprite = (charProp.FindPropertyRelative("m_scenePrefab").objectReferenceValue as GameObject)?
                .GetComponent<SpriteRenderer>()?.sprite;
            string name = charProp.FindPropertyRelative("m_name").stringValue;

            CampingActorWizard.OpenWizard("Create Camping Prefab")
                .configuration = new CampingActorFactory.Configuration
                {
                    objectName = $"{name} Camping",
                    sprite = sprite
                    // Not enough information to get character ID.
                };
        }

        return new Rect(remainingSpace) { yMin = fixRect.yMax + spacing };
    }

    private Rect DrawScenePrefab(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty scenePrefabProp = charProp.FindPropertyRelative("m_scenePrefab");
        return DrawSingleLineProp(remainingSpace, scenePrefabProp);
    }

    private Rect DrawStats(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty statsProp = charProp.FindPropertyRelative("m_stats");

        float height = EditorGUI.GetPropertyHeight(statsProp, true);
        Rect rect = new Rect(remainingSpace) { height = height };
        EditorGUI.PropertyField(rect, statsProp);

        return new Rect(remainingSpace) { yMin = rect.yMax + spacing };
    }

    private Rect DrawInventory(Rect remainingSpace, SerializedProperty charProp)
    {
        SerializedProperty inventoryProp = charProp.FindPropertyRelative("m_inventory");

        float height = EditorGUI.GetPropertyHeight(inventoryProp, true);
        Rect rect = new Rect(remainingSpace) { height = height };
        EditorGUI.PropertyField(rect, inventoryProp);

        return new Rect(remainingSpace) { yMin = rect.yMax + spacing };
    }

    private Rect DrawSingleLineProp(Rect remainingSpace, SerializedProperty prop)
    {
        Rect rect = new Rect(remainingSpace) { height = lineHeight };
        EditorGUI.PropertyField(rect, prop);
        return new Rect(remainingSpace) { yMin = rect.yMax + spacing };
    }

    private static readonly float lineHeight = EditorGUIUtility.singleLineHeight;
    private static readonly float spacing = EditorGUIUtility.standardVerticalSpacing;
    private static readonly float lineAndSpace = lineHeight + spacing;
    private static readonly float iconSize = lineHeight * 4 + spacing * 3;
}
