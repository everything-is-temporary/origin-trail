﻿using System;
using GameUtility;
using GameUtility.Serialization;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SerializableDictionaryAttribute))]
public class SerializableDictionaryDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializableDictionaryAttribute attr = attribute as SerializableDictionaryAttribute;

        SerializedProperty keysProp = property.FindPropertyRelative("m_keys");
        SerializedProperty valuesProp = property.FindPropertyRelative("m_values");

        // In these cases, someone used the attribute incorrectly.
        if (keysProp == null || valuesProp == null)
            return base.GetPropertyHeight(property, label);
        if (!keysProp.isArray || !valuesProp.isArray)
            return base.GetPropertyHeight(property, label);

        // Label height + vertical spacing
        float totalHeight = EditorGUIUtility.singleLineHeight;

        // The height of each entry + vertical spacing
        ForeachEntry(keysProp, valuesProp, (key, value, _) =>
        {
            totalHeight += 2 * EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight + GetEntryHeight(key, value);
            return false;
        });

        // Add an extra line for the "add" button
        if (attr.ShowAddButton)
            totalHeight += EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight;

        return totalHeight;
    }

    public override bool CanCacheInspectorGUI(SerializedProperty property)
    {
        // It appears that Unity can't propagate CanCacheInspectorGUI() calls,
        // so if the key or value is an ItemReference, it will flicker. This code
        // determines if either the key or value type cannot cache inspector GUI.
        SerializedProperty keysProp = property.FindPropertyRelative("m_keys");
        SerializedProperty valuesProp = property.FindPropertyRelative("m_values");

        if (keysProp == null || valuesProp == null)
            return base.CanCacheInspectorGUI(property);

        bool keysCanCache = true;
        bool valuesCanCache = true;

        if (keysProp.arraySize > 0)
            keysCanCache = EditorGUI.CanCacheInspectorGUI(keysProp.GetArrayElementAtIndex(0));
        if (valuesProp.arraySize > 0)
            valuesCanCache = EditorGUI.CanCacheInspectorGUI(valuesProp.GetArrayElementAtIndex(0));

        return keysCanCache && valuesCanCache;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializableDictionaryAttribute attr = attribute as SerializableDictionaryAttribute;

        SerializedProperty keysProp = property.FindPropertyRelative("m_keys");
        SerializedProperty valuesProp = property.FindPropertyRelative("m_values");

        // In these cases, someone used the attribute incorrectly.
        if (keysProp == null || valuesProp == null || !keysProp.isArray || !valuesProp.isArray)
        {
            base.OnGUI(position, property, label);
            return;
        }

        label = EditorGUI.BeginProperty(position, label, property);

        // Draw label.
        Rect labelRect = new Rect(position) { height = EditorGUIUtility.singleLineHeight };
        EditorGUI.LabelField(labelRect, label);
        position.yMin += labelRect.height + EditorGUIUtility.standardVerticalSpacing;

        // Draw each entry.
        ForeachEntry(keysProp, valuesProp, (key, value, idx) =>
        {
            float entryHeight = GetEntryHeight(key, value);

            Rect entryRect = new Rect(position)
            {
                height = entryHeight + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing
            };

            bool didDelete = false;

            if (attr.ShowDeleteButton)
            {
                entryRect = DrawDeleteButton(entryRect, out didDelete);
            }

            if (didDelete)
            {
                keysProp.DeleteArrayElementAtIndex(idx);
                valuesProp.DeleteArrayElementAtIndex(idx);

                // Exit text-editing mode, or else the text that is selected will
                // remain in the inspector, and this might freak people out.
                EditorGUIUtility.editingTextField = false;
            }
            else
            {
                DrawEntry(entryRect, key, value);
            }

            position.yMin = entryRect.yMax + EditorGUIUtility.standardVerticalSpacing;
            return didDelete;
        });

        // Draw add button.
        if (attr.ShowAddButton && GUI.Button(position, "Add"))
        {
            // This will create a duplicate key. The SerializableDictionary class
            // will detect the duplicate key and raise its OnCreateNewEntry event.
            keysProp.InsertArrayElementAtIndex(keysProp.arraySize);
            valuesProp.InsertArrayElementAtIndex(valuesProp.arraySize);
        }

        EditorGUI.EndProperty();
    }

    /// <summary>
    /// Performs an action for each entry in the dictionary.
    /// </summary>
    /// <param name="keys">The keys property.</param>
    /// <param name="values">The values property.</param>
    /// <param name="action">The action to perform. This should return true if the item was deleted, and false otherwise.</param>
    private void ForeachEntry(SerializedProperty keys, SerializedProperty values,
        Func<SerializedProperty, SerializedProperty, int, bool> action)
    {
        int numEntries = GetNumEntries(keys, values);

        for (int i = 0; i < numEntries; ++i)
        {
            if (action(keys.GetArrayElementAtIndex(i), values.GetArrayElementAtIndex(i), i))
            {
                // Decrease index if element got deleted.
                --i;
                --numEntries;
            }
        }
    }

    private int GetNumEntries(SerializedProperty keys, SerializedProperty values)
    {
        return Mathf.Min(keys.arraySize, values.arraySize);
    }

    private float GetEntryHeight(SerializedProperty key, SerializedProperty value)
    {
        float keyHeight = EditorGUI.GetPropertyHeight(key, true);
        float valueHeight = EditorGUI.GetPropertyHeight(value, true);

        return keyHeight + valueHeight + EditorGUIUtility.standardVerticalSpacing;
    }

    private Rect DrawDeleteButton(Rect entryRect, out bool didDelete)
    {
        Rect removeRect = new Rect(entryRect)
        {
            width = 20 // just enough for an X
        };

        didDelete = GUI.Button(removeRect, "X");
        return new Rect(entryRect) { xMin = removeRect.xMax };
    }

    private void DrawEntry(Rect entryRect, SerializedProperty key, SerializedProperty value)
    {
        Rect keyRect = new Rect(entryRect) { height = EditorGUI.GetPropertyHeight(key, true) };
        Rect valueRect = new Rect(entryRect) { yMin = keyRect.yMax + EditorGUIUtility.standardVerticalSpacing };

        DrawKey(keyRect, key);
        DrawValue(valueRect, value);
    }

    private void DrawKey(Rect keyRect, SerializedProperty key)
    {
        SerializableDictionaryAttribute attr = attribute as SerializableDictionaryAttribute;

        // Align with the value rect.
        keyRect.xMin += 10;
        keyRect = ShortPrefixLabel(keyRect, new GUIContent("Key"), 50);

        if (attr.KeyIsLongId)
        {
            if (attr.ReadonlyKey)
            {
                EditorGUI.SelectableLabel(keyRect, IdConverter.ToReadableString(key.longValue));
            }
            else
            {
                string newValue = EditorGUI.DelayedTextField(keyRect, IdConverter.ToReadableString(key.longValue));
                key.longValue = IdConverter.FromReadableString(newValue);
            }
        }
        else
        {
            // Draw the key, but possibly disable it.
            bool guiEnabled = GUI.enabled;
            if (attr.ReadonlyKey)
                GUI.enabled = false;

            EditorGUI.PropertyField(keyRect, key, GUIContent.none, true);
            GUI.enabled = guiEnabled;
        }
    }

    private void DrawValue(Rect valueRect, SerializedProperty value)
    {
        // Without this, the foldout arrow will appear outside of the rectangle.
        valueRect.xMin += 10;

        EditorGUI.PropertyField(valueRect, value, new GUIContent("Value"), true);
    }

    private Rect ShortPrefixLabel(Rect totalRect, GUIContent label, float maxWidth)
    {
        float oldWidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = maxWidth;

        Rect rect = EditorGUI.PrefixLabel(totalRect, label);

        EditorGUIUtility.labelWidth = oldWidth;

        return rect;
    }

    private static readonly GUIContent keyLabel = new GUIContent("Key");
    private static readonly GUIContent valueLabel = new GUIContent("Value");
}
