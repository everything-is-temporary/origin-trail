﻿using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using GameUtility;

/// <summary>
/// Helper class for editing definition lists. Creates a master-detail layout
/// where a sidebar ("master view") contains a list of all defined objects and
/// a "detail view" displays the selected object.
/// </summary>
public abstract class DefinitionListEditorWindow : EditorWindow
{
    protected void Initialize()
    {
        ResetListView();
        wantsMouseMove = true;
        m_isInitialized = true;
    }

    protected virtual void OnEnable()
    {
        m_isInitialized = false;
    }

    protected virtual string[] GetSortingOptions()
    {
        return null;
    }

    protected virtual MutableIntPermutation Sort(string sortingOption)
    {
        return m_objectPermutation;
    }

    protected abstract int CountObjects { get; }

    protected abstract void DrawObject(int propIdx, Rect rect);

    protected abstract void SaveChanges();

    protected abstract string GetDisplayName(int propIdx);

    protected abstract void InsertNewObject(int propIdx);

    protected abstract bool ShouldDeleteObject(int propIdx);
    protected abstract void DeleteObject(int propIdx);

    protected abstract void PrepareOnGUI();

    private void ResetListView()
    {
        int numObjects = CountObjects;

        m_objectPermutation = new MutableIntPermutation(numObjects);
        m_listView = new Utilities.ListView(numObjects);
    }

    private void OnGUI()
    {
        if (!m_isInitialized)
        {
            EditorGUI.HelpBox(new Rect(5, 5, Screen.width - 10, 50),
                "No selection.",
                MessageType.Warning);
            return;
        }

        PrepareOnGUI();

        if (Event.current.type == EventType.Layout)
        {
            // This can change the number of controls that are drawn by
            // changing which object is selected, so it should happen
            // during the Layout event or Unity will complain.

            if (m_lastSelectedIndex != m_listView.SelectedIndex)
                EditorGUIUtility.editingTextField = false;

            m_lastSelectedIndex = m_listView.SelectedIndex;
        }

        float sidebarWidth = 200;

        Rect sidebarRect = new Rect(0, 0, sidebarWidth, Screen.height - 30);
        Rect detailRect = new Rect(sidebarWidth + 5, 5, Screen.width - sidebarWidth - 10, Screen.height - 30);

        DrawSidebar(sidebarRect);
        DrawDetailView(detailRect);

        if (m_sortingType != null)
        {
            SortItemsAndUpdateSelection(m_sortingType);

            Repaint();
        }

        if (m_listView.NeedsRepaint)
            Repaint();
    }

    private void DrawSidebar(Rect sidebarRect)
    {
        float lineHeight = EditorGUIUtility.singleLineHeight;
        float spacing = EditorGUIUtility.standardVerticalSpacing;

        bool shouldDrawSortingOptions = ShouldDrawSortingSelection();

        Rect listViewRect = new Rect(sidebarRect)
        {
            yMin = shouldDrawSortingOptions ? sidebarRect.yMin + spacing + lineHeight : sidebarRect.yMin,
            yMax = sidebarRect.yMax - 2 * (spacing + lineHeight)
        };

        Rect sortRect = new Rect(sidebarRect) { yMax = listViewRect.yMin - spacing };
        Rect addRect = new Rect(listViewRect) { y = listViewRect.yMax + spacing, height = lineHeight };
        Rect delRect = new Rect(addRect) { y = addRect.yMax + spacing };

        if (shouldDrawSortingOptions)
            DrawSortingSelection(sortRect);
        DrawItemList(listViewRect);

        if (GUI.Button(addRect, "Add"))
        {
            AddNewItem();
            Repaint();
        }

        bool prevEnabled = GUI.enabled;
        GUI.enabled = m_listView.SelectedIndex != null;

        if (GUI.Button(delRect, "Delete Selected"))
        {
            int propIdx = m_objectPermutation.Inverse(m_listView.SelectedIndex.Value);
            PromptUserAndDeleteItem(propIdx);
            Repaint();
        }

        GUI.enabled = prevEnabled;
    }

    private bool ShouldDrawSortingSelection()
    {
        return GetSortingOptions() != null;
    }

    private void DrawSortingSelection(Rect rect)
    {
        string[] sortingOptions = GetSortingOptions();
        string[] allOptions = new string[sortingOptions.Length + 1];
        allOptions[0] = "Sort";
        sortingOptions.CopyTo(allOptions, 1);

        int selected = EditorGUI.Popup(rect, 0, allOptions);

        if (selected != 0)
            m_sortingType = allOptions[selected];
        else
            m_sortingType = null;
    }

    private void DrawItemList(Rect rect)
    {
        for (int propIdx = 0; propIdx < m_listView.Entries.Length; ++propIdx)
        {
            int viewIdx = m_objectPermutation.Permute(propIdx);
            m_listView.Entries[viewIdx] = new GUIContent(GetDisplayName(propIdx));
        }

        m_listView.OnGUI(rect);
    }

    private void DrawDetailView(Rect detailRect)
    {
        int? selected = m_listView.SelectedIndex;
        if (selected.HasValue)
        {
            int propIdx = m_objectPermutation.Inverse(selected.Value);

            DrawObject(propIdx, detailRect);
            SaveChanges();
        }
        else
        {
            Rect helpRect = new Rect(detailRect) { height = 50 };
            EditorGUI.HelpBox(helpRect, "Nothing selected.", MessageType.Info);
        }
    }

    private void SortItemsAndUpdateSelection(string sortingType)
    {
        int? viewIdx = m_listView.SelectedIndex;
        int? propIdx = null;
        if (viewIdx != null)
            propIdx = m_objectPermutation.Inverse(viewIdx.Value);

        m_objectPermutation = Sort(sortingType);

        if (propIdx != null)
        {
            viewIdx = m_objectPermutation.Permute(propIdx.Value);
            m_listView.SelectedIndex = viewIdx;
        }
    }

    private void AddNewItem()
    {
        int newIdx = m_objectPermutation.AddObject();
        InsertNewObject(newIdx);

        m_listView = new Utilities.ListView(m_objectPermutation.Size)
        {
            SelectedIndex = newIdx
        };
    }

    private void PromptUserAndDeleteItem(int propIdx)
    {
        if (ShouldDeleteObject(propIdx))
        {
            DeleteObject(propIdx);
            m_objectPermutation.RemoveObject(propIdx);

            // Update selected index so that it's not out of bounds.
            int? oldIdx = m_listView.SelectedIndex;
            if (oldIdx != null && oldIdx.Value >= m_objectPermutation.Size)
            {
                if (m_objectPermutation.Size > 0)
                    oldIdx = m_objectPermutation.Size - 1;
                else
                    oldIdx = null;
            }

            m_listView = new Utilities.ListView(m_objectPermutation.Size)
            {
                SelectedIndex = oldIdx
            };
        }
    }

    [SerializeField]
    private Utilities.ListView m_listView;

    [SerializeField]
    private MutableIntPermutation m_objectPermutation;

    [SerializeField]
    private int? m_lastSelectedIndex;

    private string m_sortingType;
    private bool m_isInitialized;
}

