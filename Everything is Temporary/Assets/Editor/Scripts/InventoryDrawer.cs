﻿using UnityEngine;
using UnityEditor;
using GameInventory;
using Utilities;

[CustomPropertyDrawer(typeof(Inventory))]
public class InventoryDrawer : PropertyDrawer
{
    public override bool CanCacheInspectorGUI(SerializedProperty property)
    {
        return false; // otherwise item icons will flicker
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty itemStacks = GetItemStacksProperty(property);
        int numStacks = itemStacks.arraySize;

        float labelHeight = EditorGUIUtility.singleLineHeight;
        float spacing = EditorGUIUtility.standardVerticalSpacing;
        float addConfigurationHeight = m_itemReferenceHeight;
        float addButtonHeight = EditorGUIUtility.singleLineHeight;

        return labelHeight + spacing
            + m_gridLayout.ComputePreferredHeight(Screen.width, numStacks) + spacing
            + addConfigurationHeight + spacing
            + addButtonHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        m_itemList = ItemDefinitionList.Instance;
        m_deleteIdx = null;


        // Compute rects.
        float labelHeight = EditorGUIUtility.singleLineHeight;
        float spacing = EditorGUIUtility.standardVerticalSpacing;
        float addConfigurationHeight = m_itemReferenceHeight;
        float addButtonHeight = EditorGUIUtility.singleLineHeight;

        Rect labelRect = new Rect(position) { height = labelHeight };
        Rect addConfigurationRect = new Rect(position)
        {
            yMin = position.yMax - addButtonHeight - spacing - addConfigurationHeight
        };
        Rect gridRect = new Rect(position)
        {
            yMin = labelRect.yMax + spacing,
            yMax = addConfigurationRect.yMin - spacing
        };

        // Draw main grid.
        label = EditorGUI.BeginProperty(position, label, property);
        GUI.Label(labelRect, label);
        SetupGridLayout(property);
        m_gridLayout.OnGUI(gridRect);
        EditorGUI.EndProperty();

        // Draw add button and item reference.
        bool shouldAdd = DrawAddItemReferenceButton(addConfigurationRect);

        // Add item if user chose to add.
        if (shouldAdd)
        {
            SerializedProperty itemStacksProp = GetItemStacksProperty(property);
            itemStacksProp.InsertArrayElementAtIndex(itemStacksProp.arraySize);

            SerializedProperty newItemWithAmountProp = itemStacksProp.GetArrayElementAtIndex(itemStacksProp.arraySize - 1);
            SerializedProperty newAmountProp = newItemWithAmountProp.FindPropertyRelative("m_amount");
            SerializedProperty newItemIdProp = newItemWithAmountProp.FindPropertyRelative("m_itemReference.m_id");

            newAmountProp.intValue = 1;
            newItemIdProp.longValue = m_itemReferenceContainer.ItemReference.Id;

            m_itemReferenceContainer.ItemReference = ItemReference.FromId(0);
        }
        // Delete item if user selected something to delete.
        else if (m_deleteIdx.HasValue && m_itemList != null)
        {
            DeleteItemIfUserWants(property, m_deleteIdx.Value);
        }
    }

    /// <summary>
    /// Sets the <see cref="FlexibleGridLayout.Elements"/> array of
    /// <see cref="m_gridLayout"/> to use <see cref="DrawElement"/>.
    /// </summary>
    /// <param name="inventoryProperty">Inventory property.</param>
    private void SetupGridLayout(SerializedProperty inventoryProperty)
    {
        SerializedProperty itemStacks = GetItemStacksProperty(inventoryProperty);
        m_gridLayout.Elements.Clear();
        for (int i = 0; i < itemStacks.arraySize; ++i)
        {
            int itemIndex = i;
            m_gridLayout.Elements.Add(pos =>
            {
                if (DrawElement(pos, itemStacks.GetArrayElementAtIndex(itemIndex)))
                    m_deleteIdx = itemIndex;
            });
        }
    }

    /// <summary>
    /// Draws the <see cref="ItemWithAmount"/> property, and returns true if
    /// the user wants to remove it from the inventory.
    /// </summary>
    /// <returns><c>true</c>, if should be removed from inventory, <c>false</c> otherwise.</returns>
    /// <param name="position">Position where to draw this.</param>
    /// <param name="itemWithAmountProperty"><see cref="ItemWithAmount"/> property.</param>
    private bool DrawElement(Rect position, SerializedProperty itemWithAmountProperty)
    {
        if (m_itemList == null)
            return false;

        SerializedProperty itemAmountProperty = itemWithAmountProperty.FindPropertyRelative("m_amount");
        SerializedProperty itemIdProperty = itemWithAmountProperty.FindPropertyRelative("m_itemReference.m_id");

        int itemAmount = itemAmountProperty.intValue;
        long itemId = itemIdProperty.longValue;

        float lineHeight = EditorGUIUtility.singleLineHeight;
        float spacing = EditorGUIUtility.standardVerticalSpacing;

        // With the name style, names will show up at the top of the rect.
        Rect nameRect = new Rect(position);
        Rect iconRect = new Rect(position) { yMin = position.yMin + lineHeight + spacing, height = 50 };
        Rect amountRect = new Rect(position) { yMin = iconRect.yMax + spacing };

        // Covers name and icon.
        Rect selectionRect = new Rect(position) { yMax = iconRect.yMax };

        // Draw icon and name.
        if (m_itemList.HasItem(itemId))
        {
            InventoryItem item = m_itemList.GetItem(itemId);

            GUISpriteDrawer.DrawCorrectAspect(iconRect, item.Icon);
            GUI.Label(nameRect, item.Name, m_itemNameStyle);
        }
        else
        {
            GUISpriteDrawer.Draw(iconRect, null, null);
            GUI.Label(nameRect, "??", m_itemNameStyle);
        }

        // Draw amount field.
        int newItemAmount = EditorGUI.IntField(amountRect, itemAmount);

        // Update property if necessary.
        if (newItemAmount != itemAmount)
            itemAmountProperty.intValue = newItemAmount;


        // Delete if user clicks.
        return GUIRectUtils.ClickableRect(selectionRect, MouseCursor.ArrowMinus);
    }

    /// <summary>
    /// Draws an <see cref="ItemReference"/> selection box and an Add button. Returns
    /// true if the user pressed the Add button and <see cref="m_itemReferenceContainer"/>
    /// has a valid <see cref="ItemReference"/>.
    /// </summary>
    /// <returns><c>true</c>, if the user wants to add a new item to the inventory, <c>false</c> otherwise.</returns>
    /// <param name="position">Position where to draw this.</param>
    private bool DrawAddItemReferenceButton(Rect position)
    {
        if (m_itemList == null)
        {
            EditorGUI.HelpBox(position, "No item definition list found.", MessageType.Warning);
            return false;
        }

        bool canAdd = true;
        string itemName = null;

        float itemRefHeight = m_itemReferenceHeight;
        float spacing = EditorGUIUtility.standardVerticalSpacing;

        Rect itemRefRect = new Rect(position) { height = itemRefHeight };
        Rect addButtonRect = new Rect(position) { yMin = itemRefRect.yMax + spacing };

        // Draw item reference with picker.
        {
            // Not using the Item property of ItemReference because it's cached,
            // and the ItemReference might try to access the ItemDefinitionList.Instance
            // while it's still null.
            if (m_itemList.HasItem(m_itemReferenceContainer.ItemReference.Id))
            {
                InventoryItem item = m_itemList.GetItem(m_itemReferenceContainer.ItemReference.Id);
                itemName = item.Name;

                Rect itemIconRect = new Rect(itemRefRect) { width = itemRefRect.height };
                Rect itemNameRect = new Rect(itemRefRect) { xMin = itemIconRect.xMax + spacing };
                GUISpriteDrawer.DrawCorrectAspect(itemIconRect, item.Icon);
                GUI.Label(itemNameRect, item.Name);
            }
            else
            {
                EditorGUI.HelpBox(itemRefRect, "Click to select new item.", MessageType.Info);
                canAdd = false;
            }

            if (GUIRectUtils.ClickableRect(itemRefRect, MouseCursor.Link))
            {
                ItemPicker.ShowForItemReferenceContainer(m_itemReferenceContainer, "Select item to add to inventory");
            }
        }

        // Draw add button.
        bool oldEnabled = GUI.enabled;
        GUI.enabled &= canAdd;
        bool didAdd = GUI.Button(addButtonRect, itemName == null ? "Add" : $"Add {itemName}");
        GUI.enabled = oldEnabled;

        return didAdd;
    }

    /// <summary>
    /// Deletes the item at the given index in the inventory property after
    /// prompting the user.
    /// </summary>
    /// <param name="inventoryProperty">Inventory property.</param>
    /// <param name="deleteIdx">Delete index.</param>
    private void DeleteItemIfUserWants(SerializedProperty inventoryProperty, int deleteIdx)
    {
        SerializedProperty itemStacks = GetItemStacksProperty(inventoryProperty);

        long itemId = itemStacks.GetArrayElementAtIndex(deleteIdx)
            .FindPropertyRelative("m_itemReference.m_id").longValue;

        bool shouldDelete;

        if (!m_itemList.HasItem(itemId))
        {
            shouldDelete = true;
        }
        else
        {
            InventoryItem item = m_itemList.GetItem(itemId);

            shouldDelete = EditorUtility.DisplayDialog("Delete?",
                $"Are you sure you want to remove '{item.Name}' from this inventory?",
                "Delete", "Cancel");
        }

        if (shouldDelete)
            itemStacks.DeleteArrayElementAtIndex(deleteIdx);
    }

    /// <summary>
    /// Gets the property corresponding to the list of item stacks in the inventory.
    /// </summary>
    /// <returns>The item stacks property.</returns>
    /// <param name="inventoryProperty">Inventory property.</param>
    private SerializedProperty GetItemStacksProperty(SerializedProperty inventoryProperty)
    {
        return inventoryProperty.FindPropertyRelative("m_itemStacksSerialized");
    }

    private readonly FlexibleGridLayout m_gridLayout = new FlexibleGridLayout
    {
        ElementHeight = 2 * (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing) + 50,
        ElementWidth = 70
    };

    private readonly GUIStyle m_itemNameStyle = new GUIStyle(GUI.skin.label)
    {
        wordWrap = true,
        alignment = TextAnchor.UpperCenter
    };

    private int? m_deleteIdx;
    private ItemPicker.ItemReferenceContainer m_itemReferenceContainer = new ItemPicker.ItemReferenceContainer();
    private ItemDefinitionList m_itemList;

    private readonly float m_itemReferenceHeight = 50;
}
