﻿using UnityEngine;
using UnityEditor;
using GameInventory;
using System.Linq;

public static class ShopTools
{
    [MenuItem("Tools/Origin Trail/Shops/Go To Shops Folder", priority = 1)]
    private static void OpenShopsFolder()
    {
        EditorUtility.FocusProjectWindow();

        // NOTE: Asset paths in Unity always use forward slashes.
        // https://docs.unity3d.com/ScriptReference/Resources.Load.html
        var folderAsset = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Shops", typeof(Object));
        Selection.activeObject = folderAsset;
        EditorGUIUtility.PingObject(folderAsset);
    }

    [MenuItem("Tools/Origin Trail/Shops/Create New Shop", priority = 2)]
    private static void CreateNewShop()
    {
        Shop newShop = ScriptableObject.CreateInstance<Shop>();

        string newShopPath = AssetDatabase.GenerateUniqueAssetPath("Assets/Prefabs/Shops/New Shop.asset");

        AssetDatabase.CreateAsset(newShop, newShopPath);

        EditorUtility.FocusProjectWindow();
        ProjectWindowUtil.ShowCreatedAsset(newShop);
    }
}
