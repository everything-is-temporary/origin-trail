﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameInventory;
using System.IO;

public class ShopAssetPostprocessor : AssetPostprocessor
{
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        bool modifiedShopList = false;

        foreach (string path in importedAssets.Concat(movedAssets))
        {
            if (IsShopPath(path))
                modifiedShopList |= HandleNewItemInShopsFolder(path);
        }

        foreach (string path in deletedAssets)
        {
            if (IsShopPath(path))
                modifiedShopList |= HandleItemRemovedFromShopsFolder(path);
        }

        if (modifiedShopList)
        {
            EditorUtility.SetDirty(ShopDefinitionList.Instance);
            AssetDatabase.SaveAssets();
        }
    }

    /// <summary>
    /// Quick check to determine whether this path is inside the Shops folder.
    /// </summary>
    /// <returns><c>true</c>, if shop path was ised, <c>false</c> otherwise.</returns>
    /// <param name="path">Path relative to project folder.</param>
    static bool IsShopPath(string path)
    {
        string pathPrefix = "Assets/Prefabs/Shops".Replace('/', Path.DirectorySeparatorChar);
        return path.StartsWith(pathPrefix, System.StringComparison.InvariantCulture);
    }

    static bool HandleNewItemInShopsFolder(string path)
    {
        Shop obj = GetShopAsset(path);

        if (obj != null)
        {
            ShopDefinitionList.Instance.AddScriptableShop(obj);
            return true;
        }

        return false;
    }

    static bool HandleItemRemovedFromShopsFolder(string path)
    {
        ShopDefinitionList.Instance.RemoveNullShops();
        return true;
    }

    static Shop GetShopAsset(string path)
    {
        return AssetDatabase.LoadAssetAtPath<Shop>(path);
    }
}
