﻿using System;
using UnityEngine;
using UnityEditor;
using Interaction;
using GameUtility;
using Utilities;

public class InteractiveObjectCreator : ScriptableWizard
{
    public GameObject parent;
    public bool startsInkKnot;
    public string inkKnot;

    public static void AddSpeechBubble(BasicInteractiveObject interactiveObject)
    {
        var speechBubblePrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Speech Bubble.prefab");
        if (speechBubblePrefab == null)
        {
            Debug.LogWarning("Couldn't locate speech bubble prefab at Prefabs/Speech Bubble");
        }
        else
        {
            var speechBubble = PrefabUtility.InstantiatePrefab(speechBubblePrefab) as GameObject;

            GameObjectUtility.SetParentAndAlign(speechBubble, interactiveObject.gameObject);
            speechBubble.transform.localPosition = Vector3.up * 0.3f;

            interactiveObject.selectionVisualObject = speechBubble;
        }
    }

    public static void AddDialogueInteraction(BasicInteractiveObject interactiveObject, string inkKnot)
    {
        var dialogueStarterScript = interactiveObject.gameObject.GetOrAddComponent<DialogueStarterScript>();

        interactiveObject.onInteract.AddPersistentListener(dialogueStarterScript.StartDialogue, inkKnot);
    }

    [MenuItem("GameObject/Origin Trail/Interactive Object", false, 10)]
    private static void CreateInteractiveObject(MenuCommand menuCommand)
    {
        var wizard = DisplayWizard<InteractiveObjectCreator>("Create an interactive object");

        if (menuCommand.context is GameObject parent)
            wizard.parent = parent;
    }

    protected override bool DrawWizardGUI()
    {
        EditorGUI.BeginChangeCheck();

        parent = EditorGUILayout.ObjectField(new GUIContent("Parent"), parent, typeof(GameObject), true) as GameObject;

        startsInkKnot = EditorGUILayout.Toggle(new GUIContent("Starts Ink knot?"), startsInkKnot);

        if (startsInkKnot)
            inkKnot = EditorGUILayout.TextField(new GUIContent("Ink knot"), inkKnot);

        return EditorGUI.EndChangeCheck();
    }

    private void OnWizardCreate()
    {
        var go = new GameObject("Interactive Object");

        var interactiveObject = go.AddComponent<BasicInteractiveObject>();

        AddSpeechBubble(interactiveObject);

        if (startsInkKnot)
            AddDialogueInteraction(interactiveObject, inkKnot);

        if (parent != null)
            GameObjectUtility.SetParentAndAlign(go, parent);

        Selection.activeObject = go;
        EditorGUIUtility.PingObject(go);
        Undo.RegisterCreatedObjectUndo(go, "Created interactive object");
    }
}
