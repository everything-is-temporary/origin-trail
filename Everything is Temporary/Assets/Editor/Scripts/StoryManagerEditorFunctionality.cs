﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Dialogue;

[CustomEditor(typeof(StoryManager))]
public class StoryManagerEditor : Editor
{

    private ChoicesElement currentChoices = null;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        StoryManager storyManager = (StoryManager)target;
        if(GUILayout.Button("Story Proceed"))
        {
            StoryElement result = storyManager.PeekNext(); // The old functionality was removed from here, will this be sufficient to replace it?
            if (result is ChoicesElement)
            {
                currentChoices = (Dialogue.ChoicesElement)result;
            }
            result.Handle();
        }
        if (currentChoices != null)
        {
            foreach (System.Collections.Generic.KeyValuePair<int, DialogueElement> choice in currentChoices.Choices)
            {
                if(GUILayout.Button(choice.Value.ToString())){
                    makeChoice(storyManager, choice.Key);
                }
            }
        }
    }

    private void makeChoice(StoryManager manager, int index)
    {
        manager.MakeChoice(index);
        currentChoices = null;
    }
}
