﻿using GameUtility;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class RunSceneTools : ScriptableObject
{
    [MenuItem("Tools/Origin Trail/Run Main Scene", priority = 1)]
    static void RunMainScene()
    {
        EditorSceneManager.OpenScene("Assets/Scenes/MainGameScene.unity");
        EditorApplication.isPlaying = true;
    }

    [MenuItem("Tools/Origin Trail/Load Game Scene", priority = 2)]
    static void LoadGameScene()
    {
        LoadGameSceneAsync().FireAndForget();

        async Task LoadGameSceneAsync()
        {
            string scenePath = await SceneSelectionWindow.GetSceneSelection();
            string sceneName = Path.GetFileNameWithoutExtension(scenePath);
            await CoreGameManager.Instance.OpenGameSceneAsync(sceneName);
        }
    }

    [MenuItem("Tools/Origin Trail/Load Game Scene", true)]
    static bool ValidateLoadGameScene()
    {
        return EditorApplication.isPlaying;
    }

    [MenuItem("Tools/Origin Trail/Open Scene", priority = 1)]
    static void OpenScene()
    {
        OpenSceneAsync().FireAndForget();

        async Task OpenSceneAsync()
        {
            string scenePath = await SceneSelectionWindow.GetSceneSelection();
            EditorSceneManager.OpenScene(scenePath);
        }
    }

    [MenuItem("Tools/Origin Trail/Open Scene", true)]
    static bool ValidateOpenScene()
    {
        return !EditorApplication.isPlaying;
    }
}
