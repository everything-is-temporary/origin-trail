﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using System.Linq;
using System.Collections.Generic;

public class SidescrollerSceneCreator : EditorWindow
{
    [MenuItem("Tools/Origin Trail/New Sidescroller Scene")]
    static void ShowCreateNewScene()
    {
        GetWindow<SidescrollerSceneCreator>();
    }

    private void OnGUI()
    {
        m_sceneName = GUILayout.TextField(m_sceneName);

        string sceneRelativePath = Path.Combine("Scenes/Sidescroller Scenes/", $"{m_sceneName}.unity");

        // Verify the scene name and display an error message if it is empty or if the
        // scene already exists.
        bool sceneNameValid;
        if (m_sceneName.Length == 0)
        {
            EditorGUILayout.HelpBox("Please enter the name of the new scene.", MessageType.Error);
            sceneNameValid = false;
        }
        else if (File.Exists(Path.Combine(Application.dataPath, sceneRelativePath)))
        {
            EditorGUILayout.HelpBox($"The file '{sceneRelativePath}' already exists.", MessageType.Error);
            sceneNameValid = false;
        }
        else
        {
            EditorGUILayout.HelpBox($"The scene will be saved as {sceneRelativePath}", MessageType.Info);
            sceneNameValid = true;
        }

        bool oldEnabled = GUI.enabled;
        GUI.enabled = sceneNameValid; // disables button if name not valid

        if (GUILayout.Button("Create!"))
        {
            Scene newScene = CreateNewScene(m_sceneName, sceneRelativePath);

            AddSceneToBuildSettings(newScene);
            Debug.Log($"Added the scene '{m_sceneName}' to the build settings.");

            Close();
        }

        GUI.enabled = oldEnabled;

    }

    /// <summary>
    /// Creates a new scene named <paramref name="sceneName"/> saving it at path
    /// <paramref name="path"/>.
    /// </summary>
    /// <param name="sceneName">Name of new scene.</param>
    /// <param name="path">Path to new scene, specified relative to
    /// Assets folder and ending with '.unity.',
    /// e.g. "Scenes/Sidescroller Scenes/NewScene.unity".</param>
    private Scene CreateNewScene(string sceneName, string path)
    {
        string projectRelativePath = Path.Combine("Assets/", path);
        string absolutePath = Path.Combine(Application.dataPath, path);

        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

        // Copy the template scene to the new location.
        File.Copy(templateScenePath, absolutePath);

        var newScene = EditorSceneManager.OpenScene(projectRelativePath);

        // This will trigger the SidescrollerSceneVerifier, which will add any
        // essential things to the scene.
        EditorSceneManager.SaveScene(newScene, projectRelativePath);

        return newScene;
    }

    /// <summary>
    /// Adds the scene to the build settings, assuming that it has an associated
    /// path.
    /// </summary>
    /// <param name="scene">Scene.</param>
    private void AddSceneToBuildSettings(Scene scene)
    {
        // Add the scene to build settings automatically so that it can be loaded in game.
        List<EditorBuildSettingsScene> buildSettingsScenes
            = new List<EditorBuildSettingsScene>(EditorBuildSettings.scenes);
        buildSettingsScenes.Add(new EditorBuildSettingsScene(scene.path, true));
        EditorBuildSettings.scenes = buildSettingsScenes.ToArray();
    }

    private string m_sceneName = "";
    private static readonly string templateScenePath = Path.Combine(Application.dataPath, "Editor Default Resources/Default Sidescroller Scene.unity");
}
