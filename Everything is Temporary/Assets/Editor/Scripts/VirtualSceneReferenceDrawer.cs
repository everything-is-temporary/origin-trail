﻿using UnityEngine;
using UnityEditor;
using Audio;

[CustomPropertyDrawer(typeof(VirtualSceneReference))]
public class VirtualSceneReferenceDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return lineHeight * 4 + spacing * 2;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty idProp = property.FindPropertyRelative("m_sceneId");
        SerializedProperty managerProp = property.FindPropertyRelative("m_sceneManager");

        VirtualSceneManager sceneManager = managerProp.objectReferenceValue as VirtualSceneManager;

        Rect labelRect = new Rect(position) { height = lineHeight };
        Rect managerRect = new Rect(position) { yMin = labelRect.yMax + spacing, height = lineHeight };
        Rect idRect = new Rect(position) { yMin = managerRect.yMax + spacing, height = 2 * lineHeight };


        label = EditorGUI.BeginProperty(position, label, property);

        EditorGUI.LabelField(labelRect, label);
        EditorGUI.ObjectField(managerRect, managerProp);

        if (sceneManager != null)
        {
            long id = idProp.longValue;
            string name = sceneManager.GetSceneName(id) ?? "None";

            if (EditorGUI.DropdownButton(idRect, new GUIContent(name), FocusType.Passive))
                GetSceneDropdown(sceneManager, id).DropDown(idRect);
        }
        else
        {
            EditorGUI.HelpBox(idRect, "No scene manager", MessageType.Warning);
        }

        if (m_selected.HasValue)
            idProp.longValue = m_selected.Value;

        EditorGUI.EndProperty();
    }

    private GenericMenu GetSceneDropdown(VirtualSceneManager manager, long selected)
    {
        GenericMenu menu = new GenericMenu();

        foreach (long id in manager.ScenesWithNames)
        {
            string name = manager.GetSceneName(id);
            long idCopy = id;
            menu.AddItem(new GUIContent(name), selected == id, () => m_selected = idCopy);
        }

        return menu;
    }

    private long? m_selected;
    private static readonly float spacing = EditorGUIUtility.standardVerticalSpacing;
    private static readonly float lineHeight = EditorGUIUtility.singleLineHeight;
}
