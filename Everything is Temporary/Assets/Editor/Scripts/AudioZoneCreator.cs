﻿using System;
using UnityEngine;
using UnityEditor;
using Audio;

public static class AudioZoneCreator
{
    [MenuItem("GameObject/Origin Trail/Audio/Audio Zone", false, 10)]
    private static void CreateVirtualAudioSource(MenuCommand menuCommand)
    {
        GameObject parent = menuCommand.context as GameObject;

        // Create Audio Zone
        GameObject audioZoneGo = new GameObject("Audio Zone");
        audioZoneGo.AddComponent<AudioZoneScript>();

        // Align to parent, if any.
        if (parent != null)
            GameObjectUtility.SetParentAndAlign(audioZoneGo, parent);

        // Register undo operation and select the new object.
        Undo.RegisterCreatedObjectUndo(audioZoneGo, "Created Audio Zone");
        EditorGUIUtility.PingObject(audioZoneGo);
        Selection.activeObject = audioZoneGo;
    }
}
