﻿using System;
using UnityEngine;
using UnityEditor;
using Audio;

[CustomEditor(typeof(AudioZoneScript))]
public class AudioZoneEditor : Editor
{
    private void OnSceneGUI()
    {
        AudioZoneScript audioZone = (AudioZoneScript)target;

        // These are copies because Rect is a struct.
        Rect worldZone = audioZone.audioZone;
        Rect worldBounds = audioZone.audioBounds;

        worldZone.center = audioZone.transform.position;
        worldBounds.center = audioZone.transform.position;

        worldZone = DrawEditableRect(worldZone, Color.green);
        worldBounds = DrawEditableRect(worldBounds, Color.red);

        audioZone.audioZone = new Rect(worldZone) { center = audioZone.audioZone.center };
        audioZone.audioBounds = new Rect(worldBounds) { center = audioZone.audioBounds.center };
    }

    private Rect DrawEditableRect(Rect worldRect, Color color)
    {
        Handles.DrawSolidRectangleWithOutline(worldRect, new Color(0, 0, 0, 0), color);

        var handles = new[]
        {
            (Vector3.up, worldRect.center + worldRect.size.y * Vector2.up),
            (Vector3.right, worldRect.center + worldRect.size.x * Vector2.right),
            (Vector3.down, worldRect.center + worldRect.size.y * Vector2.down),
            (Vector3.left, worldRect.center + worldRect.size.x * Vector2.left)
        };

        float[] adjustments = new float[4];

        for (int i = 0; i < 4; ++i)
        {
            (Vector3 dir, Vector2 pos) = handles[i];

            Vector3 newPos = Handles.Slider(pos, dir);

            adjustments[i] = Vector3.Dot(newPos - (Vector3)pos, dir);
        }

        return new Rect(worldRect)
        {
            width = worldRect.width + adjustments[1] + adjustments[3],
            height = worldRect.height + adjustments[0] + adjustments[2]
        };
    }
}
