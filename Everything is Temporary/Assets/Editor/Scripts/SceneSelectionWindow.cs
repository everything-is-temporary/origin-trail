﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class SceneSelectionWindow : EditorWindow
{
    /// <summary>
    /// Opens a scene selection window and prompts the user for input.
    /// </summary>
    /// <exception cref="OperationCanceledException">Thrown if the operation is canceled.</exception>
    /// <returns>The selected scene's path.</returns>
    public static async Task<string> GetSceneSelection()
    {
        SceneSelectionWindow window = GetWindow<SceneSelectionWindow>("Select a scene");

        if (window.m_selectionTask != null)
        {
            window.m_selectionTask.SetCanceled();
        }

        window.m_selectionTask = new TaskCompletionSource<string>();

        try
        {
            string result = await window.m_selectionTask.Task;

            window.Close();

            return result;
        }
        catch (OperationCanceledException ex)
        {
            throw new OperationCanceledException("Some code called GetSceneSelection()" +
                " while a scene selection was in progress, canceling the original one.", ex);
        }

    }

    private void OnGUI()
    {
        EditorGUILayout.BeginVertical();

        EditorGUILayout.HelpBox("Only scenes in the build settings are shown.", MessageType.Info);

        int numScenesInBuild = SceneManager.sceneCountInBuildSettings;

        for (int sceneIdx = 0; sceneIdx < numScenesInBuild; ++sceneIdx)
        {
            string scenePath = SceneUtility.GetScenePathByBuildIndex(sceneIdx);

            if (GUILayout.Button(new GUIContent($"({sceneIdx}) {scenePath}")))
                m_selectionTask.TrySetResult(scenePath);
        }

        EditorGUILayout.EndVertical();
    }

    private TaskCompletionSource<string> m_selectionTask;
}
