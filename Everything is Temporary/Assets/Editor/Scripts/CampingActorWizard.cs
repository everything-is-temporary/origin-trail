﻿using System;
using UnityEngine;
using UnityEditor;
using Characters;

public class CampingActorWizard : ScriptableWizard
{
    public CampingActorFactory.Configuration configuration;

    public static CampingActorWizard OpenWizard(string title)
    {
        return DisplayWizard<CampingActorWizard>(title);
    }

    private void OnWizardUpdate()
    {
        errorString = "";
        if (configuration?.character?.CharacterOrNull == null)
            errorString = "Invalid character ID";
    }

    private void OnWizardCreate()
    {
        GameObject go = CampingActorFactory.Create(configuration);

        string fileName = string.IsNullOrWhiteSpace(configuration.objectName) ? "New Camping Prefab" : configuration.objectName;
        string filePath = $"Assets/Prefabs/Party Members/{fileName}.prefab";
        GameObject prefab = PrefabUtility.SaveAsPrefabAsset(go, filePath, out bool success);

        if (!success)
        {
            EditorUtility.DisplayDialog("Failed", $"Failed to create prefab at {filePath}", "Ok");
        }
        else
        {
            EditorGUIUtility.PingObject(prefab);
            Selection.activeObject = prefab;
        }

        Undo.DestroyObjectImmediate(go);
    }
}
