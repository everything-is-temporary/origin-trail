﻿using UnityEditor;
using UnityEngine;
using GameInventory;

[CustomPropertyDrawer(typeof(ItemReference))]
public class ItemReferenceDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 70;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var idProp = property.FindPropertyRelative("m_id");

        Debug.Assert(idProp != null);

        position = EditorGUI.IndentedRect(position);

        var labelRect = new Rect(position.x, position.y, position.width, 15);
        var iconRect = new Rect(position.x, labelRect.yMax + 5, 50, 50);
        var nameRect = new Rect(iconRect.xMax + 5, position.y, position.xMax - iconRect.xMax - 5, 50);

        label = EditorGUI.BeginProperty(position, label, property);

        GUI.Label(labelRect, label, new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleLeft });

        ItemDefinitionList definitionList = ItemDefinitionList.Instance;

        if (definitionList == null || !definitionList.HasItem(idProp.longValue))
        {
            EditorGUI.HelpBox(iconRect, "No item.", MessageType.Warning);
        }
        else
        {
            var item = definitionList.GetItem(idProp.longValue);

            GUISpriteDrawer.DrawCorrectAspect(iconRect, item.Icon);

            EditorGUI.SelectableLabel(nameRect, item.Name, new GUIStyle(GUI.skin.label) { alignment = TextAnchor.MiddleLeft });
        }


        EditorGUIUtility.AddCursorRect(iconRect, MouseCursor.Zoom);

        if (iconRect.Contains(Event.current.mousePosition) && Event.current.type == EventType.MouseUp)
            ItemPicker.ShowForItemReferenceProperty(property);

        EditorGUI.EndProperty();
    }

    public override bool CanCacheInspectorGUI(SerializedProperty property)
    {
        // This prevents the item icon from flickering
        // https://answers.unity.com/questions/377207/drawing-a-texture-in-a-custom-propertydrawer.html
        return false;
    }
}
