﻿using System.Linq;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

using GameInventory;


public class ItemPicker : EditorWindow
{
    public class ItemReferenceContainer
    {
        public event System.Action<ItemReference> OnChanged;

        public ItemReference ItemReference
        {
            get => m_itemReference;
            set
            {
                m_itemReference = value;
                OnChanged?.Invoke(value);
            }
        }

        private ItemReference m_itemReference = ItemReference.FromId(0);
    }

    public static void ShowForItemReferenceProperty(SerializedProperty itemRefProp)
    {
        var itemPicker = GetWindow<ItemPicker>($"Select item for '{itemRefProp.name}'");
        itemPicker.Initialize(itemRefProp);
    }

    /// <summary>
    /// Shows item picker that modifies the <see cref="ItemReference"/> in the
    /// container object. The window will close when the user selects an option.
    /// </summary>
    /// <param name="itemRefContainer">Item reference container.</param>
    /// <param name="title">Title for the window.</param>
    public static void ShowForItemReferenceContainer(ItemReferenceContainer itemRefContainer, string title)
    {
        var itemPicker = GetWindow<ItemPicker>(title);
        itemPicker.Initialize(itemRefContainer);
    }

    private void OnGUI()
    {
        DrawRegular();
    }

    private void Initialize(SerializedProperty itemRefProp)
    {
        Debug.Assert(itemRefProp != null);

        m_itemRefProp = itemRefProp;
        m_selectedItemId = m_itemRefProp.FindPropertyRelative("m_id").longValue;

        wantsMouseMove = true;
    }

    private void Initialize(ItemReferenceContainer itemRefContainer)
    {
        Debug.Assert(itemRefContainer != null);

        m_itemRefContainer = itemRefContainer;
        m_selectedItemId = m_itemRefContainer.ItemReference.Id;

        wantsMouseMove = true;
    }


    private void DrawRegular()
    {
        var itemList = ItemDefinitionList.Instance;

        if (itemList != null)
            DrawSelectionGrid(itemList);
        else
            EditorGUILayout.HelpBox("No ItemDefinitionList instance found.", MessageType.Error);
    }


    private void DrawSelectionGrid(ItemDefinitionList itemList)
    {
        float margins = 10;
        float width = Screen.width - margins * 2;

        float widthPerItem = 100;
        float heightPerItem = widthPerItem + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

        float widthPerGap = 10;

        int itemsPerLine = (int)((width - widthPerItem) / (widthPerItem + widthPerGap)) + 1;

        List<long> itemIds = itemList.EnumerateItemIds().ToList();

        m_scrollPosition = EditorGUILayout.BeginScrollView(m_scrollPosition);

        EditorGUILayout.BeginVertical();
        GUILayout.Space(margins);

        int itemIdx = 0;
        while (itemIdx < itemIds.Count)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(margins);

            for (int itemLineIdx = 0; itemLineIdx < itemsPerLine && itemIdx < itemIds.Count; ++itemLineIdx, ++itemIdx)
            {
                var itemRect = GUILayoutUtility.GetRect(widthPerItem, widthPerItem, heightPerItem, heightPerItem);

                DrawSelectableItem(itemRect, itemIds[itemIdx], itemList);

                // Put a gap between all items.
                if (itemLineIdx < itemsPerLine - 1)
                    GUILayout.Space(widthPerGap);
            }

            // Push all items to the left, so that they don't automatically spread out.
            GUILayout.FlexibleSpace();

            EditorGUILayout.EndHorizontal();

            // Put a gap between rows of items.
            GUILayout.Space(widthPerGap);

        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.EndScrollView();
    }


    private void DrawSelectableItem(Rect rect, long itemId, ItemDefinitionList itemList)
    {
        float verticalSpacing = EditorGUIUtility.standardVerticalSpacing;
        float lineHeight = EditorGUIUtility.singleLineHeight;

        Rect nameRect = new Rect(rect) { yMax = rect.yMin + lineHeight };
        Rect iconRect = new Rect(rect) { yMin = nameRect.yMax + verticalSpacing };

        var item = itemList.GetItem(itemId);

        GUI.Label(nameRect, item.Name);

        if (rect.Contains(Event.current.mousePosition))
        {
            GUISpriteDrawer.DrawCorrectAspect(iconRect, item.Icon, Color.gray);

            if (m_hoveredItemId != itemId)
            {
                m_hoveredItemId = itemId;
                Repaint(); // mouse move events don't trigger repaint events
            }

            if (Event.current.type == EventType.MouseUp)
            {
                SelectItem(itemId);
            }
        }
        else
        {
            GUISpriteDrawer.DrawCorrectAspect(iconRect, item.Icon);

            if (m_hoveredItemId == itemId)
            {
                m_hoveredItemId = 0;
                Repaint();
            }
        }


        // If this is the selected item, draw a transparent box over it.
        if (itemId == m_selectedItemId)
        {
            var prevColor = GUI.color;
            GUI.color = new Color(1, 1, 1, 0.5f);
            GUI.Box(rect, "");
            GUI.color = prevColor;
        }


        EditorGUIUtility.AddCursorRect(rect, MouseCursor.ArrowPlus);
    }


    private void SelectItem(long itemId)
    {
        m_selectedItemId = itemId;

        if (m_itemRefProp != null)
        {
            var itemIdProp = m_itemRefProp.FindPropertyRelative("m_id");
            Debug.Assert(itemIdProp != null);

            itemIdProp.longValue = itemId;

            m_itemRefProp.serializedObject.ApplyModifiedProperties();
        }
        else
        {
            Debug.Assert(m_itemRefContainer != null);
            m_itemRefContainer.ItemReference = ItemReference.FromId(itemId);

            // When used with an item reference container, the item picker
            // should close when the user picks an item.
            Close();
        }
    }


    private long m_selectedItemId;
    private long m_hoveredItemId;
    private Vector2 m_scrollPosition;

    // Exactly one of these is non-null.
    private SerializedProperty m_itemRefProp;
    private ItemReferenceContainer m_itemRefContainer;
}
