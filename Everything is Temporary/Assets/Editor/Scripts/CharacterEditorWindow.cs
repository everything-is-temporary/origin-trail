﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GameInventory;
using GameUtility;
using Characters;

public class CharacterEditorWindow : DefinitionListEditorWindow
{
    public static void Open(CharacterDefinitionList characterList)
    {
        var window = GetWindow<CharacterEditorWindow>("Character Editor");

        window.Instantiate(characterList);
    }

    protected override int CountObjects
    {
        get
        {
            return EnumerateCharacters().Count();
        }
    }

    protected override void DrawObject(int propIdx, Rect rect)
    {
        SerializedProperty characterProp = m_characterProps[propIdx];
        SerializedProperty characterIdProp = m_characterIdProps[propIdx];

        float lineHeight = EditorGUIUtility.singleLineHeight;
        float spacing = EditorGUIUtility.standardVerticalSpacing;

        Rect idRect = new Rect(rect) { height = lineHeight };
        rect.yMin = idRect.yMax + spacing;

        idRect = EditorGUI.PrefixLabel(idRect, new GUIContent("Id:"));
        EditorGUI.SelectableLabel(idRect, IdConverter.ToReadableString(characterIdProp.longValue));
        DrawCharacterProp(rect, characterProp);
    }

    protected override bool ShouldDeleteObject(int propIdx)
    {
        string charName = GetCharacterName(propIdx);
        string charId = IdConverter.ToReadableString(GetCharacterId(propIdx));

        return EditorUtility.DisplayDialog("Delete character?",
            $"Are you sure you want to delete '{charName}' (id: '{charId}')?",
            "Delete", "Cancel");
    }

    protected override void DeleteObject(int propIdx)
    {
        SerializedProperty idsProp = m_allCharactersObject.FindProperty("m_characters.m_keys");
        SerializedProperty charactersProp = m_allCharactersObject.FindProperty("m_characters.m_values");

        idsProp.DeleteArrayElementAtIndex(propIdx);
        charactersProp.DeleteArrayElementAtIndex(propIdx);

        m_characterIdProps = EnumerateCharacters().ToList();
        m_characterProps = EnumerateCharacterIds().ToList();
    }

    protected override void InsertNewObject(int propIdx)
    {
        SerializedProperty idsProp = m_allCharactersObject.FindProperty("m_characters.m_keys");
        SerializedProperty charactersProp = m_allCharactersObject.FindProperty("m_characters.m_values");

        idsProp.InsertArrayElementAtIndex(propIdx);
        charactersProp.InsertArrayElementAtIndex(propIdx);

        SerializedProperty newCharacterProp = charactersProp.GetArrayElementAtIndex(propIdx);
        SerializedProperty newCharacterNameProp = newCharacterProp.FindPropertyRelative("m_name");
        SerializedProperty newCharacterIdProp = idsProp.GetArrayElementAtIndex(propIdx);

        newCharacterNameProp.stringValue = "New Character";
        newCharacterIdProp.longValue = ((CharacterDefinitionList)m_allCharactersObject.targetObject).GenerateUniqueId();

        m_characterProps.Add(newCharacterProp);
        m_characterIdProps.Add(newCharacterIdProp);
    }

    protected override void PrepareOnGUI()
    {
        m_allCharactersObject.Update();
        m_characterProps = EnumerateCharacters().ToList();
        m_characterIdProps = EnumerateCharacterIds().ToList();
    }

    protected override void SaveChanges()
    {
        if (m_allCharactersObject.ApplyModifiedProperties())
        {
            EditorUtility.SetDirty(m_allCharactersObject.targetObject);
            AssetDatabase.SaveAssets();
        }

    }

    protected override string GetDisplayName(int propIdx)
    {
        return GetCharacterName(propIdx);
    }

    private void Instantiate(CharacterDefinitionList characterList)
    {
        if (characterList == null)
        {
            m_allCharactersObject = null;
            return;
        }

        m_allCharactersObject = new SerializedObject(characterList);
        m_characterIdProps = EnumerateCharacterIds().ToList();
        m_characterProps = EnumerateCharacters().ToList();

        Initialize();
    }

    private void DrawCharacterProp(Rect rect, SerializedProperty characterProp)
    {
        GUILayout.BeginArea(rect);

        m_itemScroll = EditorGUILayout.BeginScrollView(m_itemScroll);
        EditorGUILayout.PropertyField(characterProp, true);
        EditorGUILayout.EndScrollView();

        GUILayout.EndArea();
    }

    private IEnumerable<SerializedProperty> EnumerateCharacters()
    {
        SerializedProperty charactersProp = m_allCharactersObject.FindProperty("m_characters.m_values");

        for (int i = 0; i < charactersProp.arraySize; ++i)
            yield return charactersProp.GetArrayElementAtIndex(i);
    }

    private IEnumerable<SerializedProperty> EnumerateCharacterIds()
    {
        SerializedProperty characterIdsProp = m_allCharactersObject.FindProperty("m_characters.m_keys");

        for (int i = 0; i < characterIdsProp.arraySize; ++i)
            yield return characterIdsProp.GetArrayElementAtIndex(i);
    }

    private string GetCharacterName(int propIdx)
    {
        return m_characterProps[propIdx].FindPropertyRelative("m_name").stringValue;
    }

    private long GetCharacterId(int propIdx)
    {
        return m_characterIdProps[propIdx].longValue;
    }

    [SerializeField]
    private Vector2 m_itemScroll;

    // Unity can't serialize SerializedObject and SerializedProperty, so these
    // are reset every time Unity recompiles.
    private SerializedObject m_allCharactersObject;
    private List<SerializedProperty> m_characterIdProps;
    private List<SerializedProperty> m_characterProps;
}
