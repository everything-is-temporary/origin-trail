﻿using System.Linq;
using Characters;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Stats))]
public class StatsDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        int numStats = Stats.StatNames.Length;

        return EditorGUIUtility.singleLineHeight
                + numStats * (EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty statValuesProp = property.FindPropertyRelative("m_statValues");

        label = EditorGUI.BeginProperty(position, label, property);

        Rect labelRect = new Rect(position) { yMax = position.yMin + EditorGUIUtility.singleLineHeight };
        EditorGUI.LabelField(labelRect, label);

        Rect previousRect = labelRect;
        float statNameWidth = GetMaxStatNameWidth(GUI.skin.label);
        for (int i = 0; i < statValuesProp.arraySize; ++i)
        {
            Rect statRect = new Rect(previousRect) { y = previousRect.yMax + EditorGUIUtility.standardVerticalSpacing };
            previousRect = statRect;

            Rect nameRect = new Rect(statRect) { width = statNameWidth };
            Rect valueRect = new Rect(statRect) { xMin = nameRect.xMax };

            EditorGUI.LabelField(nameRect, Stats.StatNames[i]);

            SerializedProperty valueProp = statValuesProp.GetArrayElementAtIndex(i);

            DrawStatValue(valueRect, valueProp);
        }

        EditorGUI.EndProperty();
    }

    private void DrawStatValue(Rect valueRect, SerializedProperty valueProp)
    {
        EditorGUI.BeginChangeCheck();
        double newValue = EditorGUI.DoubleField(valueRect, valueProp.doubleValue);

        if (EditorGUI.EndChangeCheck())
        {
            valueProp.doubleValue = newValue;
        }
    }

    private float GetMaxStatNameWidth(GUIStyle style)
    {
        return Stats.StatNames.Max(name => style.CalcSize(new GUIContent(name)).x);
    }
}
