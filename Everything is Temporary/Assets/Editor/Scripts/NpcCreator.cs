﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Characters;
using Interaction;
using Utilities;
using GameUtility;
using Dialogue;

public class NpcCreator : ScriptableWizard
{
    public enum Behavior
    {
        CreateForExistingCharacter,
        CreateInteractiveSprite
    }

    [System.Serializable]
    public class ExistingCharacterConfig
    {
        public CharacterReference characterReference = CharacterReference.FromId(0);
    }

    [System.Serializable]
    public class InteractiveSpriteConfig
    {
        public string characterName;
        public Sprite characterSprite;
        public string inkKnot;
    }

    public GameObject parent;
    public Behavior behavior;

    public ExistingCharacterConfig existingCharacterConfig = new ExistingCharacterConfig();
    public InteractiveSpriteConfig interactiveSpriteConfig = new InteractiveSpriteConfig();


    [MenuItem("GameObject/Origin Trail/Character", false, 10)]
    private static void CreateNpc(MenuCommand menuCommand)
    {
        var wizard = DisplayWizard<NpcCreator>("Create Character in Scene");

        if (menuCommand.context is GameObject @object)
            wizard.parent = @object;
    }

    private void OnWizardUpdate()
    {
        isValid = true;
        List<string> errors = new List<string>();

        switch (behavior)
        {
            case Behavior.CreateForExistingCharacter:
                {
                    bool isReferenceValid = existingCharacterConfig.characterReference.CharacterOrNull != null;
                    bool doesCharacterHavePrefab = existingCharacterConfig.characterReference.CharacterOrNull?.ScenePrefab != null;

                    isValid &= isReferenceValid;
                    isValid &= doesCharacterHavePrefab;

                    if (!isReferenceValid)
                        errors.Add("Invalid character ID.");
                    else if (!doesCharacterHavePrefab)
                        errors.Add("Character does not have a Scene Prefab.");

                    helpString = "This will instantiate a character prefab in the scene.";
                    break;
                }

            case Behavior.CreateInteractiveSprite:
                {
                    helpString = "This will create a sprite that can be interacted with to start an ink knot.";
                    break;
                }
        }

        if (!isValid)
            errorString = string.Join("\n", errors);
        else
            errorString = null;
    }

    private void OnWizardCreate()
    {
        GameObject newCharacter = null;

        switch (behavior)
        {
            case Behavior.CreateForExistingCharacter:
                {
                    newCharacter = PrefabUtility.InstantiatePrefab(
                        existingCharacterConfig.characterReference.Character.ScenePrefab) as GameObject;

                    break;
                }
            case Behavior.CreateInteractiveSprite:
                {
                    // Start with an empty game object.
                    newCharacter = new GameObject(interactiveSpriteConfig.characterName);

                    // Set sprite.
                    var sr = newCharacter.AddComponent<SpriteRenderer>();
                    sr.sprite = interactiveSpriteConfig.characterSprite;

                    // Add interactive object script, make it start dialogue on interaction.
                    var interactiveObj = newCharacter.AddComponent<BasicInteractiveObject>();
                    interactiveObj.maxInteractionDistance = 1;

                    InteractiveObjectCreator.AddDialogueInteraction(interactiveObj, interactiveSpriteConfig.inkKnot);
                    InteractiveObjectCreator.AddSpeechBubble(interactiveObj);

                    break;
                }
            default:
                throw new System.NotImplementedException();
        }

        if (parent != null)
            GameObjectUtility.SetParentAndAlign(newCharacter, parent);

        Selection.activeGameObject = newCharacter;
        EditorGUIUtility.PingObject(newCharacter);
        Undo.RegisterCreatedObjectUndo(newCharacter, "Created Character");
    }

    protected override bool DrawWizardGUI()
    {
        using (SerializedObject serializedThis = new SerializedObject(this))
        {
            SerializedProperty parentProp = serializedThis.FindProperty(nameof(parent));
            SerializedProperty createForExistingCharacterProp = serializedThis.FindProperty(nameof(behavior));

            EditorGUILayout.PropertyField(parentProp);
            EditorGUILayout.PropertyField(createForExistingCharacterProp);

            switch (behavior)
            {
                case Behavior.CreateForExistingCharacter:
                    SerializedProperty existingCharacterConfigProp = serializedThis.FindProperty(nameof(existingCharacterConfig));
                    EditorGUILayout.PropertyField(existingCharacterConfigProp, new GUIContent("Configuration"), true);
                    break;

                case Behavior.CreateInteractiveSprite:
                    SerializedProperty interactiveSpriteConfigProp = serializedThis.FindProperty(nameof(interactiveSpriteConfig));
                    EditorGUILayout.PropertyField(interactiveSpriteConfigProp, new GUIContent("Configuration"), true);
                    break;
            }

            return serializedThis.ApplyModifiedProperties();
        }
    }
}
