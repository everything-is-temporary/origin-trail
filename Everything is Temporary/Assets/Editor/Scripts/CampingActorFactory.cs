﻿using System;
using UnityEngine;
using UnityEditor;
using Characters;
using Camping;

/// <summary>
/// Provides a function to create an actor object suitable for a camping scene.
/// </summary>
public static class CampingActorFactory
{
    [Serializable]
    public class Configuration
    {
        public string objectName;
        public Sprite sprite;
        public string sortingLayerName = "Characters";
        public int sortingOrder;
        public CharacterReference character;
    }

    /// <summary>
    /// Creates a game object suitable to be used as the camping prefab for
    /// a character. The GameObject will be added to the active scene and
    /// an undo operation for it will be registered.
    /// </summary>
    /// <returns>The created object.</returns>
    /// <param name="config">The configuration for the camping actor.</param>
    public static GameObject Create(Configuration config)
    {
        GameObject go = new GameObject(config.objectName);

        AddSpriteRenderer(go, config);
        AddPhysics(go);
        AddActor(go, config);
        AddCamping(go);

        Undo.RegisterCreatedObjectUndo(go, "Created Camping Actor");
        return go;
    }

    private static void AddSpriteRenderer(GameObject go, Configuration config)
    {
        var sr = go.AddComponent<SpriteRenderer>();
        sr.sprite = config.sprite;
        sr.sortingLayerID = SortingLayer.NameToID(config.sortingLayerName ?? "Default");
        sr.sortingOrder = config.sortingOrder;
    }

    private static void AddPhysics(GameObject go)
    {
        go.AddComponent<Rigidbody2D>();
        go.AddComponent<BoxCollider2D>();
    }

    private static void AddActor(GameObject go, Configuration config)
    {
        var actor = go.AddComponent<Actor>();

        // Set the CharacterReference on the actor. It is a private variable,
        // so we cheat and set it through serialization.
        using (var serializedActor = new SerializedObject(actor))
        {
            var characterIdProp = serializedActor.FindProperty("m_character.m_id");

            if (characterIdProp == null)
            {
                Debug.LogWarning("The Actor class does not have an m_character.m_id property.");
            }
            else if (config.character != null)
            {
                characterIdProp.longValue = config.character.Id;
            }

            serializedActor.ApplyModifiedProperties();
        }
    }

    private static void AddCamping(GameObject go)
    {
        go.AddComponent<CampingActorScript>();
    }
}
